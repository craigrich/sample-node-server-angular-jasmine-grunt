#  [![Build Status][travis-image]][travis-url] [![Coverage Status][coverage-image]][coverage-url] [![Dependency Status][daviddm-image]][daviddm-url] [![DevDependency Status][daviddm-dev-image]][daviddm-dev-url]

> A collection of front-end / back-end code samples.
>
> Tech Stack: Angular, Node, Express, Mocha, Jasmine, Karma, Bootstrap.

[travis-image]: https://travis-ci.org/craigrich/meta-stats.svg?branch=master
[travis-url]: https://travis-ci.org/craigrich/meta-stats

[daviddm-image]: https://david-dm.org/craigrich/meta-stats.svg?theme=shields.io
[daviddm-url]: https://david-dm.org/craigrich/meta-stats

[daviddm-dev-image]: https://david-dm.org/craigrich/meta-stats/dev-status.svg?theme=shields.io
[daviddm-dev-url]: https://david-dm.org/craigrich/meta-stats#info=devDependencies

[coverage-image]: https://coveralls.io/repos/craigrich/meta-stats/badge.svg?branch=master
[coverage-url]: https://coveralls.io/r/craigrich/meta-stats?branch=master
