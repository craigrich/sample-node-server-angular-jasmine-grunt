import Project from '../api/project/project.model';
import User from '../api/user/user.model';

User.find({})
  .removeAsync()
  .then(() => {

    User.createAsync({
      provider: 'local',
      name: 'Craig Richardson',
      email: 'test@example.com',
      password: 'test'
    }, {
      provider: 'local',
      role: 'admin',
      name: 'Admin',
      email: 'admin@example.com',
      password: 'admin'
    })
      .then(() => {
        console.log('finished populating users');
        return User.find({
          email: 'test@example.com'
        });
      })
      .then((user) => {
        populateProjects(user);
      });
  });

function populateProjects(user) {
  Project.find({})
    .removeAsync()
    .then(() => {
      Project.create({
        userId: user[0]._id,
        name: 'Web Technology Group',
        startDate: '2015-09-27T15:37:38+00:00',
        endDate: '20',
        dayRate: '350',
        contact: 'John Smith',
        contactId: 'someId'
      }, {
        userId: user[0]._id,
        name: 'VML',
        startDate: '10',
        endDate: '20',
        dayRate: '350',
        contact: 'John Smith',
        contactId: 'someId'
      }, {
        userId: user[0]._id,
        name: 'Blue Hive',
        startDate: '10',
        endDate: '20',
        dayRate: '350',
        contact: 'John Smith',
        contactId: 'someId'
      }, {
        userId: 'Another User',
        name: 'WTG Group',
        startDate: '30',
        endDate: '40',
        dayRate: '400',
        contact: 'John Smith',
        contactId: 'someId'
      });
    });
}
