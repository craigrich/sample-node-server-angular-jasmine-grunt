var mongoose = require('bluebird').promisifyAll(require('mongoose'));
var Schema = mongoose.Schema;

var ProjectSchema = new Schema({
  userId: String,
  name: String,
  startDate: Date,
  endDate: Date,
  dayRate: Number,
  contact: String,
  contactId: String
});

module.exports = mongoose.model('Project', ProjectSchema);
