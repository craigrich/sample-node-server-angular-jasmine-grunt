// //GistID:1c45ca694ad333baeddd
// 'use strict';

angular.module('stateMock', []);


angular.module('stateMock').service('$state', function($q) {

    'use strict';

    this.current = {};

    this.transitionTo = function(stateName) {
        this.current.name = stateName;
        var deferred = $q.defer();
        var promise = deferred.promise;
        deferred.resolve();
        return promise;
    };

    this.go = this.transitionTo;

});
