(function() {

    'use strict';

    angular
        .module('gamestatsApp')
        .directive('preloader', preloader);

    function preloader() {
        return {
            templateUrl: 'components/preloader/preloader.html',
            restrict: 'E'
        };
    }

})();