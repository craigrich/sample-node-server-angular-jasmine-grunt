'use strict';

describe('Service: highchart', function() {

    beforeEach(module('gamestatsApp'));

    beforeEach(module(function($urlRouterProvider) {
        $urlRouterProvider.deferIntercept();
    }));

    var $httpBackend, HighChartService;

    beforeEach(inject(function(_$httpBackend_, _HighChartService_) {
        $httpBackend = _$httpBackend_;
        HighChartService = _HighChartService_;
    }));

    describe('getChart Function', function() {

        var chartConfig = {
            "chart": {
                "type": "column"
            },
            "credits": {
                "enabled": false
            },
            "title": {
                "text": "Top Games by Platform"
            },
            "subtitle": {
                "text": "Scores above 90%. Source: metacritic.com."
            },
            "legend": {
                "enabled": false
            },
            "yAxis": {
                "max": 400,
                "title": {
                    "text": "Total No of Games"
                }
            },
            "xAxis": {
                "categories": []
            },
            "series": [{
                "data": {}
            }]
        };

        var gameData = [{
            _id: 'PS4',
            count: 2
        }, {
            _id: 'XBOXONE',
            count: 6
        }];

        it('should exist', function() {
            expect(HighChartService.getChart).toBeDefined();
        });


        it('should set chart categories on the x axis', function() {
            $httpBackend.expectGET('/config/example.json').respond(200, chartConfig);
            $httpBackend.expectGET('/api/games/example').respond(200, gameData);
            HighChartService.getChart({
                config: '/config/example.json',
                data: '/api/games/example'
            }).then(function(getChartResponseData) {
                expect(getChartResponseData.xAxis.categories).toEqual(['PS4', 'XBOXONE']);
            })
            $httpBackend.flush();

        });

        it('should populate series data with response data', function() {

            $httpBackend.expectGET('/config/example.json').respond(200, chartConfig);
            $httpBackend.expectGET('/api/games/example').respond(200, gameData);

            HighChartService.getChart({
                config: '/config/example.json',
                data: '/api/games/example'
            }).then(function(getChartResponseData) {
                expect(getChartResponseData.series[0].data).toEqual([2, 6]);
            })

            $httpBackend.flush();
        });

        it('should reject with a message on error', function() {

            $httpBackend.expectGET('/config/example.json').respond(401, 'Some Error Message');
            $httpBackend.expectGET('/api/games/example').respond(401, 'Some Error Message');

            HighChartService.getChart({
                config: '/config/example.json',
                data: '/api/games/example'
            }).catch(function(getChartError) {
                expect(getChartError).toEqual('Some Error Message');
            })

            $httpBackend.flush();

        });

    });

    describe('getPie Function', function() {

        var getPieResponseData;

        var chartConfig = {
            "chart": {
                "plotBackgroundColor": null,
                "plotBorderWidth": null,
                "plotShadow": false
            },
            "credits": {
                "enabled": false
            },
            "title": {
                "text": "Total Distribution of Games"
            },
            "subtitle": {
                "text": "Grouped by platform. Source: metacritic.com."
            },
            "tooltip": {
                "pointFormat": "Count: <b>{point.count}</b> | <b>{point.percentage:.0f}%</b>"
            },
            "plotOptions": {
                "pie": {
                    "allowPointSelect": true,
                    "cursor": "pointer",
                    "dataLabels": {
                        "enabled": false
                    },
                    "showInLegend": true
                }
            },
            "series": [{
                "data": {}
            }]
        };

        var gameData = [{
            _id: 'PS4',
            count: 605,
            name: 'PS4',
            y: 4.120692003814194
        }, {
            _id: 'XBOXONE',
            count: 272,
            name: 'XBOXONE',
            y: 1.8526086364255552
        }];

        it('should exist', function() {
            expect(HighChartService.getPie).toBeDefined();
        });

        it('should populate series data with response data', function() {

            $httpBackend.expectGET('/config/example.json').respond(200, chartConfig);
            $httpBackend.expectGET('/api/games/example').respond(200, gameData);

            HighChartService.getPie({
                config: '/config/example.json',
                data: '/api/games/example'
            }).then(function(getPieResponseData) {
                expect(getPieResponseData.series[0].data).toEqual(gameData);
            });

            $httpBackend.flush();

        });

        it('should reject with a message on error', function() {

            $httpBackend.expectGET('/config/example.json').respond(401, 'Some Error Message');
            $httpBackend.expectGET('/api/games/example').respond(401, 'Some Error Message');

            HighChartService.getPie({
                config: '/config/example.json',
                data: '/api/games/example'
            }).catch(function(getPieError) {
                expect(getPieError).toEqual('Some Error Message');
            });

            $httpBackend.flush();

        });

    });

});