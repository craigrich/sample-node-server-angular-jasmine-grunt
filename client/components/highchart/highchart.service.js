    /**
     * HighChartService Factory
     * @namespace Factories
     */
    (function() {

        'use strict';

        angular
            .module('gamestatsApp')
            .factory('HighChartService', HighChartService);

        /**
         * @namespace HighChartService
         * @desc Fetches Highchart Configs and
         *       Game Day
         *
         * @memberOf Factories
         */
        function HighChartService($http, $q, $log) {

            return {
                getChart: getChart,
                getPie: getPie
            };


            /**
             * @name getChart
             * @desc fetches generic chart configs and data
             *
             * @param {object} chart urls for chart data and config
             * @memberOf Factories.HighChartService
             */
            function getChart(chart) {

                return _getChartData(chart.config, chart.data)
                    .then(getChartComplete)
                    .catch(getChartFailed);

                function getChartComplete(response) {

                    var graphConfig = response[0].data,
                        graphData = response[1].data;

                    graphConfig.xAxis.categories = graphData.map(function mapID(x) {
                        return x._id;
                    });

                    graphConfig.series[0].data = graphData.map(function mapCount(x) {
                        return x.count;
                    });

                    return graphConfig;

                }

                function getChartFailed(error) {
                    $log.error('XHR Failed for getChart.' + error.data);
                    $q.reject(error);
                }

            }

            /**
             * @name getPie
             * @desc fetches pie specific chart configs and data
             *
             * @param {object} chart urls for chart data and config
             * @memberOf Factories.HighChartService
             */
            function getPie(chart) {

                return _getChartData(chart.config, chart.data)
                    .then(getPieComplete)
                    .catch(getPieFailed);

                function getPieComplete(response) {

                    var graphConfig = response[0].data,
                        graphData = response[1].data;

                    graphConfig.series[0].data = graphData;
                    return graphConfig;

                }

                function getPieFailed(error) {
                    $log.error('XHR Failed for GetPie.' + error.data);
                    $q.reject(error);
                }

            }


            /**
             * @name _getChartData
             * @desc fetches data with given URLs
             *
             * @param  {string} configURL URL for chart config
             * @param  {string} dataURL   URL for game data
             * @memberOf Factories.HighChartService
             */
            function _getChartData(configURL, dataURL) {
                return $q.all([
                    $http.get(configURL, {
                        cache: true
                    }),
                    $http.get(dataURL, {
                        cache: true
                    })
                ]);
            }
        }

    })();