/**
 * AuthService Factory
 * @namespace Factories
 */
(function() {

    'use strict';

    angular
        .module('gamestatsApp')
        .factory('AuthService', AuthService);

    /**
     * @namespace AuthService
     * @desc main factory to handle authentication
     *
     * @memberOf Factories
     */
    function AuthService(UserService, $http, $cookieStore, $q) {

        var currentUser = {};

        /* istanbul ignore if */
        if ($cookieStore.get('token')) {
            currentUser = UserService.get();
        }

        return {
            login: login,
            logout: logout,
            createUser: createUser,
            changePassword: changePassword,
            getCurrentUser: getCurrentUser,
            isLoggedIn: isLoggedIn,
            isAdmin: isAdmin,
            getToken: getToken
        };

        /**
         * @name login
         * @desc will call passport auth service
         *       and set token on success.
         *
         * @param {object} user data from login controller
         * @memberOf Factories.AuthService
         */
        function login(user) {
            return $http
                .post('/auth/local', {
                    email: user.email,
                    password: user.password
                })
                .then(function(res) {
                    currentUser = UserService.get();
                    $cookieStore.put('token', res.data.token);
                    return true;
                })
                .catch(function(err) {
                    logout();
                    return $q.reject(err);
                });

        }

        /**
         * @name logout
         * @desc will clear token and reset
         *       currentUser
         *
         * @memberOf Factories.AuthService
         */
        function logout() {
            $cookieStore.remove('token');
            currentUser = {};
        }

        /**
         * @name createUser
         * @desc will create a new user on mongod
         *       and set token on success
         *
         * @param {object} user data from signup controller
         * @memberOf Factories.AuthService
         */
        function createUser(user) {

            return UserService
                .save(user, userSaveSuccess, userSaveError).$promise;

            function userSaveSuccess(data) {
                $cookieStore.put('token', data.token);
                currentUser = UserService.get();
                return user;
            }

            function userSaveError(err) {
                logout();
                return $q.reject(err);
            }

        }

        /**
         * @name changePassword
         * @desc will set new password for
         *       a specific user
         *
         * @param {object} user data from changePassword controller
         * @memberOf Factories.AuthService
         */
        function changePassword(user) {

            return UserService
                .changePassword({
                        id: currentUser._id
                    }, {
                        oldPassword: user.oldPassword,
                        newPassword: user.newPassword
                    },
                    changePasswordSuccess, changePasswordError).$promise;

            function changePasswordSuccess(user) {
                return user;
            }

            function changePasswordError(err) {
                return $q.reject(err);
            }

        }


        /**
         * @name getCurrentUser
         * @desc fetches the current user
         *
         * @memberOf Factories.AuthService
         */
        function getCurrentUser() {
            return currentUser;
        }

        /**
         * @name isLoggedIn
         * @desc determines if current user
         *       has logged in
         *
         * @memberOf Factories.AuthService
         */
        function isLoggedIn() {
            return currentUser.hasOwnProperty('role');
        }

        /**
         * @name isAdmin
         * @desc check is current role is admin
         *
         * @memberOf Factories.AuthService
         */
        function isAdmin() {
            return currentUser.role === 'admin';
        }

        /**
         * @name getToken
         * @desc fetches token from cookie store
         *
         * @memberOf Factories.AuthService
         */
        function getToken() {
            return $cookieStore.get('token');
        }

    }

})();