var CookieStoreMock, UserService, AuthService, $httpBackend, $http;

describe('Service: AuthService', function() {

    beforeEach(module('gamestatsApp'));

    beforeEach(module(function($provide, $urlRouterProvider) {
        CookieStoreMock = jasmine.createSpyObj('$cookieStore', ['get', 'put', 'remove']);
        $provide.value('$cookieStore', CookieStoreMock);
    
        $urlRouterProvider.deferIntercept();
    }));

    beforeEach(inject(function(_AuthService_, _UserService_, _$httpBackend_) {                    
        $httpBackend = _$httpBackend_;
        AuthService = _AuthService_;        
        UserService = _UserService_;

        spyOn(AuthService, 'logout').and.callThrough();
        spyOn(UserService, 'get').and.callThrough();
        spyOn(UserService, 'save').and.callThrough();        
        spyOn(UserService, 'changePassword').and.callThrough();
    }));


    describe('Login Function', function() {

        var user = {
            email: 'user@someEmail.com',
            password: 'somePassword'
        };

        it('should exist', function() {
            expect(AuthService.login).toBeDefined();
        });

        it('should set the current user to a cookie on success', function() {
            $httpBackend.expectPOST('/auth/local').respond(200, {
                token: 'someToken'
            });
            $httpBackend.expectGET('/api/users/me').respond(200, {});
            AuthService.login(user);
            $httpBackend.flush();

            expect(CookieStoreMock.put).toHaveBeenCalledWith('token', 'someToken');
            expect(UserService.get).toHaveBeenCalled();
        });


        it('should logout and reject promise on error', function() {
            $httpBackend.expectPOST('/auth/local').respond(500);
            AuthService.login(user);
            $httpBackend.flush();

            expect(CookieStoreMock.remove).toHaveBeenCalled();
        });

    });

    describe('Logout Function', function() {

        it('should exist', function() {
            expect(AuthService.logout).toBeDefined();
        });

        it('should clear cookies', function() {
            AuthService.logout();
            expect(CookieStoreMock.remove).toHaveBeenCalled();
        });

    });

    describe('Create User Function', function() {

        it('should exist', function() {
            expect(AuthService.createUser).toBeDefined();
        });

        it('should call the user save() method', function() {
            AuthService.createUser();
            expect(UserService.save).toHaveBeenCalled();
        });

        it('should set the current user to a cookie on user save success', function() {
            $httpBackend.expectPOST('/api/users').respond(200);
            $httpBackend.expectGET('/api/users/me').respond(200);
            AuthService.createUser();
            $httpBackend.flush();

            expect(CookieStoreMock.put).toHaveBeenCalled();
        });

        it('should call logout on error', function() {
            $httpBackend.expectPOST('/api/users').respond(500);
            AuthService.createUser();
            $httpBackend.flush();
        });

    });

    describe('Change Password Function', function() {

        it('should exist', function() {
            expect(AuthService.changePassword).toBeDefined();
        });

        it('should call the user changePassword method', function() {
            $httpBackend.expectPUT('/api/users/password').respond(200, 'user');
            var user = AuthService.changePassword({
                oldPassword: 'oldPassword',
                newPassword: 'newPassword'
            });
            $httpBackend.flush();
           
            expect(UserService.changePassword).toHaveBeenCalled();
        });

        it('should return the user on success', function() {});

        it('should reject promise of error', function() {
            $httpBackend.expectPUT('/api/users/password').respond(500);
            AuthService.changePassword({
                oldPassword: 'oldPassword',
                newPassword: 'newPassword'
            });
            $httpBackend.flush();
        });

    });

    describe('Get User', function() {

        it('should exist', function() {
            expect(AuthService.getCurrentUser).toBeDefined();
        });

        it('should return the current user', function() {
            var user = AuthService.getCurrentUser();
            expect(user).toBeDefined();
        });

    });

    describe('Check logged in', function() {

        it('should exist', function() {
            expect(AuthService.isLoggedIn).toBeDefined();
        });

        it('should check if user is logged in', function() {
            var isLoggedIn = AuthService.isLoggedIn();
            expect(isLoggedIn).toBe(false);
        });

    });

    describe('Check admin', function() {

        it('should exist', function() {
            expect(AuthService.isAdmin).toBeDefined();
        });

        it('should check if user is admin', function() {
            var isAdmin = AuthService.isAdmin();
            expect(isAdmin).toBe(false);
        });

    });

    describe('Get Token', function() {

        it('should exist', function() {
            expect(AuthService.getToken).toBeDefined();
        });

        it('should fetch a JWT from cookies', function() {
            CookieStoreMock.get.and.returnValue('someToken');

            var getToken = AuthService.getToken();
            expect(getToken).toBe('someToken');
        });

    });

});