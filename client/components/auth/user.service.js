/**
 * UserService Factory
 * @namespace Factories
 */
(function() {

    'use strict';

    angular
        .module('gamestatsApp')
        .factory('UserService', UserService);

    /**
     * @namespace AuthService
     * @desc main factory to handle user queries
     *
     * @memberOf Factories
     */
    function UserService($resource) {
        return $resource('/api/users/:id/:controller', {
            id: '@_id'
        }, {
            changePassword: {
                method: 'PUT',
                params: {
                    controller: 'password'
                }
            },
            get: {
                method: 'GET',
                params: {
                    id: 'me'
                }
            }
        });
    }

})();