/**
 * Auth Interceptor
 * @namespace Interceptors
 */
(function() {

    'use strict';

    angular
        .module('gamestatsApp')
        .factory('AuthInterceptor', AuthInterceptor);

    /**
     * @namespace AuthInterceptor
     * @desc main HTTP interceptor for app
     *
     * @memberOf Interceptors
     */
    function AuthInterceptor($q, $cookieStore, $location) {

        return {
            request: request,
            responseError: responseError
        };

        /**
         * @name request
         * @desc will add a JWT header if user
         *       is logged in
         *
         * @param {object} config HTTP config
         * @memberOf Interceptors.AuthInterceptor
         */
        function request(config) {
            if ($cookieStore.get('token')) {
                config.headers.Authorization = 'Bearer ' + $cookieStore.get('token');
            }
            return config;
        }

        /**
         * @name responseError
         * @desc will clear token and redirect
         *       to login if forbidden         
         *
         * @param {object} response data from API
         * @memberOf Interceptors.AuthInterceptor
         */
        function responseError(response) {
            if (response.status === 401) {
                $cookieStore.remove('token');
                $location.path('/login');
            }
            return $q.reject(response);
        }

    }

})();