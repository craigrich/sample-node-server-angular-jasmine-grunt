'use strict';

var $q, $rootScope, $location, AuthInterceptor, cookieStoreMock;

describe('Interceptor: Auth', function() {

    beforeEach(module('gamestatsApp'));

    beforeEach(module(function($provide) {
        cookieStoreMock = jasmine.createSpyObj('$cookieStore', ['get', 'remove']);
        $provide.value('$cookieStore', cookieStoreMock);
    }));

    beforeEach(inject(function(_$q_, _$rootScope_, _$location_, _AuthInterceptor_) {
        $q = _$q_;
        $rootScope = _$rootScope_;
        $location = _$location_;
        AuthInterceptor = _AuthInterceptor_;
    }));

    it('should be defined', function() {
        expect(AuthInterceptor).toBeDefined();
    });

    describe('Request', function() {

        it('should have a request method defined', function() {
            expect(AuthInterceptor.request).toBeDefined();
        });

        it('should add a token to request headers if token exists', function() {
            cookieStoreMock.get.and.returnValue('someToken');
            var config = AuthInterceptor.request({
                headers: {}
            });
            expect(config.headers['Authorization']).toBe('Bearer someToken');
        });

        it('should not add a token to the request headers if no token is set', function() {
            cookieStoreMock.get.and.returnValue(undefined);
            var config = AuthInterceptor.request({
                headers: {}
            });
            expect(config.headers['Authorization']).toBe(undefined);
        });

    });

    describe('Response Errors', function() {

        it('should have a request method defined', function() {
            expect(AuthInterceptor.request).toBeDefined();
        });

        it('should redirect to login if forbidden', function() {
            spyOn($location, 'path');
            $rootScope.$digest();
            AuthInterceptor.responseError({
                status: 401
            });
            expect($location.path).toHaveBeenCalledWith('/login');
        });

        it('should remove a token if forbidden', function() {
            AuthInterceptor.responseError({
                status: 401
            });
            expect(cookieStoreMock.remove).toHaveBeenCalledWith('token');
        });

        it('Should reject with the given error code', function() {
            var error = AuthInterceptor.responseError({
                status: 500
            });
            expect(error).toEqual($q.reject({
                status: 500
            }));
        });

    });

});