'use strict';

describe('Controller: NavbarCtrl', function() {

    beforeEach(module('gamestatsApp'));

    var NavbarCtrl;
    var locationMock = jasmine.createSpyObj('$location', ['path']);
    var authServiceMock = jasmine.createSpyObj('AuthService', ['isLoggedIn', 'isAdmin', 'getCurrentUser', 'logout']);

    beforeEach(inject(function($controller, $rootScope) {
        NavbarCtrl = $controller('NavbarCtrl', {
            $scope: $rootScope.$new(),
            $location: locationMock,
            AuthService: authServiceMock
        });
    }));

    describe('logout', function() {

        beforeEach(function() {
            NavbarCtrl.logout();
        });

        it('should should call logout on the AuthService', function() {
            expect(authServiceMock.logout).toHaveBeenCalled();
        });
        it('should set the location path to login', function() {
            expect(locationMock.path).toHaveBeenCalledWith('/login');
        });
    });

    describe('isActive', function() {

        beforeEach(function() {
            locationMock.path.and.returnValue('metacritic');
        });

        it('should return true if value matches current route', function() {
            expect(NavbarCtrl.isActive('metacritic')).toBe(true);
        });

        it('should return false if value does not match the current route', function() {
            expect(NavbarCtrl.isActive('dashboard')).toBe(false);
        });

    });

});