/**
 * Navbar Controller
 * @namespace Controllers
 */
(function() {

    'use strict';

    angular
        .module('gamestatsApp')
        .controller('NavbarCtrl', NavbarCtrl);

    /**
     * @namespace NavbarCtrl
     * @desc main controller for navbar
     *
     * @memberOf Controllers
     */
    function NavbarCtrl($scope, $location, AuthService) {

        var vm = this;

        vm.isLoggedIn = AuthService.isLoggedIn;
        vm.isAdmin = AuthService.isAdmin;
        vm.getCurrentUser = AuthService.getCurrentUser;

        vm.logout = logout;
        vm.isActive = isActive;


        /**
         * @name logout
         * @desc will call auth logout and change route
         *
         * @memberOf Controllers.NavbarCtrl
         */
        function logout() {
            AuthService.logout();
            $location.path('/login');
        }

        /**
         * @name isActive
         * @desc checks if route is the current route
         *
         * @param  {string} route the name of a given route 
         * @memberOf Controllers.NavbarCtrl
         */
        function isActive(route) {
            return route === $location.path();
        }

    }

})();