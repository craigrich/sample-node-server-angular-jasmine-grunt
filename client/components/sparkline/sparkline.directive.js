/**
 * Sparkline Directive
 * @namespace Directives
 */
(function() {

    'use strict';

    angular
        .module('gamestatsApp')
        .directive('sparkline', sparkline);

    /**
     * @namespace sparkline
     * @desc directive to render sparkline charts
     *
     * @memberOf Controllers
     */
    function sparkline() {
        return {
            scope: {},
            templateUrl: 'components/sparkline/sparkline.html',
            restrict: 'EA',
            controller: SparklineCtrl,
            controllerAs: 'sparklineCtrl',            
            link: link
        };
    }

    function SparklineCtrl($attrs) {
        var vm = this;
        vm.header = $attrs.title;
        vm.val = $attrs.value;

        console.log(vm);


    }

    function link(scope, element, attrs) {

        var chart = element.find('.chart');

        if (!attrs.type) {
            throw 'Sparkline: This element has no "type" attribute. Please use "Line", "Bar" or "Pie"';
        }

        if (_.indexOf(['bar', 'line', 'pie'], attrs.type.toLowerCase()) === -1) {
            throw 'Sparkline: This element has an invalid "type" attribute. Please use "Line", "Bar" or "Pie"';
        }

        if (attrs.type === 'bar') {
            renderBar(chart);
        }

        if (attrs.type === 'line') {
            renderLine(chart, attrs);
        }

        function easyPieChart(selector, trackColor, scaleColor, barColor, lineWidth, lineCap, size) {
            $(selector).easyPieChart({
                trackColor: trackColor,
                scaleColor: scaleColor,
                barColor: barColor,
                lineWidth: lineWidth,
                lineCap: lineCap,
                size: size
            });
        }

        easyPieChart('.main-pie', 'rgba(255,255,255,0.2)', 'rgba(255,255,255,0.5)', 'rgba(255,255,255,0.7)', 7, 'butt', 148);
        easyPieChart('.sub-pie-1', '#eee', '#ccc', '#2196F3', 4, 'butt', 95);
        easyPieChart('.sub-pie-2', '#eee', '#ccc', '#FFC107', 4, 'butt', 95);

    }

    function renderBar($el) {
        $el.sparkline(_.shuffle([6, 4, 8, 6, 5, 6, 7, 8, 3, 5, 9, 5, 8, 4, 3, 6, 8]), {
            type: 'bar',
            height: '45px',
            barWidth: 3,
            barColor: '#fff',
            barSpacing: 2
        });
    }

    function renderLine($el, attrs) {
        console.log(attrs.width);
        console.log(attrs.height);
        
        $el.sparkline(_.shuffle([9, 4, 6, 5, 6, 4, 5, 7, 9, 3, 6, 5]), {
            type: 'line',
            width: attrs.width || 85,
            height: attrs.height || 45,
            lineColor: '#fff',
            fillColor: 'rgba(0,0,0,0)',
            lineWidth: 1.25,
            maxSpotColor: 'rgba(255,255,255,0.4)',
            minSpotColor: 'rgba(255,255,255,0.4)',
            spotColor: 'rgba(255,255,255,0.4)',
            spotRadius: 3,
            highlightSpotColor: '#fff',
            highlightLineColor: 'rgba(255,255,255,0.4)'
        });
    }

})();