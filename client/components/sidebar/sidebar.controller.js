/**
 * Sidebar Controller
 * @namespace Controllers
 */
(function() {

    'use strict';

    angular
        .module('gamestatsApp')
        .controller('SidebarCtrl', SidebarCtrl);

    /**
     * @namespace SidebarCtrl
     * @desc main controller for navbar
     *
     * @memberOf Controllers
     */
    function SidebarCtrl($scope, $location, AuthService) {

        var vm = this;
        vm.isActive = isActive;
        vm.logout = logout;

        vm.isLoggedIn = AuthService.isLoggedIn;
        vm.isAdmin = AuthService.isAdmin;
        vm.getCurrentUser = AuthService.getCurrentUser;

        /**
         * @name isActive
         * @desc checks if route is the current route
         *
         * @param  {string} route the name of a given route
         * @memberOf Controllers.SidebarCtrl
         */
        function isActive(route) {            
            return route === $location.path();
        }

        /**
         * @name logout
         * @desc will call auth logout and change route
         *
         * @memberOf Controllers.NavbarCtrl
         */
        function logout() {
            AuthService.logout();
            $location.path('/login');            
        }


    }

})();