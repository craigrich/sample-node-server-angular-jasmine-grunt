'use strict';

describe('Controller: SidebarCtrl', function() {

    // load the controller's module
    beforeEach(module('gamestatsApp'));

    var SidebarCtrl, locationMock = jasmine.createSpyObj('$location', ['path']);

    beforeEach(inject(function($controller, $rootScope) {

        SidebarCtrl = $controller('SidebarCtrl', {
            $scope: $rootScope.$new(),
            $location: locationMock
        });

    }));

    describe('isActive', function() {

        beforeEach(function() {
            locationMock.path.and.returnValue('metacritic');
        })

        it('should return true if value matches current route', function() {
            expect(SidebarCtrl.isActive('metacritic')).toBe(true);
        });

        it('should return false if value does not match the current route', function() {
            expect(SidebarCtrl.isActive('dashboard')).toBe(false);
        });

    });

});