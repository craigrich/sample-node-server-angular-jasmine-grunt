if (! function(t, e) {
    "object" == typeof module && "object" == typeof module.exports ? module.exports = t.document ? e(t, !0) : function(t) {
        if (!t.document) throw new Error("jQuery requires a window with a document");
        return e(t)
    } : e(t)
}("undefined" != typeof window ? window : this, function(t, e) {
    function n(t) {
        var e = "length" in t && t.length,
            n = J.type(t);
        return "function" === n || J.isWindow(t) ? !1 : 1 === t.nodeType && e ? !0 : "array" === n || 0 === e || "number" == typeof e && e > 0 && e - 1 in t
    }

    function i(t, e, n) {
        if (J.isFunction(e)) return J.grep(t, function(t, i) {
            return !!e.call(t, i, t) !== n
        });
        if (e.nodeType) return J.grep(t, function(t) {
            return t === e !== n
        });
        if ("string" == typeof e) {
            if (at.test(e)) return J.filter(e, t, n);
            e = J.filter(e, t)
        }
        return J.grep(t, function(t) {
            return G.call(e, t) >= 0 !== n
        })
    }

    function o(t, e) {
        for (;
            (t = t[e]) && 1 !== t.nodeType;);
        return t
    }

    function r(t) {
        var e = pt[t] = {};
        return J.each(t.match(ft) || [], function(t, n) {
            e[n] = !0
        }), e
    }

    function s() {
        Q.removeEventListener("DOMContentLoaded", s, !1), t.removeEventListener("load", s, !1), J.ready()
    }

    function a() {
        Object.defineProperty(this.cache = {}, 0, {
            get: function() {
                return {}
            }
        }), this.expando = J.expando + a.uid++
    }

    function l(t, e, n) {
        var i;
        if (void 0 === n && 1 === t.nodeType)
            if (i = "data-" + e.replace(bt, "-$1").toLowerCase(), n = t.getAttribute(i), "string" == typeof n) {
                try {
                    n = "true" === n ? !0 : "false" === n ? !1 : "null" === n ? null : +n + "" === n ? +n : wt.test(n) ? J.parseJSON(n) : n
                } catch (o) {}
                yt.set(t, e, n)
            } else n = void 0;
        return n
    }

    function c() {
        return !0
    }

    function u() {
        return !1
    }

    function d() {
        try {
            return Q.activeElement
        } catch (t) {}
    }

    function h(t, e) {
        return J.nodeName(t, "table") && J.nodeName(11 !== e.nodeType ? e : e.firstChild, "tr") ? t.getElementsByTagName("tbody")[0] || t.appendChild(t.ownerDocument.createElement("tbody")) : t
    }

    function f(t) {
        return t.type = (null !== t.getAttribute("type")) + "/" + t.type, t
    }

    function p(t) {
        var e = Ot.exec(t.type);
        return e ? t.type = e[1] : t.removeAttribute("type"), t
    }

    function g(t, e) {
        for (var n = 0, i = t.length; i > n; n++) vt.set(t[n], "globalEval", !e || vt.get(e[n], "globalEval"))
    }

    function m(t, e) {
        var n, i, o, r, s, a, l, c;
        if (1 === e.nodeType) {
            if (vt.hasData(t) && (r = vt.access(t), s = vt.set(e, r), c = r.events)) {
                delete s.handle, s.events = {};
                for (o in c)
                    for (n = 0, i = c[o].length; i > n; n++) J.event.add(e, o, c[o][n])
            }
            yt.hasData(t) && (a = yt.access(t), l = J.extend({}, a), yt.set(e, l))
        }
    }

    function v(t, e) {
        var n = t.getElementsByTagName ? t.getElementsByTagName(e || "*") : t.querySelectorAll ? t.querySelectorAll(e || "*") : [];
        return void 0 === e || e && J.nodeName(t, e) ? J.merge([t], n) : n
    }

    function y(t, e) {
        var n = e.nodeName.toLowerCase();
        "input" === n && Ct.test(t.type) ? e.checked = t.checked : ("input" === n || "textarea" === n) && (e.defaultValue = t.defaultValue)
    }

    function w(e, n) {
        var i, o = J(n.createElement(e)).appendTo(n.body),
            r = t.getDefaultComputedStyle && (i = t.getDefaultComputedStyle(o[0])) ? i.display : J.css(o[0], "display");
        return o.detach(), r
    }

    function b(t) {
        var e = Q,
            n = Wt[t];
        return n || (n = w(t, e), "none" !== n && n || (Ft = (Ft || J("<iframe frameborder='0' width='0' height='0'/>")).appendTo(e.documentElement), e = Ft[0].contentDocument, e.write(), e.close(), n = w(t, e), Ft.detach()), Wt[t] = n), n
    }

    function x(t, e, n) {
        var i, o, r, s, a = t.style;
        return n = n || $t(t), n && (s = n.getPropertyValue(e) || n[e]), n && ("" !== s || J.contains(t.ownerDocument, t) || (s = J.style(t, e)), Yt.test(s) && jt.test(e) && (i = a.width, o = a.minWidth, r = a.maxWidth, a.minWidth = a.maxWidth = a.width = s, s = n.width, a.width = i, a.minWidth = o, a.maxWidth = r)), void 0 !== s ? s + "" : s
    }

    function S(t, e) {
        return {
            get: function() {
                return t() ? void delete this.get : (this.get = e).apply(this, arguments)
            }
        }
    }

    function T(t, e) {
        if (e in t) return e;
        for (var n = e[0].toUpperCase() + e.slice(1), i = e, o = Xt.length; o--;)
            if (e = Xt[o] + n, e in t) return e;
        return i
    }

    function C(t, e, n) {
        var i = Bt.exec(e);
        return i ? Math.max(0, i[1] - (n || 0)) + (i[2] || "px") : e
    }

    function k(t, e, n, i, o) {
        for (var r = n === (i ? "border" : "content") ? 4 : "width" === e ? 1 : 0, s = 0; 4 > r; r += 2) "margin" === n && (s += J.css(t, n + St[r], !0, o)), i ? ("content" === n && (s -= J.css(t, "padding" + St[r], !0, o)), "margin" !== n && (s -= J.css(t, "border" + St[r] + "Width", !0, o))) : (s += J.css(t, "padding" + St[r], !0, o), "padding" !== n && (s += J.css(t, "border" + St[r] + "Width", !0, o)));
        return s
    }

    function E(t, e, n) {
        var i = !0,
            o = "width" === e ? t.offsetWidth : t.offsetHeight,
            r = $t(t),
            s = "border-box" === J.css(t, "boxSizing", !1, r);
        if (0 >= o || null == o) {
            if (o = x(t, e, r), (0 > o || null == o) && (o = t.style[e]), Yt.test(o)) return o;
            i = s && (Z.boxSizingReliable() || o === t.style[e]), o = parseFloat(o) || 0
        }
        return o + k(t, e, n || (s ? "border" : "content"), i, r) + "px"
    }

    function D(t, e) {
        for (var n, i, o, r = [], s = 0, a = t.length; a > s; s++) i = t[s], i.style && (r[s] = vt.get(i, "olddisplay"), n = i.style.display, e ? (r[s] || "none" !== n || (i.style.display = ""), "" === i.style.display && Tt(i) && (r[s] = vt.access(i, "olddisplay", b(i.nodeName)))) : (o = Tt(i), "none" === n && o || vt.set(i, "olddisplay", o ? n : J.css(i, "display"))));
        for (s = 0; a > s; s++) i = t[s], i.style && (e && "none" !== i.style.display && "" !== i.style.display || (i.style.display = e ? r[s] || "" : "none"));
        return t
    }

    function M(t, e, n, i, o) {
        return new M.prototype.init(t, e, n, i, o)
    }

    function _() {
        return setTimeout(function() {
            Zt = void 0
        }), Zt = J.now()
    }

    function R(t, e) {
        var n, i = 0,
            o = {
                height: t
            };
        for (e = e ? 1 : 0; 4 > i; i += 2 - e) n = St[i], o["margin" + n] = o["padding" + n] = t;
        return e && (o.opacity = o.width = t), o
    }

    function H(t, e, n) {
        for (var i, o = (ne[e] || []).concat(ne["*"]), r = 0, s = o.length; s > r; r++)
            if (i = o[r].call(n, e, t)) return i
    }

    function z(t, e, n) {
        var i, o, r, s, a, l, c, u, d = this,
            h = {},
            f = t.style,
            p = t.nodeType && Tt(t),
            g = vt.get(t, "fxshow");
        n.queue || (a = J._queueHooks(t, "fx"), null == a.unqueued && (a.unqueued = 0, l = a.empty.fire, a.empty.fire = function() {
            a.unqueued || l()
        }), a.unqueued++, d.always(function() {
            d.always(function() {
                a.unqueued--, J.queue(t, "fx").length || a.empty.fire()
            })
        })), 1 === t.nodeType && ("height" in e || "width" in e) && (n.overflow = [f.overflow, f.overflowX, f.overflowY], c = J.css(t, "display"), u = "none" === c ? vt.get(t, "olddisplay") || b(t.nodeName) : c, "inline" === u && "none" === J.css(t, "float") && (f.display = "inline-block")), n.overflow && (f.overflow = "hidden", d.always(function() {
            f.overflow = n.overflow[0], f.overflowX = n.overflow[1], f.overflowY = n.overflow[2]
        }));
        for (i in e)
            if (o = e[i], Kt.exec(o)) {
                if (delete e[i], r = r || "toggle" === o, o === (p ? "hide" : "show")) {
                    if ("show" !== o || !g || void 0 === g[i]) continue;
                    p = !0
                }
                h[i] = g && g[i] || J.style(t, i)
            } else c = void 0;
        if (J.isEmptyObject(h)) "inline" === ("none" === c ? b(t.nodeName) : c) && (f.display = c);
        else {
            g ? "hidden" in g && (p = g.hidden) : g = vt.access(t, "fxshow", {}), r && (g.hidden = !p), p ? J(t).show() : d.done(function() {
                J(t).hide()
            }), d.done(function() {
                var e;
                vt.remove(t, "fxshow");
                for (e in h) J.style(t, e, h[e])
            });
            for (i in h) s = H(p ? g[i] : 0, i, d), i in g || (g[i] = s.start, p && (s.end = s.start, s.start = "width" === i || "height" === i ? 1 : 0))
        }
    }

    function A(t, e) {
        var n, i, o, r, s;
        for (n in t)
            if (i = J.camelCase(n), o = e[i], r = t[n], J.isArray(r) && (o = r[1], r = t[n] = r[0]), n !== i && (t[i] = r, delete t[n]), s = J.cssHooks[i], s && "expand" in s) {
                r = s.expand(r), delete t[i];
                for (n in r) n in t || (t[n] = r[n], e[n] = o)
            } else e[i] = o
    }

    function L(t, e, n) {
        var i, o, r = 0,
            s = ee.length,
            a = J.Deferred().always(function() {
                delete l.elem
            }),
            l = function() {
                if (o) return !1;
                for (var e = Zt || _(), n = Math.max(0, c.startTime + c.duration - e), i = n / c.duration || 0, r = 1 - i, s = 0, l = c.tweens.length; l > s; s++) c.tweens[s].run(r);
                return a.notifyWith(t, [c, r, n]), 1 > r && l ? n : (a.resolveWith(t, [c]), !1)
            },
            c = a.promise({
                elem: t,
                props: J.extend({}, e),
                opts: J.extend(!0, {
                    specialEasing: {}
                }, n),
                originalProperties: e,
                originalOptions: n,
                startTime: Zt || _(),
                duration: n.duration,
                tweens: [],
                createTween: function(e, n) {
                    var i = J.Tween(t, c.opts, e, n, c.opts.specialEasing[e] || c.opts.easing);
                    return c.tweens.push(i), i
                },
                stop: function(e) {
                    var n = 0,
                        i = e ? c.tweens.length : 0;
                    if (o) return this;
                    for (o = !0; i > n; n++) c.tweens[n].run(1);
                    return e ? a.resolveWith(t, [c, e]) : a.rejectWith(t, [c, e]), this
                }
            }),
            u = c.props;
        for (A(u, c.opts.specialEasing); s > r; r++)
            if (i = ee[r].call(c, t, u, c.opts)) return i;
        return J.map(u, H, c), J.isFunction(c.opts.start) && c.opts.start.call(t, c), J.fx.timer(J.extend(l, {
            elem: t,
            anim: c,
            queue: c.opts.queue
        })), c.progress(c.opts.progress).done(c.opts.done, c.opts.complete).fail(c.opts.fail).always(c.opts.always)
    }

    function N(t) {
        return function(e, n) {
            "string" != typeof e && (n = e, e = "*");
            var i, o = 0,
                r = e.toLowerCase().match(ft) || [];
            if (J.isFunction(n))
                for (; i = r[o++];) "+" === i[0] ? (i = i.slice(1) || "*", (t[i] = t[i] || []).unshift(n)) : (t[i] = t[i] || []).push(n)
        }
    }

    function O(t, e, n, i) {
        function o(a) {
            var l;
            return r[a] = !0, J.each(t[a] || [], function(t, a) {
                var c = a(e, n, i);
                return "string" != typeof c || s || r[c] ? s ? !(l = c) : void 0 : (e.dataTypes.unshift(c), o(c), !1)
            }), l
        }
        var r = {},
            s = t === we;
        return o(e.dataTypes[0]) || !r["*"] && o("*")
    }

    function I(t, e) {
        var n, i, o = J.ajaxSettings.flatOptions || {};
        for (n in e) void 0 !== e[n] && ((o[n] ? t : i || (i = {}))[n] = e[n]);
        return i && J.extend(!0, t, i), t
    }

    function P(t, e, n) {
        for (var i, o, r, s, a = t.contents, l = t.dataTypes;
            "*" === l[0];) l.shift(), void 0 === i && (i = t.mimeType || e.getResponseHeader("Content-Type"));
        if (i)
            for (o in a)
                if (a[o] && a[o].test(i)) {
                    l.unshift(o);
                    break
                }
        if (l[0] in n) r = l[0];
        else {
            for (o in n) {
                if (!l[0] || t.converters[o + " " + l[0]]) {
                    r = o;
                    break
                }
                s || (s = o)
            }
            r = r || s
        }
        return r ? (r !== l[0] && l.unshift(r), n[r]) : void 0
    }

    function F(t, e, n, i) {
        var o, r, s, a, l, c = {},
            u = t.dataTypes.slice();
        if (u[1])
            for (s in t.converters) c[s.toLowerCase()] = t.converters[s];
        for (r = u.shift(); r;)
            if (t.responseFields[r] && (n[t.responseFields[r]] = e), !l && i && t.dataFilter && (e = t.dataFilter(e, t.dataType)), l = r, r = u.shift())
                if ("*" === r) r = l;
                else if ("*" !== l && l !== r) {
            if (s = c[l + " " + r] || c["* " + r], !s)
                for (o in c)
                    if (a = o.split(" "), a[1] === r && (s = c[l + " " + a[0]] || c["* " + a[0]])) {
                        s === !0 ? s = c[o] : c[o] !== !0 && (r = a[0], u.unshift(a[1]));
                        break
                    }
            if (s !== !0)
                if (s && t["throws"]) e = s(e);
                else try {
                    e = s(e)
                } catch (d) {
                    return {
                        state: "parsererror",
                        error: s ? d : "No conversion from " + l + " to " + r
                    }
                }
        }
        return {
            state: "success",
            data: e
        }
    }

    function W(t, e, n, i) {
        var o;
        if (J.isArray(e)) J.each(e, function(e, o) {
            n || Ce.test(t) ? i(t, o) : W(t + "[" + ("object" == typeof o ? e : "") + "]", o, n, i)
        });
        else if (n || "object" !== J.type(e)) i(t, e);
        else
            for (o in e) W(t + "[" + o + "]", e[o], n, i)
    }

    function j(t) {
        return J.isWindow(t) ? t : 9 === t.nodeType && t.defaultView
    }
    var Y = [],
        $ = Y.slice,
        q = Y.concat,
        B = Y.push,
        G = Y.indexOf,
        V = {},
        U = V.toString,
        X = V.hasOwnProperty,
        Z = {},
        Q = t.document,
        K = "2.1.4",
        J = function(t, e) {
            return new J.fn.init(t, e)
        },
        tt = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g,
        et = /^-ms-/,
        nt = /-([\da-z])/gi,
        it = function(t, e) {
            return e.toUpperCase()
        };
    J.fn = J.prototype = {
        jquery: K,
        constructor: J,
        selector: "",
        length: 0,
        toArray: function() {
            return $.call(this)
        },
        get: function(t) {
            return null != t ? 0 > t ? this[t + this.length] : this[t] : $.call(this)
        },
        pushStack: function(t) {
            var e = J.merge(this.constructor(), t);
            return e.prevObject = this, e.context = this.context, e
        },
        each: function(t, e) {
            return J.each(this, t, e)
        },
        map: function(t) {
            return this.pushStack(J.map(this, function(e, n) {
                return t.call(e, n, e)
            }))
        },
        slice: function() {
            return this.pushStack($.apply(this, arguments))
        },
        first: function() {
            return this.eq(0)
        },
        last: function() {
            return this.eq(-1)
        },
        eq: function(t) {
            var e = this.length,
                n = +t + (0 > t ? e : 0);
            return this.pushStack(n >= 0 && e > n ? [this[n]] : [])
        },
        end: function() {
            return this.prevObject || this.constructor(null)
        },
        push: B,
        sort: Y.sort,
        splice: Y.splice
    }, J.extend = J.fn.extend = function() {
        var t, e, n, i, o, r, s = arguments[0] || {},
            a = 1,
            l = arguments.length,
            c = !1;
        for ("boolean" == typeof s && (c = s, s = arguments[a] || {}, a++), "object" == typeof s || J.isFunction(s) || (s = {}), a === l && (s = this, a--); l > a; a++)
            if (null != (t = arguments[a]))
                for (e in t) n = s[e], i = t[e], s !== i && (c && i && (J.isPlainObject(i) || (o = J.isArray(i))) ? (o ? (o = !1, r = n && J.isArray(n) ? n : []) : r = n && J.isPlainObject(n) ? n : {}, s[e] = J.extend(c, r, i)) : void 0 !== i && (s[e] = i));
        return s
    }, J.extend({
        expando: "jQuery" + (K + Math.random()).replace(/\D/g, ""),
        isReady: !0,
        error: function(t) {
            throw new Error(t)
        },
        noop: function() {},
        isFunction: function(t) {
            return "function" === J.type(t)
        },
        isArray: Array.isArray,
        isWindow: function(t) {
            return null != t && t === t.window
        },
        isNumeric: function(t) {
            return !J.isArray(t) && t - parseFloat(t) + 1 >= 0
        },
        isPlainObject: function(t) {
            return "object" !== J.type(t) || t.nodeType || J.isWindow(t) ? !1 : t.constructor && !X.call(t.constructor.prototype, "isPrototypeOf") ? !1 : !0
        },
        isEmptyObject: function(t) {
            var e;
            for (e in t) return !1;
            return !0
        },
        type: function(t) {
            return null == t ? t + "" : "object" == typeof t || "function" == typeof t ? V[U.call(t)] || "object" : typeof t
        },
        globalEval: function(t) {
            var e, n = eval;
            t = J.trim(t), t && (1 === t.indexOf("use strict") ? (e = Q.createElement("script"), e.text = t, Q.head.appendChild(e).parentNode.removeChild(e)) : n(t))
        },
        camelCase: function(t) {
            return t.replace(et, "ms-").replace(nt, it)
        },
        nodeName: function(t, e) {
            return t.nodeName && t.nodeName.toLowerCase() === e.toLowerCase()
        },
        each: function(t, e, i) {
            var o, r = 0,
                s = t.length,
                a = n(t);
            if (i) {
                if (a)
                    for (; s > r && (o = e.apply(t[r], i), o !== !1); r++);
                else
                    for (r in t)
                        if (o = e.apply(t[r], i), o === !1) break
            } else if (a)
                for (; s > r && (o = e.call(t[r], r, t[r]), o !== !1); r++);
            else
                for (r in t)
                    if (o = e.call(t[r], r, t[r]), o === !1) break; return t
        },
        trim: function(t) {
            return null == t ? "" : (t + "").replace(tt, "")
        },
        makeArray: function(t, e) {
            var i = e || [];
            return null != t && (n(Object(t)) ? J.merge(i, "string" == typeof t ? [t] : t) : B.call(i, t)), i
        },
        inArray: function(t, e, n) {
            return null == e ? -1 : G.call(e, t, n)
        },
        merge: function(t, e) {
            for (var n = +e.length, i = 0, o = t.length; n > i; i++) t[o++] = e[i];
            return t.length = o, t
        },
        grep: function(t, e, n) {
            for (var i, o = [], r = 0, s = t.length, a = !n; s > r; r++) i = !e(t[r], r), i !== a && o.push(t[r]);
            return o
        },
        map: function(t, e, i) {
            var o, r = 0,
                s = t.length,
                a = n(t),
                l = [];
            if (a)
                for (; s > r; r++) o = e(t[r], r, i), null != o && l.push(o);
            else
                for (r in t) o = e(t[r], r, i), null != o && l.push(o);
            return q.apply([], l)
        },
        guid: 1,
        proxy: function(t, e) {
            var n, i, o;
            return "string" == typeof e && (n = t[e], e = t, t = n), J.isFunction(t) ? (i = $.call(arguments, 2), o = function() {
                return t.apply(e || this, i.concat($.call(arguments)))
            }, o.guid = t.guid = t.guid || J.guid++, o) : void 0
        },
        now: Date.now,
        support: Z
    }), J.each("Boolean Number String Function Array Date RegExp Object Error".split(" "), function(t, e) {
        V["[object " + e + "]"] = e.toLowerCase()
    });
    var ot = function(t) {
        function e(t, e, n, i) {
            var o, r, s, a, l, c, d, f, p, g;
            if ((e ? e.ownerDocument || e : W) !== z && H(e), e = e || z, n = n || [], a = e.nodeType, "string" != typeof t || !t || 1 !== a && 9 !== a && 11 !== a) return n;
            if (!i && L) {
                if (11 !== a && (o = yt.exec(t)))
                    if (s = o[1]) {
                        if (9 === a) {
                            if (r = e.getElementById(s), !r || !r.parentNode) return n;
                            if (r.id === s) return n.push(r), n
                        } else if (e.ownerDocument && (r = e.ownerDocument.getElementById(s)) && P(e, r) && r.id === s) return n.push(r), n
                    } else {
                        if (o[2]) return K.apply(n, e.getElementsByTagName(t)), n;
                        if ((s = o[3]) && x.getElementsByClassName) return K.apply(n, e.getElementsByClassName(s)), n
                    }
                if (x.qsa && (!N || !N.test(t))) {
                    if (f = d = F, p = e, g = 1 !== a && t, 1 === a && "object" !== e.nodeName.toLowerCase()) {
                        for (c = k(t), (d = e.getAttribute("id")) ? f = d.replace(bt, "\\$&") : e.setAttribute("id", f), f = "[id='" + f + "'] ", l = c.length; l--;) c[l] = f + h(c[l]);
                        p = wt.test(t) && u(e.parentNode) || e, g = c.join(",")
                    }
                    if (g) try {
                        return K.apply(n, p.querySelectorAll(g)), n
                    } catch (m) {} finally {
                        d || e.removeAttribute("id")
                    }
                }
            }
            return D(t.replace(lt, "$1"), e, n, i)
        }

        function n() {
            function t(n, i) {
                return e.push(n + " ") > S.cacheLength && delete t[e.shift()], t[n + " "] = i
            }
            var e = [];
            return t
        }

        function i(t) {
            return t[F] = !0, t
        }

        function o(t) {
            var e = z.createElement("div");
            try {
                return !!t(e)
            } catch (n) {
                return !1
            } finally {
                e.parentNode && e.parentNode.removeChild(e), e = null
            }
        }

        function r(t, e) {
            for (var n = t.split("|"), i = t.length; i--;) S.attrHandle[n[i]] = e
        }

        function s(t, e) {
            var n = e && t,
                i = n && 1 === t.nodeType && 1 === e.nodeType && (~e.sourceIndex || V) - (~t.sourceIndex || V);
            if (i) return i;
            if (n)
                for (; n = n.nextSibling;)
                    if (n === e) return -1;
            return t ? 1 : -1
        }

        function a(t) {
            return function(e) {
                var n = e.nodeName.toLowerCase();
                return "input" === n && e.type === t
            }
        }

        function l(t) {
            return function(e) {
                var n = e.nodeName.toLowerCase();
                return ("input" === n || "button" === n) && e.type === t
            }
        }

        function c(t) {
            return i(function(e) {
                return e = +e, i(function(n, i) {
                    for (var o, r = t([], n.length, e), s = r.length; s--;) n[o = r[s]] && (n[o] = !(i[o] = n[o]))
                })
            })
        }

        function u(t) {
            return t && "undefined" != typeof t.getElementsByTagName && t
        }

        function d() {}

        function h(t) {
            for (var e = 0, n = t.length, i = ""; n > e; e++) i += t[e].value;
            return i
        }

        function f(t, e, n) {
            var i = e.dir,
                o = n && "parentNode" === i,
                r = Y++;
            return e.first ? function(e, n, r) {
                for (; e = e[i];)
                    if (1 === e.nodeType || o) return t(e, n, r)
            } : function(e, n, s) {
                var a, l, c = [j, r];
                if (s) {
                    for (; e = e[i];)
                        if ((1 === e.nodeType || o) && t(e, n, s)) return !0
                } else
                    for (; e = e[i];)
                        if (1 === e.nodeType || o) {
                            if (l = e[F] || (e[F] = {}), (a = l[i]) && a[0] === j && a[1] === r) return c[2] = a[2];
                            if (l[i] = c, c[2] = t(e, n, s)) return !0
                        }
            }
        }

        function p(t) {
            return t.length > 1 ? function(e, n, i) {
                for (var o = t.length; o--;)
                    if (!t[o](e, n, i)) return !1;
                return !0
            } : t[0]
        }

        function g(t, n, i) {
            for (var o = 0, r = n.length; r > o; o++) e(t, n[o], i);
            return i
        }

        function m(t, e, n, i, o) {
            for (var r, s = [], a = 0, l = t.length, c = null != e; l > a; a++)(r = t[a]) && (!n || n(r, i, o)) && (s.push(r), c && e.push(a));
            return s
        }

        function v(t, e, n, o, r, s) {
            return o && !o[F] && (o = v(o)), r && !r[F] && (r = v(r, s)), i(function(i, s, a, l) {
                var c, u, d, h = [],
                    f = [],
                    p = s.length,
                    v = i || g(e || "*", a.nodeType ? [a] : a, []),
                    y = !t || !i && e ? v : m(v, h, t, a, l),
                    w = n ? r || (i ? t : p || o) ? [] : s : y;
                if (n && n(y, w, a, l), o)
                    for (c = m(w, f), o(c, [], a, l), u = c.length; u--;)(d = c[u]) && (w[f[u]] = !(y[f[u]] = d));
                if (i) {
                    if (r || t) {
                        if (r) {
                            for (c = [], u = w.length; u--;)(d = w[u]) && c.push(y[u] = d);
                            r(null, w = [], c, l)
                        }
                        for (u = w.length; u--;)(d = w[u]) && (c = r ? tt(i, d) : h[u]) > -1 && (i[c] = !(s[c] = d))
                    }
                } else w = m(w === s ? w.splice(p, w.length) : w), r ? r(null, s, w, l) : K.apply(s, w)
            })
        }

        function y(t) {
            for (var e, n, i, o = t.length, r = S.relative[t[0].type], s = r || S.relative[" "], a = r ? 1 : 0, l = f(function(t) {
                return t === e
            }, s, !0), c = f(function(t) {
                return tt(e, t) > -1
            }, s, !0), u = [
                function(t, n, i) {
                    var o = !r && (i || n !== M) || ((e = n).nodeType ? l(t, n, i) : c(t, n, i));
                    return e = null, o
                }
            ]; o > a; a++)
                if (n = S.relative[t[a].type]) u = [f(p(u), n)];
                else {
                    if (n = S.filter[t[a].type].apply(null, t[a].matches), n[F]) {
                        for (i = ++a; o > i && !S.relative[t[i].type]; i++);
                        return v(a > 1 && p(u), a > 1 && h(t.slice(0, a - 1).concat({
                            value: " " === t[a - 2].type ? "*" : ""
                        })).replace(lt, "$1"), n, i > a && y(t.slice(a, i)), o > i && y(t = t.slice(i)), o > i && h(t))
                    }
                    u.push(n)
                }
            return p(u)
        }

        function w(t, n) {
            var o = n.length > 0,
                r = t.length > 0,
                s = function(i, s, a, l, c) {
                    var u, d, h, f = 0,
                        p = "0",
                        g = i && [],
                        v = [],
                        y = M,
                        w = i || r && S.find.TAG("*", c),
                        b = j += null == y ? 1 : Math.random() || .1,
                        x = w.length;
                    for (c && (M = s !== z && s); p !== x && null != (u = w[p]); p++) {
                        if (r && u) {
                            for (d = 0; h = t[d++];)
                                if (h(u, s, a)) {
                                    l.push(u);
                                    break
                                }
                            c && (j = b)
                        }
                        o && ((u = !h && u) && f--, i && g.push(u))
                    }
                    if (f += p, o && p !== f) {
                        for (d = 0; h = n[d++];) h(g, v, s, a);
                        if (i) {
                            if (f > 0)
                                for (; p--;) g[p] || v[p] || (v[p] = Z.call(l));
                            v = m(v)
                        }
                        K.apply(l, v), c && !i && v.length > 0 && f + n.length > 1 && e.uniqueSort(l)
                    }
                    return c && (j = b, M = y), g
                };
            return o ? i(s) : s
        }
        var b, x, S, T, C, k, E, D, M, _, R, H, z, A, L, N, O, I, P, F = "sizzle" + 1 * new Date,
            W = t.document,
            j = 0,
            Y = 0,
            $ = n(),
            q = n(),
            B = n(),
            G = function(t, e) {
                return t === e && (R = !0), 0
            },
            V = 1 << 31,
            U = {}.hasOwnProperty,
            X = [],
            Z = X.pop,
            Q = X.push,
            K = X.push,
            J = X.slice,
            tt = function(t, e) {
                for (var n = 0, i = t.length; i > n; n++)
                    if (t[n] === e) return n;
                return -1
            },
            et = "checked|selected|async|autofocus|autoplay|controls|defer|disabled|hidden|ismap|loop|multiple|open|readonly|required|scoped",
            nt = "[\\x20\\t\\r\\n\\f]",
            it = "(?:\\\\.|[\\w-]|[^\\x00-\\xa0])+",
            ot = it.replace("w", "w#"),
            rt = "\\[" + nt + "*(" + it + ")(?:" + nt + "*([*^$|!~]?=)" + nt + "*(?:'((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\"|(" + ot + "))|)" + nt + "*\\]",
            st = ":(" + it + ")(?:\\((('((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\")|((?:\\\\.|[^\\\\()[\\]]|" + rt + ")*)|.*)\\)|)",
            at = new RegExp(nt + "+", "g"),
            lt = new RegExp("^" + nt + "+|((?:^|[^\\\\])(?:\\\\.)*)" + nt + "+$", "g"),
            ct = new RegExp("^" + nt + "*," + nt + "*"),
            ut = new RegExp("^" + nt + "*([>+~]|" + nt + ")" + nt + "*"),
            dt = new RegExp("=" + nt + "*([^\\]'\"]*?)" + nt + "*\\]", "g"),
            ht = new RegExp(st),
            ft = new RegExp("^" + ot + "$"),
            pt = {
                ID: new RegExp("^#(" + it + ")"),
                CLASS: new RegExp("^\\.(" + it + ")"),
                TAG: new RegExp("^(" + it.replace("w", "w*") + ")"),
                ATTR: new RegExp("^" + rt),
                PSEUDO: new RegExp("^" + st),
                CHILD: new RegExp("^:(only|first|last|nth|nth-last)-(child|of-type)(?:\\(" + nt + "*(even|odd|(([+-]|)(\\d*)n|)" + nt + "*(?:([+-]|)" + nt + "*(\\d+)|))" + nt + "*\\)|)", "i"),
                bool: new RegExp("^(?:" + et + ")$", "i"),
                needsContext: new RegExp("^" + nt + "*[>+~]|:(even|odd|eq|gt|lt|nth|first|last)(?:\\(" + nt + "*((?:-\\d)?\\d*)" + nt + "*\\)|)(?=[^-]|$)", "i")
            },
            gt = /^(?:input|select|textarea|button)$/i,
            mt = /^h\d$/i,
            vt = /^[^{]+\{\s*\[native \w/,
            yt = /^(?:#([\w-]+)|(\w+)|\.([\w-]+))$/,
            wt = /[+~]/,
            bt = /'|\\/g,
            xt = new RegExp("\\\\([\\da-f]{1,6}" + nt + "?|(" + nt + ")|.)", "ig"),
            St = function(t, e, n) {
                var i = "0x" + e - 65536;
                return i !== i || n ? e : 0 > i ? String.fromCharCode(i + 65536) : String.fromCharCode(i >> 10 | 55296, 1023 & i | 56320)
            },
            Tt = function() {
                H()
            };
        try {
            K.apply(X = J.call(W.childNodes), W.childNodes), X[W.childNodes.length].nodeType
        } catch (Ct) {
            K = {
                apply: X.length ? function(t, e) {
                    Q.apply(t, J.call(e))
                } : function(t, e) {
                    for (var n = t.length, i = 0; t[n++] = e[i++];);
                    t.length = n - 1
                }
            }
        }
        x = e.support = {}, C = e.isXML = function(t) {
            var e = t && (t.ownerDocument || t).documentElement;
            return e ? "HTML" !== e.nodeName : !1
        }, H = e.setDocument = function(t) {
            var e, n, i = t ? t.ownerDocument || t : W;
            return i !== z && 9 === i.nodeType && i.documentElement ? (z = i, A = i.documentElement, n = i.defaultView, n && n !== n.top && (n.addEventListener ? n.addEventListener("unload", Tt, !1) : n.attachEvent && n.attachEvent("onunload", Tt)), L = !C(i), x.attributes = o(function(t) {
                return t.className = "i", !t.getAttribute("className")
            }), x.getElementsByTagName = o(function(t) {
                return t.appendChild(i.createComment("")), !t.getElementsByTagName("*").length
            }), x.getElementsByClassName = vt.test(i.getElementsByClassName), x.getById = o(function(t) {
                return A.appendChild(t).id = F, !i.getElementsByName || !i.getElementsByName(F).length
            }), x.getById ? (S.find.ID = function(t, e) {
                if ("undefined" != typeof e.getElementById && L) {
                    var n = e.getElementById(t);
                    return n && n.parentNode ? [n] : []
                }
            }, S.filter.ID = function(t) {
                var e = t.replace(xt, St);
                return function(t) {
                    return t.getAttribute("id") === e
                }
            }) : (delete S.find.ID, S.filter.ID = function(t) {
                var e = t.replace(xt, St);
                return function(t) {
                    var n = "undefined" != typeof t.getAttributeNode && t.getAttributeNode("id");
                    return n && n.value === e
                }
            }), S.find.TAG = x.getElementsByTagName ? function(t, e) {
                return "undefined" != typeof e.getElementsByTagName ? e.getElementsByTagName(t) : x.qsa ? e.querySelectorAll(t) : void 0
            } : function(t, e) {
                var n, i = [],
                    o = 0,
                    r = e.getElementsByTagName(t);
                if ("*" === t) {
                    for (; n = r[o++];) 1 === n.nodeType && i.push(n);
                    return i
                }
                return r
            }, S.find.CLASS = x.getElementsByClassName && function(t, e) {
                return L ? e.getElementsByClassName(t) : void 0
            }, O = [], N = [], (x.qsa = vt.test(i.querySelectorAll)) && (o(function(t) {
                A.appendChild(t).innerHTML = "<a id='" + F + "'></a><select id='" + F + "-\f]' msallowcapture=''><option selected=''></option></select>", t.querySelectorAll("[msallowcapture^='']").length && N.push("[*^$]=" + nt + "*(?:''|\"\")"), t.querySelectorAll("[selected]").length || N.push("\\[" + nt + "*(?:value|" + et + ")"), t.querySelectorAll("[id~=" + F + "-]").length || N.push("~="), t.querySelectorAll(":checked").length || N.push(":checked"), t.querySelectorAll("a#" + F + "+*").length || N.push(".#.+[+~]")
            }), o(function(t) {
                var e = i.createElement("input");
                e.setAttribute("type", "hidden"), t.appendChild(e).setAttribute("name", "D"), t.querySelectorAll("[name=d]").length && N.push("name" + nt + "*[*^$|!~]?="), t.querySelectorAll(":enabled").length || N.push(":enabled", ":disabled"), t.querySelectorAll("*,:x"), N.push(",.*:")
            })), (x.matchesSelector = vt.test(I = A.matches || A.webkitMatchesSelector || A.mozMatchesSelector || A.oMatchesSelector || A.msMatchesSelector)) && o(function(t) {
                x.disconnectedMatch = I.call(t, "div"), I.call(t, "[s!='']:x"), O.push("!=", st)
            }), N = N.length && new RegExp(N.join("|")), O = O.length && new RegExp(O.join("|")), e = vt.test(A.compareDocumentPosition), P = e || vt.test(A.contains) ? function(t, e) {
                var n = 9 === t.nodeType ? t.documentElement : t,
                    i = e && e.parentNode;
                return t === i || !(!i || 1 !== i.nodeType || !(n.contains ? n.contains(i) : t.compareDocumentPosition && 16 & t.compareDocumentPosition(i)))
            } : function(t, e) {
                if (e)
                    for (; e = e.parentNode;)
                        if (e === t) return !0;
                return !1
            }, G = e ? function(t, e) {
                if (t === e) return R = !0, 0;
                var n = !t.compareDocumentPosition - !e.compareDocumentPosition;
                return n ? n : (n = (t.ownerDocument || t) === (e.ownerDocument || e) ? t.compareDocumentPosition(e) : 1, 1 & n || !x.sortDetached && e.compareDocumentPosition(t) === n ? t === i || t.ownerDocument === W && P(W, t) ? -1 : e === i || e.ownerDocument === W && P(W, e) ? 1 : _ ? tt(_, t) - tt(_, e) : 0 : 4 & n ? -1 : 1)
            } : function(t, e) {
                if (t === e) return R = !0, 0;
                var n, o = 0,
                    r = t.parentNode,
                    a = e.parentNode,
                    l = [t],
                    c = [e];
                if (!r || !a) return t === i ? -1 : e === i ? 1 : r ? -1 : a ? 1 : _ ? tt(_, t) - tt(_, e) : 0;
                if (r === a) return s(t, e);
                for (n = t; n = n.parentNode;) l.unshift(n);
                for (n = e; n = n.parentNode;) c.unshift(n);
                for (; l[o] === c[o];) o++;
                return o ? s(l[o], c[o]) : l[o] === W ? -1 : c[o] === W ? 1 : 0
            }, i) : z
        }, e.matches = function(t, n) {
            return e(t, null, null, n)
        }, e.matchesSelector = function(t, n) {
            if ((t.ownerDocument || t) !== z && H(t), n = n.replace(dt, "='$1']"), !(!x.matchesSelector || !L || O && O.test(n) || N && N.test(n))) try {
                var i = I.call(t, n);
                if (i || x.disconnectedMatch || t.document && 11 !== t.document.nodeType) return i
            } catch (o) {}
            return e(n, z, null, [t]).length > 0
        }, e.contains = function(t, e) {
            return (t.ownerDocument || t) !== z && H(t), P(t, e)
        }, e.attr = function(t, e) {
            (t.ownerDocument || t) !== z && H(t);
            var n = S.attrHandle[e.toLowerCase()],
                i = n && U.call(S.attrHandle, e.toLowerCase()) ? n(t, e, !L) : void 0;
            return void 0 !== i ? i : x.attributes || !L ? t.getAttribute(e) : (i = t.getAttributeNode(e)) && i.specified ? i.value : null
        }, e.error = function(t) {
            throw new Error("Syntax error, unrecognized expression: " + t)
        }, e.uniqueSort = function(t) {
            var e, n = [],
                i = 0,
                o = 0;
            if (R = !x.detectDuplicates, _ = !x.sortStable && t.slice(0), t.sort(G), R) {
                for (; e = t[o++];) e === t[o] && (i = n.push(o));
                for (; i--;) t.splice(n[i], 1)
            }
            return _ = null, t
        }, T = e.getText = function(t) {
            var e, n = "",
                i = 0,
                o = t.nodeType;
            if (o) {
                if (1 === o || 9 === o || 11 === o) {
                    if ("string" == typeof t.textContent) return t.textContent;
                    for (t = t.firstChild; t; t = t.nextSibling) n += T(t)
                } else if (3 === o || 4 === o) return t.nodeValue
            } else
                for (; e = t[i++];) n += T(e);
            return n
        }, S = e.selectors = {
            cacheLength: 50,
            createPseudo: i,
            match: pt,
            attrHandle: {},
            find: {},
            relative: {
                ">": {
                    dir: "parentNode",
                    first: !0
                },
                " ": {
                    dir: "parentNode"
                },
                "+": {
                    dir: "previousSibling",
                    first: !0
                },
                "~": {
                    dir: "previousSibling"
                }
            },
            preFilter: {
                ATTR: function(t) {
                    return t[1] = t[1].replace(xt, St), t[3] = (t[3] || t[4] || t[5] || "").replace(xt, St), "~=" === t[2] && (t[3] = " " + t[3] + " "), t.slice(0, 4)
                },
                CHILD: function(t) {
                    return t[1] = t[1].toLowerCase(), "nth" === t[1].slice(0, 3) ? (t[3] || e.error(t[0]), t[4] = +(t[4] ? t[5] + (t[6] || 1) : 2 * ("even" === t[3] || "odd" === t[3])), t[5] = +(t[7] + t[8] || "odd" === t[3])) : t[3] && e.error(t[0]), t
                },
                PSEUDO: function(t) {
                    var e, n = !t[6] && t[2];
                    return pt.CHILD.test(t[0]) ? null : (t[3] ? t[2] = t[4] || t[5] || "" : n && ht.test(n) && (e = k(n, !0)) && (e = n.indexOf(")", n.length - e) - n.length) && (t[0] = t[0].slice(0, e), t[2] = n.slice(0, e)), t.slice(0, 3))
                }
            },
            filter: {
                TAG: function(t) {
                    var e = t.replace(xt, St).toLowerCase();
                    return "*" === t ? function() {
                        return !0
                    } : function(t) {
                        return t.nodeName && t.nodeName.toLowerCase() === e
                    }
                },
                CLASS: function(t) {
                    var e = $[t + " "];
                    return e || (e = new RegExp("(^|" + nt + ")" + t + "(" + nt + "|$)")) && $(t, function(t) {
                        return e.test("string" == typeof t.className && t.className || "undefined" != typeof t.getAttribute && t.getAttribute("class") || "")
                    })
                },
                ATTR: function(t, n, i) {
                    return function(o) {
                        var r = e.attr(o, t);
                        return null == r ? "!=" === n : n ? (r += "", "=" === n ? r === i : "!=" === n ? r !== i : "^=" === n ? i && 0 === r.indexOf(i) : "*=" === n ? i && r.indexOf(i) > -1 : "$=" === n ? i && r.slice(-i.length) === i : "~=" === n ? (" " + r.replace(at, " ") + " ").indexOf(i) > -1 : "|=" === n ? r === i || r.slice(0, i.length + 1) === i + "-" : !1) : !0
                    }
                },
                CHILD: function(t, e, n, i, o) {
                    var r = "nth" !== t.slice(0, 3),
                        s = "last" !== t.slice(-4),
                        a = "of-type" === e;
                    return 1 === i && 0 === o ? function(t) {
                        return !!t.parentNode
                    } : function(e, n, l) {
                        var c, u, d, h, f, p, g = r !== s ? "nextSibling" : "previousSibling",
                            m = e.parentNode,
                            v = a && e.nodeName.toLowerCase(),
                            y = !l && !a;
                        if (m) {
                            if (r) {
                                for (; g;) {
                                    for (d = e; d = d[g];)
                                        if (a ? d.nodeName.toLowerCase() === v : 1 === d.nodeType) return !1;
                                    p = g = "only" === t && !p && "nextSibling"
                                }
                                return !0
                            }
                            if (p = [s ? m.firstChild : m.lastChild], s && y) {
                                for (u = m[F] || (m[F] = {}), c = u[t] || [], f = c[0] === j && c[1], h = c[0] === j && c[2], d = f && m.childNodes[f]; d = ++f && d && d[g] || (h = f = 0) || p.pop();)
                                    if (1 === d.nodeType && ++h && d === e) {
                                        u[t] = [j, f, h];
                                        break
                                    }
                            } else if (y && (c = (e[F] || (e[F] = {}))[t]) && c[0] === j) h = c[1];
                            else
                                for (;
                                    (d = ++f && d && d[g] || (h = f = 0) || p.pop()) && ((a ? d.nodeName.toLowerCase() !== v : 1 !== d.nodeType) || !++h || (y && ((d[F] || (d[F] = {}))[t] = [j, h]), d !== e)););
                            return h -= o, h === i || h % i === 0 && h / i >= 0
                        }
                    }
                },
                PSEUDO: function(t, n) {
                    var o, r = S.pseudos[t] || S.setFilters[t.toLowerCase()] || e.error("unsupported pseudo: " + t);
                    return r[F] ? r(n) : r.length > 1 ? (o = [t, t, "", n], S.setFilters.hasOwnProperty(t.toLowerCase()) ? i(function(t, e) {
                        for (var i, o = r(t, n), s = o.length; s--;) i = tt(t, o[s]), t[i] = !(e[i] = o[s])
                    }) : function(t) {
                        return r(t, 0, o)
                    }) : r
                }
            },
            pseudos: {
                not: i(function(t) {
                    var e = [],
                        n = [],
                        o = E(t.replace(lt, "$1"));
                    return o[F] ? i(function(t, e, n, i) {
                        for (var r, s = o(t, null, i, []), a = t.length; a--;)(r = s[a]) && (t[a] = !(e[a] = r))
                    }) : function(t, i, r) {
                        return e[0] = t, o(e, null, r, n), e[0] = null, !n.pop()
                    }
                }),
                has: i(function(t) {
                    return function(n) {
                        return e(t, n).length > 0
                    }
                }),
                contains: i(function(t) {
                    return t = t.replace(xt, St),
                        function(e) {
                            return (e.textContent || e.innerText || T(e)).indexOf(t) > -1
                        }
                }),
                lang: i(function(t) {
                    return ft.test(t || "") || e.error("unsupported lang: " + t), t = t.replace(xt, St).toLowerCase(),
                        function(e) {
                            var n;
                            do
                                if (n = L ? e.lang : e.getAttribute("xml:lang") || e.getAttribute("lang")) return n = n.toLowerCase(), n === t || 0 === n.indexOf(t + "-");
                            while ((e = e.parentNode) && 1 === e.nodeType);
                            return !1
                        }
                }),
                target: function(e) {
                    var n = t.location && t.location.hash;
                    return n && n.slice(1) === e.id
                },
                root: function(t) {
                    return t === A
                },
                focus: function(t) {
                    return t === z.activeElement && (!z.hasFocus || z.hasFocus()) && !!(t.type || t.href || ~t.tabIndex)
                },
                enabled: function(t) {
                    return t.disabled === !1
                },
                disabled: function(t) {
                    return t.disabled === !0
                },
                checked: function(t) {
                    var e = t.nodeName.toLowerCase();
                    return "input" === e && !!t.checked || "option" === e && !!t.selected
                },
                selected: function(t) {
                    return t.parentNode && t.parentNode.selectedIndex, t.selected === !0
                },
                empty: function(t) {
                    for (t = t.firstChild; t; t = t.nextSibling)
                        if (t.nodeType < 6) return !1;
                    return !0
                },
                parent: function(t) {
                    return !S.pseudos.empty(t)
                },
                header: function(t) {
                    return mt.test(t.nodeName)
                },
                input: function(t) {
                    return gt.test(t.nodeName)
                },
                button: function(t) {
                    var e = t.nodeName.toLowerCase();
                    return "input" === e && "button" === t.type || "button" === e
                },
                text: function(t) {
                    var e;
                    return "input" === t.nodeName.toLowerCase() && "text" === t.type && (null == (e = t.getAttribute("type")) || "text" === e.toLowerCase())
                },
                first: c(function() {
                    return [0]
                }),
                last: c(function(t, e) {
                    return [e - 1]
                }),
                eq: c(function(t, e, n) {
                    return [0 > n ? n + e : n]
                }),
                even: c(function(t, e) {
                    for (var n = 0; e > n; n += 2) t.push(n);
                    return t
                }),
                odd: c(function(t, e) {
                    for (var n = 1; e > n; n += 2) t.push(n);
                    return t
                }),
                lt: c(function(t, e, n) {
                    for (var i = 0 > n ? n + e : n; --i >= 0;) t.push(i);
                    return t
                }),
                gt: c(function(t, e, n) {
                    for (var i = 0 > n ? n + e : n; ++i < e;) t.push(i);
                    return t
                })
            }
        }, S.pseudos.nth = S.pseudos.eq;
        for (b in {
            radio: !0,
            checkbox: !0,
            file: !0,
            password: !0,
            image: !0
        }) S.pseudos[b] = a(b);
        for (b in {
            submit: !0,
            reset: !0
        }) S.pseudos[b] = l(b);
        return d.prototype = S.filters = S.pseudos, S.setFilters = new d, k = e.tokenize = function(t, n) {
            var i, o, r, s, a, l, c, u = q[t + " "];
            if (u) return n ? 0 : u.slice(0);
            for (a = t, l = [], c = S.preFilter; a;) {
                (!i || (o = ct.exec(a))) && (o && (a = a.slice(o[0].length) || a), l.push(r = [])), i = !1, (o = ut.exec(a)) && (i = o.shift(), r.push({
                    value: i,
                    type: o[0].replace(lt, " ")
                }), a = a.slice(i.length));
                for (s in S.filter)!(o = pt[s].exec(a)) || c[s] && !(o = c[s](o)) || (i = o.shift(), r.push({
                    value: i,
                    type: s,
                    matches: o
                }), a = a.slice(i.length));
                if (!i) break
            }
            return n ? a.length : a ? e.error(t) : q(t, l).slice(0)
        }, E = e.compile = function(t, e) {
            var n, i = [],
                o = [],
                r = B[t + " "];
            if (!r) {
                for (e || (e = k(t)), n = e.length; n--;) r = y(e[n]), r[F] ? i.push(r) : o.push(r);
                r = B(t, w(o, i)), r.selector = t
            }
            return r
        }, D = e.select = function(t, e, n, i) {
            var o, r, s, a, l, c = "function" == typeof t && t,
                d = !i && k(t = c.selector || t);
            if (n = n || [], 1 === d.length) {
                if (r = d[0] = d[0].slice(0), r.length > 2 && "ID" === (s = r[0]).type && x.getById && 9 === e.nodeType && L && S.relative[r[1].type]) {
                    if (e = (S.find.ID(s.matches[0].replace(xt, St), e) || [])[0], !e) return n;
                    c && (e = e.parentNode), t = t.slice(r.shift().value.length)
                }
                for (o = pt.needsContext.test(t) ? 0 : r.length; o-- && (s = r[o], !S.relative[a = s.type]);)
                    if ((l = S.find[a]) && (i = l(s.matches[0].replace(xt, St), wt.test(r[0].type) && u(e.parentNode) || e))) {
                        if (r.splice(o, 1), t = i.length && h(r), !t) return K.apply(n, i), n;
                        break
                    }
            }
            return (c || E(t, d))(i, e, !L, n, wt.test(t) && u(e.parentNode) || e), n
        }, x.sortStable = F.split("").sort(G).join("") === F, x.detectDuplicates = !!R, H(), x.sortDetached = o(function(t) {
            return 1 & t.compareDocumentPosition(z.createElement("div"))
        }), o(function(t) {
            return t.innerHTML = "<a href='#'></a>", "#" === t.firstChild.getAttribute("href")
        }) || r("type|href|height|width", function(t, e, n) {
            return n ? void 0 : t.getAttribute(e, "type" === e.toLowerCase() ? 1 : 2)
        }), x.attributes && o(function(t) {
            return t.innerHTML = "<input/>", t.firstChild.setAttribute("value", ""), "" === t.firstChild.getAttribute("value")
        }) || r("value", function(t, e, n) {
            return n || "input" !== t.nodeName.toLowerCase() ? void 0 : t.defaultValue
        }), o(function(t) {
            return null == t.getAttribute("disabled")
        }) || r(et, function(t, e, n) {
            var i;
            return n ? void 0 : t[e] === !0 ? e.toLowerCase() : (i = t.getAttributeNode(e)) && i.specified ? i.value : null
        }), e
    }(t);
    J.find = ot, J.expr = ot.selectors, J.expr[":"] = J.expr.pseudos, J.unique = ot.uniqueSort, J.text = ot.getText, J.isXMLDoc = ot.isXML, J.contains = ot.contains;
    var rt = J.expr.match.needsContext,
        st = /^<(\w+)\s*\/?>(?:<\/\1>|)$/,
        at = /^.[^:#\[\.,]*$/;
    J.filter = function(t, e, n) {
        var i = e[0];
        return n && (t = ":not(" + t + ")"), 1 === e.length && 1 === i.nodeType ? J.find.matchesSelector(i, t) ? [i] : [] : J.find.matches(t, J.grep(e, function(t) {
            return 1 === t.nodeType
        }))
    }, J.fn.extend({
        find: function(t) {
            var e, n = this.length,
                i = [],
                o = this;

            if ("string" != typeof t) return this.pushStack(J(t).filter(function() {
                for (e = 0; n > e; e++)
                    if (J.contains(o[e], this)) return !0
            }));
            for (e = 0; n > e; e++) J.find(t, o[e], i);
            return i = this.pushStack(n > 1 ? J.unique(i) : i), i.selector = this.selector ? this.selector + " " + t : t, i
        },
        filter: function(t) {
            return this.pushStack(i(this, t || [], !1))
        },
        not: function(t) {
            return this.pushStack(i(this, t || [], !0))
        },
        is: function(t) {
            return !!i(this, "string" == typeof t && rt.test(t) ? J(t) : t || [], !1).length
        }
    });
    var lt, ct = /^(?:\s*(<[\w\W]+>)[^>]*|#([\w-]*))$/,
        ut = J.fn.init = function(t, e) {
            var n, i;
            if (!t) return this;
            if ("string" == typeof t) {
                if (n = "<" === t[0] && ">" === t[t.length - 1] && t.length >= 3 ? [null, t, null] : ct.exec(t), !n || !n[1] && e) return !e || e.jquery ? (e || lt).find(t) : this.constructor(e).find(t);
                if (n[1]) {
                    if (e = e instanceof J ? e[0] : e, J.merge(this, J.parseHTML(n[1], e && e.nodeType ? e.ownerDocument || e : Q, !0)), st.test(n[1]) && J.isPlainObject(e))
                        for (n in e) J.isFunction(this[n]) ? this[n](e[n]) : this.attr(n, e[n]);
                    return this
                }
                return i = Q.getElementById(n[2]), i && i.parentNode && (this.length = 1, this[0] = i), this.context = Q, this.selector = t, this
            }
            return t.nodeType ? (this.context = this[0] = t, this.length = 1, this) : J.isFunction(t) ? "undefined" != typeof lt.ready ? lt.ready(t) : t(J) : (void 0 !== t.selector && (this.selector = t.selector, this.context = t.context), J.makeArray(t, this))
        };
    ut.prototype = J.fn, lt = J(Q);
    var dt = /^(?:parents|prev(?:Until|All))/,
        ht = {
            children: !0,
            contents: !0,
            next: !0,
            prev: !0
        };
    J.extend({
        dir: function(t, e, n) {
            for (var i = [], o = void 0 !== n;
                (t = t[e]) && 9 !== t.nodeType;)
                if (1 === t.nodeType) {
                    if (o && J(t).is(n)) break;
                    i.push(t)
                }
            return i
        },
        sibling: function(t, e) {
            for (var n = []; t; t = t.nextSibling) 1 === t.nodeType && t !== e && n.push(t);
            return n
        }
    }), J.fn.extend({
        has: function(t) {
            var e = J(t, this),
                n = e.length;
            return this.filter(function() {
                for (var t = 0; n > t; t++)
                    if (J.contains(this, e[t])) return !0
            })
        },
        closest: function(t, e) {
            for (var n, i = 0, o = this.length, r = [], s = rt.test(t) || "string" != typeof t ? J(t, e || this.context) : 0; o > i; i++)
                for (n = this[i]; n && n !== e; n = n.parentNode)
                    if (n.nodeType < 11 && (s ? s.index(n) > -1 : 1 === n.nodeType && J.find.matchesSelector(n, t))) {
                        r.push(n);
                        break
                    }
            return this.pushStack(r.length > 1 ? J.unique(r) : r)
        },
        index: function(t) {
            return t ? "string" == typeof t ? G.call(J(t), this[0]) : G.call(this, t.jquery ? t[0] : t) : this[0] && this[0].parentNode ? this.first().prevAll().length : -1
        },
        add: function(t, e) {
            return this.pushStack(J.unique(J.merge(this.get(), J(t, e))))
        },
        addBack: function(t) {
            return this.add(null == t ? this.prevObject : this.prevObject.filter(t))
        }
    }), J.each({
        parent: function(t) {
            var e = t.parentNode;
            return e && 11 !== e.nodeType ? e : null
        },
        parents: function(t) {
            return J.dir(t, "parentNode")
        },
        parentsUntil: function(t, e, n) {
            return J.dir(t, "parentNode", n)
        },
        next: function(t) {
            return o(t, "nextSibling")
        },
        prev: function(t) {
            return o(t, "previousSibling")
        },
        nextAll: function(t) {
            return J.dir(t, "nextSibling")
        },
        prevAll: function(t) {
            return J.dir(t, "previousSibling")
        },
        nextUntil: function(t, e, n) {
            return J.dir(t, "nextSibling", n)
        },
        prevUntil: function(t, e, n) {
            return J.dir(t, "previousSibling", n)
        },
        siblings: function(t) {
            return J.sibling((t.parentNode || {}).firstChild, t)
        },
        children: function(t) {
            return J.sibling(t.firstChild)
        },
        contents: function(t) {
            return t.contentDocument || J.merge([], t.childNodes)
        }
    }, function(t, e) {
        J.fn[t] = function(n, i) {
            var o = J.map(this, e, n);
            return "Until" !== t.slice(-5) && (i = n), i && "string" == typeof i && (o = J.filter(i, o)), this.length > 1 && (ht[t] || J.unique(o), dt.test(t) && o.reverse()), this.pushStack(o)
        }
    });
    var ft = /\S+/g,
        pt = {};
    J.Callbacks = function(t) {
        t = "string" == typeof t ? pt[t] || r(t) : J.extend({}, t);
        var e, n, i, o, s, a, l = [],
            c = !t.once && [],
            u = function(r) {
                for (e = t.memory && r, n = !0, a = o || 0, o = 0, s = l.length, i = !0; l && s > a; a++)
                    if (l[a].apply(r[0], r[1]) === !1 && t.stopOnFalse) {
                        e = !1;
                        break
                    }
                i = !1, l && (c ? c.length && u(c.shift()) : e ? l = [] : d.disable())
            },
            d = {
                add: function() {
                    if (l) {
                        var n = l.length;
                        ! function r(e) {
                            J.each(e, function(e, n) {
                                var i = J.type(n);
                                "function" === i ? t.unique && d.has(n) || l.push(n) : n && n.length && "string" !== i && r(n)
                            })
                        }(arguments), i ? s = l.length : e && (o = n, u(e))
                    }
                    return this
                },
                remove: function() {
                    return l && J.each(arguments, function(t, e) {
                        for (var n;
                            (n = J.inArray(e, l, n)) > -1;) l.splice(n, 1), i && (s >= n && s--, a >= n && a--)
                    }), this
                },
                has: function(t) {
                    return t ? J.inArray(t, l) > -1 : !(!l || !l.length)
                },
                empty: function() {
                    return l = [], s = 0, this
                },
                disable: function() {
                    return l = c = e = void 0, this
                },
                disabled: function() {
                    return !l
                },
                lock: function() {
                    return c = void 0, e || d.disable(), this
                },
                locked: function() {
                    return !c
                },
                fireWith: function(t, e) {
                    return !l || n && !c || (e = e || [], e = [t, e.slice ? e.slice() : e], i ? c.push(e) : u(e)), this
                },
                fire: function() {
                    return d.fireWith(this, arguments), this
                },
                fired: function() {
                    return !!n
                }
            };
        return d
    }, J.extend({
        Deferred: function(t) {
            var e = [
                    ["resolve", "done", J.Callbacks("once memory"), "resolved"],
                    ["reject", "fail", J.Callbacks("once memory"), "rejected"],
                    ["notify", "progress", J.Callbacks("memory")]
                ],
                n = "pending",
                i = {
                    state: function() {
                        return n
                    },
                    always: function() {
                        return o.done(arguments).fail(arguments), this
                    },
                    then: function() {
                        var t = arguments;
                        return J.Deferred(function(n) {
                            J.each(e, function(e, r) {
                                var s = J.isFunction(t[e]) && t[e];
                                o[r[1]](function() {
                                    var t = s && s.apply(this, arguments);
                                    t && J.isFunction(t.promise) ? t.promise().done(n.resolve).fail(n.reject).progress(n.notify) : n[r[0] + "With"](this === i ? n.promise() : this, s ? [t] : arguments)
                                })
                            }), t = null
                        }).promise()
                    },
                    promise: function(t) {
                        return null != t ? J.extend(t, i) : i
                    }
                },
                o = {};
            return i.pipe = i.then, J.each(e, function(t, r) {
                var s = r[2],
                    a = r[3];
                i[r[1]] = s.add, a && s.add(function() {
                    n = a
                }, e[1 ^ t][2].disable, e[2][2].lock), o[r[0]] = function() {
                    return o[r[0] + "With"](this === o ? i : this, arguments), this
                }, o[r[0] + "With"] = s.fireWith
            }), i.promise(o), t && t.call(o, o), o
        },
        when: function(t) {
            var e, n, i, o = 0,
                r = $.call(arguments),
                s = r.length,
                a = 1 !== s || t && J.isFunction(t.promise) ? s : 0,
                l = 1 === a ? t : J.Deferred(),
                c = function(t, n, i) {
                    return function(o) {
                        n[t] = this, i[t] = arguments.length > 1 ? $.call(arguments) : o, i === e ? l.notifyWith(n, i) : --a || l.resolveWith(n, i)
                    }
                };
            if (s > 1)
                for (e = new Array(s), n = new Array(s), i = new Array(s); s > o; o++) r[o] && J.isFunction(r[o].promise) ? r[o].promise().done(c(o, i, r)).fail(l.reject).progress(c(o, n, e)) : --a;
            return a || l.resolveWith(i, r), l.promise()
        }
    });
    var gt;
    J.fn.ready = function(t) {
        return J.ready.promise().done(t), this
    }, J.extend({
        isReady: !1,
        readyWait: 1,
        holdReady: function(t) {
            t ? J.readyWait++ : J.ready(!0)
        },
        ready: function(t) {
            (t === !0 ? --J.readyWait : J.isReady) || (J.isReady = !0, t !== !0 && --J.readyWait > 0 || (gt.resolveWith(Q, [J]), J.fn.triggerHandler && (J(Q).triggerHandler("ready"), J(Q).off("ready"))))
        }
    }), J.ready.promise = function(e) {
        return gt || (gt = J.Deferred(), "complete" === Q.readyState ? setTimeout(J.ready) : (Q.addEventListener("DOMContentLoaded", s, !1), t.addEventListener("load", s, !1))), gt.promise(e)
    }, J.ready.promise();
    var mt = J.access = function(t, e, n, i, o, r, s) {
        var a = 0,
            l = t.length,
            c = null == n;
        if ("object" === J.type(n)) {
            o = !0;
            for (a in n) J.access(t, e, a, n[a], !0, r, s)
        } else if (void 0 !== i && (o = !0, J.isFunction(i) || (s = !0), c && (s ? (e.call(t, i), e = null) : (c = e, e = function(t, e, n) {
            return c.call(J(t), n)
        })), e))
            for (; l > a; a++) e(t[a], n, s ? i : i.call(t[a], a, e(t[a], n)));
        return o ? t : c ? e.call(t) : l ? e(t[0], n) : r
    };
    J.acceptData = function(t) {
        return 1 === t.nodeType || 9 === t.nodeType || !+t.nodeType
    }, a.uid = 1, a.accepts = J.acceptData, a.prototype = {
        key: function(t) {
            if (!a.accepts(t)) return 0;
            var e = {},
                n = t[this.expando];
            if (!n) {
                n = a.uid++;
                try {
                    e[this.expando] = {
                        value: n
                    }, Object.defineProperties(t, e)
                } catch (i) {
                    e[this.expando] = n, J.extend(t, e)
                }
            }
            return this.cache[n] || (this.cache[n] = {}), n
        },
        set: function(t, e, n) {
            var i, o = this.key(t),
                r = this.cache[o];
            if ("string" == typeof e) r[e] = n;
            else if (J.isEmptyObject(r)) J.extend(this.cache[o], e);
            else
                for (i in e) r[i] = e[i];
            return r
        },
        get: function(t, e) {
            var n = this.cache[this.key(t)];
            return void 0 === e ? n : n[e]
        },
        access: function(t, e, n) {
            var i;
            return void 0 === e || e && "string" == typeof e && void 0 === n ? (i = this.get(t, e), void 0 !== i ? i : this.get(t, J.camelCase(e))) : (this.set(t, e, n), void 0 !== n ? n : e)
        },
        remove: function(t, e) {
            var n, i, o, r = this.key(t),
                s = this.cache[r];
            if (void 0 === e) this.cache[r] = {};
            else {
                J.isArray(e) ? i = e.concat(e.map(J.camelCase)) : (o = J.camelCase(e), e in s ? i = [e, o] : (i = o, i = i in s ? [i] : i.match(ft) || [])), n = i.length;
                for (; n--;) delete s[i[n]]
            }
        },
        hasData: function(t) {
            return !J.isEmptyObject(this.cache[t[this.expando]] || {})
        },
        discard: function(t) {
            t[this.expando] && delete this.cache[t[this.expando]]
        }
    };
    var vt = new a,
        yt = new a,
        wt = /^(?:\{[\w\W]*\}|\[[\w\W]*\])$/,
        bt = /([A-Z])/g;
    J.extend({
        hasData: function(t) {
            return yt.hasData(t) || vt.hasData(t)
        },
        data: function(t, e, n) {
            return yt.access(t, e, n)
        },
        removeData: function(t, e) {
            yt.remove(t, e)
        },
        _data: function(t, e, n) {
            return vt.access(t, e, n)
        },
        _removeData: function(t, e) {
            vt.remove(t, e)
        }
    }), J.fn.extend({
        data: function(t, e) {
            var n, i, o, r = this[0],
                s = r && r.attributes;
            if (void 0 === t) {
                if (this.length && (o = yt.get(r), 1 === r.nodeType && !vt.get(r, "hasDataAttrs"))) {
                    for (n = s.length; n--;) s[n] && (i = s[n].name, 0 === i.indexOf("data-") && (i = J.camelCase(i.slice(5)), l(r, i, o[i])));
                    vt.set(r, "hasDataAttrs", !0)
                }
                return o
            }
            return "object" == typeof t ? this.each(function() {
                yt.set(this, t)
            }) : mt(this, function(e) {
                var n, i = J.camelCase(t);
                if (r && void 0 === e) {
                    if (n = yt.get(r, t), void 0 !== n) return n;
                    if (n = yt.get(r, i), void 0 !== n) return n;
                    if (n = l(r, i, void 0), void 0 !== n) return n
                } else this.each(function() {
                    var n = yt.get(this, i);
                    yt.set(this, i, e), -1 !== t.indexOf("-") && void 0 !== n && yt.set(this, t, e)
                })
            }, null, e, arguments.length > 1, null, !0)
        },
        removeData: function(t) {
            return this.each(function() {
                yt.remove(this, t)
            })
        }
    }), J.extend({
        queue: function(t, e, n) {
            var i;
            return t ? (e = (e || "fx") + "queue", i = vt.get(t, e), n && (!i || J.isArray(n) ? i = vt.access(t, e, J.makeArray(n)) : i.push(n)), i || []) : void 0
        },
        dequeue: function(t, e) {
            e = e || "fx";
            var n = J.queue(t, e),
                i = n.length,
                o = n.shift(),
                r = J._queueHooks(t, e),
                s = function() {
                    J.dequeue(t, e)
                };
            "inprogress" === o && (o = n.shift(), i--), o && ("fx" === e && n.unshift("inprogress"), delete r.stop, o.call(t, s, r)), !i && r && r.empty.fire()
        },
        _queueHooks: function(t, e) {
            var n = e + "queueHooks";
            return vt.get(t, n) || vt.access(t, n, {
                empty: J.Callbacks("once memory").add(function() {
                    vt.remove(t, [e + "queue", n])
                })
            })
        }
    }), J.fn.extend({
        queue: function(t, e) {
            var n = 2;
            return "string" != typeof t && (e = t, t = "fx", n--), arguments.length < n ? J.queue(this[0], t) : void 0 === e ? this : this.each(function() {
                var n = J.queue(this, t, e);
                J._queueHooks(this, t), "fx" === t && "inprogress" !== n[0] && J.dequeue(this, t)
            })
        },
        dequeue: function(t) {
            return this.each(function() {
                J.dequeue(this, t)
            })
        },
        clearQueue: function(t) {
            return this.queue(t || "fx", [])
        },
        promise: function(t, e) {
            var n, i = 1,
                o = J.Deferred(),
                r = this,
                s = this.length,
                a = function() {
                    --i || o.resolveWith(r, [r])
                };
            for ("string" != typeof t && (e = t, t = void 0), t = t || "fx"; s--;) n = vt.get(r[s], t + "queueHooks"), n && n.empty && (i++, n.empty.add(a));
            return a(), o.promise(e)
        }
    });
    var xt = /[+-]?(?:\d*\.|)\d+(?:[eE][+-]?\d+|)/.source,
        St = ["Top", "Right", "Bottom", "Left"],
        Tt = function(t, e) {
            return t = e || t, "none" === J.css(t, "display") || !J.contains(t.ownerDocument, t)
        },
        Ct = /^(?:checkbox|radio)$/i;
    ! function() {
        var t = Q.createDocumentFragment(),
            e = t.appendChild(Q.createElement("div")),
            n = Q.createElement("input");
        n.setAttribute("type", "radio"), n.setAttribute("checked", "checked"), n.setAttribute("name", "t"), e.appendChild(n), Z.checkClone = e.cloneNode(!0).cloneNode(!0).lastChild.checked, e.innerHTML = "<textarea>x</textarea>", Z.noCloneChecked = !!e.cloneNode(!0).lastChild.defaultValue
    }();
    var kt = "undefined";
    Z.focusinBubbles = "onfocusin" in t;
    var Et = /^key/,
        Dt = /^(?:mouse|pointer|contextmenu)|click/,
        Mt = /^(?:focusinfocus|focusoutblur)$/,
        _t = /^([^.]*)(?:\.(.+)|)$/;
    J.event = {
        global: {},
        add: function(t, e, n, i, o) {
            var r, s, a, l, c, u, d, h, f, p, g, m = vt.get(t);
            if (m)
                for (n.handler && (r = n, n = r.handler, o = r.selector), n.guid || (n.guid = J.guid++), (l = m.events) || (l = m.events = {}), (s = m.handle) || (s = m.handle = function(e) {
                    return typeof J !== kt && J.event.triggered !== e.type ? J.event.dispatch.apply(t, arguments) : void 0
                }), e = (e || "").match(ft) || [""], c = e.length; c--;) a = _t.exec(e[c]) || [], f = g = a[1], p = (a[2] || "").split(".").sort(), f && (d = J.event.special[f] || {}, f = (o ? d.delegateType : d.bindType) || f, d = J.event.special[f] || {}, u = J.extend({
                    type: f,
                    origType: g,
                    data: i,
                    handler: n,
                    guid: n.guid,
                    selector: o,
                    needsContext: o && J.expr.match.needsContext.test(o),
                    namespace: p.join(".")
                }, r), (h = l[f]) || (h = l[f] = [], h.delegateCount = 0, d.setup && d.setup.call(t, i, p, s) !== !1 || t.addEventListener && t.addEventListener(f, s, !1)), d.add && (d.add.call(t, u), u.handler.guid || (u.handler.guid = n.guid)), o ? h.splice(h.delegateCount++, 0, u) : h.push(u), J.event.global[f] = !0)
        },
        remove: function(t, e, n, i, o) {
            var r, s, a, l, c, u, d, h, f, p, g, m = vt.hasData(t) && vt.get(t);
            if (m && (l = m.events)) {
                for (e = (e || "").match(ft) || [""], c = e.length; c--;)
                    if (a = _t.exec(e[c]) || [], f = g = a[1], p = (a[2] || "").split(".").sort(), f) {
                        for (d = J.event.special[f] || {}, f = (i ? d.delegateType : d.bindType) || f, h = l[f] || [], a = a[2] && new RegExp("(^|\\.)" + p.join("\\.(?:.*\\.|)") + "(\\.|$)"), s = r = h.length; r--;) u = h[r], !o && g !== u.origType || n && n.guid !== u.guid || a && !a.test(u.namespace) || i && i !== u.selector && ("**" !== i || !u.selector) || (h.splice(r, 1), u.selector && h.delegateCount--, d.remove && d.remove.call(t, u));
                        s && !h.length && (d.teardown && d.teardown.call(t, p, m.handle) !== !1 || J.removeEvent(t, f, m.handle), delete l[f])
                    } else
                        for (f in l) J.event.remove(t, f + e[c], n, i, !0);
                J.isEmptyObject(l) && (delete m.handle, vt.remove(t, "events"))
            }
        },
        trigger: function(e, n, i, o) {
            var r, s, a, l, c, u, d, h = [i || Q],
                f = X.call(e, "type") ? e.type : e,
                p = X.call(e, "namespace") ? e.namespace.split(".") : [];
            if (s = a = i = i || Q, 3 !== i.nodeType && 8 !== i.nodeType && !Mt.test(f + J.event.triggered) && (f.indexOf(".") >= 0 && (p = f.split("."), f = p.shift(), p.sort()), c = f.indexOf(":") < 0 && "on" + f, e = e[J.expando] ? e : new J.Event(f, "object" == typeof e && e), e.isTrigger = o ? 2 : 3, e.namespace = p.join("."), e.namespace_re = e.namespace ? new RegExp("(^|\\.)" + p.join("\\.(?:.*\\.|)") + "(\\.|$)") : null, e.result = void 0, e.target || (e.target = i), n = null == n ? [e] : J.makeArray(n, [e]), d = J.event.special[f] || {}, o || !d.trigger || d.trigger.apply(i, n) !== !1)) {
                if (!o && !d.noBubble && !J.isWindow(i)) {
                    for (l = d.delegateType || f, Mt.test(l + f) || (s = s.parentNode); s; s = s.parentNode) h.push(s), a = s;
                    a === (i.ownerDocument || Q) && h.push(a.defaultView || a.parentWindow || t)
                }
                for (r = 0;
                    (s = h[r++]) && !e.isPropagationStopped();) e.type = r > 1 ? l : d.bindType || f, u = (vt.get(s, "events") || {})[e.type] && vt.get(s, "handle"), u && u.apply(s, n), u = c && s[c], u && u.apply && J.acceptData(s) && (e.result = u.apply(s, n), e.result === !1 && e.preventDefault());
                return e.type = f, o || e.isDefaultPrevented() || d._default && d._default.apply(h.pop(), n) !== !1 || !J.acceptData(i) || c && J.isFunction(i[f]) && !J.isWindow(i) && (a = i[c], a && (i[c] = null), J.event.triggered = f, i[f](), J.event.triggered = void 0, a && (i[c] = a)), e.result
            }
        },
        dispatch: function(t) {
            t = J.event.fix(t);
            var e, n, i, o, r, s = [],
                a = $.call(arguments),
                l = (vt.get(this, "events") || {})[t.type] || [],
                c = J.event.special[t.type] || {};
            if (a[0] = t, t.delegateTarget = this, !c.preDispatch || c.preDispatch.call(this, t) !== !1) {
                for (s = J.event.handlers.call(this, t, l), e = 0;
                    (o = s[e++]) && !t.isPropagationStopped();)
                    for (t.currentTarget = o.elem, n = 0;
                        (r = o.handlers[n++]) && !t.isImmediatePropagationStopped();)(!t.namespace_re || t.namespace_re.test(r.namespace)) && (t.handleObj = r, t.data = r.data, i = ((J.event.special[r.origType] || {}).handle || r.handler).apply(o.elem, a), void 0 !== i && (t.result = i) === !1 && (t.preventDefault(), t.stopPropagation()));
                return c.postDispatch && c.postDispatch.call(this, t), t.result
            }
        },
        handlers: function(t, e) {
            var n, i, o, r, s = [],
                a = e.delegateCount,
                l = t.target;
            if (a && l.nodeType && (!t.button || "click" !== t.type))
                for (; l !== this; l = l.parentNode || this)
                    if (l.disabled !== !0 || "click" !== t.type) {
                        for (i = [], n = 0; a > n; n++) r = e[n], o = r.selector + " ", void 0 === i[o] && (i[o] = r.needsContext ? J(o, this).index(l) >= 0 : J.find(o, this, null, [l]).length), i[o] && i.push(r);
                        i.length && s.push({
                            elem: l,
                            handlers: i
                        })
                    }
            return a < e.length && s.push({
                elem: this,
                handlers: e.slice(a)
            }), s
        },
        props: "altKey bubbles cancelable ctrlKey currentTarget eventPhase metaKey relatedTarget shiftKey target timeStamp view which".split(" "),
        fixHooks: {},
        keyHooks: {
            props: "char charCode key keyCode".split(" "),
            filter: function(t, e) {
                return null == t.which && (t.which = null != e.charCode ? e.charCode : e.keyCode), t
            }
        },
        mouseHooks: {
            props: "button buttons clientX clientY offsetX offsetY pageX pageY screenX screenY toElement".split(" "),
            filter: function(t, e) {
                var n, i, o, r = e.button;
                return null == t.pageX && null != e.clientX && (n = t.target.ownerDocument || Q, i = n.documentElement, o = n.body, t.pageX = e.clientX + (i && i.scrollLeft || o && o.scrollLeft || 0) - (i && i.clientLeft || o && o.clientLeft || 0), t.pageY = e.clientY + (i && i.scrollTop || o && o.scrollTop || 0) - (i && i.clientTop || o && o.clientTop || 0)), t.which || void 0 === r || (t.which = 1 & r ? 1 : 2 & r ? 3 : 4 & r ? 2 : 0), t
            }
        },
        fix: function(t) {
            if (t[J.expando]) return t;
            var e, n, i, o = t.type,
                r = t,
                s = this.fixHooks[o];
            for (s || (this.fixHooks[o] = s = Dt.test(o) ? this.mouseHooks : Et.test(o) ? this.keyHooks : {}), i = s.props ? this.props.concat(s.props) : this.props, t = new J.Event(r), e = i.length; e--;) n = i[e], t[n] = r[n];
            return t.target || (t.target = Q), 3 === t.target.nodeType && (t.target = t.target.parentNode), s.filter ? s.filter(t, r) : t
        },
        special: {
            load: {
                noBubble: !0
            },
            focus: {
                trigger: function() {
                    return this !== d() && this.focus ? (this.focus(), !1) : void 0
                },
                delegateType: "focusin"
            },
            blur: {
                trigger: function() {
                    return this === d() && this.blur ? (this.blur(), !1) : void 0
                },
                delegateType: "focusout"
            },
            click: {
                trigger: function() {
                    return "checkbox" === this.type && this.click && J.nodeName(this, "input") ? (this.click(), !1) : void 0
                },
                _default: function(t) {
                    return J.nodeName(t.target, "a")
                }
            },
            beforeunload: {
                postDispatch: function(t) {
                    void 0 !== t.result && t.originalEvent && (t.originalEvent.returnValue = t.result)
                }
            }
        },
        simulate: function(t, e, n, i) {
            var o = J.extend(new J.Event, n, {
                type: t,
                isSimulated: !0,
                originalEvent: {}
            });
            i ? J.event.trigger(o, null, e) : J.event.dispatch.call(e, o), o.isDefaultPrevented() && n.preventDefault()
        }
    }, J.removeEvent = function(t, e, n) {
        t.removeEventListener && t.removeEventListener(e, n, !1)
    }, J.Event = function(t, e) {
        return this instanceof J.Event ? (t && t.type ? (this.originalEvent = t, this.type = t.type, this.isDefaultPrevented = t.defaultPrevented || void 0 === t.defaultPrevented && t.returnValue === !1 ? c : u) : this.type = t, e && J.extend(this, e), this.timeStamp = t && t.timeStamp || J.now(), void(this[J.expando] = !0)) : new J.Event(t, e)
    }, J.Event.prototype = {
        isDefaultPrevented: u,
        isPropagationStopped: u,
        isImmediatePropagationStopped: u,
        preventDefault: function() {
            var t = this.originalEvent;
            this.isDefaultPrevented = c, t && t.preventDefault && t.preventDefault()
        },
        stopPropagation: function() {
            var t = this.originalEvent;
            this.isPropagationStopped = c, t && t.stopPropagation && t.stopPropagation()
        },
        stopImmediatePropagation: function() {
            var t = this.originalEvent;
            this.isImmediatePropagationStopped = c, t && t.stopImmediatePropagation && t.stopImmediatePropagation(), this.stopPropagation()
        }
    }, J.each({
        mouseenter: "mouseover",
        mouseleave: "mouseout",
        pointerenter: "pointerover",
        pointerleave: "pointerout"
    }, function(t, e) {
        J.event.special[t] = {
            delegateType: e,
            bindType: e,
            handle: function(t) {
                var n, i = this,
                    o = t.relatedTarget,
                    r = t.handleObj;
                return (!o || o !== i && !J.contains(i, o)) && (t.type = r.origType, n = r.handler.apply(this, arguments), t.type = e), n
            }
        }
    }), Z.focusinBubbles || J.each({
        focus: "focusin",
        blur: "focusout"
    }, function(t, e) {
        var n = function(t) {
            J.event.simulate(e, t.target, J.event.fix(t), !0)
        };
        J.event.special[e] = {
            setup: function() {
                var i = this.ownerDocument || this,
                    o = vt.access(i, e);
                o || i.addEventListener(t, n, !0), vt.access(i, e, (o || 0) + 1)
            },
            teardown: function() {
                var i = this.ownerDocument || this,
                    o = vt.access(i, e) - 1;
                o ? vt.access(i, e, o) : (i.removeEventListener(t, n, !0), vt.remove(i, e))
            }
        }
    }), J.fn.extend({
        on: function(t, e, n, i, o) {
            var r, s;
            if ("object" == typeof t) {
                "string" != typeof e && (n = n || e, e = void 0);
                for (s in t) this.on(s, e, n, t[s], o);
                return this
            }
            if (null == n && null == i ? (i = e, n = e = void 0) : null == i && ("string" == typeof e ? (i = n, n = void 0) : (i = n, n = e, e = void 0)), i === !1) i = u;
            else if (!i) return this;
            return 1 === o && (r = i, i = function(t) {
                return J().off(t), r.apply(this, arguments)
            }, i.guid = r.guid || (r.guid = J.guid++)), this.each(function() {
                J.event.add(this, t, i, n, e)
            })
        },
        one: function(t, e, n, i) {
            return this.on(t, e, n, i, 1)
        },
        off: function(t, e, n) {
            var i, o;
            if (t && t.preventDefault && t.handleObj) return i = t.handleObj, J(t.delegateTarget).off(i.namespace ? i.origType + "." + i.namespace : i.origType, i.selector, i.handler), this;
            if ("object" == typeof t) {
                for (o in t) this.off(o, e, t[o]);
                return this
            }
            return (e === !1 || "function" == typeof e) && (n = e, e = void 0), n === !1 && (n = u), this.each(function() {
                J.event.remove(this, t, n, e)
            })
        },
        trigger: function(t, e) {
            return this.each(function() {
                J.event.trigger(t, e, this)
            })
        },
        triggerHandler: function(t, e) {
            var n = this[0];
            return n ? J.event.trigger(t, e, n, !0) : void 0
        }
    });
    var Rt = /<(?!area|br|col|embed|hr|img|input|link|meta|param)(([\w:]+)[^>]*)\/>/gi,
        Ht = /<([\w:]+)/,
        zt = /<|&#?\w+;/,
        At = /<(?:script|style|link)/i,
        Lt = /checked\s*(?:[^=]|=\s*.checked.)/i,
        Nt = /^$|\/(?:java|ecma)script/i,
        Ot = /^true\/(.*)/,
        It = /^\s*<!(?:\[CDATA\[|--)|(?:\]\]|--)>\s*$/g,
        Pt = {
            option: [1, "<select multiple='multiple'>", "</select>"],
            thead: [1, "<table>", "</table>"],
            col: [2, "<table><colgroup>", "</colgroup></table>"],
            tr: [2, "<table><tbody>", "</tbody></table>"],
            td: [3, "<table><tbody><tr>", "</tr></tbody></table>"],
            _default: [0, "", ""]
        };
    Pt.optgroup = Pt.option, Pt.tbody = Pt.tfoot = Pt.colgroup = Pt.caption = Pt.thead, Pt.th = Pt.td, J.extend({
        clone: function(t, e, n) {
            var i, o, r, s, a = t.cloneNode(!0),
                l = J.contains(t.ownerDocument, t);
            if (!(Z.noCloneChecked || 1 !== t.nodeType && 11 !== t.nodeType || J.isXMLDoc(t)))
                for (s = v(a), r = v(t), i = 0, o = r.length; o > i; i++) y(r[i], s[i]);
            if (e)
                if (n)
                    for (r = r || v(t), s = s || v(a), i = 0, o = r.length; o > i; i++) m(r[i], s[i]);
                else m(t, a);
            return s = v(a, "script"), s.length > 0 && g(s, !l && v(t, "script")), a
        },
        buildFragment: function(t, e, n, i) {
            for (var o, r, s, a, l, c, u = e.createDocumentFragment(), d = [], h = 0, f = t.length; f > h; h++)
                if (o = t[h], o || 0 === o)
                    if ("object" === J.type(o)) J.merge(d, o.nodeType ? [o] : o);
                    else if (zt.test(o)) {
                for (r = r || u.appendChild(e.createElement("div")), s = (Ht.exec(o) || ["", ""])[1].toLowerCase(), a = Pt[s] || Pt._default, r.innerHTML = a[1] + o.replace(Rt, "<$1></$2>") + a[2], c = a[0]; c--;) r = r.lastChild;
                J.merge(d, r.childNodes), r = u.firstChild, r.textContent = ""
            } else d.push(e.createTextNode(o));
            for (u.textContent = "", h = 0; o = d[h++];)
                if ((!i || -1 === J.inArray(o, i)) && (l = J.contains(o.ownerDocument, o), r = v(u.appendChild(o), "script"), l && g(r), n))
                    for (c = 0; o = r[c++];) Nt.test(o.type || "") && n.push(o);
            return u
        },
        cleanData: function(t) {
            for (var e, n, i, o, r = J.event.special, s = 0; void 0 !== (n = t[s]); s++) {
                if (J.acceptData(n) && (o = n[vt.expando], o && (e = vt.cache[o]))) {
                    if (e.events)
                        for (i in e.events) r[i] ? J.event.remove(n, i) : J.removeEvent(n, i, e.handle);
                    vt.cache[o] && delete vt.cache[o]
                }
                delete yt.cache[n[yt.expando]]
            }
        }
    }), J.fn.extend({
        text: function(t) {
            return mt(this, function(t) {
                return void 0 === t ? J.text(this) : this.empty().each(function() {
                    (1 === this.nodeType || 11 === this.nodeType || 9 === this.nodeType) && (this.textContent = t)
                })
            }, null, t, arguments.length)
        },
        append: function() {
            return this.domManip(arguments, function(t) {
                if (1 === this.nodeType || 11 === this.nodeType || 9 === this.nodeType) {
                    var e = h(this, t);
                    e.appendChild(t)
                }
            })
        },
        prepend: function() {
            return this.domManip(arguments, function(t) {
                if (1 === this.nodeType || 11 === this.nodeType || 9 === this.nodeType) {
                    var e = h(this, t);
                    e.insertBefore(t, e.firstChild)
                }
            })
        },
        before: function() {
            return this.domManip(arguments, function(t) {
                this.parentNode && this.parentNode.insertBefore(t, this)
            })
        },
        after: function() {
            return this.domManip(arguments, function(t) {
                this.parentNode && this.parentNode.insertBefore(t, this.nextSibling)
            })
        },
        remove: function(t, e) {
            for (var n, i = t ? J.filter(t, this) : this, o = 0; null != (n = i[o]); o++) e || 1 !== n.nodeType || J.cleanData(v(n)), n.parentNode && (e && J.contains(n.ownerDocument, n) && g(v(n, "script")), n.parentNode.removeChild(n));
            return this
        },
        empty: function() {
            for (var t, e = 0; null != (t = this[e]); e++) 1 === t.nodeType && (J.cleanData(v(t, !1)), t.textContent = "");
            return this
        },
        clone: function(t, e) {
            return t = null == t ? !1 : t, e = null == e ? t : e, this.map(function() {
                return J.clone(this, t, e)
            })
        },
        html: function(t) {
            return mt(this, function(t) {
                var e = this[0] || {},
                    n = 0,
                    i = this.length;
                if (void 0 === t && 1 === e.nodeType) return e.innerHTML;
                if ("string" == typeof t && !At.test(t) && !Pt[(Ht.exec(t) || ["", ""])[1].toLowerCase()]) {
                    t = t.replace(Rt, "<$1></$2>");
                    try {
                        for (; i > n; n++) e = this[n] || {}, 1 === e.nodeType && (J.cleanData(v(e, !1)), e.innerHTML = t);
                        e = 0
                    } catch (o) {}
                }
                e && this.empty().append(t)
            }, null, t, arguments.length)
        },
        replaceWith: function() {
            var t = arguments[0];
            return this.domManip(arguments, function(e) {
                t = this.parentNode, J.cleanData(v(this)), t && t.replaceChild(e, this)
            }), t && (t.length || t.nodeType) ? this : this.remove()
        },
        detach: function(t) {
            return this.remove(t, !0)
        },
        domManip: function(t, e) {
            t = q.apply([], t);
            var n, i, o, r, s, a, l = 0,
                c = this.length,
                u = this,
                d = c - 1,
                h = t[0],
                g = J.isFunction(h);
            if (g || c > 1 && "string" == typeof h && !Z.checkClone && Lt.test(h)) return this.each(function(n) {
                var i = u.eq(n);
                g && (t[0] = h.call(this, n, i.html())), i.domManip(t, e)
            });
            if (c && (n = J.buildFragment(t, this[0].ownerDocument, !1, this), i = n.firstChild, 1 === n.childNodes.length && (n = i), i)) {
                for (o = J.map(v(n, "script"), f), r = o.length; c > l; l++) s = n, l !== d && (s = J.clone(s, !0, !0), r && J.merge(o, v(s, "script"))), e.call(this[l], s, l);
                if (r)
                    for (a = o[o.length - 1].ownerDocument, J.map(o, p), l = 0; r > l; l++) s = o[l], Nt.test(s.type || "") && !vt.access(s, "globalEval") && J.contains(a, s) && (s.src ? J._evalUrl && J._evalUrl(s.src) : J.globalEval(s.textContent.replace(It, "")))
            }
            return this
        }
    }), J.each({
        appendTo: "append",
        prependTo: "prepend",
        insertBefore: "before",
        insertAfter: "after",
        replaceAll: "replaceWith"
    }, function(t, e) {
        J.fn[t] = function(t) {
            for (var n, i = [], o = J(t), r = o.length - 1, s = 0; r >= s; s++) n = s === r ? this : this.clone(!0), J(o[s])[e](n), B.apply(i, n.get());
            return this.pushStack(i)
        }
    });
    var Ft, Wt = {},
        jt = /^margin/,
        Yt = new RegExp("^(" + xt + ")(?!px)[a-z%]+$", "i"),
        $t = function(e) {
            return e.ownerDocument.defaultView.opener ? e.ownerDocument.defaultView.getComputedStyle(e, null) : t.getComputedStyle(e, null)
        };
    ! function() {
        function e() {
            s.style.cssText = "-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box;display:block;margin-top:1%;top:1%;border:1px;padding:1px;width:4px;position:absolute", s.innerHTML = "", o.appendChild(r);
            var e = t.getComputedStyle(s, null);
            n = "1%" !== e.top, i = "4px" === e.width, o.removeChild(r)
        }
        var n, i, o = Q.documentElement,
            r = Q.createElement("div"),
            s = Q.createElement("div");
        s.style && (s.style.backgroundClip = "content-box", s.cloneNode(!0).style.backgroundClip = "", Z.clearCloneStyle = "content-box" === s.style.backgroundClip, r.style.cssText = "border:0;width:0;height:0;top:0;left:-9999px;margin-top:1px;position:absolute", r.appendChild(s), t.getComputedStyle && J.extend(Z, {
            pixelPosition: function() {
                return e(), n
            },
            boxSizingReliable: function() {
                return null == i && e(), i
            },
            reliableMarginRight: function() {
                var e, n = s.appendChild(Q.createElement("div"));
                return n.style.cssText = s.style.cssText = "-webkit-box-sizing:content-box;-moz-box-sizing:content-box;box-sizing:content-box;display:block;margin:0;border:0;padding:0", n.style.marginRight = n.style.width = "0", s.style.width = "1px", o.appendChild(r), e = !parseFloat(t.getComputedStyle(n, null).marginRight), o.removeChild(r), s.removeChild(n), e
            }
        }))
    }(), J.swap = function(t, e, n, i) {
        var o, r, s = {};
        for (r in e) s[r] = t.style[r], t.style[r] = e[r];
        o = n.apply(t, i || []);
        for (r in e) t.style[r] = s[r];
        return o
    };
    var qt = /^(none|table(?!-c[ea]).+)/,
        Bt = new RegExp("^(" + xt + ")(.*)$", "i"),
        Gt = new RegExp("^([+-])=(" + xt + ")", "i"),
        Vt = {
            position: "absolute",
            visibility: "hidden",
            display: "block"
        },
        Ut = {
            letterSpacing: "0",
            fontWeight: "400"
        },
        Xt = ["Webkit", "O", "Moz", "ms"];
    J.extend({
        cssHooks: {
            opacity: {
                get: function(t, e) {
                    if (e) {
                        var n = x(t, "opacity");
                        return "" === n ? "1" : n
                    }
                }
            }
        },
        cssNumber: {
            columnCount: !0,
            fillOpacity: !0,
            flexGrow: !0,
            flexShrink: !0,
            fontWeight: !0,
            lineHeight: !0,
            opacity: !0,
            order: !0,
            orphans: !0,
            widows: !0,
            zIndex: !0,
            zoom: !0
        },
        cssProps: {
            "float": "cssFloat"
        },
        style: function(t, e, n, i) {
            if (t && 3 !== t.nodeType && 8 !== t.nodeType && t.style) {
                var o, r, s, a = J.camelCase(e),
                    l = t.style;
                return e = J.cssProps[a] || (J.cssProps[a] = T(l, a)), s = J.cssHooks[e] || J.cssHooks[a], void 0 === n ? s && "get" in s && void 0 !== (o = s.get(t, !1, i)) ? o : l[e] : (r = typeof n, "string" === r && (o = Gt.exec(n)) && (n = (o[1] + 1) * o[2] + parseFloat(J.css(t, e)), r = "number"), void(null != n && n === n && ("number" !== r || J.cssNumber[a] || (n += "px"), Z.clearCloneStyle || "" !== n || 0 !== e.indexOf("background") || (l[e] = "inherit"), s && "set" in s && void 0 === (n = s.set(t, n, i)) || (l[e] = n))))
            }
        },
        css: function(t, e, n, i) {
            var o, r, s, a = J.camelCase(e);
            return e = J.cssProps[a] || (J.cssProps[a] = T(t.style, a)), s = J.cssHooks[e] || J.cssHooks[a], s && "get" in s && (o = s.get(t, !0, n)), void 0 === o && (o = x(t, e, i)), "normal" === o && e in Ut && (o = Ut[e]), "" === n || n ? (r = parseFloat(o), n === !0 || J.isNumeric(r) ? r || 0 : o) : o
        }
    }), J.each(["height", "width"], function(t, e) {
        J.cssHooks[e] = {
            get: function(t, n, i) {
                return n ? qt.test(J.css(t, "display")) && 0 === t.offsetWidth ? J.swap(t, Vt, function() {
                    return E(t, e, i)
                }) : E(t, e, i) : void 0
            },
            set: function(t, n, i) {
                var o = i && $t(t);
                return C(t, n, i ? k(t, e, i, "border-box" === J.css(t, "boxSizing", !1, o), o) : 0)
            }
        }
    }), J.cssHooks.marginRight = S(Z.reliableMarginRight, function(t, e) {
        return e ? J.swap(t, {
            display: "inline-block"
        }, x, [t, "marginRight"]) : void 0
    }), J.each({
        margin: "",
        padding: "",
        border: "Width"
    }, function(t, e) {
        J.cssHooks[t + e] = {
            expand: function(n) {
                for (var i = 0, o = {}, r = "string" == typeof n ? n.split(" ") : [n]; 4 > i; i++) o[t + St[i] + e] = r[i] || r[i - 2] || r[0];
                return o
            }
        }, jt.test(t) || (J.cssHooks[t + e].set = C)
    }), J.fn.extend({
        css: function(t, e) {
            return mt(this, function(t, e, n) {
                var i, o, r = {},
                    s = 0;
                if (J.isArray(e)) {
                    for (i = $t(t), o = e.length; o > s; s++) r[e[s]] = J.css(t, e[s], !1, i);
                    return r
                }
                return void 0 !== n ? J.style(t, e, n) : J.css(t, e)
            }, t, e, arguments.length > 1)
        },
        show: function() {
            return D(this, !0)
        },
        hide: function() {
            return D(this)
        },
        toggle: function(t) {
            return "boolean" == typeof t ? t ? this.show() : this.hide() : this.each(function() {
                Tt(this) ? J(this).show() : J(this).hide()
            })
        }
    }), J.Tween = M, M.prototype = {
        constructor: M,
        init: function(t, e, n, i, o, r) {
            this.elem = t, this.prop = n, this.easing = o || "swing", this.options = e, this.start = this.now = this.cur(), this.end = i, this.unit = r || (J.cssNumber[n] ? "" : "px")
        },
        cur: function() {
            var t = M.propHooks[this.prop];
            return t && t.get ? t.get(this) : M.propHooks._default.get(this)
        },
        run: function(t) {
            var e, n = M.propHooks[this.prop];
            return this.pos = e = this.options.duration ? J.easing[this.easing](t, this.options.duration * t, 0, 1, this.options.duration) : t, this.now = (this.end - this.start) * e + this.start, this.options.step && this.options.step.call(this.elem, this.now, this), n && n.set ? n.set(this) : M.propHooks._default.set(this), this
        }
    }, M.prototype.init.prototype = M.prototype, M.propHooks = {
        _default: {
            get: function(t) {
                var e;
                return null == t.elem[t.prop] || t.elem.style && null != t.elem.style[t.prop] ? (e = J.css(t.elem, t.prop, ""), e && "auto" !== e ? e : 0) : t.elem[t.prop]
            },
            set: function(t) {
                J.fx.step[t.prop] ? J.fx.step[t.prop](t) : t.elem.style && (null != t.elem.style[J.cssProps[t.prop]] || J.cssHooks[t.prop]) ? J.style(t.elem, t.prop, t.now + t.unit) : t.elem[t.prop] = t.now
            }
        }
    }, M.propHooks.scrollTop = M.propHooks.scrollLeft = {
        set: function(t) {
            t.elem.nodeType && t.elem.parentNode && (t.elem[t.prop] = t.now)
        }
    }, J.easing = {
        linear: function(t) {
            return t
        },
        swing: function(t) {
            return .5 - Math.cos(t * Math.PI) / 2
        }
    }, J.fx = M.prototype.init, J.fx.step = {};
    var Zt, Qt, Kt = /^(?:toggle|show|hide)$/,
        Jt = new RegExp("^(?:([+-])=|)(" + xt + ")([a-z%]*)$", "i"),
        te = /queueHooks$/,
        ee = [z],
        ne = {
            "*": [
                function(t, e) {
                    var n = this.createTween(t, e),
                        i = n.cur(),
                        o = Jt.exec(e),
                        r = o && o[3] || (J.cssNumber[t] ? "" : "px"),
                        s = (J.cssNumber[t] || "px" !== r && +i) && Jt.exec(J.css(n.elem, t)),
                        a = 1,
                        l = 20;
                    if (s && s[3] !== r) {
                        r = r || s[3], o = o || [], s = +i || 1;
                        do a = a || ".5", s /= a, J.style(n.elem, t, s + r); while (a !== (a = n.cur() / i) && 1 !== a && --l)
                    }
                    return o && (s = n.start = +s || +i || 0, n.unit = r, n.end = o[1] ? s + (o[1] + 1) * o[2] : +o[2]), n
                }
            ]
        };
    J.Animation = J.extend(L, {
        tweener: function(t, e) {
            J.isFunction(t) ? (e = t, t = ["*"]) : t = t.split(" ");
            for (var n, i = 0, o = t.length; o > i; i++) n = t[i], ne[n] = ne[n] || [], ne[n].unshift(e)
        },
        prefilter: function(t, e) {
            e ? ee.unshift(t) : ee.push(t)
        }
    }), J.speed = function(t, e, n) {
        var i = t && "object" == typeof t ? J.extend({}, t) : {
            complete: n || !n && e || J.isFunction(t) && t,
            duration: t,
            easing: n && e || e && !J.isFunction(e) && e
        };
        return i.duration = J.fx.off ? 0 : "number" == typeof i.duration ? i.duration : i.duration in J.fx.speeds ? J.fx.speeds[i.duration] : J.fx.speeds._default, (null == i.queue || i.queue === !0) && (i.queue = "fx"), i.old = i.complete, i.complete = function() {
            J.isFunction(i.old) && i.old.call(this), i.queue && J.dequeue(this, i.queue)
        }, i
    }, J.fn.extend({
        fadeTo: function(t, e, n, i) {
            return this.filter(Tt).css("opacity", 0).show().end().animate({
                opacity: e
            }, t, n, i)
        },
        animate: function(t, e, n, i) {
            var o = J.isEmptyObject(t),
                r = J.speed(e, n, i),
                s = function() {
                    var e = L(this, J.extend({}, t), r);
                    (o || vt.get(this, "finish")) && e.stop(!0)
                };
            return s.finish = s, o || r.queue === !1 ? this.each(s) : this.queue(r.queue, s)
        },
        stop: function(t, e, n) {
            var i = function(t) {
                var e = t.stop;
                delete t.stop, e(n)
            };
            return "string" != typeof t && (n = e, e = t, t = void 0), e && t !== !1 && this.queue(t || "fx", []), this.each(function() {
                var e = !0,
                    o = null != t && t + "queueHooks",
                    r = J.timers,
                    s = vt.get(this);
                if (o) s[o] && s[o].stop && i(s[o]);
                else
                    for (o in s) s[o] && s[o].stop && te.test(o) && i(s[o]);
                for (o = r.length; o--;) r[o].elem !== this || null != t && r[o].queue !== t || (r[o].anim.stop(n), e = !1, r.splice(o, 1));
                (e || !n) && J.dequeue(this, t)
            })
        },
        finish: function(t) {
            return t !== !1 && (t = t || "fx"), this.each(function() {
                var e, n = vt.get(this),
                    i = n[t + "queue"],
                    o = n[t + "queueHooks"],
                    r = J.timers,
                    s = i ? i.length : 0;
                for (n.finish = !0, J.queue(this, t, []), o && o.stop && o.stop.call(this, !0),
                    e = r.length; e--;) r[e].elem === this && r[e].queue === t && (r[e].anim.stop(!0), r.splice(e, 1));
                for (e = 0; s > e; e++) i[e] && i[e].finish && i[e].finish.call(this);
                delete n.finish
            })
        }
    }), J.each(["toggle", "show", "hide"], function(t, e) {
        var n = J.fn[e];
        J.fn[e] = function(t, i, o) {
            return null == t || "boolean" == typeof t ? n.apply(this, arguments) : this.animate(R(e, !0), t, i, o)
        }
    }), J.each({
        slideDown: R("show"),
        slideUp: R("hide"),
        slideToggle: R("toggle"),
        fadeIn: {
            opacity: "show"
        },
        fadeOut: {
            opacity: "hide"
        },
        fadeToggle: {
            opacity: "toggle"
        }
    }, function(t, e) {
        J.fn[t] = function(t, n, i) {
            return this.animate(e, t, n, i)
        }
    }), J.timers = [], J.fx.tick = function() {
        var t, e = 0,
            n = J.timers;
        for (Zt = J.now(); e < n.length; e++) t = n[e], t() || n[e] !== t || n.splice(e--, 1);
        n.length || J.fx.stop(), Zt = void 0
    }, J.fx.timer = function(t) {
        J.timers.push(t), t() ? J.fx.start() : J.timers.pop()
    }, J.fx.interval = 13, J.fx.start = function() {
        Qt || (Qt = setInterval(J.fx.tick, J.fx.interval))
    }, J.fx.stop = function() {
        clearInterval(Qt), Qt = null
    }, J.fx.speeds = {
        slow: 600,
        fast: 200,
        _default: 400
    }, J.fn.delay = function(t, e) {
        return t = J.fx ? J.fx.speeds[t] || t : t, e = e || "fx", this.queue(e, function(e, n) {
            var i = setTimeout(e, t);
            n.stop = function() {
                clearTimeout(i)
            }
        })
    },
    function() {
        var t = Q.createElement("input"),
            e = Q.createElement("select"),
            n = e.appendChild(Q.createElement("option"));
        t.type = "checkbox", Z.checkOn = "" !== t.value, Z.optSelected = n.selected, e.disabled = !0, Z.optDisabled = !n.disabled, t = Q.createElement("input"), t.value = "t", t.type = "radio", Z.radioValue = "t" === t.value
    }();
    var ie, oe, re = J.expr.attrHandle;
    J.fn.extend({
        attr: function(t, e) {
            return mt(this, J.attr, t, e, arguments.length > 1)
        },
        removeAttr: function(t) {
            return this.each(function() {
                J.removeAttr(this, t)
            })
        }
    }), J.extend({
        attr: function(t, e, n) {
            var i, o, r = t.nodeType;
            return t && 3 !== r && 8 !== r && 2 !== r ? typeof t.getAttribute === kt ? J.prop(t, e, n) : (1 === r && J.isXMLDoc(t) || (e = e.toLowerCase(), i = J.attrHooks[e] || (J.expr.match.bool.test(e) ? oe : ie)), void 0 === n ? i && "get" in i && null !== (o = i.get(t, e)) ? o : (o = J.find.attr(t, e), null == o ? void 0 : o) : null !== n ? i && "set" in i && void 0 !== (o = i.set(t, n, e)) ? o : (t.setAttribute(e, n + ""), n) : void J.removeAttr(t, e)) : void 0
        },
        removeAttr: function(t, e) {
            var n, i, o = 0,
                r = e && e.match(ft);
            if (r && 1 === t.nodeType)
                for (; n = r[o++];) i = J.propFix[n] || n, J.expr.match.bool.test(n) && (t[i] = !1), t.removeAttribute(n)
        },
        attrHooks: {
            type: {
                set: function(t, e) {
                    if (!Z.radioValue && "radio" === e && J.nodeName(t, "input")) {
                        var n = t.value;
                        return t.setAttribute("type", e), n && (t.value = n), e
                    }
                }
            }
        }
    }), oe = {
        set: function(t, e, n) {
            return e === !1 ? J.removeAttr(t, n) : t.setAttribute(n, n), n
        }
    }, J.each(J.expr.match.bool.source.match(/\w+/g), function(t, e) {
        var n = re[e] || J.find.attr;
        re[e] = function(t, e, i) {
            var o, r;
            return i || (r = re[e], re[e] = o, o = null != n(t, e, i) ? e.toLowerCase() : null, re[e] = r), o
        }
    });
    var se = /^(?:input|select|textarea|button)$/i;
    J.fn.extend({
        prop: function(t, e) {
            return mt(this, J.prop, t, e, arguments.length > 1)
        },
        removeProp: function(t) {
            return this.each(function() {
                delete this[J.propFix[t] || t]
            })
        }
    }), J.extend({
        propFix: {
            "for": "htmlFor",
            "class": "className"
        },
        prop: function(t, e, n) {
            var i, o, r, s = t.nodeType;
            return t && 3 !== s && 8 !== s && 2 !== s ? (r = 1 !== s || !J.isXMLDoc(t), r && (e = J.propFix[e] || e, o = J.propHooks[e]), void 0 !== n ? o && "set" in o && void 0 !== (i = o.set(t, n, e)) ? i : t[e] = n : o && "get" in o && null !== (i = o.get(t, e)) ? i : t[e]) : void 0
        },
        propHooks: {
            tabIndex: {
                get: function(t) {
                    return t.hasAttribute("tabindex") || se.test(t.nodeName) || t.href ? t.tabIndex : -1
                }
            }
        }
    }), Z.optSelected || (J.propHooks.selected = {
        get: function(t) {
            var e = t.parentNode;
            return e && e.parentNode && e.parentNode.selectedIndex, null
        }
    }), J.each(["tabIndex", "readOnly", "maxLength", "cellSpacing", "cellPadding", "rowSpan", "colSpan", "useMap", "frameBorder", "contentEditable"], function() {
        J.propFix[this.toLowerCase()] = this
    });
    var ae = /[\t\r\n\f]/g;
    J.fn.extend({
        addClass: function(t) {
            var e, n, i, o, r, s, a = "string" == typeof t && t,
                l = 0,
                c = this.length;
            if (J.isFunction(t)) return this.each(function(e) {
                J(this).addClass(t.call(this, e, this.className))
            });
            if (a)
                for (e = (t || "").match(ft) || []; c > l; l++)
                    if (n = this[l], i = 1 === n.nodeType && (n.className ? (" " + n.className + " ").replace(ae, " ") : " ")) {
                        for (r = 0; o = e[r++];) i.indexOf(" " + o + " ") < 0 && (i += o + " ");
                        s = J.trim(i), n.className !== s && (n.className = s)
                    }
            return this
        },
        removeClass: function(t) {
            var e, n, i, o, r, s, a = 0 === arguments.length || "string" == typeof t && t,
                l = 0,
                c = this.length;
            if (J.isFunction(t)) return this.each(function(e) {
                J(this).removeClass(t.call(this, e, this.className))
            });
            if (a)
                for (e = (t || "").match(ft) || []; c > l; l++)
                    if (n = this[l], i = 1 === n.nodeType && (n.className ? (" " + n.className + " ").replace(ae, " ") : "")) {
                        for (r = 0; o = e[r++];)
                            for (; i.indexOf(" " + o + " ") >= 0;) i = i.replace(" " + o + " ", " ");
                        s = t ? J.trim(i) : "", n.className !== s && (n.className = s)
                    }
            return this
        },
        toggleClass: function(t, e) {
            var n = typeof t;
            return "boolean" == typeof e && "string" === n ? e ? this.addClass(t) : this.removeClass(t) : this.each(J.isFunction(t) ? function(n) {
                J(this).toggleClass(t.call(this, n, this.className, e), e)
            } : function() {
                if ("string" === n)
                    for (var e, i = 0, o = J(this), r = t.match(ft) || []; e = r[i++];) o.hasClass(e) ? o.removeClass(e) : o.addClass(e);
                else(n === kt || "boolean" === n) && (this.className && vt.set(this, "__className__", this.className), this.className = this.className || t === !1 ? "" : vt.get(this, "__className__") || "")
            })
        },
        hasClass: function(t) {
            for (var e = " " + t + " ", n = 0, i = this.length; i > n; n++)
                if (1 === this[n].nodeType && (" " + this[n].className + " ").replace(ae, " ").indexOf(e) >= 0) return !0;
            return !1
        }
    });
    var le = /\r/g;
    J.fn.extend({
        val: function(t) {
            var e, n, i, o = this[0];
            return arguments.length ? (i = J.isFunction(t), this.each(function(n) {
                var o;
                1 === this.nodeType && (o = i ? t.call(this, n, J(this).val()) : t, null == o ? o = "" : "number" == typeof o ? o += "" : J.isArray(o) && (o = J.map(o, function(t) {
                    return null == t ? "" : t + ""
                })), e = J.valHooks[this.type] || J.valHooks[this.nodeName.toLowerCase()], e && "set" in e && void 0 !== e.set(this, o, "value") || (this.value = o))
            })) : o ? (e = J.valHooks[o.type] || J.valHooks[o.nodeName.toLowerCase()], e && "get" in e && void 0 !== (n = e.get(o, "value")) ? n : (n = o.value, "string" == typeof n ? n.replace(le, "") : null == n ? "" : n)) : void 0
        }
    }), J.extend({
        valHooks: {
            option: {
                get: function(t) {
                    var e = J.find.attr(t, "value");
                    return null != e ? e : J.trim(J.text(t))
                }
            },
            select: {
                get: function(t) {
                    for (var e, n, i = t.options, o = t.selectedIndex, r = "select-one" === t.type || 0 > o, s = r ? null : [], a = r ? o + 1 : i.length, l = 0 > o ? a : r ? o : 0; a > l; l++)
                        if (n = i[l], !(!n.selected && l !== o || (Z.optDisabled ? n.disabled : null !== n.getAttribute("disabled")) || n.parentNode.disabled && J.nodeName(n.parentNode, "optgroup"))) {
                            if (e = J(n).val(), r) return e;
                            s.push(e)
                        }
                    return s
                },
                set: function(t, e) {
                    for (var n, i, o = t.options, r = J.makeArray(e), s = o.length; s--;) i = o[s], (i.selected = J.inArray(i.value, r) >= 0) && (n = !0);
                    return n || (t.selectedIndex = -1), r
                }
            }
        }
    }), J.each(["radio", "checkbox"], function() {
        J.valHooks[this] = {
            set: function(t, e) {
                return J.isArray(e) ? t.checked = J.inArray(J(t).val(), e) >= 0 : void 0
            }
        }, Z.checkOn || (J.valHooks[this].get = function(t) {
            return null === t.getAttribute("value") ? "on" : t.value
        })
    }), J.each("blur focus focusin focusout load resize scroll unload click dblclick mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave change select submit keydown keypress keyup error contextmenu".split(" "), function(t, e) {
        J.fn[e] = function(t, n) {
            return arguments.length > 0 ? this.on(e, null, t, n) : this.trigger(e)
        }
    }), J.fn.extend({
        hover: function(t, e) {
            return this.mouseenter(t).mouseleave(e || t)
        },
        bind: function(t, e, n) {
            return this.on(t, null, e, n)
        },
        unbind: function(t, e) {
            return this.off(t, null, e)
        },
        delegate: function(t, e, n, i) {
            return this.on(e, t, n, i)
        },
        undelegate: function(t, e, n) {
            return 1 === arguments.length ? this.off(t, "**") : this.off(e, t || "**", n)
        }
    });
    var ce = J.now(),
        ue = /\?/;
    J.parseJSON = function(t) {
        return JSON.parse(t + "")
    }, J.parseXML = function(t) {
        var e, n;
        if (!t || "string" != typeof t) return null;
        try {
            n = new DOMParser, e = n.parseFromString(t, "text/xml")
        } catch (i) {
            e = void 0
        }
        return (!e || e.getElementsByTagName("parsererror").length) && J.error("Invalid XML: " + t), e
    };
    var de = /#.*$/,
        he = /([?&])_=[^&]*/,
        fe = /^(.*?):[ \t]*([^\r\n]*)$/gm,
        pe = /^(?:about|app|app-storage|.+-extension|file|res|widget):$/,
        ge = /^(?:GET|HEAD)$/,
        me = /^\/\//,
        ve = /^([\w.+-]+:)(?:\/\/(?:[^\/?#]*@|)([^\/?#:]*)(?::(\d+)|)|)/,
        ye = {},
        we = {},
        be = "*/".concat("*"),
        xe = t.location.href,
        Se = ve.exec(xe.toLowerCase()) || [];
    J.extend({
        active: 0,
        lastModified: {},
        etag: {},
        ajaxSettings: {
            url: xe,
            type: "GET",
            isLocal: pe.test(Se[1]),
            global: !0,
            processData: !0,
            async: !0,
            contentType: "application/x-www-form-urlencoded; charset=UTF-8",
            accepts: {
                "*": be,
                text: "text/plain",
                html: "text/html",
                xml: "application/xml, text/xml",
                json: "application/json, text/javascript"
            },
            contents: {
                xml: /xml/,
                html: /html/,
                json: /json/
            },
            responseFields: {
                xml: "responseXML",
                text: "responseText",
                json: "responseJSON"
            },
            converters: {
                "* text": String,
                "text html": !0,
                "text json": J.parseJSON,
                "text xml": J.parseXML
            },
            flatOptions: {
                url: !0,
                context: !0
            }
        },
        ajaxSetup: function(t, e) {
            return e ? I(I(t, J.ajaxSettings), e) : I(J.ajaxSettings, t)
        },
        ajaxPrefilter: N(ye),
        ajaxTransport: N(we),
        ajax: function(t, e) {
            function n(t, e, n, s) {
                var l, u, v, y, b, S = e;
                2 !== w && (w = 2, a && clearTimeout(a), i = void 0, r = s || "", x.readyState = t > 0 ? 4 : 0, l = t >= 200 && 300 > t || 304 === t, n && (y = P(d, x, n)), y = F(d, y, x, l), l ? (d.ifModified && (b = x.getResponseHeader("Last-Modified"), b && (J.lastModified[o] = b), b = x.getResponseHeader("etag"), b && (J.etag[o] = b)), 204 === t || "HEAD" === d.type ? S = "nocontent" : 304 === t ? S = "notmodified" : (S = y.state, u = y.data, v = y.error, l = !v)) : (v = S, (t || !S) && (S = "error", 0 > t && (t = 0))), x.status = t, x.statusText = (e || S) + "", l ? p.resolveWith(h, [u, S, x]) : p.rejectWith(h, [x, S, v]), x.statusCode(m), m = void 0, c && f.trigger(l ? "ajaxSuccess" : "ajaxError", [x, d, l ? u : v]), g.fireWith(h, [x, S]), c && (f.trigger("ajaxComplete", [x, d]), --J.active || J.event.trigger("ajaxStop")))
            }
            "object" == typeof t && (e = t, t = void 0), e = e || {};
            var i, o, r, s, a, l, c, u, d = J.ajaxSetup({}, e),
                h = d.context || d,
                f = d.context && (h.nodeType || h.jquery) ? J(h) : J.event,
                p = J.Deferred(),
                g = J.Callbacks("once memory"),
                m = d.statusCode || {},
                v = {},
                y = {},
                w = 0,
                b = "canceled",
                x = {
                    readyState: 0,
                    getResponseHeader: function(t) {
                        var e;
                        if (2 === w) {
                            if (!s)
                                for (s = {}; e = fe.exec(r);) s[e[1].toLowerCase()] = e[2];
                            e = s[t.toLowerCase()]
                        }
                        return null == e ? null : e
                    },
                    getAllResponseHeaders: function() {
                        return 2 === w ? r : null
                    },
                    setRequestHeader: function(t, e) {
                        var n = t.toLowerCase();
                        return w || (t = y[n] = y[n] || t, v[t] = e), this
                    },
                    overrideMimeType: function(t) {
                        return w || (d.mimeType = t), this
                    },
                    statusCode: function(t) {
                        var e;
                        if (t)
                            if (2 > w)
                                for (e in t) m[e] = [m[e], t[e]];
                            else x.always(t[x.status]);
                        return this
                    },
                    abort: function(t) {
                        var e = t || b;
                        return i && i.abort(e), n(0, e), this
                    }
                };
            if (p.promise(x).complete = g.add, x.success = x.done, x.error = x.fail, d.url = ((t || d.url || xe) + "").replace(de, "").replace(me, Se[1] + "//"), d.type = e.method || e.type || d.method || d.type, d.dataTypes = J.trim(d.dataType || "*").toLowerCase().match(ft) || [""], null == d.crossDomain && (l = ve.exec(d.url.toLowerCase()), d.crossDomain = !(!l || l[1] === Se[1] && l[2] === Se[2] && (l[3] || ("http:" === l[1] ? "80" : "443")) === (Se[3] || ("http:" === Se[1] ? "80" : "443")))), d.data && d.processData && "string" != typeof d.data && (d.data = J.param(d.data, d.traditional)), O(ye, d, e, x), 2 === w) return x;
            c = J.event && d.global, c && 0 === J.active++ && J.event.trigger("ajaxStart"), d.type = d.type.toUpperCase(), d.hasContent = !ge.test(d.type), o = d.url, d.hasContent || (d.data && (o = d.url += (ue.test(o) ? "&" : "?") + d.data, delete d.data), d.cache === !1 && (d.url = he.test(o) ? o.replace(he, "$1_=" + ce++) : o + (ue.test(o) ? "&" : "?") + "_=" + ce++)), d.ifModified && (J.lastModified[o] && x.setRequestHeader("If-Modified-Since", J.lastModified[o]), J.etag[o] && x.setRequestHeader("If-None-Match", J.etag[o])), (d.data && d.hasContent && d.contentType !== !1 || e.contentType) && x.setRequestHeader("Content-Type", d.contentType), x.setRequestHeader("Accept", d.dataTypes[0] && d.accepts[d.dataTypes[0]] ? d.accepts[d.dataTypes[0]] + ("*" !== d.dataTypes[0] ? ", " + be + "; q=0.01" : "") : d.accepts["*"]);
            for (u in d.headers) x.setRequestHeader(u, d.headers[u]);
            if (d.beforeSend && (d.beforeSend.call(h, x, d) === !1 || 2 === w)) return x.abort();
            b = "abort";
            for (u in {
                success: 1,
                error: 1,
                complete: 1
            }) x[u](d[u]);
            if (i = O(we, d, e, x)) {
                x.readyState = 1, c && f.trigger("ajaxSend", [x, d]), d.async && d.timeout > 0 && (a = setTimeout(function() {
                    x.abort("timeout")
                }, d.timeout));
                try {
                    w = 1, i.send(v, n)
                } catch (S) {
                    if (!(2 > w)) throw S;
                    n(-1, S)
                }
            } else n(-1, "No Transport");
            return x
        },
        getJSON: function(t, e, n) {
            return J.get(t, e, n, "json")
        },
        getScript: function(t, e) {
            return J.get(t, void 0, e, "script")
        }
    }), J.each(["get", "post"], function(t, e) {
        J[e] = function(t, n, i, o) {
            return J.isFunction(n) && (o = o || i, i = n, n = void 0), J.ajax({
                url: t,
                type: e,
                dataType: o,
                data: n,
                success: i
            })
        }
    }), J._evalUrl = function(t) {
        return J.ajax({
            url: t,
            type: "GET",
            dataType: "script",
            async: !1,
            global: !1,
            "throws": !0
        })
    }, J.fn.extend({
        wrapAll: function(t) {
            var e;
            return J.isFunction(t) ? this.each(function(e) {
                J(this).wrapAll(t.call(this, e))
            }) : (this[0] && (e = J(t, this[0].ownerDocument).eq(0).clone(!0), this[0].parentNode && e.insertBefore(this[0]), e.map(function() {
                for (var t = this; t.firstElementChild;) t = t.firstElementChild;
                return t
            }).append(this)), this)
        },
        wrapInner: function(t) {
            return this.each(J.isFunction(t) ? function(e) {
                J(this).wrapInner(t.call(this, e))
            } : function() {
                var e = J(this),
                    n = e.contents();
                n.length ? n.wrapAll(t) : e.append(t)
            })
        },
        wrap: function(t) {
            var e = J.isFunction(t);
            return this.each(function(n) {
                J(this).wrapAll(e ? t.call(this, n) : t)
            })
        },
        unwrap: function() {
            return this.parent().each(function() {
                J.nodeName(this, "body") || J(this).replaceWith(this.childNodes)
            }).end()
        }
    }), J.expr.filters.hidden = function(t) {
        return t.offsetWidth <= 0 && t.offsetHeight <= 0
    }, J.expr.filters.visible = function(t) {
        return !J.expr.filters.hidden(t)
    };
    var Te = /%20/g,
        Ce = /\[\]$/,
        ke = /\r?\n/g,
        Ee = /^(?:submit|button|image|reset|file)$/i,
        De = /^(?:input|select|textarea|keygen)/i;
    J.param = function(t, e) {
        var n, i = [],
            o = function(t, e) {
                e = J.isFunction(e) ? e() : null == e ? "" : e, i[i.length] = encodeURIComponent(t) + "=" + encodeURIComponent(e)
            };
        if (void 0 === e && (e = J.ajaxSettings && J.ajaxSettings.traditional), J.isArray(t) || t.jquery && !J.isPlainObject(t)) J.each(t, function() {
            o(this.name, this.value)
        });
        else
            for (n in t) W(n, t[n], e, o);
        return i.join("&").replace(Te, "+")
    }, J.fn.extend({
        serialize: function() {
            return J.param(this.serializeArray())
        },
        serializeArray: function() {
            return this.map(function() {
                var t = J.prop(this, "elements");
                return t ? J.makeArray(t) : this
            }).filter(function() {
                var t = this.type;
                return this.name && !J(this).is(":disabled") && De.test(this.nodeName) && !Ee.test(t) && (this.checked || !Ct.test(t))
            }).map(function(t, e) {
                var n = J(this).val();
                return null == n ? null : J.isArray(n) ? J.map(n, function(t) {
                    return {
                        name: e.name,
                        value: t.replace(ke, "\r\n")
                    }
                }) : {
                    name: e.name,
                    value: n.replace(ke, "\r\n")
                }
            }).get()
        }
    }), J.ajaxSettings.xhr = function() {
        try {
            return new XMLHttpRequest
        } catch (t) {}
    };
    var Me = 0,
        _e = {},
        Re = {
            0: 200,
            1223: 204
        },
        He = J.ajaxSettings.xhr();
    t.attachEvent && t.attachEvent("onunload", function() {
        for (var t in _e) _e[t]()
    }), Z.cors = !!He && "withCredentials" in He, Z.ajax = He = !!He, J.ajaxTransport(function(t) {
        var e;
        return Z.cors || He && !t.crossDomain ? {
            send: function(n, i) {
                var o, r = t.xhr(),
                    s = ++Me;
                if (r.open(t.type, t.url, t.async, t.username, t.password), t.xhrFields)
                    for (o in t.xhrFields) r[o] = t.xhrFields[o];
                t.mimeType && r.overrideMimeType && r.overrideMimeType(t.mimeType), t.crossDomain || n["X-Requested-With"] || (n["X-Requested-With"] = "XMLHttpRequest");
                for (o in n) r.setRequestHeader(o, n[o]);
                e = function(t) {
                    return function() {
                        e && (delete _e[s], e = r.onload = r.onerror = null, "abort" === t ? r.abort() : "error" === t ? i(r.status, r.statusText) : i(Re[r.status] || r.status, r.statusText, "string" == typeof r.responseText ? {
                            text: r.responseText
                        } : void 0, r.getAllResponseHeaders()))
                    }
                }, r.onload = e(), r.onerror = e("error"), e = _e[s] = e("abort");
                try {
                    r.send(t.hasContent && t.data || null)
                } catch (a) {
                    if (e) throw a
                }
            },
            abort: function() {
                e && e()
            }
        } : void 0
    }), J.ajaxSetup({
        accepts: {
            script: "text/javascript, application/javascript, application/ecmascript, application/x-ecmascript"
        },
        contents: {
            script: /(?:java|ecma)script/
        },
        converters: {
            "text script": function(t) {
                return J.globalEval(t), t
            }
        }
    }), J.ajaxPrefilter("script", function(t) {
        void 0 === t.cache && (t.cache = !1), t.crossDomain && (t.type = "GET")
    }), J.ajaxTransport("script", function(t) {
        if (t.crossDomain) {
            var e, n;
            return {
                send: function(i, o) {
                    e = J("<script>").prop({
                        async: !0,
                        charset: t.scriptCharset,
                        src: t.url
                    }).on("load error", n = function(t) {
                        e.remove(), n = null, t && o("error" === t.type ? 404 : 200, t.type)
                    }), Q.head.appendChild(e[0])
                },
                abort: function() {
                    n && n()
                }
            }
        }
    });
    var ze = [],
        Ae = /(=)\?(?=&|$)|\?\?/;
    J.ajaxSetup({
        jsonp: "callback",
        jsonpCallback: function() {
            var t = ze.pop() || J.expando + "_" + ce++;
            return this[t] = !0, t
        }
    }), J.ajaxPrefilter("json jsonp", function(e, n, i) {
        var o, r, s, a = e.jsonp !== !1 && (Ae.test(e.url) ? "url" : "string" == typeof e.data && !(e.contentType || "").indexOf("application/x-www-form-urlencoded") && Ae.test(e.data) && "data");
        return a || "jsonp" === e.dataTypes[0] ? (o = e.jsonpCallback = J.isFunction(e.jsonpCallback) ? e.jsonpCallback() : e.jsonpCallback, a ? e[a] = e[a].replace(Ae, "$1" + o) : e.jsonp !== !1 && (e.url += (ue.test(e.url) ? "&" : "?") + e.jsonp + "=" + o), e.converters["script json"] = function() {
            return s || J.error(o + " was not called"), s[0]
        }, e.dataTypes[0] = "json", r = t[o], t[o] = function() {
            s = arguments
        }, i.always(function() {
            t[o] = r, e[o] && (e.jsonpCallback = n.jsonpCallback, ze.push(o)), s && J.isFunction(r) && r(s[0]), s = r = void 0
        }), "script") : void 0
    }), J.parseHTML = function(t, e, n) {
        if (!t || "string" != typeof t) return null;
        "boolean" == typeof e && (n = e, e = !1), e = e || Q;
        var i = st.exec(t),
            o = !n && [];
        return i ? [e.createElement(i[1])] : (i = J.buildFragment([t], e, o), o && o.length && J(o).remove(), J.merge([], i.childNodes))
    };
    var Le = J.fn.load;
    J.fn.load = function(t, e, n) {
        if ("string" != typeof t && Le) return Le.apply(this, arguments);
        var i, o, r, s = this,
            a = t.indexOf(" ");
        return a >= 0 && (i = J.trim(t.slice(a)), t = t.slice(0, a)), J.isFunction(e) ? (n = e, e = void 0) : e && "object" == typeof e && (o = "POST"), s.length > 0 && J.ajax({
            url: t,
            type: o,
            dataType: "html",
            data: e
        }).done(function(t) {
            r = arguments, s.html(i ? J("<div>").append(J.parseHTML(t)).find(i) : t)
        }).complete(n && function(t, e) {
            s.each(n, r || [t.responseText, e, t])
        }), this
    }, J.each(["ajaxStart", "ajaxStop", "ajaxComplete", "ajaxError", "ajaxSuccess", "ajaxSend"], function(t, e) {
        J.fn[e] = function(t) {
            return this.on(e, t)
        }
    }), J.expr.filters.animated = function(t) {
        return J.grep(J.timers, function(e) {
            return t === e.elem
        }).length
    };
    var Ne = t.document.documentElement;
    J.offset = {
        setOffset: function(t, e, n) {
            var i, o, r, s, a, l, c, u = J.css(t, "position"),
                d = J(t),
                h = {};
            "static" === u && (t.style.position = "relative"), a = d.offset(), r = J.css(t, "top"), l = J.css(t, "left"), c = ("absolute" === u || "fixed" === u) && (r + l).indexOf("auto") > -1, c ? (i = d.position(), s = i.top, o = i.left) : (s = parseFloat(r) || 0, o = parseFloat(l) || 0), J.isFunction(e) && (e = e.call(t, n, a)), null != e.top && (h.top = e.top - a.top + s), null != e.left && (h.left = e.left - a.left + o), "using" in e ? e.using.call(t, h) : d.css(h)
        }
    }, J.fn.extend({
        offset: function(t) {
            if (arguments.length) return void 0 === t ? this : this.each(function(e) {
                J.offset.setOffset(this, t, e)
            });
            var e, n, i = this[0],
                o = {
                    top: 0,
                    left: 0
                },
                r = i && i.ownerDocument;
            return r ? (e = r.documentElement, J.contains(e, i) ? (typeof i.getBoundingClientRect !== kt && (o = i.getBoundingClientRect()), n = j(r), {
                top: o.top + n.pageYOffset - e.clientTop,
                left: o.left + n.pageXOffset - e.clientLeft
            }) : o) : void 0
        },
        position: function() {
            if (this[0]) {
                var t, e, n = this[0],
                    i = {
                        top: 0,
                        left: 0
                    };
                return "fixed" === J.css(n, "position") ? e = n.getBoundingClientRect() : (t = this.offsetParent(), e = this.offset(), J.nodeName(t[0], "html") || (i = t.offset()), i.top += J.css(t[0], "borderTopWidth", !0), i.left += J.css(t[0], "borderLeftWidth", !0)), {
                    top: e.top - i.top - J.css(n, "marginTop", !0),
                    left: e.left - i.left - J.css(n, "marginLeft", !0)
                }
            }
        },
        offsetParent: function() {
            return this.map(function() {
                for (var t = this.offsetParent || Ne; t && !J.nodeName(t, "html") && "static" === J.css(t, "position");) t = t.offsetParent;
                return t || Ne
            })
        }
    }), J.each({
        scrollLeft: "pageXOffset",
        scrollTop: "pageYOffset"
    }, function(e, n) {
        var i = "pageYOffset" === n;
        J.fn[e] = function(o) {
            return mt(this, function(e, o, r) {
                var s = j(e);
                return void 0 === r ? s ? s[n] : e[o] : void(s ? s.scrollTo(i ? t.pageXOffset : r, i ? r : t.pageYOffset) : e[o] = r)
            }, e, o, arguments.length, null)
        }
    }), J.each(["top", "left"], function(t, e) {
        J.cssHooks[e] = S(Z.pixelPosition, function(t, n) {
            return n ? (n = x(t, e), Yt.test(n) ? J(t).position()[e] + "px" : n) : void 0
        })
    }), J.each({
        Height: "height",
        Width: "width"
    }, function(t, e) {
        J.each({
            padding: "inner" + t,
            content: e,
            "": "outer" + t
        }, function(n, i) {
            J.fn[i] = function(i, o) {
                var r = arguments.length && (n || "boolean" != typeof i),
                    s = n || (i === !0 || o === !0 ? "margin" : "border");
                return mt(this, function(e, n, i) {
                    var o;
                    return J.isWindow(e) ? e.document.documentElement["client" + t] : 9 === e.nodeType ? (o = e.documentElement, Math.max(e.body["scroll" + t], o["scroll" + t], e.body["offset" + t], o["offset" + t], o["client" + t])) : void 0 === i ? J.css(e, n, s) : J.style(e, n, i, s)
                }, e, r ? i : void 0, r, null)
            }
        })
    }), J.fn.size = function() {
        return this.length
    }, J.fn.andSelf = J.fn.addBack, "function" == typeof define && define.amd && define("jquery", [], function() {
        return J
    });
    var Oe = t.jQuery,
        Ie = t.$;
    return J.noConflict = function(e) {
        return t.$ === J && (t.$ = Ie), e && t.jQuery === J && (t.jQuery = Oe), J
    }, typeof e === kt && (t.jQuery = t.$ = J), J
}), "undefined" == typeof jQuery) throw new Error("Bootstrap's JavaScript requires jQuery"); + function(t) {
    "use strict";
    var e = t.fn.jquery.split(" ")[0].split(".");
    if (e[0] < 2 && e[1] < 9 || 1 == e[0] && 9 == e[1] && e[2] < 1) throw new Error("Bootstrap's JavaScript requires jQuery version 1.9.1 or higher")
}(jQuery), + function(t) {
    "use strict";

    function e() {
        var t = document.createElement("bootstrap"),
            e = {
                WebkitTransition: "webkitTransitionEnd",
                MozTransition: "transitionend",
                OTransition: "oTransitionEnd otransitionend",
                transition: "transitionend"
            };
        for (var n in e)
            if (void 0 !== t.style[n]) return {
                end: e[n]
            };
        return !1
    }
    t.fn.emulateTransitionEnd = function(e) {
        var n = !1,
            i = this;
        t(this).one("bsTransitionEnd", function() {
            n = !0
        });
        var o = function() {
            n || t(i).trigger(t.support.transition.end)
        };
        return setTimeout(o, e), this
    }, t(function() {
        t.support.transition = e(), t.support.transition && (t.event.special.bsTransitionEnd = {
            bindType: t.support.transition.end,
            delegateType: t.support.transition.end,
            handle: function(e) {
                return t(e.target).is(this) ? e.handleObj.handler.apply(this, arguments) : void 0
            }
        })
    })
}(jQuery), + function(t) {
    "use strict";

    function e(e) {
        return this.each(function() {
            var n = t(this),
                o = n.data("bs.alert");
            o || n.data("bs.alert", o = new i(this)), "string" == typeof e && o[e].call(n)
        })
    }
    var n = '[data-dismiss="alert"]',
        i = function(e) {
            t(e).on("click", n, this.close)
        };
    i.VERSION = "3.3.4", i.TRANSITION_DURATION = 150, i.prototype.close = function(e) {
        function n() {
            s.detach().trigger("closed.bs.alert").remove()
        }
        var o = t(this),
            r = o.attr("data-target");
        r || (r = o.attr("href"), r = r && r.replace(/.*(?=#[^\s]*$)/, ""));
        var s = t(r);
        e && e.preventDefault(), s.length || (s = o.closest(".alert")), s.trigger(e = t.Event("close.bs.alert")), e.isDefaultPrevented() || (s.removeClass("in"), t.support.transition && s.hasClass("fade") ? s.one("bsTransitionEnd", n).emulateTransitionEnd(i.TRANSITION_DURATION) : n())
    };
    var o = t.fn.alert;
    t.fn.alert = e, t.fn.alert.Constructor = i, t.fn.alert.noConflict = function() {
        return t.fn.alert = o, this
    }, t(document).on("click.bs.alert.data-api", n, i.prototype.close)
}(jQuery), + function(t) {
    "use strict";

    function e(e) {
        return this.each(function() {
            var i = t(this),
                o = i.data("bs.button"),
                r = "object" == typeof e && e;
            o || i.data("bs.button", o = new n(this, r)), "toggle" == e ? o.toggle() : e && o.setState(e)
        })
    }
    var n = function(e, i) {
        this.$element = t(e), this.options = t.extend({}, n.DEFAULTS, i), this.isLoading = !1
    };
    n.VERSION = "3.3.4", n.DEFAULTS = {
        loadingText: "loading..."
    }, n.prototype.setState = function(e) {
        var n = "disabled",
            i = this.$element,
            o = i.is("input") ? "val" : "html",
            r = i.data();
        e += "Text", null == r.resetText && i.data("resetText", i[o]()), setTimeout(t.proxy(function() {
            i[o](null == r[e] ? this.options[e] : r[e]), "loadingText" == e ? (this.isLoading = !0, i.addClass(n).attr(n, n)) : this.isLoading && (this.isLoading = !1, i.removeClass(n).removeAttr(n))
        }, this), 0)
    }, n.prototype.toggle = function() {
        var t = !0,
            e = this.$element.closest('[data-toggle="buttons"]');
        if (e.length) {
            var n = this.$element.find("input");
            "radio" == n.prop("type") && (n.prop("checked") && this.$element.hasClass("active") ? t = !1 : e.find(".active").removeClass("active")), t && n.prop("checked", !this.$element.hasClass("active")).trigger("change")
        } else this.$element.attr("aria-pressed", !this.$element.hasClass("active"));
        t && this.$element.toggleClass("active")
    };
    var i = t.fn.button;
    t.fn.button = e, t.fn.button.Constructor = n, t.fn.button.noConflict = function() {
        return t.fn.button = i, this
    }, t(document).on("click.bs.button.data-api", '[data-toggle^="button"]', function(n) {
        var i = t(n.target);
        i.hasClass("btn") || (i = i.closest(".btn")), e.call(i, "toggle"), n.preventDefault()
    }).on("focus.bs.button.data-api blur.bs.button.data-api", '[data-toggle^="button"]', function(e) {
        t(e.target).closest(".btn").toggleClass("focus", /^focus(in)?$/.test(e.type))
    })
}(jQuery), + function(t) {
    "use strict";

    function e(e) {
        return this.each(function() {
            var i = t(this),
                o = i.data("bs.carousel"),
                r = t.extend({}, n.DEFAULTS, i.data(), "object" == typeof e && e),
                s = "string" == typeof e ? e : r.slide;
            o || i.data("bs.carousel", o = new n(this, r)), "number" == typeof e ? o.to(e) : s ? o[s]() : r.interval && o.pause().cycle()
        })
    }
    var n = function(e, n) {
        this.$element = t(e), this.$indicators = this.$element.find(".carousel-indicators"), this.options = n, this.paused = null, this.sliding = null, this.interval = null, this.$active = null, this.$items = null, this.options.keyboard && this.$element.on("keydown.bs.carousel", t.proxy(this.keydown, this)), "hover" == this.options.pause && !("ontouchstart" in document.documentElement) && this.$element.on("mouseenter.bs.carousel", t.proxy(this.pause, this)).on("mouseleave.bs.carousel", t.proxy(this.cycle, this))
    };
    n.VERSION = "3.3.4", n.TRANSITION_DURATION = 600, n.DEFAULTS = {
        interval: 5e3,
        pause: "hover",
        wrap: !0,
        keyboard: !0
    }, n.prototype.keydown = function(t) {
        if (!/input|textarea/i.test(t.target.tagName)) {
            switch (t.which) {
                case 37:
                    this.prev();
                    break;
                case 39:
                    this.next();
                    break;
                default:
                    return
            }
            t.preventDefault()
        }
    }, n.prototype.cycle = function(e) {
        return e || (this.paused = !1), this.interval && clearInterval(this.interval), this.options.interval && !this.paused && (this.interval = setInterval(t.proxy(this.next, this), this.options.interval)), this
    }, n.prototype.getItemIndex = function(t) {
        return this.$items = t.parent().children(".item"), this.$items.index(t || this.$active)
    }, n.prototype.getItemForDirection = function(t, e) {
        var n = this.getItemIndex(e),
            i = "prev" == t && 0 === n || "next" == t && n == this.$items.length - 1;
        if (i && !this.options.wrap) return e;
        var o = "prev" == t ? -1 : 1,
            r = (n + o) % this.$items.length;
        return this.$items.eq(r)
    }, n.prototype.to = function(t) {
        var e = this,
            n = this.getItemIndex(this.$active = this.$element.find(".item.active"));
        return t > this.$items.length - 1 || 0 > t ? void 0 : this.sliding ? this.$element.one("slid.bs.carousel", function() {
            e.to(t)
        }) : n == t ? this.pause().cycle() : this.slide(t > n ? "next" : "prev", this.$items.eq(t))
    }, n.prototype.pause = function(e) {
        return e || (this.paused = !0), this.$element.find(".next, .prev").length && t.support.transition && (this.$element.trigger(t.support.transition.end), this.cycle(!0)), this.interval = clearInterval(this.interval), this
    }, n.prototype.next = function() {
        return this.sliding ? void 0 : this.slide("next")
    }, n.prototype.prev = function() {
        return this.sliding ? void 0 : this.slide("prev")
    }, n.prototype.slide = function(e, i) {
        var o = this.$element.find(".item.active"),
            r = i || this.getItemForDirection(e, o),
            s = this.interval,
            a = "next" == e ? "left" : "right",
            l = this;
        if (r.hasClass("active")) return this.sliding = !1;
        var c = r[0],
            u = t.Event("slide.bs.carousel", {
                relatedTarget: c,
                direction: a
            });
        if (this.$element.trigger(u), !u.isDefaultPrevented()) {
            if (this.sliding = !0, s && this.pause(), this.$indicators.length) {
                this.$indicators.find(".active").removeClass("active");
                var d = t(this.$indicators.children()[this.getItemIndex(r)]);
                d && d.addClass("active")
            }
            var h = t.Event("slid.bs.carousel", {
                relatedTarget: c,
                direction: a
            });
            return t.support.transition && this.$element.hasClass("slide") ? (r.addClass(e), r[0].offsetWidth, o.addClass(a), r.addClass(a), o.one("bsTransitionEnd", function() {
                r.removeClass([e, a].join(" ")).addClass("active"), o.removeClass(["active", a].join(" ")), l.sliding = !1, setTimeout(function() {
                    l.$element.trigger(h)
                }, 0)
            }).emulateTransitionEnd(n.TRANSITION_DURATION)) : (o.removeClass("active"), r.addClass("active"), this.sliding = !1, this.$element.trigger(h)), s && this.cycle(), this
        }
    };
    var i = t.fn.carousel;
    t.fn.carousel = e, t.fn.carousel.Constructor = n, t.fn.carousel.noConflict = function() {
        return t.fn.carousel = i, this
    };
    var o = function(n) {
        var i, o = t(this),
            r = t(o.attr("data-target") || (i = o.attr("href")) && i.replace(/.*(?=#[^\s]+$)/, ""));
        if (r.hasClass("carousel")) {
            var s = t.extend({}, r.data(), o.data()),
                a = o.attr("data-slide-to");
            a && (s.interval = !1), e.call(r, s), a && r.data("bs.carousel").to(a), n.preventDefault()
        }
    };
    t(document).on("click.bs.carousel.data-api", "[data-slide]", o).on("click.bs.carousel.data-api", "[data-slide-to]", o), t(window).on("load", function() {
        t('[data-ride="carousel"]').each(function() {
            var n = t(this);
            e.call(n, n.data())
        })
    })
}(jQuery), + function(t) {
    "use strict";

    function e(e) {
        var n, i = e.attr("data-target") || (n = e.attr("href")) && n.replace(/.*(?=#[^\s]+$)/, "");
        return t(i)
    }

    function n(e) {
        return this.each(function() {
            var n = t(this),
                o = n.data("bs.collapse"),
                r = t.extend({}, i.DEFAULTS, n.data(), "object" == typeof e && e);
            !o && r.toggle && /show|hide/.test(e) && (r.toggle = !1), o || n.data("bs.collapse", o = new i(this, r)), "string" == typeof e && o[e]()
        })
    }
    var i = function(e, n) {
        this.$element = t(e), this.options = t.extend({}, i.DEFAULTS, n), this.$trigger = t('[data-toggle="collapse"][href="#' + e.id + '"],[data-toggle="collapse"][data-target="#' + e.id + '"]'), this.transitioning = null, this.options.parent ? this.$parent = this.getParent() : this.addAriaAndCollapsedClass(this.$element, this.$trigger), this.options.toggle && this.toggle()
    };
    i.VERSION = "3.3.4", i.TRANSITION_DURATION = 350, i.DEFAULTS = {
        toggle: !0
    }, i.prototype.dimension = function() {
        var t = this.$element.hasClass("width");
        return t ? "width" : "height"
    }, i.prototype.show = function() {
        if (!this.transitioning && !this.$element.hasClass("in")) {
            var e, o = this.$parent && this.$parent.children(".panel").children(".in, .collapsing");
            if (!(o && o.length && (e = o.data("bs.collapse"), e && e.transitioning))) {
                var r = t.Event("show.bs.collapse");
                if (this.$element.trigger(r), !r.isDefaultPrevented()) {
                    o && o.length && (n.call(o, "hide"), e || o.data("bs.collapse", null));
                    var s = this.dimension();
                    this.$element.removeClass("collapse").addClass("collapsing")[s](0).attr("aria-expanded", !0), this.$trigger.removeClass("collapsed").attr("aria-expanded", !0), this.transitioning = 1;
                    var a = function() {
                        this.$element.removeClass("collapsing").addClass("collapse in")[s](""), this.transitioning = 0, this.$element.trigger("shown.bs.collapse")
                    };
                    if (!t.support.transition) return a.call(this);
                    var l = t.camelCase(["scroll", s].join("-"));
                    this.$element.one("bsTransitionEnd", t.proxy(a, this)).emulateTransitionEnd(i.TRANSITION_DURATION)[s](this.$element[0][l])
                }
            }
        }
    }, i.prototype.hide = function() {
        if (!this.transitioning && this.$element.hasClass("in")) {
            var e = t.Event("hide.bs.collapse");
            if (this.$element.trigger(e), !e.isDefaultPrevented()) {
                var n = this.dimension();
                this.$element[n](this.$element[n]())[0].offsetHeight, this.$element.addClass("collapsing").removeClass("collapse in").attr("aria-expanded", !1), this.$trigger.addClass("collapsed").attr("aria-expanded", !1), this.transitioning = 1;
                var o = function() {
                    this.transitioning = 0, this.$element.removeClass("collapsing").addClass("collapse").trigger("hidden.bs.collapse")
                };
                return t.support.transition ? void this.$element[n](0).one("bsTransitionEnd", t.proxy(o, this)).emulateTransitionEnd(i.TRANSITION_DURATION) : o.call(this)
            }
        }
    }, i.prototype.toggle = function() {
        this[this.$element.hasClass("in") ? "hide" : "show"]()
    }, i.prototype.getParent = function() {
        return t(this.options.parent).find('[data-toggle="collapse"][data-parent="' + this.options.parent + '"]').each(t.proxy(function(n, i) {
            var o = t(i);
            this.addAriaAndCollapsedClass(e(o), o)
        }, this)).end()
    }, i.prototype.addAriaAndCollapsedClass = function(t, e) {
        var n = t.hasClass("in");
        t.attr("aria-expanded", n), e.toggleClass("collapsed", !n).attr("aria-expanded", n)
    };
    var o = t.fn.collapse;
    t.fn.collapse = n, t.fn.collapse.Constructor = i, t.fn.collapse.noConflict = function() {
        return t.fn.collapse = o, this
    }, t(document).on("click.bs.collapse.data-api", '[data-toggle="collapse"]', function(i) {
        var o = t(this);
        o.attr("data-target") || i.preventDefault();
        var r = e(o),
            s = r.data("bs.collapse"),
            a = s ? "toggle" : o.data();
        n.call(r, a)
    })
}(jQuery), + function(t) {
    "use strict";

    function e(e) {
        e && 3 === e.which || (t(o).remove(), t(r).each(function() {
            var i = t(this),
                o = n(i),
                r = {
                    relatedTarget: this
                };
            o.hasClass("open") && (o.trigger(e = t.Event("hide.bs.dropdown", r)), e.isDefaultPrevented() || (i.attr("aria-expanded", "false"), o.removeClass("open").trigger("hidden.bs.dropdown", r)))
        }))
    }

    function n(e) {
        var n = e.attr("data-target");
        n || (n = e.attr("href"), n = n && /#[A-Za-z]/.test(n) && n.replace(/.*(?=#[^\s]*$)/, ""));
        var i = n && t(n);
        return i && i.length ? i : e.parent()
    }

    function i(e) {
        return this.each(function() {
            var n = t(this),
                i = n.data("bs.dropdown");
            i || n.data("bs.dropdown", i = new s(this)), "string" == typeof e && i[e].call(n)
        })
    }
    var o = ".dropdown-backdrop",
        r = '[data-toggle="dropdown"]',
        s = function(e) {
            t(e).on("click.bs.dropdown", this.toggle)
        };
    s.VERSION = "3.3.4", s.prototype.toggle = function(i) {
        var o = t(this);
        if (!o.is(".disabled, :disabled")) {
            var r = n(o),
                s = r.hasClass("open");
            if (e(), !s) {
                "ontouchstart" in document.documentElement && !r.closest(".navbar-nav").length && t('<div class="dropdown-backdrop"/>').insertAfter(t(this)).on("click", e);
                var a = {
                    relatedTarget: this
                };
                if (r.trigger(i = t.Event("show.bs.dropdown", a)), i.isDefaultPrevented()) return;
                o.trigger("focus").attr("aria-expanded", "true"), r.toggleClass("open").trigger("shown.bs.dropdown", a)
            }
            return !1
        }
    }, s.prototype.keydown = function(e) {
        if (/(38|40|27|32)/.test(e.which) && !/input|textarea/i.test(e.target.tagName)) {
            var i = t(this);
            if (e.preventDefault(), e.stopPropagation(), !i.is(".disabled, :disabled")) {
                var o = n(i),
                    s = o.hasClass("open");
                if (!s && 27 != e.which || s && 27 == e.which) return 27 == e.which && o.find(r).trigger("focus"), i.trigger("click");
                var a = " li:not(.disabled):visible a",
                    l = o.find('[role="menu"]' + a + ', [role="listbox"]' + a);
                if (l.length) {
                    var c = l.index(e.target);
                    38 == e.which && c > 0 && c--, 40 == e.which && c < l.length - 1 && c++, ~c || (c = 0), l.eq(c).trigger("focus")
                }
            }
        }
    };
    var a = t.fn.dropdown;
    t.fn.dropdown = i, t.fn.dropdown.Constructor = s, t.fn.dropdown.noConflict = function() {
        return t.fn.dropdown = a, this
    }, t(document).on("click.bs.dropdown.data-api", e).on("click.bs.dropdown.data-api", ".dropdown form", function(t) {
        t.stopPropagation()
    }).on("click.bs.dropdown.data-api", r, s.prototype.toggle).on("keydown.bs.dropdown.data-api", r, s.prototype.keydown).on("keydown.bs.dropdown.data-api", '[role="menu"]', s.prototype.keydown).on("keydown.bs.dropdown.data-api", '[role="listbox"]', s.prototype.keydown)
}(jQuery), + function(t) {
    "use strict";

    function e(e, i) {
        return this.each(function() {
            var o = t(this),
                r = o.data("bs.modal"),
                s = t.extend({}, n.DEFAULTS, o.data(), "object" == typeof e && e);
            r || o.data("bs.modal", r = new n(this, s)), "string" == typeof e ? r[e](i) : s.show && r.show(i)
        })
    }
    var n = function(e, n) {
        this.options = n, this.$body = t(document.body), this.$element = t(e), this.$dialog = this.$element.find(".modal-dialog"), this.$backdrop = null, this.isShown = null, this.originalBodyPad = null, this.scrollbarWidth = 0, this.ignoreBackdropClick = !1, this.options.remote && this.$element.find(".modal-content").load(this.options.remote, t.proxy(function() {
            this.$element.trigger("loaded.bs.modal")
        }, this))
    };
    n.VERSION = "3.3.4", n.TRANSITION_DURATION = 300, n.BACKDROP_TRANSITION_DURATION = 150, n.DEFAULTS = {
        backdrop: !0,
        keyboard: !0,
        show: !0
    }, n.prototype.toggle = function(t) {
        return this.isShown ? this.hide() : this.show(t)
    }, n.prototype.show = function(e) {
        var i = this,
            o = t.Event("show.bs.modal", {
                relatedTarget: e
            });
        this.$element.trigger(o), this.isShown || o.isDefaultPrevented() || (this.isShown = !0, this.checkScrollbar(), this.setScrollbar(), this.$body.addClass("modal-open"), this.escape(), this.resize(), this.$element.on("click.dismiss.bs.modal", '[data-dismiss="modal"]', t.proxy(this.hide, this)), this.$dialog.on("mousedown.dismiss.bs.modal", function() {
            i.$element.one("mouseup.dismiss.bs.modal", function(e) {
                t(e.target).is(i.$element) && (i.ignoreBackdropClick = !0)
            })
        }), this.backdrop(function() {
            var o = t.support.transition && i.$element.hasClass("fade");
            i.$element.parent().length || i.$element.appendTo(i.$body), i.$element.show().scrollTop(0), i.adjustDialog(), o && i.$element[0].offsetWidth, i.$element.addClass("in").attr("aria-hidden", !1), i.enforceFocus();
            var r = t.Event("shown.bs.modal", {
                relatedTarget: e
            });
            o ? i.$dialog.one("bsTransitionEnd", function() {
                i.$element.trigger("focus").trigger(r)
            }).emulateTransitionEnd(n.TRANSITION_DURATION) : i.$element.trigger("focus").trigger(r)
        }))
    }, n.prototype.hide = function(e) {
        e && e.preventDefault(), e = t.Event("hide.bs.modal"), this.$element.trigger(e), this.isShown && !e.isDefaultPrevented() && (this.isShown = !1, this.escape(), this.resize(), t(document).off("focusin.bs.modal"), this.$element.removeClass("in").attr("aria-hidden", !0).off("click.dismiss.bs.modal").off("mouseup.dismiss.bs.modal"), this.$dialog.off("mousedown.dismiss.bs.modal"), t.support.transition && this.$element.hasClass("fade") ? this.$element.one("bsTransitionEnd", t.proxy(this.hideModal, this)).emulateTransitionEnd(n.TRANSITION_DURATION) : this.hideModal())
    }, n.prototype.enforceFocus = function() {
        t(document).off("focusin.bs.modal").on("focusin.bs.modal", t.proxy(function(t) {
            this.$element[0] === t.target || this.$element.has(t.target).length || this.$element.trigger("focus")
        }, this))
    }, n.prototype.escape = function() {
        this.isShown && this.options.keyboard ? this.$element.on("keydown.dismiss.bs.modal", t.proxy(function(t) {
            27 == t.which && this.hide()
        }, this)) : this.isShown || this.$element.off("keydown.dismiss.bs.modal")
    }, n.prototype.resize = function() {
        this.isShown ? t(window).on("resize.bs.modal", t.proxy(this.handleUpdate, this)) : t(window).off("resize.bs.modal")
    }, n.prototype.hideModal = function() {
        var t = this;
        this.$element.hide(), this.backdrop(function() {
            t.$body.removeClass("modal-open"), t.resetAdjustments(), t.resetScrollbar(), t.$element.trigger("hidden.bs.modal")
        })
    }, n.prototype.removeBackdrop = function() {
        this.$backdrop && this.$backdrop.remove(), this.$backdrop = null
    }, n.prototype.backdrop = function(e) {
        var i = this,
            o = this.$element.hasClass("fade") ? "fade" : "";
        if (this.isShown && this.options.backdrop) {
            var r = t.support.transition && o;
            if (this.$backdrop = t('<div class="modal-backdrop ' + o + '" />').appendTo(this.$body), this.$element.on("click.dismiss.bs.modal", t.proxy(function(t) {
                return this.ignoreBackdropClick ? void(this.ignoreBackdropClick = !1) : void(t.target === t.currentTarget && ("static" == this.options.backdrop ? this.$element[0].focus() : this.hide()))
            }, this)), r && this.$backdrop[0].offsetWidth, this.$backdrop.addClass("in"), !e) return;
            r ? this.$backdrop.one("bsTransitionEnd", e).emulateTransitionEnd(n.BACKDROP_TRANSITION_DURATION) : e()
        } else if (!this.isShown && this.$backdrop) {
            this.$backdrop.removeClass("in");
            var s = function() {
                i.removeBackdrop(), e && e()
            };
            t.support.transition && this.$element.hasClass("fade") ? this.$backdrop.one("bsTransitionEnd", s).emulateTransitionEnd(n.BACKDROP_TRANSITION_DURATION) : s()
        } else e && e()
    }, n.prototype.handleUpdate = function() {
        this.adjustDialog()
    }, n.prototype.adjustDialog = function() {
        var t = this.$element[0].scrollHeight > document.documentElement.clientHeight;
        this.$element.css({
            paddingLeft: !this.bodyIsOverflowing && t ? this.scrollbarWidth : "",
            paddingRight: this.bodyIsOverflowing && !t ? this.scrollbarWidth : ""
        })
    }, n.prototype.resetAdjustments = function() {
        this.$element.css({
            paddingLeft: "",
            paddingRight: ""
        })
    }, n.prototype.checkScrollbar = function() {
        var t = window.innerWidth;
        if (!t) {
            var e = document.documentElement.getBoundingClientRect();
            t = e.right - Math.abs(e.left)
        }
        this.bodyIsOverflowing = document.body.clientWidth < t, this.scrollbarWidth = this.measureScrollbar()
    }, n.prototype.setScrollbar = function() {
        var t = parseInt(this.$body.css("padding-right") || 0, 10);
        this.originalBodyPad = document.body.style.paddingRight || "", this.bodyIsOverflowing && this.$body.css("padding-right", t + this.scrollbarWidth)
    }, n.prototype.resetScrollbar = function() {
        this.$body.css("padding-right", this.originalBodyPad)
    }, n.prototype.measureScrollbar = function() {
        var t = document.createElement("div");
        t.className = "modal-scrollbar-measure", this.$body.append(t);
        var e = t.offsetWidth - t.clientWidth;
        return this.$body[0].removeChild(t), e
    };
    var i = t.fn.modal;
    t.fn.modal = e, t.fn.modal.Constructor = n, t.fn.modal.noConflict = function() {
        return t.fn.modal = i, this
    }, t(document).on("click.bs.modal.data-api", '[data-toggle="modal"]', function(n) {
        var i = t(this),
            o = i.attr("href"),
            r = t(i.attr("data-target") || o && o.replace(/.*(?=#[^\s]+$)/, "")),
            s = r.data("bs.modal") ? "toggle" : t.extend({
                remote: !/#/.test(o) && o
            }, r.data(), i.data());
        i.is("a") && n.preventDefault(), r.one("show.bs.modal", function(t) {
            t.isDefaultPrevented() || r.one("hidden.bs.modal", function() {
                i.is(":visible") && i.trigger("focus")
            })
        }), e.call(r, s, this)
    })
}(jQuery), + function(t) {
    "use strict";

    function e(e) {
        return this.each(function() {
            var i = t(this),
                o = i.data("bs.tooltip"),
                r = "object" == typeof e && e;
            (o || !/destroy|hide/.test(e)) && (o || i.data("bs.tooltip", o = new n(this, r)), "string" == typeof e && o[e]())
        })
    }
    var n = function(t, e) {
        this.type = null, this.options = null, this.enabled = null, this.timeout = null, this.hoverState = null, this.$element = null, this.init("tooltip", t, e)
    };
    n.VERSION = "3.3.4", n.TRANSITION_DURATION = 150, n.DEFAULTS = {
        animation: !0,
        placement: "top",
        selector: !1,
        template: '<div class="tooltip" role="tooltip"><div class="tooltip-arrow"></div><div class="tooltip-inner"></div></div>',
        trigger: "hover focus",
        title: "",
        delay: 0,
        html: !1,
        container: !1,
        viewport: {
            selector: "body",
            padding: 0
        }
    }, n.prototype.init = function(e, n, i) {
        if (this.enabled = !0, this.type = e, this.$element = t(n), this.options = this.getOptions(i), this.$viewport = this.options.viewport && t(this.options.viewport.selector || this.options.viewport), this.$element[0] instanceof document.constructor && !this.options.selector) throw new Error("`selector` option must be specified when initializing " + this.type + " on the window.document object!");
        for (var o = this.options.trigger.split(" "), r = o.length; r--;) {
            var s = o[r];
            if ("click" == s) this.$element.on("click." + this.type, this.options.selector, t.proxy(this.toggle, this));
            else if ("manual" != s) {
                var a = "hover" == s ? "mouseenter" : "focusin",
                    l = "hover" == s ? "mouseleave" : "focusout";
                this.$element.on(a + "." + this.type, this.options.selector, t.proxy(this.enter, this)), this.$element.on(l + "." + this.type, this.options.selector, t.proxy(this.leave, this))
            }
        }
        this.options.selector ? this._options = t.extend({}, this.options, {
            trigger: "manual",
            selector: ""
        }) : this.fixTitle()
    }, n.prototype.getDefaults = function() {
        return n.DEFAULTS
    }, n.prototype.getOptions = function(e) {
        return e = t.extend({}, this.getDefaults(), this.$element.data(), e), e.delay && "number" == typeof e.delay && (e.delay = {
            show: e.delay,
            hide: e.delay
        }), e
    }, n.prototype.getDelegateOptions = function() {
        var e = {},
            n = this.getDefaults();
        return this._options && t.each(this._options, function(t, i) {
            n[t] != i && (e[t] = i)
        }), e
    }, n.prototype.enter = function(e) {
        var n = e instanceof this.constructor ? e : t(e.currentTarget).data("bs." + this.type);
        return n && n.$tip && n.$tip.is(":visible") ? void(n.hoverState = "in") : (n || (n = new this.constructor(e.currentTarget, this.getDelegateOptions()), t(e.currentTarget).data("bs." + this.type, n)), clearTimeout(n.timeout), n.hoverState = "in", n.options.delay && n.options.delay.show ? void(n.timeout = setTimeout(function() {
            "in" == n.hoverState && n.show()
        }, n.options.delay.show)) : n.show())
    }, n.prototype.leave = function(e) {
        var n = e instanceof this.constructor ? e : t(e.currentTarget).data("bs." + this.type);
        return n || (n = new this.constructor(e.currentTarget, this.getDelegateOptions()), t(e.currentTarget).data("bs." + this.type, n)), clearTimeout(n.timeout), n.hoverState = "out", n.options.delay && n.options.delay.hide ? void(n.timeout = setTimeout(function() {
            "out" == n.hoverState && n.hide()
        }, n.options.delay.hide)) : n.hide()
    }, n.prototype.show = function() {
        var e = t.Event("show.bs." + this.type);
        if (this.hasContent() && this.enabled) {
            this.$element.trigger(e);
            var i = t.contains(this.$element[0].ownerDocument.documentElement, this.$element[0]);
            if (e.isDefaultPrevented() || !i) return;
            var o = this,
                r = this.tip(),
                s = this.getUID(this.type);
            this.setContent(), r.attr("id", s), this.$element.attr("aria-describedby", s), this.options.animation && r.addClass("fade");
            var a = "function" == typeof this.options.placement ? this.options.placement.call(this, r[0], this.$element[0]) : this.options.placement,
                l = /\s?auto?\s?/i,
                c = l.test(a);
            c && (a = a.replace(l, "") || "top"), r.detach().css({
                top: 0,
                left: 0,
                display: "block"
            }).addClass(a).data("bs." + this.type, this), this.options.container ? r.appendTo(this.options.container) : r.insertAfter(this.$element);
            var u = this.getPosition(),
                d = r[0].offsetWidth,
                h = r[0].offsetHeight;
            if (c) {
                var f = a,
                    p = this.options.container ? t(this.options.container) : this.$element.parent(),
                    g = this.getPosition(p);
                a = "bottom" == a && u.bottom + h > g.bottom ? "top" : "top" == a && u.top - h < g.top ? "bottom" : "right" == a && u.right + d > g.width ? "left" : "left" == a && u.left - d < g.left ? "right" : a, r.removeClass(f).addClass(a)
            }
            var m = this.getCalculatedOffset(a, u, d, h);
            this.applyPlacement(m, a);
            var v = function() {
                var t = o.hoverState;
                o.$element.trigger("shown.bs." + o.type), o.hoverState = null, "out" == t && o.leave(o)
            };
            t.support.transition && this.$tip.hasClass("fade") ? r.one("bsTransitionEnd", v).emulateTransitionEnd(n.TRANSITION_DURATION) : v()
        }
    }, n.prototype.applyPlacement = function(e, n) {
        var i = this.tip(),
            o = i[0].offsetWidth,
            r = i[0].offsetHeight,
            s = parseInt(i.css("margin-top"), 10),
            a = parseInt(i.css("margin-left"), 10);
        isNaN(s) && (s = 0), isNaN(a) && (a = 0), e.top = e.top + s, e.left = e.left + a, t.offset.setOffset(i[0], t.extend({
            using: function(t) {
                i.css({
                    top: Math.round(t.top),
                    left: Math.round(t.left)
                })
            }
        }, e), 0), i.addClass("in");
        var l = i[0].offsetWidth,
            c = i[0].offsetHeight;
        "top" == n && c != r && (e.top = e.top + r - c);
        var u = this.getViewportAdjustedDelta(n, e, l, c);
        u.left ? e.left += u.left : e.top += u.top;
        var d = /top|bottom/.test(n),
            h = d ? 2 * u.left - o + l : 2 * u.top - r + c,
            f = d ? "offsetWidth" : "offsetHeight";
        i.offset(e), this.replaceArrow(h, i[0][f], d)
    }, n.prototype.replaceArrow = function(t, e, n) {
        this.arrow().css(n ? "left" : "top", 50 * (1 - t / e) + "%").css(n ? "top" : "left", "")
    }, n.prototype.setContent = function() {
        var t = this.tip(),
            e = this.getTitle();
        t.find(".tooltip-inner")[this.options.html ? "html" : "text"](e), t.removeClass("fade in top bottom left right")
    }, n.prototype.hide = function(e) {
        function i() {
            "in" != o.hoverState && r.detach(), o.$element.removeAttr("aria-describedby").trigger("hidden.bs." + o.type), e && e()
        }
        var o = this,
            r = t(this.$tip),
            s = t.Event("hide.bs." + this.type);
        return this.$element.trigger(s), s.isDefaultPrevented() ? void 0 : (r.removeClass("in"), t.support.transition && r.hasClass("fade") ? r.one("bsTransitionEnd", i).emulateTransitionEnd(n.TRANSITION_DURATION) : i(), this.hoverState = null, this)
    }, n.prototype.fixTitle = function() {
        var t = this.$element;
        (t.attr("title") || "string" != typeof t.attr("data-original-title")) && t.attr("data-original-title", t.attr("title") || "").attr("title", "")
    }, n.prototype.hasContent = function() {
        return this.getTitle()
    }, n.prototype.getPosition = function(e) {
        e = e || this.$element;
        var n = e[0],
            i = "BODY" == n.tagName,
            o = n.getBoundingClientRect();
        null == o.width && (o = t.extend({}, o, {
            width: o.right - o.left,
            height: o.bottom - o.top
        }));
        var r = i ? {
                top: 0,
                left: 0
            } : e.offset(),
            s = {
                scroll: i ? document.documentElement.scrollTop || document.body.scrollTop : e.scrollTop()
            },
            a = i ? {
                width: t(window).width(),
                height: t(window).height()
            } : null;
        return t.extend({}, o, s, a, r)
    }, n.prototype.getCalculatedOffset = function(t, e, n, i) {
        return "bottom" == t ? {
            top: e.top + e.height,
            left: e.left + e.width / 2 - n / 2
        } : "top" == t ? {
            top: e.top - i,
            left: e.left + e.width / 2 - n / 2
        } : "left" == t ? {
            top: e.top + e.height / 2 - i / 2,
            left: e.left - n
        } : {
            top: e.top + e.height / 2 - i / 2,
            left: e.left + e.width
        }
    }, n.prototype.getViewportAdjustedDelta = function(t, e, n, i) {
        var o = {
            top: 0,
            left: 0
        };
        if (!this.$viewport) return o;
        var r = this.options.viewport && this.options.viewport.padding || 0,
            s = this.getPosition(this.$viewport);
        if (/right|left/.test(t)) {
            var a = e.top - r - s.scroll,
                l = e.top + r - s.scroll + i;
            a < s.top ? o.top = s.top - a : l > s.top + s.height && (o.top = s.top + s.height - l)
        } else {
            var c = e.left - r,
                u = e.left + r + n;
            c < s.left ? o.left = s.left - c : u > s.width && (o.left = s.left + s.width - u)
        }
        return o
    }, n.prototype.getTitle = function() {
        var t, e = this.$element,
            n = this.options;
        return t = e.attr("data-original-title") || ("function" == typeof n.title ? n.title.call(e[0]) : n.title)
    }, n.prototype.getUID = function(t) {
        do t += ~~(1e6 * Math.random()); while (document.getElementById(t));
        return t
    }, n.prototype.tip = function() {
        return this.$tip = this.$tip || t(this.options.template)
    }, n.prototype.arrow = function() {
        return this.$arrow = this.$arrow || this.tip().find(".tooltip-arrow")
    }, n.prototype.enable = function() {
        this.enabled = !0
    }, n.prototype.disable = function() {
        this.enabled = !1
    }, n.prototype.toggleEnabled = function() {
        this.enabled = !this.enabled
    }, n.prototype.toggle = function(e) {
        var n = this;
        e && (n = t(e.currentTarget).data("bs." + this.type), n || (n = new this.constructor(e.currentTarget, this.getDelegateOptions()), t(e.currentTarget).data("bs." + this.type, n))), n.tip().hasClass("in") ? n.leave(n) : n.enter(n)
    }, n.prototype.destroy = function() {
        var t = this;
        clearTimeout(this.timeout), this.hide(function() {
            t.$element.off("." + t.type).removeData("bs." + t.type)
        })
    };
    var i = t.fn.tooltip;
    t.fn.tooltip = e, t.fn.tooltip.Constructor = n, t.fn.tooltip.noConflict = function() {
        return t.fn.tooltip = i, this
    }
}(jQuery), + function(t) {
    "use strict";

    function e(e) {
        return this.each(function() {
            var i = t(this),
                o = i.data("bs.popover"),
                r = "object" == typeof e && e;
            (o || !/destroy|hide/.test(e)) && (o || i.data("bs.popover", o = new n(this, r)), "string" == typeof e && o[e]())
        })
    }
    var n = function(t, e) {
        this.init("popover", t, e)
    };
    if (!t.fn.tooltip) throw new Error("Popover requires tooltip.js");
    n.VERSION = "3.3.4", n.DEFAULTS = t.extend({}, t.fn.tooltip.Constructor.DEFAULTS, {
        placement: "right",
        trigger: "click",
        content: "",
        template: '<div class="popover" role="tooltip"><div class="arrow"></div><h3 class="popover-title"></h3><div class="popover-content"></div></div>'
    }), n.prototype = t.extend({}, t.fn.tooltip.Constructor.prototype), n.prototype.constructor = n, n.prototype.getDefaults = function() {
        return n.DEFAULTS
    }, n.prototype.setContent = function() {
        var t = this.tip(),
            e = this.getTitle(),
            n = this.getContent();
        t.find(".popover-title")[this.options.html ? "html" : "text"](e), t.find(".popover-content").children().detach().end()[this.options.html ? "string" == typeof n ? "html" : "append" : "text"](n), t.removeClass("fade top bottom left right in"), t.find(".popover-title").html() || t.find(".popover-title").hide()
    }, n.prototype.hasContent = function() {
        return this.getTitle() || this.getContent()
    }, n.prototype.getContent = function() {
        var t = this.$element,
            e = this.options;
        return t.attr("data-content") || ("function" == typeof e.content ? e.content.call(t[0]) : e.content)
    }, n.prototype.arrow = function() {
        return this.$arrow = this.$arrow || this.tip().find(".arrow")
    };
    var i = t.fn.popover;
    t.fn.popover = e, t.fn.popover.Constructor = n, t.fn.popover.noConflict = function() {
        return t.fn.popover = i, this
    }
}(jQuery), + function(t) {
    "use strict";

    function e(n, i) {
        this.$body = t(document.body), this.$scrollElement = t(t(n).is(document.body) ? window : n), this.options = t.extend({}, e.DEFAULTS, i), this.selector = (this.options.target || "") + " .nav li > a", this.offsets = [], this.targets = [], this.activeTarget = null, this.scrollHeight = 0, this.$scrollElement.on("scroll.bs.scrollspy", t.proxy(this.process, this)), this.refresh(), this.process()
    }

    function n(n) {
        return this.each(function() {
            var i = t(this),
                o = i.data("bs.scrollspy"),
                r = "object" == typeof n && n;
            o || i.data("bs.scrollspy", o = new e(this, r)), "string" == typeof n && o[n]()
        })
    }
    e.VERSION = "3.3.4", e.DEFAULTS = {
        offset: 10
    }, e.prototype.getScrollHeight = function() {
        return this.$scrollElement[0].scrollHeight || Math.max(this.$body[0].scrollHeight, document.documentElement.scrollHeight)
    }, e.prototype.refresh = function() {
        var e = this,
            n = "offset",
            i = 0;
        this.offsets = [], this.targets = [], this.scrollHeight = this.getScrollHeight(), t.isWindow(this.$scrollElement[0]) || (n = "position", i = this.$scrollElement.scrollTop()), this.$body.find(this.selector).map(function() {
            var e = t(this),
                o = e.data("target") || e.attr("href"),
                r = /^#./.test(o) && t(o);
            return r && r.length && r.is(":visible") && [
                [r[n]().top + i, o]
            ] || null
        }).sort(function(t, e) {
            return t[0] - e[0]
        }).each(function() {
            e.offsets.push(this[0]), e.targets.push(this[1])
        })
    }, e.prototype.process = function() {
        var t, e = this.$scrollElement.scrollTop() + this.options.offset,
            n = this.getScrollHeight(),
            i = this.options.offset + n - this.$scrollElement.height(),
            o = this.offsets,
            r = this.targets,
            s = this.activeTarget;
        if (this.scrollHeight != n && this.refresh(), e >= i) return s != (t = r[r.length - 1]) && this.activate(t);
        if (s && e < o[0]) return this.activeTarget = null, this.clear();
        for (t = o.length; t--;) s != r[t] && e >= o[t] && (void 0 === o[t + 1] || e < o[t + 1]) && this.activate(r[t])
    }, e.prototype.activate = function(e) {
        this.activeTarget = e, this.clear();
        var n = this.selector + '[data-target="' + e + '"],' + this.selector + '[href="' + e + '"]',
            i = t(n).parents("li").addClass("active");
        i.parent(".dropdown-menu").length && (i = i.closest("li.dropdown").addClass("active")), i.trigger("activate.bs.scrollspy")
    }, e.prototype.clear = function() {
        t(this.selector).parentsUntil(this.options.target, ".active").removeClass("active")
    };
    var i = t.fn.scrollspy;
    t.fn.scrollspy = n, t.fn.scrollspy.Constructor = e, t.fn.scrollspy.noConflict = function() {
        return t.fn.scrollspy = i, this
    }, t(window).on("load.bs.scrollspy.data-api", function() {
        t('[data-spy="scroll"]').each(function() {
            var e = t(this);
            n.call(e, e.data())
        })
    })
}(jQuery), + function(t) {
    "use strict";

    function e(e) {
        return this.each(function() {
            var i = t(this),
                o = i.data("bs.tab");
            o || i.data("bs.tab", o = new n(this)), "string" == typeof e && o[e]()
        })
    }
    var n = function(e) {
        this.element = t(e)
    };
    n.VERSION = "3.3.4", n.TRANSITION_DURATION = 150, n.prototype.show = function() {
        var e = this.element,
            n = e.closest("ul:not(.dropdown-menu)"),
            i = e.data("target");
        if (i || (i = e.attr("href"), i = i && i.replace(/.*(?=#[^\s]*$)/, "")), !e.parent("li").hasClass("active")) {
            var o = n.find(".active:last a"),
                r = t.Event("hide.bs.tab", {
                    relatedTarget: e[0]
                }),
                s = t.Event("show.bs.tab", {
                    relatedTarget: o[0]
                });
            if (o.trigger(r), e.trigger(s), !s.isDefaultPrevented() && !r.isDefaultPrevented()) {
                var a = t(i);
                this.activate(e.closest("li"), n), this.activate(a, a.parent(), function() {
                    o.trigger({
                        type: "hidden.bs.tab",
                        relatedTarget: e[0]
                    }), e.trigger({
                        type: "shown.bs.tab",
                        relatedTarget: o[0]
                    })
                })
            }
        }
    }, n.prototype.activate = function(e, i, o) {
        function r() {
            s.removeClass("active").find("> .dropdown-menu > .active").removeClass("active").end().find('[data-toggle="tab"]').attr("aria-expanded", !1), e.addClass("active").find('[data-toggle="tab"]').attr("aria-expanded", !0), a ? (e[0].offsetWidth, e.addClass("in")) : e.removeClass("fade"), e.parent(".dropdown-menu").length && e.closest("li.dropdown").addClass("active").end().find('[data-toggle="tab"]').attr("aria-expanded", !0), o && o()
        }
        var s = i.find("> .active"),
            a = o && t.support.transition && (s.length && s.hasClass("fade") || !!i.find("> .fade").length);
        s.length && a ? s.one("bsTransitionEnd", r).emulateTransitionEnd(n.TRANSITION_DURATION) : r(), s.removeClass("in")
    };
    var i = t.fn.tab;
    t.fn.tab = e, t.fn.tab.Constructor = n, t.fn.tab.noConflict = function() {
        return t.fn.tab = i, this
    };
    var o = function(n) {
        n.preventDefault(), e.call(t(this), "show")
    };
    t(document).on("click.bs.tab.data-api", '[data-toggle="tab"]', o).on("click.bs.tab.data-api", '[data-toggle="pill"]', o)
}(jQuery), + function(t) {
    "use strict";

    function e(e) {
        return this.each(function() {
            var i = t(this),
                o = i.data("bs.affix"),
                r = "object" == typeof e && e;
            o || i.data("bs.affix", o = new n(this, r)), "string" == typeof e && o[e]()
        })
    }
    var n = function(e, i) {
        this.options = t.extend({}, n.DEFAULTS, i), this.$target = t(this.options.target).on("scroll.bs.affix.data-api", t.proxy(this.checkPosition, this)).on("click.bs.affix.data-api", t.proxy(this.checkPositionWithEventLoop, this)), this.$element = t(e), this.affixed = null, this.unpin = null, this.pinnedOffset = null, this.checkPosition()
    };
    n.VERSION = "3.3.4", n.RESET = "affix affix-top affix-bottom", n.DEFAULTS = {
        offset: 0,
        target: window
    }, n.prototype.getState = function(t, e, n, i) {
        var o = this.$target.scrollTop(),
            r = this.$element.offset(),
            s = this.$target.height();
        if (null != n && "top" == this.affixed) return n > o ? "top" : !1;
        if ("bottom" == this.affixed) return null != n ? o + this.unpin <= r.top ? !1 : "bottom" : t - i >= o + s ? !1 : "bottom";
        var a = null == this.affixed,
            l = a ? o : r.top,
            c = a ? s : e;
        return null != n && n >= o ? "top" : null != i && l + c >= t - i ? "bottom" : !1
    }, n.prototype.getPinnedOffset = function() {
        if (this.pinnedOffset) return this.pinnedOffset;
        this.$element.removeClass(n.RESET).addClass("affix");
        var t = this.$target.scrollTop(),
            e = this.$element.offset();
        return this.pinnedOffset = e.top - t
    }, n.prototype.checkPositionWithEventLoop = function() {
        setTimeout(t.proxy(this.checkPosition, this), 1)
    }, n.prototype.checkPosition = function() {
        if (this.$element.is(":visible")) {
            var e = this.$element.height(),
                i = this.options.offset,
                o = i.top,
                r = i.bottom,
                s = t(document.body).height();
            "object" != typeof i && (r = o = i), "function" == typeof o && (o = i.top(this.$element)), "function" == typeof r && (r = i.bottom(this.$element));
            var a = this.getState(s, e, o, r);
            if (this.affixed != a) {
                null != this.unpin && this.$element.css("top", "");
                var l = "affix" + (a ? "-" + a : ""),
                    c = t.Event(l + ".bs.affix");
                if (this.$element.trigger(c), c.isDefaultPrevented()) return;
                this.affixed = a, this.unpin = "bottom" == a ? this.getPinnedOffset() : null, this.$element.removeClass(n.RESET).addClass(l).trigger(l.replace("affix", "affixed") + ".bs.affix")
            }
            "bottom" == a && this.$element.offset({
                top: s - e - r
            })
        }
    };
    var i = t.fn.affix;
    t.fn.affix = e, t.fn.affix.Constructor = n, t.fn.affix.noConflict = function() {
        return t.fn.affix = i, this
    }, t(window).on("load", function() {
        t('[data-spy="affix"]').each(function() {
            var n = t(this),
                i = n.data();
            i.offset = i.offset || {}, null != i.offsetBottom && (i.offset.bottom = i.offsetBottom), null != i.offsetTop && (i.offset.top = i.offsetTop), e.call(n, i)
        })
    })
}(jQuery),
function(t) {
    t.color = {}, t.color.make = function(e, n, i, o) {
        var r = {};
        return r.r = e || 0, r.g = n || 0, r.b = i || 0, r.a = null != o ? o : 1, r.add = function(t, e) {
            for (var n = 0; n < t.length; ++n) r[t.charAt(n)] += e;
            return r.normalize()
        }, r.scale = function(t, e) {
            for (var n = 0; n < t.length; ++n) r[t.charAt(n)] *= e;
            return r.normalize()
        }, r.toString = function() {
            return r.a >= 1 ? "rgb(" + [r.r, r.g, r.b].join(",") + ")" : "rgba(" + [r.r, r.g, r.b, r.a].join(",") + ")"
        }, r.normalize = function() {
            function t(t, e, n) {
                return t > e ? t : e > n ? n : e
            }
            return r.r = t(0, parseInt(r.r), 255), r.g = t(0, parseInt(r.g), 255), r.b = t(0, parseInt(r.b), 255), r.a = t(0, r.a, 1), r
        }, r.clone = function() {
            return t.color.make(r.r, r.b, r.g, r.a)
        }, r.normalize()
    }, t.color.extract = function(e, n) {
        var i;
        do {
            if (i = e.css(n).toLowerCase(), "" != i && "transparent" != i) break;
            e = e.parent()
        } while (e.length && !t.nodeName(e.get(0), "body"));
        return "rgba(0, 0, 0, 0)" == i && (i = "transparent"), t.color.parse(i)
    }, t.color.parse = function(n) {
        var i, o = t.color.make;
        if (i = /rgb\(\s*([0-9]{1,3})\s*,\s*([0-9]{1,3})\s*,\s*([0-9]{1,3})\s*\)/.exec(n)) return o(parseInt(i[1], 10), parseInt(i[2], 10), parseInt(i[3], 10));
        if (i = /rgba\(\s*([0-9]{1,3})\s*,\s*([0-9]{1,3})\s*,\s*([0-9]{1,3})\s*,\s*([0-9]+(?:\.[0-9]+)?)\s*\)/.exec(n)) return o(parseInt(i[1], 10), parseInt(i[2], 10), parseInt(i[3], 10), parseFloat(i[4]));
        if (i = /rgb\(\s*([0-9]+(?:\.[0-9]+)?)\%\s*,\s*([0-9]+(?:\.[0-9]+)?)\%\s*,\s*([0-9]+(?:\.[0-9]+)?)\%\s*\)/.exec(n)) return o(2.55 * parseFloat(i[1]), 2.55 * parseFloat(i[2]), 2.55 * parseFloat(i[3]));
        if (i = /rgba\(\s*([0-9]+(?:\.[0-9]+)?)\%\s*,\s*([0-9]+(?:\.[0-9]+)?)\%\s*,\s*([0-9]+(?:\.[0-9]+)?)\%\s*,\s*([0-9]+(?:\.[0-9]+)?)\s*\)/.exec(n)) return o(2.55 * parseFloat(i[1]), 2.55 * parseFloat(i[2]), 2.55 * parseFloat(i[3]), parseFloat(i[4]));
        if (i = /#([a-fA-F0-9]{2})([a-fA-F0-9]{2})([a-fA-F0-9]{2})/.exec(n)) return o(parseInt(i[1], 16), parseInt(i[2], 16), parseInt(i[3], 16));
        if (i = /#([a-fA-F0-9])([a-fA-F0-9])([a-fA-F0-9])/.exec(n)) return o(parseInt(i[1] + i[1], 16), parseInt(i[2] + i[2], 16), parseInt(i[3] + i[3], 16));
        var r = t.trim(n).toLowerCase();
        return "transparent" == r ? o(255, 255, 255, 0) : (i = e[r] || [0, 0, 0], o(i[0], i[1], i[2]))
    };
    var e = {
        aqua: [0, 255, 255],
        azure: [240, 255, 255],
        beige: [245, 245, 220],
        black: [0, 0, 0],
        blue: [0, 0, 255],
        brown: [165, 42, 42],
        cyan: [0, 255, 255],
        darkblue: [0, 0, 139],
        darkcyan: [0, 139, 139],
        darkgrey: [169, 169, 169],
        darkgreen: [0, 100, 0],
        darkkhaki: [189, 183, 107],
        darkmagenta: [139, 0, 139],
        darkolivegreen: [85, 107, 47],
        darkorange: [255, 140, 0],
        darkorchid: [153, 50, 204],
        darkred: [139, 0, 0],
        darksalmon: [233, 150, 122],
        darkviolet: [148, 0, 211],
        fuchsia: [255, 0, 255],
        gold: [255, 215, 0],
        green: [0, 128, 0],
        indigo: [75, 0, 130],
        khaki: [240, 230, 140],
        lightblue: [173, 216, 230],
        lightcyan: [224, 255, 255],
        lightgreen: [144, 238, 144],
        lightgrey: [211, 211, 211],
        lightpink: [255, 182, 193],
        lightyellow: [255, 255, 224],
        lime: [0, 255, 0],
        magenta: [255, 0, 255],
        maroon: [128, 0, 0],
        navy: [0, 0, 128],
        olive: [128, 128, 0],
        orange: [255, 165, 0],
        pink: [255, 192, 203],
        purple: [128, 0, 128],
        violet: [128, 0, 128],
        red: [255, 0, 0],
        silver: [192, 192, 192],
        white: [255, 255, 255],
        yellow: [255, 255, 0]
    }
}(jQuery),
function(t) {
    function e(e, n) {
        var i = n.children("." + e)[0];
        if (null == i && (i = document.createElement("canvas"), i.className = e, t(i).css({
            direction: "ltr",
            position: "absolute",
            left: 0,
            top: 0
        }).appendTo(n), !i.getContext)) {
            if (!window.G_vmlCanvasManager) throw new Error("Canvas is not available. If you're using IE with a fall-back such as Excanvas, then there's either a mistake in your conditional include, or the page has no DOCTYPE and is rendering in Quirks Mode.");
            i = window.G_vmlCanvasManager.initElement(i)
        }
        this.element = i;
        var o = this.context = i.getContext("2d"),
            r = window.devicePixelRatio || 1,
            s = o.webkitBackingStorePixelRatio || o.mozBackingStorePixelRatio || o.msBackingStorePixelRatio || o.oBackingStorePixelRatio || o.backingStorePixelRatio || 1;
        this.pixelRatio = r / s, this.resize(n.width(), n.height()), this.textContainer = null, this.text = {}, this._textCache = {}
    }

    function n(n, o, r, s) {
        function a(t, e) {
            e = [mt].concat(e);
            for (var n = 0; n < t.length; ++n) t[n].apply(this, e)
        }

        function l() {
            for (var n = {
                Canvas: e
            }, i = 0; i < s.length; ++i) {
                var o = s[i];
                o.init(mt, n), o.options && t.extend(!0, ot, o.options)
            }
        }

        function c(e) {
            t.extend(!0, ot, e), e && e.colors && (ot.colors = e.colors), null == ot.xaxis.color && (ot.xaxis.color = t.color.parse(ot.grid.color).scale("a", .22).toString()), null == ot.yaxis.color && (ot.yaxis.color = t.color.parse(ot.grid.color).scale("a", .22).toString()), null == ot.xaxis.tickColor && (ot.xaxis.tickColor = ot.grid.tickColor || ot.xaxis.color), null == ot.yaxis.tickColor && (ot.yaxis.tickColor = ot.grid.tickColor || ot.yaxis.color), null == ot.grid.borderColor && (ot.grid.borderColor = ot.grid.color), null == ot.grid.tickColor && (ot.grid.tickColor = t.color.parse(ot.grid.color).scale("a", .22).toString());
            var i, o, r, s = n.css("font-size"),
                l = s ? +s.replace("px", "") : 13,
                c = {
                    style: n.css("font-style"),
                    size: Math.round(.8 * l),
                    variant: n.css("font-variant"),
                    weight: n.css("font-weight"),
                    family: n.css("font-family")
                };
            for (r = ot.xaxes.length || 1, i = 0; r > i; ++i) o = ot.xaxes[i], o && !o.tickColor && (o.tickColor = o.color), o = t.extend(!0, {}, ot.xaxis, o), ot.xaxes[i] = o, o.font && (o.font = t.extend({}, c, o.font), o.font.color || (o.font.color = o.color), o.font.lineHeight || (o.font.lineHeight = Math.round(1.15 * o.font.size)));
            for (r = ot.yaxes.length || 1, i = 0; r > i; ++i) o = ot.yaxes[i], o && !o.tickColor && (o.tickColor = o.color), o = t.extend(!0, {}, ot.yaxis, o), ot.yaxes[i] = o, o.font && (o.font = t.extend({}, c, o.font), o.font.color || (o.font.color = o.color), o.font.lineHeight || (o.font.lineHeight = Math.round(1.15 * o.font.size)));
            for (ot.xaxis.noTicks && null == ot.xaxis.ticks && (ot.xaxis.ticks = ot.xaxis.noTicks), ot.yaxis.noTicks && null == ot.yaxis.ticks && (ot.yaxis.ticks = ot.yaxis.noTicks), ot.x2axis && (ot.xaxes[1] = t.extend(!0, {}, ot.xaxis, ot.x2axis), ot.xaxes[1].position = "top", null == ot.x2axis.min && (ot.xaxes[1].min = null), null == ot.x2axis.max && (ot.xaxes[1].max = null)), ot.y2axis && (ot.yaxes[1] = t.extend(!0, {}, ot.yaxis, ot.y2axis), ot.yaxes[1].position = "right", null == ot.y2axis.min && (ot.yaxes[1].min = null), null == ot.y2axis.max && (ot.yaxes[1].max = null)), ot.grid.coloredAreas && (ot.grid.markings = ot.grid.coloredAreas), ot.grid.coloredAreasColor && (ot.grid.markingsColor = ot.grid.coloredAreasColor), ot.lines && t.extend(!0, ot.series.lines, ot.lines), ot.points && t.extend(!0, ot.series.points, ot.points), ot.bars && t.extend(!0, ot.series.bars, ot.bars), null != ot.shadowSize && (ot.series.shadowSize = ot.shadowSize), null != ot.highlightColor && (ot.series.highlightColor = ot.highlightColor), i = 0; i < ot.xaxes.length; ++i) m(ut, i + 1).options = ot.xaxes[i];
            for (i = 0; i < ot.yaxes.length; ++i) m(dt, i + 1).options = ot.yaxes[i];
            for (var u in gt) ot.hooks[u] && ot.hooks[u].length && (gt[u] = gt[u].concat(ot.hooks[u]));
            a(gt.processOptions, [ot])
        }

        function u(t) {
            it = d(t), v(), y()
        }

        function d(e) {
            for (var n = [], i = 0; i < e.length; ++i) {
                var o = t.extend(!0, {}, ot.series);
                null != e[i].data ? (o.data = e[i].data, delete e[i].data, t.extend(!0, o, e[i]), e[i].data = o.data) : o.data = e[i], n.push(o)
            }
            return n
        }

        function h(t, e) {
            var n = t[e + "axis"];
            return "object" == typeof n && (n = n.n), "number" != typeof n && (n = 1), n
        }

        function f() {
            return t.grep(ut.concat(dt), function(t) {
                return t
            })
        }

        function p(t) {
            var e, n, i = {};
            for (e = 0; e < ut.length; ++e) n = ut[e], n && n.used && (i["x" + n.n] = n.c2p(t.left));
            for (e = 0; e < dt.length; ++e) n = dt[e], n && n.used && (i["y" + n.n] = n.c2p(t.top));
            return void 0 !== i.x1 && (i.x = i.x1), void 0 !== i.y1 && (i.y = i.y1), i
        }

        function g(t) {
            var e, n, i, o = {};
            for (e = 0; e < ut.length; ++e)
                if (n = ut[e], n && n.used && (i = "x" + n.n, null == t[i] && 1 == n.n && (i = "x"), null != t[i])) {
                    o.left = n.p2c(t[i]);
                    break
                }
            for (e = 0; e < dt.length; ++e)
                if (n = dt[e], n && n.used && (i = "y" + n.n, null == t[i] && 1 == n.n && (i = "y"), null != t[i])) {
                    o.top = n.p2c(t[i]);
                    break
                }
            return o
        }

        function m(e, n) {
            return e[n - 1] || (e[n - 1] = {
                n: n,
                direction: e == ut ? "x" : "y",
                options: t.extend(!0, {}, e == ut ? ot.xaxis : ot.yaxis)
            }), e[n - 1]
        }

        function v() {
            var e, n = it.length,
                i = -1;
            for (e = 0; e < it.length; ++e) {
                var o = it[e].color;
                null != o && (n--, "number" == typeof o && o > i && (i = o))
            }
            i >= n && (n = i + 1);
            var r, s = [],
                a = ot.colors,
                l = a.length,
                c = 0;
            for (e = 0; n > e; e++) r = t.color.parse(a[e % l] || "#666"), e % l == 0 && e && (c = c >= 0 ? .5 > c ? -c - .2 : 0 : -c), s[e] = r.scale("rgb", 1 + c);
            var u, d = 0;
            for (e = 0; e < it.length; ++e) {
                if (u = it[e], null == u.color ? (u.color = s[d].toString(), ++d) : "number" == typeof u.color && (u.color = s[u.color].toString()), null == u.lines.show) {
                    var f, p = !0;
                    for (f in u)
                        if (u[f] && u[f].show) {
                            p = !1;
                            break
                        }
                    p && (u.lines.show = !0)
                }
                null == u.lines.zero && (u.lines.zero = !!u.lines.fill), u.xaxis = m(ut, h(u, "x")), u.yaxis = m(dt, h(u, "y"));

            }
        }

        function y() {
            function e(t, e, n) {
                e < t.datamin && e != -y && (t.datamin = e), n > t.datamax && n != y && (t.datamax = n)
            }
            var n, i, o, r, s, l, c, u, d, h, p, g, m = Number.POSITIVE_INFINITY,
                v = Number.NEGATIVE_INFINITY,
                y = Number.MAX_VALUE;
            for (t.each(f(), function(t, e) {
                e.datamin = m, e.datamax = v, e.used = !1
            }), n = 0; n < it.length; ++n) s = it[n], s.datapoints = {
                points: []
            }, a(gt.processRawData, [s, s.data, s.datapoints]);
            for (n = 0; n < it.length; ++n) {
                if (s = it[n], p = s.data, g = s.datapoints.format, !g) {
                    if (g = [], g.push({
                        x: !0,
                        number: !0,
                        required: !0
                    }), g.push({
                        y: !0,
                        number: !0,
                        required: !0
                    }), s.bars.show || s.lines.show && s.lines.fill) {
                        var w = !!(s.bars.show && s.bars.zero || s.lines.show && s.lines.zero);
                        g.push({
                            y: !0,
                            number: !0,
                            required: !1,
                            defaultValue: 0,
                            autoscale: w
                        }), s.bars.horizontal && (delete g[g.length - 1].y, g[g.length - 1].x = !0)
                    }
                    s.datapoints.format = g
                }
                if (null == s.datapoints.pointsize) {
                    s.datapoints.pointsize = g.length, c = s.datapoints.pointsize, l = s.datapoints.points;
                    var b = s.lines.show && s.lines.steps;
                    for (s.xaxis.used = s.yaxis.used = !0, i = o = 0; i < p.length; ++i, o += c) {
                        h = p[i];
                        var x = null == h;
                        if (!x)
                            for (r = 0; c > r; ++r) u = h[r], d = g[r], d && (d.number && null != u && (u = +u, isNaN(u) ? u = null : u == 1 / 0 ? u = y : u == -(1 / 0) && (u = -y)), null == u && (d.required && (x = !0), null != d.defaultValue && (u = d.defaultValue))), l[o + r] = u;
                        if (x)
                            for (r = 0; c > r; ++r) u = l[o + r], null != u && (d = g[r], d.autoscale !== !1 && (d.x && e(s.xaxis, u, u), d.y && e(s.yaxis, u, u))), l[o + r] = null;
                        else if (b && o > 0 && null != l[o - c] && l[o - c] != l[o] && l[o - c + 1] != l[o + 1]) {
                            for (r = 0; c > r; ++r) l[o + c + r] = l[o + r];
                            l[o + 1] = l[o - c + 1], o += c
                        }
                    }
                }
            }
            for (n = 0; n < it.length; ++n) s = it[n], a(gt.processDatapoints, [s, s.datapoints]);
            for (n = 0; n < it.length; ++n) {
                s = it[n], l = s.datapoints.points, c = s.datapoints.pointsize, g = s.datapoints.format;
                var S = m,
                    T = m,
                    C = v,
                    k = v;
                for (i = 0; i < l.length; i += c)
                    if (null != l[i])
                        for (r = 0; c > r; ++r) u = l[i + r], d = g[r], d && d.autoscale !== !1 && u != y && u != -y && (d.x && (S > u && (S = u), u > C && (C = u)), d.y && (T > u && (T = u), u > k && (k = u)));
                if (s.bars.show) {
                    var E;
                    switch (s.bars.align) {
                        case "left":
                            E = 0;
                            break;
                        case "right":
                            E = -s.bars.barWidth;
                            break;
                        default:
                            E = -s.bars.barWidth / 2
                    }
                    s.bars.horizontal ? (T += E, k += E + s.bars.barWidth) : (S += E, C += E + s.bars.barWidth)
                }
                e(s.xaxis, S, C), e(s.yaxis, T, k)
            }
            t.each(f(), function(t, e) {
                e.datamin == m && (e.datamin = null), e.datamax == v && (e.datamax = null)
            })
        }

        function w() {
            n.css("padding", 0).children().filter(function() {
                return !t(this).hasClass("flot-overlay") && !t(this).hasClass("flot-base")
            }).remove(), "static" == n.css("position") && n.css("position", "relative"), rt = new e("flot-base", n), st = new e("flot-overlay", n), lt = rt.context, ct = st.context, at = t(st.element).unbind();
            var i = n.data("plot");
            i && (i.shutdown(), st.clear()), n.data("plot", mt)
        }

        function b() {
            ot.grid.hoverable && (at.mousemove(B), at.bind("mouseleave", G)), ot.grid.clickable && at.click(V), a(gt.bindEvents, [at])
        }

        function x() {
            yt && clearTimeout(yt), at.unbind("mousemove", B), at.unbind("mouseleave", G), at.unbind("click", V), a(gt.shutdown, [at])
        }

        function S(t) {
            function e(t) {
                return t
            }
            var n, i, o = t.options.transform || e,
                r = t.options.inverseTransform;
            "x" == t.direction ? (n = t.scale = ft / Math.abs(o(t.max) - o(t.min)), i = Math.min(o(t.max), o(t.min))) : (n = t.scale = pt / Math.abs(o(t.max) - o(t.min)), n = -n, i = Math.max(o(t.max), o(t.min))), t.p2c = o == e ? function(t) {
                return (t - i) * n
            } : function(t) {
                return (o(t) - i) * n
            }, t.c2p = r ? function(t) {
                return r(i + t / n)
            } : function(t) {
                return i + t / n
            }
        }

        function T(t) {
            for (var e = t.options, n = t.ticks || [], i = e.labelWidth || 0, o = e.labelHeight || 0, r = i || ("x" == t.direction ? Math.floor(rt.width / (n.length || 1)) : null), s = t.direction + "Axis " + t.direction + t.n + "Axis", a = "flot-" + t.direction + "-axis flot-" + t.direction + t.n + "-axis " + s, l = e.font || "flot-tick-label tickLabel", c = 0; c < n.length; ++c) {
                var u = n[c];
                if (u.label) {
                    var d = rt.getTextInfo(a, u.label, l, null, r);
                    i = Math.max(i, d.width), o = Math.max(o, d.height)
                }
            }
            t.labelWidth = e.labelWidth || i, t.labelHeight = e.labelHeight || o
        }

        function C(e) {
            var n = e.labelWidth,
                i = e.labelHeight,
                o = e.options.position,
                r = "x" === e.direction,
                s = e.options.tickLength,
                a = ot.grid.axisMargin,
                l = ot.grid.labelMargin,
                c = !0,
                u = !0,
                d = !0,
                h = !1;
            t.each(r ? ut : dt, function(t, n) {
                n && (n.show || n.reserveSpace) && (n === e ? h = !0 : n.options.position === o && (h ? u = !1 : c = !1), h || (d = !1))
            }), u && (a = 0), null == s && (s = d ? "full" : 5), isNaN(+s) || (l += +s), r ? (i += l, "bottom" == o ? (ht.bottom += i + a, e.box = {
                top: rt.height - ht.bottom,
                height: i
            }) : (e.box = {
                top: ht.top + a,
                height: i
            }, ht.top += i + a)) : (n += l, "left" == o ? (e.box = {
                left: ht.left + a,
                width: n
            }, ht.left += n + a) : (ht.right += n + a, e.box = {
                left: rt.width - ht.right,
                width: n
            })), e.position = o, e.tickLength = s, e.box.padding = l, e.innermost = c
        }

        function k(t) {
            "x" == t.direction ? (t.box.left = ht.left - t.labelWidth / 2, t.box.width = rt.width - ht.left - ht.right + t.labelWidth) : (t.box.top = ht.top - t.labelHeight / 2, t.box.height = rt.height - ht.bottom - ht.top + t.labelHeight)
        }

        function E() {
            var e, n = ot.grid.minBorderMargin;
            if (null == n)
                for (n = 0, e = 0; e < it.length; ++e) n = Math.max(n, 2 * (it[e].points.radius + it[e].points.lineWidth / 2));
            var i = {
                left: n,
                right: n,
                top: n,
                bottom: n
            };
            t.each(f(), function(t, e) {
                e.reserveSpace && e.ticks && e.ticks.length && ("x" === e.direction ? (i.left = Math.max(i.left, e.labelWidth / 2), i.right = Math.max(i.right, e.labelWidth / 2)) : (i.bottom = Math.max(i.bottom, e.labelHeight / 2), i.top = Math.max(i.top, e.labelHeight / 2)))
            }), ht.left = Math.ceil(Math.max(i.left, ht.left)), ht.right = Math.ceil(Math.max(i.right, ht.right)), ht.top = Math.ceil(Math.max(i.top, ht.top)), ht.bottom = Math.ceil(Math.max(i.bottom, ht.bottom))
        }

        function D() {
            var e, n = f(),
                i = ot.grid.show;
            for (var o in ht) {
                var r = ot.grid.margin || 0;
                ht[o] = "number" == typeof r ? r : r[o] || 0
            }
            a(gt.processOffset, [ht]);
            for (var o in ht) ht[o] += "object" == typeof ot.grid.borderWidth ? i ? ot.grid.borderWidth[o] : 0 : i ? ot.grid.borderWidth : 0;
            if (t.each(n, function(t, e) {
                var n = e.options;
                e.show = null == n.show ? e.used : n.show, e.reserveSpace = null == n.reserveSpace ? e.show : n.reserveSpace, M(e)
            }), i) {
                var s = t.grep(n, function(t) {
                    return t.show || t.reserveSpace
                });
                for (t.each(s, function(t, e) {
                    _(e), R(e), H(e, e.ticks), T(e)
                }), e = s.length - 1; e >= 0; --e) C(s[e]);
                E(), t.each(s, function(t, e) {
                    k(e)
                })
            }
            ft = rt.width - ht.left - ht.right, pt = rt.height - ht.bottom - ht.top, t.each(n, function(t, e) {
                S(e)
            }), i && O(), $()
        }

        function M(t) {
            var e = t.options,
                n = +(null != e.min ? e.min : t.datamin),
                i = +(null != e.max ? e.max : t.datamax),
                o = i - n;
            if (0 == o) {
                var r = 0 == i ? 1 : .01;
                null == e.min && (n -= r), (null == e.max || null != e.min) && (i += r)
            } else {
                var s = e.autoscaleMargin;
                null != s && (null == e.min && (n -= o * s, 0 > n && null != t.datamin && t.datamin >= 0 && (n = 0)), null == e.max && (i += o * s, i > 0 && null != t.datamax && t.datamax <= 0 && (i = 0)))
            }
            t.min = n, t.max = i
        }

        function _(e) {
            var n, o = e.options;
            n = "number" == typeof o.ticks && o.ticks > 0 ? o.ticks : .3 * Math.sqrt("x" == e.direction ? rt.width : rt.height);
            var r = (e.max - e.min) / n,
                s = -Math.floor(Math.log(r) / Math.LN10),
                a = o.tickDecimals;
            null != a && s > a && (s = a);
            var l, c = Math.pow(10, -s),
                u = r / c;
            if (1.5 > u ? l = 1 : 3 > u ? (l = 2, u > 2.25 && (null == a || a >= s + 1) && (l = 2.5, ++s)) : l = 7.5 > u ? 5 : 10, l *= c, null != o.minTickSize && l < o.minTickSize && (l = o.minTickSize), e.delta = r, e.tickDecimals = Math.max(0, null != a ? a : s), e.tickSize = o.tickSize || l, "time" == o.mode && !e.tickGenerator) throw new Error("Time mode requires the flot.time plugin.");
            if (e.tickGenerator || (e.tickGenerator = function(t) {
                var e, n = [],
                    o = i(t.min, t.tickSize),
                    r = 0,
                    s = Number.NaN;
                do e = s, s = o + r * t.tickSize, n.push(s), ++r; while (s < t.max && s != e);
                return n
            }, e.tickFormatter = function(t, e) {
                var n = e.tickDecimals ? Math.pow(10, e.tickDecimals) : 1,
                    i = "" + Math.round(t * n) / n;
                if (null != e.tickDecimals) {
                    var o = i.indexOf("."),
                        r = -1 == o ? 0 : i.length - o - 1;
                    if (r < e.tickDecimals) return (r ? i : i + ".") + ("" + n).substr(1, e.tickDecimals - r)
                }
                return i
            }), t.isFunction(o.tickFormatter) && (e.tickFormatter = function(t, e) {
                return "" + o.tickFormatter(t, e)
            }), null != o.alignTicksWithAxis) {
                var d = ("x" == e.direction ? ut : dt)[o.alignTicksWithAxis - 1];
                if (d && d.used && d != e) {
                    var h = e.tickGenerator(e);
                    if (h.length > 0 && (null == o.min && (e.min = Math.min(e.min, h[0])), null == o.max && h.length > 1 && (e.max = Math.max(e.max, h[h.length - 1]))), e.tickGenerator = function(t) {
                        var e, n, i = [];
                        for (n = 0; n < d.ticks.length; ++n) e = (d.ticks[n].v - d.min) / (d.max - d.min), e = t.min + e * (t.max - t.min), i.push(e);
                        return i
                    }, !e.mode && null == o.tickDecimals) {
                        var f = Math.max(0, -Math.floor(Math.log(e.delta) / Math.LN10) + 1),
                            p = e.tickGenerator(e);
                        p.length > 1 && /\..*0$/.test((p[1] - p[0]).toFixed(f)) || (e.tickDecimals = f)
                    }
                }
            }
        }

        function R(e) {
            var n = e.options.ticks,
                i = [];
            null == n || "number" == typeof n && n > 0 ? i = e.tickGenerator(e) : n && (i = t.isFunction(n) ? n(e) : n);
            var o, r;
            for (e.ticks = [], o = 0; o < i.length; ++o) {
                var s = null,
                    a = i[o];
                "object" == typeof a ? (r = +a[0], a.length > 1 && (s = a[1])) : r = +a, null == s && (s = e.tickFormatter(r, e)), isNaN(r) || e.ticks.push({
                    v: r,
                    label: s
                })
            }
        }

        function H(t, e) {
            t.options.autoscaleMargin && e.length > 0 && (null == t.options.min && (t.min = Math.min(t.min, e[0].v)), null == t.options.max && e.length > 1 && (t.max = Math.max(t.max, e[e.length - 1].v)))
        }

        function z() {
            rt.clear(), a(gt.drawBackground, [lt]);
            var t = ot.grid;
            t.show && t.backgroundColor && L(), t.show && !t.aboveData && N();
            for (var e = 0; e < it.length; ++e) a(gt.drawSeries, [lt, it[e]]), I(it[e]);
            a(gt.draw, [lt]), t.show && t.aboveData && N(), rt.render(), X()
        }

        function A(t, e) {
            for (var n, i, o, r, s = f(), a = 0; a < s.length; ++a)
                if (n = s[a], n.direction == e && (r = e + n.n + "axis", t[r] || 1 != n.n || (r = e + "axis"), t[r])) {
                    i = t[r].from, o = t[r].to;
                    break
                }
            if (t[r] || (n = "x" == e ? ut[0] : dt[0], i = t[e + "1"], o = t[e + "2"]), null != i && null != o && i > o) {
                var l = i;
                i = o, o = l
            }
            return {
                from: i,
                to: o,
                axis: n
            }
        }

        function L() {
            lt.save(), lt.translate(ht.left, ht.top), lt.fillStyle = nt(ot.grid.backgroundColor, pt, 0, "rgba(255, 255, 255, 0)"), lt.fillRect(0, 0, ft, pt), lt.restore()
        }

        function N() {
            var e, n, i, o;
            lt.save(), lt.translate(ht.left, ht.top);
            var r = ot.grid.markings;
            if (r)
                for (t.isFunction(r) && (n = mt.getAxes(), n.xmin = n.xaxis.min, n.xmax = n.xaxis.max, n.ymin = n.yaxis.min, n.ymax = n.yaxis.max, r = r(n)), e = 0; e < r.length; ++e) {
                    var s = r[e],
                        a = A(s, "x"),
                        l = A(s, "y");
                    if (null == a.from && (a.from = a.axis.min), null == a.to && (a.to = a.axis.max), null == l.from && (l.from = l.axis.min), null == l.to && (l.to = l.axis.max), !(a.to < a.axis.min || a.from > a.axis.max || l.to < l.axis.min || l.from > l.axis.max)) {
                        a.from = Math.max(a.from, a.axis.min), a.to = Math.min(a.to, a.axis.max), l.from = Math.max(l.from, l.axis.min), l.to = Math.min(l.to, l.axis.max);
                        var c = a.from === a.to,
                            u = l.from === l.to;
                        if (!c || !u)
                            if (a.from = Math.floor(a.axis.p2c(a.from)), a.to = Math.floor(a.axis.p2c(a.to)), l.from = Math.floor(l.axis.p2c(l.from)), l.to = Math.floor(l.axis.p2c(l.to)), c || u) {
                                var d = s.lineWidth || ot.grid.markingsLineWidth,
                                    h = d % 2 ? .5 : 0;
                                lt.beginPath(), lt.strokeStyle = s.color || ot.grid.markingsColor, lt.lineWidth = d, c ? (lt.moveTo(a.to + h, l.from), lt.lineTo(a.to + h, l.to)) : (lt.moveTo(a.from, l.to + h), lt.lineTo(a.to, l.to + h)), lt.stroke()
                            } else lt.fillStyle = s.color || ot.grid.markingsColor, lt.fillRect(a.from, l.to, a.to - a.from, l.from - l.to)
                    }
                }
            n = f(), i = ot.grid.borderWidth;
            for (var p = 0; p < n.length; ++p) {
                var g, m, v, y, w = n[p],
                    b = w.box,
                    x = w.tickLength;
                if (w.show && 0 != w.ticks.length) {
                    for (lt.lineWidth = 1, "x" == w.direction ? (g = 0, m = "full" == x ? "top" == w.position ? 0 : pt : b.top - ht.top + ("top" == w.position ? b.height : 0)) : (m = 0, g = "full" == x ? "left" == w.position ? 0 : ft : b.left - ht.left + ("left" == w.position ? b.width : 0)), w.innermost || (lt.strokeStyle = w.options.color, lt.beginPath(), v = y = 0, "x" == w.direction ? v = ft + 1 : y = pt + 1, 1 == lt.lineWidth && ("x" == w.direction ? m = Math.floor(m) + .5 : g = Math.floor(g) + .5), lt.moveTo(g, m), lt.lineTo(g + v, m + y), lt.stroke()), lt.strokeStyle = w.options.tickColor, lt.beginPath(), e = 0; e < w.ticks.length; ++e) {
                        var S = w.ticks[e].v;
                        v = y = 0, isNaN(S) || S < w.min || S > w.max || "full" == x && ("object" == typeof i && i[w.position] > 0 || i > 0) && (S == w.min || S == w.max) || ("x" == w.direction ? (g = w.p2c(S), y = "full" == x ? -pt : x, "top" == w.position && (y = -y)) : (m = w.p2c(S), v = "full" == x ? -ft : x, "left" == w.position && (v = -v)), 1 == lt.lineWidth && ("x" == w.direction ? g = Math.floor(g) + .5 : m = Math.floor(m) + .5), lt.moveTo(g, m), lt.lineTo(g + v, m + y))
                    }
                    lt.stroke()
                }
            }
            i && (o = ot.grid.borderColor, "object" == typeof i || "object" == typeof o ? ("object" != typeof i && (i = {
                top: i,
                right: i,
                bottom: i,
                left: i
            }), "object" != typeof o && (o = {
                top: o,
                right: o,
                bottom: o,
                left: o
            }), i.top > 0 && (lt.strokeStyle = o.top, lt.lineWidth = i.top, lt.beginPath(), lt.moveTo(0 - i.left, 0 - i.top / 2), lt.lineTo(ft, 0 - i.top / 2), lt.stroke()), i.right > 0 && (lt.strokeStyle = o.right, lt.lineWidth = i.right, lt.beginPath(), lt.moveTo(ft + i.right / 2, 0 - i.top), lt.lineTo(ft + i.right / 2, pt), lt.stroke()), i.bottom > 0 && (lt.strokeStyle = o.bottom, lt.lineWidth = i.bottom, lt.beginPath(), lt.moveTo(ft + i.right, pt + i.bottom / 2), lt.lineTo(0, pt + i.bottom / 2), lt.stroke()), i.left > 0 && (lt.strokeStyle = o.left, lt.lineWidth = i.left, lt.beginPath(), lt.moveTo(0 - i.left / 2, pt + i.bottom), lt.lineTo(0 - i.left / 2, 0), lt.stroke())) : (lt.lineWidth = i, lt.strokeStyle = ot.grid.borderColor, lt.strokeRect(-i / 2, -i / 2, ft + i, pt + i))), lt.restore()
        }

        function O() {
            t.each(f(), function(t, e) {
                var n, i, o, r, s, a = e.box,
                    l = e.direction + "Axis " + e.direction + e.n + "Axis",
                    c = "flot-" + e.direction + "-axis flot-" + e.direction + e.n + "-axis " + l,
                    u = e.options.font || "flot-tick-label tickLabel";
                if (rt.removeText(c), e.show && 0 != e.ticks.length)
                    for (var d = 0; d < e.ticks.length; ++d) n = e.ticks[d], !n.label || n.v < e.min || n.v > e.max || ("x" == e.direction ? (r = "center", i = ht.left + e.p2c(n.v), "bottom" == e.position ? o = a.top + a.padding : (o = a.top + a.height - a.padding, s = "bottom")) : (s = "middle", o = ht.top + e.p2c(n.v), "left" == e.position ? (i = a.left + a.width - a.padding, r = "right") : i = a.left + a.padding), rt.addText(c, i, o, n.label, u, null, null, r, s))
            })
        }

        function I(t) {
            t.lines.show && P(t), t.bars.show && j(t), t.points.show && F(t)
        }

        function P(t) {
            function e(t, e, n, i, o) {
                var r = t.points,
                    s = t.pointsize,
                    a = null,
                    l = null;
                lt.beginPath();
                for (var c = s; c < r.length; c += s) {
                    var u = r[c - s],
                        d = r[c - s + 1],
                        h = r[c],
                        f = r[c + 1];
                    if (null != u && null != h) {
                        if (f >= d && d < o.min) {
                            if (f < o.min) continue;
                            u = (o.min - d) / (f - d) * (h - u) + u, d = o.min
                        } else if (d >= f && f < o.min) {
                            if (d < o.min) continue;
                            h = (o.min - d) / (f - d) * (h - u) + u, f = o.min
                        }
                        if (d >= f && d > o.max) {
                            if (f > o.max) continue;
                            u = (o.max - d) / (f - d) * (h - u) + u, d = o.max
                        } else if (f >= d && f > o.max) {
                            if (d > o.max) continue;
                            h = (o.max - d) / (f - d) * (h - u) + u, f = o.max
                        }
                        if (h >= u && u < i.min) {
                            if (h < i.min) continue;
                            d = (i.min - u) / (h - u) * (f - d) + d, u = i.min
                        } else if (u >= h && h < i.min) {
                            if (u < i.min) continue;
                            f = (i.min - u) / (h - u) * (f - d) + d, h = i.min
                        }
                        if (u >= h && u > i.max) {
                            if (h > i.max) continue;
                            d = (i.max - u) / (h - u) * (f - d) + d, u = i.max
                        } else if (h >= u && h > i.max) {
                            if (u > i.max) continue;
                            f = (i.max - u) / (h - u) * (f - d) + d, h = i.max
                        }(u != a || d != l) && lt.moveTo(i.p2c(u) + e, o.p2c(d) + n), a = h, l = f, lt.lineTo(i.p2c(h) + e, o.p2c(f) + n)
                    }
                }
                lt.stroke()
            }

            function n(t, e, n) {
                for (var i = t.points, o = t.pointsize, r = Math.min(Math.max(0, n.min), n.max), s = 0, a = !1, l = 1, c = 0, u = 0;;) {
                    if (o > 0 && s > i.length + o) break;
                    s += o;
                    var d = i[s - o],
                        h = i[s - o + l],
                        f = i[s],
                        p = i[s + l];
                    if (a) {
                        if (o > 0 && null != d && null == f) {
                            u = s, o = -o, l = 2;
                            continue
                        }
                        if (0 > o && s == c + o) {
                            lt.fill(), a = !1, o = -o, l = 1, s = c = u + o;
                            continue
                        }
                    }
                    if (null != d && null != f) {
                        if (f >= d && d < e.min) {
                            if (f < e.min) continue;
                            h = (e.min - d) / (f - d) * (p - h) + h, d = e.min
                        } else if (d >= f && f < e.min) {
                            if (d < e.min) continue;
                            p = (e.min - d) / (f - d) * (p - h) + h, f = e.min
                        }
                        if (d >= f && d > e.max) {
                            if (f > e.max) continue;
                            h = (e.max - d) / (f - d) * (p - h) + h, d = e.max
                        } else if (f >= d && f > e.max) {
                            if (d > e.max) continue;
                            p = (e.max - d) / (f - d) * (p - h) + h, f = e.max
                        }
                        if (a || (lt.beginPath(), lt.moveTo(e.p2c(d), n.p2c(r)), a = !0), h >= n.max && p >= n.max) lt.lineTo(e.p2c(d), n.p2c(n.max)), lt.lineTo(e.p2c(f), n.p2c(n.max));
                        else if (h <= n.min && p <= n.min) lt.lineTo(e.p2c(d), n.p2c(n.min)), lt.lineTo(e.p2c(f), n.p2c(n.min));
                        else {
                            var g = d,
                                m = f;
                            p >= h && h < n.min && p >= n.min ? (d = (n.min - h) / (p - h) * (f - d) + d, h = n.min) : h >= p && p < n.min && h >= n.min && (f = (n.min - h) / (p - h) * (f - d) + d, p = n.min), h >= p && h > n.max && p <= n.max ? (d = (n.max - h) / (p - h) * (f - d) + d, h = n.max) : p >= h && p > n.max && h <= n.max && (f = (n.max - h) / (p - h) * (f - d) + d, p = n.max), d != g && lt.lineTo(e.p2c(g), n.p2c(h)), lt.lineTo(e.p2c(d), n.p2c(h)), lt.lineTo(e.p2c(f), n.p2c(p)), f != m && (lt.lineTo(e.p2c(f), n.p2c(p)), lt.lineTo(e.p2c(m), n.p2c(p)))
                        }
                    }
                }
            }
            lt.save(), lt.translate(ht.left, ht.top), lt.lineJoin = "round";
            var i = t.lines.lineWidth,
                o = t.shadowSize;
            if (i > 0 && o > 0) {
                lt.lineWidth = o, lt.strokeStyle = "rgba(0,0,0,0.1)";
                var r = Math.PI / 18;
                e(t.datapoints, Math.sin(r) * (i / 2 + o / 2), Math.cos(r) * (i / 2 + o / 2), t.xaxis, t.yaxis), lt.lineWidth = o / 2, e(t.datapoints, Math.sin(r) * (i / 2 + o / 4), Math.cos(r) * (i / 2 + o / 4), t.xaxis, t.yaxis)
            }
            lt.lineWidth = i, lt.strokeStyle = t.color;
            var s = Y(t.lines, t.color, 0, pt);
            s && (lt.fillStyle = s, n(t.datapoints, t.xaxis, t.yaxis)), i > 0 && e(t.datapoints, 0, 0, t.xaxis, t.yaxis), lt.restore()
        }

        function F(t) {
            function e(t, e, n, i, o, r, s, a) {
                for (var l = t.points, c = t.pointsize, u = 0; u < l.length; u += c) {
                    var d = l[u],
                        h = l[u + 1];
                    null == d || d < r.min || d > r.max || h < s.min || h > s.max || (lt.beginPath(), d = r.p2c(d), h = s.p2c(h) + i, "circle" == a ? lt.arc(d, h, e, 0, o ? Math.PI : 2 * Math.PI, !1) : a(lt, d, h, e, o), lt.closePath(), n && (lt.fillStyle = n, lt.fill()), lt.stroke())
                }
            }
            lt.save(), lt.translate(ht.left, ht.top);
            var n = t.points.lineWidth,
                i = t.shadowSize,
                o = t.points.radius,
                r = t.points.symbol;
            if (0 == n && (n = 1e-4), n > 0 && i > 0) {
                var s = i / 2;
                lt.lineWidth = s, lt.strokeStyle = "rgba(0,0,0,0.1)", e(t.datapoints, o, null, s + s / 2, !0, t.xaxis, t.yaxis, r), lt.strokeStyle = "rgba(0,0,0,0.2)", e(t.datapoints, o, null, s / 2, !0, t.xaxis, t.yaxis, r)
            }
            lt.lineWidth = n, lt.strokeStyle = t.color, e(t.datapoints, o, Y(t.points, t.color), 0, !1, t.xaxis, t.yaxis, r), lt.restore()
        }

        function W(t, e, n, i, o, r, s, a, l, c, u) {
            var d, h, f, p, g, m, v, y, w;
            c ? (y = m = v = !0, g = !1, d = n, h = t, p = e + i, f = e + o, d > h && (w = h, h = d, d = w, g = !0, m = !1)) : (g = m = v = !0, y = !1, d = t + i, h = t + o, f = n, p = e, f > p && (w = p, p = f, f = w, y = !0, v = !1)), h < s.min || d > s.max || p < a.min || f > a.max || (d < s.min && (d = s.min, g = !1), h > s.max && (h = s.max, m = !1), f < a.min && (f = a.min, y = !1), p > a.max && (p = a.max, v = !1), d = s.p2c(d), f = a.p2c(f), h = s.p2c(h), p = a.p2c(p), r && (l.fillStyle = r(f, p), l.fillRect(d, p, h - d, f - p)), u > 0 && (g || m || v || y) && (l.beginPath(), l.moveTo(d, f), g ? l.lineTo(d, p) : l.moveTo(d, p), v ? l.lineTo(h, p) : l.moveTo(h, p), m ? l.lineTo(h, f) : l.moveTo(h, f), y ? l.lineTo(d, f) : l.moveTo(d, f), l.stroke()))
        }

        function j(t) {
            function e(e, n, i, o, r, s) {
                for (var a = e.points, l = e.pointsize, c = 0; c < a.length; c += l) null != a[c] && W(a[c], a[c + 1], a[c + 2], n, i, o, r, s, lt, t.bars.horizontal, t.bars.lineWidth)
            }
            lt.save(), lt.translate(ht.left, ht.top), lt.lineWidth = t.bars.lineWidth, lt.strokeStyle = t.color;
            var n;
            switch (t.bars.align) {
                case "left":
                    n = 0;
                    break;
                case "right":
                    n = -t.bars.barWidth;
                    break;
                default:
                    n = -t.bars.barWidth / 2
            }
            var i = t.bars.fill ? function(e, n) {
                return Y(t.bars, t.color, e, n)
            } : null;
            e(t.datapoints, n, n + t.bars.barWidth, i, t.xaxis, t.yaxis), lt.restore()
        }

        function Y(e, n, i, o) {
            var r = e.fill;
            if (!r) return null;
            if (e.fillColor) return nt(e.fillColor, i, o, n);
            var s = t.color.parse(n);
            return s.a = "number" == typeof r ? r : .4, s.normalize(), s.toString()
        }

        function $() {
            if (null != ot.legend.container ? t(ot.legend.container).html("") : n.find(".legend").remove(), ot.legend.show) {
                for (var e, i, o = [], r = [], s = !1, a = ot.legend.labelFormatter, l = 0; l < it.length; ++l) e = it[l], e.label && (i = a ? a(e.label, e) : e.label, i && r.push({
                    label: i,
                    color: e.color
                }));
                if (ot.legend.sorted)
                    if (t.isFunction(ot.legend.sorted)) r.sort(ot.legend.sorted);
                    else if ("reverse" == ot.legend.sorted) r.reverse();
                else {
                    var c = "descending" != ot.legend.sorted;
                    r.sort(function(t, e) {
                        return t.label == e.label ? 0 : t.label < e.label != c ? 1 : -1
                    })
                }
                for (var l = 0; l < r.length; ++l) {
                    var u = r[l];
                    l % ot.legend.noColumns == 0 && (s && o.push("</tr>"), o.push("<tr>"), s = !0), o.push('<td class="legendColorBox"><div style="border:1px solid ' + ot.legend.labelBoxBorderColor + ';padding:1px"><div style="width:4px;height:0;border:5px solid ' + u.color + ';overflow:hidden"></div></div></td><td class="legendLabel">' + u.label + "</td>")
                }
                if (s && o.push("</tr>"), 0 != o.length) {
                    var d = '<table style="font-size:smaller;color:' + ot.grid.color + '">' + o.join("") + "</table>";
                    if (null != ot.legend.container) t(ot.legend.container).html(d);
                    else {
                        var h = "",
                            f = ot.legend.position,
                            p = ot.legend.margin;
                        null == p[0] && (p = [p, p]), "n" == f.charAt(0) ? h += "top:" + (p[1] + ht.top) + "px;" : "s" == f.charAt(0) && (h += "bottom:" + (p[1] + ht.bottom) + "px;"), "e" == f.charAt(1) ? h += "right:" + (p[0] + ht.right) + "px;" : "w" == f.charAt(1) && (h += "left:" + (p[0] + ht.left) + "px;");
                        var g = t('<div class="legend">' + d.replace('style="', 'style="position:absolute;' + h + ";") + "</div>").appendTo(n);
                        if (0 != ot.legend.backgroundOpacity) {
                            var m = ot.legend.backgroundColor;
                            null == m && (m = ot.grid.backgroundColor, m = m && "string" == typeof m ? t.color.parse(m) : t.color.extract(g, "background-color"), m.a = 1, m = m.toString());
                            var v = g.children();
                            t('<div style="position:absolute;width:' + v.width() + "px;height:" + v.height() + "px;" + h + "background-color:" + m + ';"> </div>').prependTo(g).css("opacity", ot.legend.backgroundOpacity)
                        }
                    }
                }
            }
        }

        function q(t, e, n) {
            var i, o, r, s = ot.grid.mouseActiveRadius,
                a = s * s + 1,
                l = null;
            for (i = it.length - 1; i >= 0; --i)
                if (n(it[i])) {
                    var c = it[i],
                        u = c.xaxis,
                        d = c.yaxis,
                        h = c.datapoints.points,
                        f = u.c2p(t),
                        p = d.c2p(e),
                        g = s / u.scale,
                        m = s / d.scale;
                    if (r = c.datapoints.pointsize, u.options.inverseTransform && (g = Number.MAX_VALUE), d.options.inverseTransform && (m = Number.MAX_VALUE), c.lines.show || c.points.show)
                        for (o = 0; o < h.length; o += r) {
                            var v = h[o],
                                y = h[o + 1];
                            if (null != v && !(v - f > g || -g > v - f || y - p > m || -m > y - p)) {
                                var w = Math.abs(u.p2c(v) - t),
                                    b = Math.abs(d.p2c(y) - e),
                                    x = w * w + b * b;
                                a > x && (a = x, l = [i, o / r])
                            }
                        }
                    if (c.bars.show && !l) {
                        var S, T;
                        switch (c.bars.align) {
                            case "left":
                                S = 0;
                                break;
                            case "right":
                                S = -c.bars.barWidth;
                                break;
                            default:
                                S = -c.bars.barWidth / 2
                        }
                        for (T = S + c.bars.barWidth, o = 0; o < h.length; o += r) {
                            var v = h[o],
                                y = h[o + 1],
                                C = h[o + 2];
                            null != v && (it[i].bars.horizontal ? f <= Math.max(C, v) && f >= Math.min(C, v) && p >= y + S && y + T >= p : f >= v + S && v + T >= f && p >= Math.min(C, y) && p <= Math.max(C, y)) && (l = [i, o / r])
                        }
                    }
                }
            return l ? (i = l[0], o = l[1], r = it[i].datapoints.pointsize, {
                datapoint: it[i].datapoints.points.slice(o * r, (o + 1) * r),
                dataIndex: o,
                series: it[i],
                seriesIndex: i
            }) : null
        }

        function B(t) {
            ot.grid.hoverable && U("plothover", t, function(t) {
                return 0 != t.hoverable
            })
        }

        function G(t) {
            ot.grid.hoverable && U("plothover", t, function(t) {
                return !1
            })
        }

        function V(t) {
            U("plotclick", t, function(t) {
                return 0 != t.clickable
            })
        }

        function U(t, e, i) {
            var o = at.offset(),
                r = e.pageX - o.left - ht.left,
                s = e.pageY - o.top - ht.top,
                a = p({
                    left: r,
                    top: s
                });
            a.pageX = e.pageX, a.pageY = e.pageY;
            var l = q(r, s, i);
            if (l && (l.pageX = parseInt(l.series.xaxis.p2c(l.datapoint[0]) + o.left + ht.left, 10), l.pageY = parseInt(l.series.yaxis.p2c(l.datapoint[1]) + o.top + ht.top, 10)), ot.grid.autoHighlight) {
                for (var c = 0; c < vt.length; ++c) {
                    var u = vt[c];
                    u.auto != t || l && u.series == l.series && u.point[0] == l.datapoint[0] && u.point[1] == l.datapoint[1] || K(u.series, u.point)
                }
                l && Q(l.series, l.datapoint, t)
            }
            n.trigger(t, [a, l])
        }

        function X() {
            var t = ot.interaction.redrawOverlayInterval;
            return -1 == t ? void Z() : void(yt || (yt = setTimeout(Z, t)))
        }

        function Z() {
            yt = null, ct.save(), st.clear(), ct.translate(ht.left, ht.top);
            var t, e;
            for (t = 0; t < vt.length; ++t) e = vt[t], e.series.bars.show ? et(e.series, e.point) : tt(e.series, e.point);
            ct.restore(), a(gt.drawOverlay, [ct])
        }

        function Q(t, e, n) {
            if ("number" == typeof t && (t = it[t]), "number" == typeof e) {
                var i = t.datapoints.pointsize;
                e = t.datapoints.points.slice(i * e, i * (e + 1))
            }
            var o = J(t, e); - 1 == o ? (vt.push({
                series: t,
                point: e,
                auto: n
            }), X()) : n || (vt[o].auto = !1)
        }

        function K(t, e) {
            if (null == t && null == e) return vt = [], void X();
            if ("number" == typeof t && (t = it[t]), "number" == typeof e) {
                var n = t.datapoints.pointsize;
                e = t.datapoints.points.slice(n * e, n * (e + 1))
            }
            var i = J(t, e); - 1 != i && (vt.splice(i, 1), X())
        }

        function J(t, e) {
            for (var n = 0; n < vt.length; ++n) {
                var i = vt[n];
                if (i.series == t && i.point[0] == e[0] && i.point[1] == e[1]) return n
            }
            return -1
        }

        function tt(e, n) {
            var i = n[0],
                o = n[1],
                r = e.xaxis,
                s = e.yaxis,
                a = "string" == typeof e.highlightColor ? e.highlightColor : t.color.parse(e.color).scale("a", .5).toString();
            if (!(i < r.min || i > r.max || o < s.min || o > s.max)) {
                var l = e.points.radius + e.points.lineWidth / 2;
                ct.lineWidth = l, ct.strokeStyle = a;
                var c = 1.5 * l;
                i = r.p2c(i), o = s.p2c(o), ct.beginPath(), "circle" == e.points.symbol ? ct.arc(i, o, c, 0, 2 * Math.PI, !1) : e.points.symbol(ct, i, o, c, !1), ct.closePath(), ct.stroke()
            }
        }

        function et(e, n) {
            var i, o = "string" == typeof e.highlightColor ? e.highlightColor : t.color.parse(e.color).scale("a", .5).toString(),
                r = o;
            switch (e.bars.align) {
                case "left":
                    i = 0;
                    break;
                case "right":
                    i = -e.bars.barWidth;
                    break;
                default:
                    i = -e.bars.barWidth / 2
            }
            ct.lineWidth = e.bars.lineWidth, ct.strokeStyle = o, W(n[0], n[1], n[2] || 0, i, i + e.bars.barWidth, function() {
                return r
            }, e.xaxis, e.yaxis, ct, e.bars.horizontal, e.bars.lineWidth)
        }

        function nt(e, n, i, o) {
            if ("string" == typeof e) return e;
            for (var r = lt.createLinearGradient(0, i, 0, n), s = 0, a = e.colors.length; a > s; ++s) {
                var l = e.colors[s];
                if ("string" != typeof l) {
                    var c = t.color.parse(o);
                    null != l.brightness && (c = c.scale("rgb", l.brightness)), null != l.opacity && (c.a *= l.opacity), l = c.toString()
                }
                r.addColorStop(s / (a - 1), l)
            }
            return r
        }
        var it = [],
            ot = {
                colors: ["#edc240", "#afd8f8", "#cb4b4b", "#4da74d", "#9440ed"],
                legend: {
                    show: !0,
                    noColumns: 1,
                    labelFormatter: null,
                    labelBoxBorderColor: "#ccc",
                    container: null,
                    position: "ne",
                    margin: 5,
                    backgroundColor: null,
                    backgroundOpacity: .85,
                    sorted: null
                },
                xaxis: {
                    show: null,
                    position: "bottom",
                    mode: null,
                    font: null,
                    color: null,
                    tickColor: null,
                    transform: null,
                    inverseTransform: null,
                    min: null,
                    max: null,
                    autoscaleMargin: null,
                    ticks: null,
                    tickFormatter: null,
                    labelWidth: null,
                    labelHeight: null,
                    reserveSpace: null,
                    tickLength: null,
                    alignTicksWithAxis: null,
                    tickDecimals: null,
                    tickSize: null,
                    minTickSize: null
                },
                yaxis: {
                    autoscaleMargin: .02,
                    position: "left"
                },
                xaxes: [],
                yaxes: [],
                series: {
                    points: {
                        show: !1,
                        radius: 3,
                        lineWidth: 2,
                        fill: !0,
                        fillColor: "#ffffff",
                        symbol: "circle"
                    },
                    lines: {
                        lineWidth: 2,
                        fill: !1,
                        fillColor: null,
                        steps: !1
                    },
                    bars: {
                        show: !1,
                        lineWidth: 2,
                        barWidth: 1,
                        fill: !0,
                        fillColor: null,
                        align: "left",
                        horizontal: !1,
                        zero: !0
                    },
                    shadowSize: 3,
                    highlightColor: null
                },
                grid: {
                    show: !0,
                    aboveData: !1,
                    color: "#545454",
                    backgroundColor: null,
                    borderColor: null,
                    tickColor: null,
                    margin: 0,
                    labelMargin: 5,
                    axisMargin: 8,
                    borderWidth: 2,
                    minBorderMargin: null,
                    markings: null,
                    markingsColor: "#f4f4f4",
                    markingsLineWidth: 2,
                    clickable: !1,
                    hoverable: !1,
                    autoHighlight: !0,
                    mouseActiveRadius: 10
                },
                interaction: {
                    redrawOverlayInterval: 1e3 / 60
                },
                hooks: {}
            },
            rt = null,
            st = null,
            at = null,
            lt = null,
            ct = null,
            ut = [],
            dt = [],
            ht = {
                left: 0,
                right: 0,
                top: 0,
                bottom: 0
            },
            ft = 0,
            pt = 0,
            gt = {
                processOptions: [],
                processRawData: [],
                processDatapoints: [],
                processOffset: [],
                drawBackground: [],
                drawSeries: [],
                draw: [],
                bindEvents: [],
                drawOverlay: [],
                shutdown: []
            },
            mt = this;
        mt.setData = u, mt.setupGrid = D, mt.draw = z, mt.getPlaceholder = function() {
            return n
        }, mt.getCanvas = function() {
            return rt.element
        }, mt.getPlotOffset = function() {
            return ht
        }, mt.width = function() {
            return ft
        }, mt.height = function() {
            return pt
        }, mt.offset = function() {
            var t = at.offset();
            return t.left += ht.left, t.top += ht.top, t
        }, mt.getData = function() {
            return it
        }, mt.getAxes = function() {
            var e = {};
            return t.each(ut.concat(dt), function(t, n) {
                n && (e[n.direction + (1 != n.n ? n.n : "") + "axis"] = n)
            }), e
        }, mt.getXAxes = function() {
            return ut
        }, mt.getYAxes = function() {
            return dt
        }, mt.c2p = p, mt.p2c = g, mt.getOptions = function() {
            return ot
        }, mt.highlight = Q, mt.unhighlight = K, mt.triggerRedrawOverlay = X, mt.pointOffset = function(t) {
            return {
                left: parseInt(ut[h(t, "x") - 1].p2c(+t.x) + ht.left, 10),
                top: parseInt(dt[h(t, "y") - 1].p2c(+t.y) + ht.top, 10)
            }
        }, mt.shutdown = x, mt.destroy = function() {
            x(), n.removeData("plot").empty(), it = [], ot = null, rt = null, st = null, at = null, lt = null, ct = null, ut = [], dt = [], gt = null, vt = [], mt = null
        }, mt.resize = function() {
            var t = n.width(),
                e = n.height();
            rt.resize(t, e), st.resize(t, e)
        }, mt.hooks = gt, l(mt), c(r), w(), u(o), D(), z(), b();
        var vt = [],
            yt = null
    }

    function i(t, e) {
        return e * Math.floor(t / e)
    }
    var o = Object.prototype.hasOwnProperty;
    t.fn.detach || (t.fn.detach = function() {
        return this.each(function() {
            this.parentNode && this.parentNode.removeChild(this)
        })
    }), e.prototype.resize = function(t, e) {
        if (0 >= t || 0 >= e) throw new Error("Invalid dimensions for plot, width = " + t + ", height = " + e);
        var n = this.element,
            i = this.context,
            o = this.pixelRatio;
        this.width != t && (n.width = t * o, n.style.width = t + "px", this.width = t), this.height != e && (n.height = e * o, n.style.height = e + "px", this.height = e), i.restore(), i.save(), i.scale(o, o)
    }, e.prototype.clear = function() {
        this.context.clearRect(0, 0, this.width, this.height)
    }, e.prototype.render = function() {
        var t = this._textCache;
        for (var e in t)
            if (o.call(t, e)) {
                var n = this.getTextLayer(e),
                    i = t[e];
                n.hide();
                for (var r in i)
                    if (o.call(i, r)) {
                        var s = i[r];
                        for (var a in s)
                            if (o.call(s, a)) {
                                for (var l, c = s[a].positions, u = 0; l = c[u]; u++) l.active ? l.rendered || (n.append(l.element), l.rendered = !0) : (c.splice(u--, 1), l.rendered && l.element.detach());
                                0 == c.length && delete s[a]
                            }
                    }
                n.show()
            }
    }, e.prototype.getTextLayer = function(e) {
        var n = this.text[e];
        return null == n && (null == this.textContainer && (this.textContainer = t("<div class='flot-text'></div>").css({
            position: "absolute",
            top: 0,
            left: 0,
            bottom: 0,
            right: 0,
            "font-size": "smaller",
            color: "#545454"
        }).insertAfter(this.element)), n = this.text[e] = t("<div></div>").addClass(e).css({
            position: "absolute",
            top: 0,
            left: 0,
            bottom: 0,
            right: 0
        }).appendTo(this.textContainer)), n
    }, e.prototype.getTextInfo = function(e, n, i, o, r) {
        var s, a, l, c;
        if (n = "" + n, s = "object" == typeof i ? i.style + " " + i.variant + " " + i.weight + " " + i.size + "px/" + i.lineHeight + "px " + i.family : i, a = this._textCache[e], null == a && (a = this._textCache[e] = {}), l = a[s], null == l && (l = a[s] = {}), c = l[n], null == c) {
            var u = t("<div></div>").html(n).css({
                position: "absolute",
                "max-width": r,
                top: -9999
            }).appendTo(this.getTextLayer(e));
            "object" == typeof i ? u.css({
                font: s,
                color: i.color
            }) : "string" == typeof i && u.addClass(i), c = l[n] = {
                width: u.outerWidth(!0),
                height: u.outerHeight(!0),
                element: u,
                positions: []
            }, u.detach()
        }
        return c
    }, e.prototype.addText = function(t, e, n, i, o, r, s, a, l) {
        var c = this.getTextInfo(t, i, o, r, s),
            u = c.positions;
        "center" == a ? e -= c.width / 2 : "right" == a && (e -= c.width), "middle" == l ? n -= c.height / 2 : "bottom" == l && (n -= c.height);
        for (var d, h = 0; d = u[h]; h++)
            if (d.x == e && d.y == n) return void(d.active = !0);
        d = {
            active: !0,
            rendered: !1,
            element: u.length ? c.element.clone() : c.element,
            x: e,
            y: n
        }, u.push(d), d.element.css({
            top: Math.round(n),
            left: Math.round(e),
            "text-align": a
        })
    }, e.prototype.removeText = function(t, e, n, i, r, s) {
        if (null == i) {
            var a = this._textCache[t];
            if (null != a)
                for (var l in a)
                    if (o.call(a, l)) {
                        var c = a[l];
                        for (var u in c)
                            if (o.call(c, u))
                                for (var d, h = c[u].positions, f = 0; d = h[f]; f++) d.active = !1
                    }
        } else
            for (var d, h = this.getTextInfo(t, i, r, s).positions, f = 0; d = h[f]; f++) d.x == e && d.y == n && (d.active = !1)
    }, t.plot = function(e, i, o) {
        var r = new n(t(e), i, o, t.plot.plugins);
        return r
    }, t.plot.version = "0.8.3", t.plot.plugins = [], t.fn.plot = function(e, n) {
        return this.each(function() {
            t.plot(this, e, n)
        })
    }
}(jQuery),
function(t, e, n) {
    "$:nomunge";

    function i(n) {
        a === !0 && (a = n || 1);
        for (var l = r.length - 1; l >= 0; l--) {
            var h = t(r[l]);
            if (h[0] == e || h.is(":visible")) {
                var f = h.width(),
                    p = h.height(),
                    g = h.data(u);
                !g || f === g.w && p === g.h || (h.trigger(c, [g.w = f, g.h = p]), a = n || !0)
            } else g = h.data(u), g.w = 0, g.h = 0
        }
        null !== o && (a && (null == n || 1e3 > n - a) ? o = e.requestAnimationFrame(i) : (o = setTimeout(i, s[d]), a = !1))
    }
    var o, r = [],
        s = t.resize = t.extend(t.resize, {}),
        a = !1,
        l = "setTimeout",
        c = "resize",
        u = c + "-special-event",
        d = "pendingDelay",
        h = "activeDelay",
        f = "throttleWindow";
    s[d] = 200, s[h] = 20, s[f] = !0, t.event.special[c] = {
        setup: function() {
            if (!s[f] && this[l]) return !1;
            var e = t(this);
            r.push(this), e.data(u, {
                w: e.width(),
                h: e.height()
            }), 1 === r.length && (o = n, i())
        },
        teardown: function() {
            if (!s[f] && this[l]) return !1;
            for (var e = t(this), n = r.length - 1; n >= 0; n--)
                if (r[n] == this) {
                    r.splice(n, 1);
                    break
                }
            e.removeData(u), r.length || (a ? cancelAnimationFrame(o) : clearTimeout(o), o = null)
        },
        add: function(e) {
            function i(e, i, r) {
                var s = t(this),
                    a = s.data(u) || {};
                a.w = i !== n ? i : s.width(), a.h = r !== n ? r : s.height(), o.apply(this, arguments)
            }
            if (!s[f] && this[l]) return !1;
            var o;
            return t.isFunction(e) ? (o = e, i) : (o = e.handler, void(e.handler = i))
        }
    }, e.requestAnimationFrame || (e.requestAnimationFrame = function() {
        return e.webkitRequestAnimationFrame || e.mozRequestAnimationFrame || e.oRequestAnimationFrame || e.msRequestAnimationFrame || function(t, n) {
            return e.setTimeout(function() {
                t((new Date).getTime())
            }, s[h])
        }
    }()), e.cancelAnimationFrame || (e.cancelAnimationFrame = function() {
        return e.webkitCancelRequestAnimationFrame || e.mozCancelRequestAnimationFrame || e.oCancelRequestAnimationFrame || e.msCancelRequestAnimationFrame || clearTimeout
    }())
}(jQuery, this),
function(t) {
    function e(t) {
        function e() {
            var e = t.getPlaceholder();
            0 != e.width() && 0 != e.height() && (t.resize(), t.setupGrid(), t.draw())
        }

        function n(t, n) {
            t.getPlaceholder().resize(e)
        }

        function i(t, n) {
            t.getPlaceholder().unbind("resize", e)
        }
        t.hooks.bindEvents.push(n), t.hooks.shutdown.push(i)
    }
    var n = {};
    t.plot.plugins.push({
        init: e,
        options: n,
        name: "resize",
        version: "1.0"
    })
}(jQuery),
function(t) {
    function e(t) {
        function e(t, e) {
            e.series.curvedLines.active && t.hooks.processDatapoints.unshift(n)
        }

        function n(t, e, n) {
            var o = n.points.length / n.pointsize,
                r = .005,
                s = a(e.curvedLines);
            if (!s && 1 == e.curvedLines.apply && void 0 === e.originSeries && o > 1 + r)
                if (e.lines.fill) {
                    var l = i(n, e.curvedLines, 1),
                        c = i(n, e.curvedLines, 2);
                    n.pointsize = 3, n.points = [];
                    for (var u = 0, d = 0, h = 0, f = 2; h < l.length || u < c.length;) l[h] == c[u] ? (n.points[d] = l[h], n.points[d + 1] = l[h + 1], n.points[d + 2] = c[u + 1], u += f, h += f) : l[h] < c[u] ? (n.points[d] = l[h], n.points[d + 1] = l[h + 1], n.points[d + 2] = d > 0 ? n.points[d - 1] : null, h += f) : (n.points[d] = c[u], n.points[d + 1] = d > 1 ? n.points[d - 2] : null, n.points[d + 2] = c[u + 1], u += f), d += 3
                } else e.lines.lineWidth > 0 && (n.points = i(n, e.curvedLines, 1), n.pointsize = 2)
        }

        function i(t, e, n) {
            if ("undefined" != typeof e.legacyOverride && 0 != e.legacyOverride) {
                var i = {
                        fit: !1,
                        curvePointFactor: 20,
                        fitPointDist: void 0
                    },
                    r = jQuery.extend(i, e.legacyOverride);
                return s(t, r, n)
            }
            return o(t, e, n)
        }

        function o(t, e, n) {
            for (var i = t.points, o = t.pointsize, s = r(t, e, n), a = [], l = 0, c = 0; c < i.length - o; c += o) {
                var u = c,
                    d = c + n,
                    h = i[u],
                    f = i[u + o],
                    p = (f - h) / Number(e.nrSplinePoints);
                a.push(i[u]), a.push(i[d]);
                for (var g = h += p; f > g; g += p) a.push(g), a.push(s[l](g));
                l++
            }
            return a.push(i[i.length - o]), a.push(i[i.length - o + n]), a
        }

        function r(t, e, n) {
            for (var i = t.points, o = t.pointsize, r = [], s = [], a = 0; a < i.length - o; a += o) {
                var l = a,
                    c = a + n,
                    u = i[l + o] - i[l],
                    d = i[c + o] - i[c];
                r.push(u), s.push(d / u)
            }
            var h = [s[0]];
            if (e.monotonicFit)
                for (var a = 1; a < r.length; a++) {
                    var f = s[a],
                        p = s[a - 1];

                    if (0 >= f * p) h.push(0);
                    else {
                        var g = r[a],
                            m = r[a - 1],
                            v = g + m;
                        h.push(3 * v / ((v + g) / p + (v + m) / f))
                    }
                } else
                    for (var a = o; a < i.length - o; a += o) {
                        var l = a,
                            c = a + n;
                        h.push(Number(e.tension) * (i[c + o] - i[c - o]) / (i[l + o] - i[l - o]))
                    }
            h.push(s[s.length - 1]);
            var y = [],
                w = [];
            for (a = 0; a < r.length; a++) {
                var b = h[a],
                    x = h[a + 1],
                    f = s[a],
                    S = 1 / r[a],
                    v = b + x - f - f;
                y.push(v * S * S), w.push((f - v - b) * S)
            }
            for (var T = [], a = 0; a < r.length; a++) {
                var C = function(t, e, n, i, o) {
                    return function(r) {
                        var s = r - t,
                            a = s * s;
                        return e * s * a + n * a + i * s + o
                    }
                };
                T.push(C(i[a * o], y[a], w[a], h[a], i[a * o + n]))
            }
            return T
        }

        function s(t, e, n) {
            var i = t.points,
                o = t.pointsize,
                r = Number(e.curvePointFactor) * (i.length / o),
                s = new Array,
                a = new Array,
                l = -1,
                c = -1,
                u = 0;
            if (e.fit) {
                var d;
                if ("undefined" == typeof e.fitPointDist) {
                    var h = i[0],
                        f = i[i.length - o];
                    d = (f - h) / 5e4
                } else d = Number(e.fitPointDist);
                for (var p = 0; p < i.length; p += o) {
                    var g, m;
                    l = p, c = p + n, g = i[l] - d, m = i[l] + d;
                    for (var v = 2; g == i[l] || m == i[l];) g = i[l] - d * v, m = i[l] + d * v, v++;
                    s[u] = g, a[u] = i[c], u++, s[u] = i[l], a[u] = i[c], u++, s[u] = m, a[u] = i[c], u++
                }
            } else
                for (var p = 0; p < i.length; p += o) l = p, c = p + n, s[u] = i[l], a[u] = i[c], u++;
            var y = s.length,
                w = new Array,
                b = new Array;
            w[0] = 0, w[y - 1] = 0, b[0] = 0;
            for (var p = 1; y - 1 > p; ++p) {
                var x = s[p + 1] - s[p - 1];
                if (0 == x) return [];
                var S = (s[p] - s[p - 1]) / x,
                    T = S * w[p - 1] + 2;
                w[p] = (S - 1) / T, b[p] = (a[p + 1] - a[p]) / (s[p + 1] - s[p]) - (a[p] - a[p - 1]) / (s[p] - s[p - 1]), b[p] = (6 * b[p] / (s[p + 1] - s[p - 1]) - S * b[p - 1]) / T
            }
            for (var u = y - 2; u >= 0; --u) w[u] = w[u] * w[u + 1] + b[u];
            var C = (s[y - 1] - s[0]) / (r - 1),
                k = new Array,
                E = new Array,
                D = new Array;
            for (k[0] = s[0], E[0] = a[0], D.push(k[0]), D.push(E[0]), u = 1; r > u; ++u) {
                k[u] = k[0] + u * C;
                for (var M = y - 1, _ = 0; M - _ > 1;) {
                    var R = Math.round((M + _) / 2);
                    s[R] > k[u] ? M = R : _ = R
                }
                var H = s[M] - s[_];
                if (0 == H) return [];
                var z = (s[M] - k[u]) / H,
                    A = (k[u] - s[_]) / H;
                E[u] = z * a[_] + A * a[M] + ((z * z * z - z) * w[_] + (A * A * A - A) * w[M]) * H * H / 6, D.push(k[u]), D.push(E[u])
            }
            return D
        }

        function a(t) {
            if ("undefined" != typeof t.fit || "undefined" != typeof t.curvePointFactor || "undefined" != typeof t.fitPointDist) throw new Error("CurvedLines detected illegal parameters. The CurvedLines API changed with version 1.0.0 please check the options object.");
            return !1
        }
        t.hooks.processOptions.push(e)
    }
    var n = {
        series: {
            curvedLines: {
                active: !1,
                apply: !1,
                monotonicFit: !1,
                tension: .5,
                nrSplinePoints: 20,
                legacyOverride: void 0
            }
        }
    };
    t.plot.plugins.push({
        init: e,
        options: n,
        name: "curvedLines",
        version: "1.1.1"
    })
}(jQuery),
function(t, e, n) {
    ! function(t) {
        "function" == typeof define && define.amd ? define(["jquery"], t) : jQuery && !jQuery.fn.sparkline && t(jQuery)
    }(function(i) {
        "use strict";
        var o, r, s, a, l, c, u, d, h, f, p, g, m, v, y, w, b, x, S, T, C, k, E, D, M, _, R, H, z, A, L, N, O = {},
            I = 0;
        o = function() {
            return {
                common: {
                    type: "line",
                    lineColor: "#00f",
                    fillColor: "#cdf",
                    defaultPixelsPerValue: 3,
                    width: "auto",
                    height: "auto",
                    composite: !1,
                    tagValuesAttribute: "values",
                    tagOptionsPrefix: "spark",
                    enableTagOptions: !1,
                    enableHighlight: !0,
                    highlightLighten: 1.4,
                    tooltipSkipNull: !0,
                    tooltipPrefix: "",
                    tooltipSuffix: "",
                    disableHiddenCheck: !1,
                    numberFormatter: !1,
                    numberDigitGroupCount: 3,
                    numberDigitGroupSep: ",",
                    numberDecimalMark: ".",
                    disableTooltips: !1,
                    disableInteraction: !1
                },
                line: {
                    spotColor: "#f80",
                    highlightSpotColor: "#5f5",
                    highlightLineColor: "#f22",
                    spotRadius: 1.5,
                    minSpotColor: "#f80",
                    maxSpotColor: "#f80",
                    lineWidth: 1,
                    normalRangeMin: n,
                    normalRangeMax: n,
                    normalRangeColor: "#ccc",
                    drawNormalOnTop: !1,
                    chartRangeMin: n,
                    chartRangeMax: n,
                    chartRangeMinX: n,
                    chartRangeMaxX: n,
                    tooltipFormat: new s('<span style="color: {{color}}">&#9679;</span> {{prefix}}{{y}}{{suffix}}')
                },
                bar: {
                    barColor: "#3366cc",
                    negBarColor: "#f44",
                    stackedBarColor: ["#3366cc", "#dc3912", "#ff9900", "#109618", "#66aa00", "#dd4477", "#0099c6", "#990099"],
                    zeroColor: n,
                    nullColor: n,
                    zeroAxis: !0,
                    barWidth: 4,
                    barSpacing: 1,
                    chartRangeMax: n,
                    chartRangeMin: n,
                    chartRangeClip: !1,
                    colorMap: n,
                    tooltipFormat: new s('<span style="color: {{color}}">&#9679;</span> {{prefix}}{{value}}{{suffix}}')
                },
                tristate: {
                    barWidth: 4,
                    barSpacing: 1,
                    posBarColor: "#6f6",
                    negBarColor: "#f44",
                    zeroBarColor: "#999",
                    colorMap: {},
                    tooltipFormat: new s('<span style="color: {{color}}">&#9679;</span> {{value:map}}'),
                    tooltipValueLookups: {
                        map: {
                            "-1": "Loss",
                            0: "Draw",
                            1: "Win"
                        }
                    }
                },
                discrete: {
                    lineHeight: "auto",
                    thresholdColor: n,
                    thresholdValue: 0,
                    chartRangeMax: n,
                    chartRangeMin: n,
                    chartRangeClip: !1,
                    tooltipFormat: new s("{{prefix}}{{value}}{{suffix}}")
                },
                bullet: {
                    targetColor: "#f33",
                    targetWidth: 3,
                    performanceColor: "#33f",
                    rangeColors: ["#d3dafe", "#a8b6ff", "#7f94ff"],
                    base: n,
                    tooltipFormat: new s("{{fieldkey:fields}} - {{value}}"),
                    tooltipValueLookups: {
                        fields: {
                            r: "Range",
                            p: "Performance",
                            t: "Target"
                        }
                    }
                },
                pie: {
                    offset: 0,
                    sliceColors: ["#3366cc", "#dc3912", "#ff9900", "#109618", "#66aa00", "#dd4477", "#0099c6", "#990099"],
                    borderWidth: 0,
                    borderColor: "#000",
                    tooltipFormat: new s('<span style="color: {{color}}">&#9679;</span> {{value}} ({{percent.1}}%)')
                },
                box: {
                    raw: !1,
                    boxLineColor: "#000",
                    boxFillColor: "#cdf",
                    whiskerColor: "#000",
                    outlierLineColor: "#333",
                    outlierFillColor: "#fff",
                    medianColor: "#f00",
                    showOutliers: !0,
                    outlierIQR: 1.5,
                    spotRadius: 1.5,
                    target: n,
                    targetColor: "#4a2",
                    chartRangeMax: n,
                    chartRangeMin: n,
                    tooltipFormat: new s("{{field:fields}}: {{value}}"),
                    tooltipFormatFieldlistKey: "field",
                    tooltipValueLookups: {
                        fields: {
                            lq: "Lower Quartile",
                            med: "Median",
                            uq: "Upper Quartile",
                            lo: "Left Outlier",
                            ro: "Right Outlier",
                            lw: "Left Whisker",
                            rw: "Right Whisker"
                        }
                    }
                }
            }
        }, _ = '.jqstooltip { position: absolute;left: 0px;top: 0px;visibility: hidden;background: rgb(0, 0, 0) transparent;background-color: rgba(0,0,0,0.6);filter:progid:DXImageTransform.Microsoft.gradient(startColorstr=#99000000, endColorstr=#99000000);-ms-filter: "progid:DXImageTransform.Microsoft.gradient(startColorstr=#99000000, endColorstr=#99000000)";color: white;font: 10px arial, san serif;text-align: left;white-space: nowrap;padding: 5px;border: 1px solid white;z-index: 10000;}.jqsfield { color: white;font: 10px arial, san serif;text-align: left;}', r = function() {
            var t, e;
            return t = function() {
                this.init.apply(this, arguments)
            }, arguments.length > 1 ? (arguments[0] ? (t.prototype = i.extend(new arguments[0], arguments[arguments.length - 1]), t._super = arguments[0].prototype) : t.prototype = arguments[arguments.length - 1], arguments.length > 2 && (e = Array.prototype.slice.call(arguments, 1, -1), e.unshift(t.prototype), i.extend.apply(i, e))) : t.prototype = arguments[0], t.prototype.cls = t, t
        }, i.SPFormatClass = s = r({
            fre: /\{\{([\w.]+?)(:(.+?))?\}\}/g,
            precre: /(\w+)\.(\d+)/,
            init: function(t, e) {
                this.format = t, this.fclass = e
            },
            render: function(t, e, i) {
                var o, r, s, a, l, c = this,
                    u = t;
                return this.format.replace(this.fre, function() {
                    var t;
                    return r = arguments[1], s = arguments[3], o = c.precre.exec(r), o ? (l = o[2], r = o[1]) : l = !1, a = u[r], a === n ? "" : s && e && e[s] ? (t = e[s], t.get ? e[s].get(a) || a : e[s][a] || a) : (h(a) && (a = i.get("numberFormatter") ? i.get("numberFormatter")(a) : v(a, l, i.get("numberDigitGroupCount"), i.get("numberDigitGroupSep"), i.get("numberDecimalMark"))), a)
                })
            }
        }), i.spformat = function(t, e) {
            return new s(t, e)
        }, a = function(t, e, n) {
            return e > t ? e : t > n ? n : t
        }, l = function(t, n) {
            var i;
            return 2 === n ? (i = e.floor(t.length / 2), t.length % 2 ? t[i] : (t[i - 1] + t[i]) / 2) : t.length % 2 ? (i = (t.length * n + n) / 4, i % 1 ? (t[e.floor(i)] + t[e.floor(i) - 1]) / 2 : t[i - 1]) : (i = (t.length * n + 2) / 4, i % 1 ? (t[e.floor(i)] + t[e.floor(i) - 1]) / 2 : t[i - 1])
        }, c = function(t) {
            var e;
            switch (t) {
                case "undefined":
                    t = n;
                    break;
                case "null":
                    t = null;
                    break;
                case "true":
                    t = !0;
                    break;
                case "false":
                    t = !1;
                    break;
                default:
                    e = parseFloat(t), t == e && (t = e)
            }
            return t
        }, u = function(t) {
            var e, n = [];
            for (e = t.length; e--;) n[e] = c(t[e]);
            return n
        }, d = function(t, e) {
            var n, i, o = [];
            for (n = 0, i = t.length; i > n; n++) t[n] !== e && o.push(t[n]);
            return o
        }, h = function(t) {
            return !isNaN(parseFloat(t)) && isFinite(t)
        }, v = function(t, e, n, o, r) {
            var s, a;
            for (t = (e === !1 ? parseFloat(t).toString() : t.toFixed(e)).split(""), s = (s = i.inArray(".", t)) < 0 ? t.length : s, s < t.length && (t[s] = r), a = s - n; a > 0; a -= n) t.splice(a, 0, o);
            return t.join("")
        }, f = function(t, e, n) {
            var i;
            for (i = e.length; i--;)
                if ((!n || null !== e[i]) && e[i] !== t) return !1;
            return !0
        }, p = function(t) {
            var e, n = 0;
            for (e = t.length; e--;) n += "number" == typeof t[e] ? t[e] : 0;
            return n
        }, m = function(t) {
            return i.isArray(t) ? t : [t]
        }, g = function(e) {
            var n;
            t.createStyleSheet ? t.createStyleSheet().cssText = e : (n = t.createElement("style"), n.type = "text/css", t.getElementsByTagName("head")[0].appendChild(n), n["string" == typeof t.body.style.WebkitAppearance ? "innerText" : "innerHTML"] = e)
        }, i.fn.simpledraw = function(e, o, r, s) {
            var a, l;
            if (r && (a = this.data("_jqs_vcanvas"))) return a;
            if (i.fn.sparkline.canvas === !1) return !1;
            if (i.fn.sparkline.canvas === n) {
                var c = t.createElement("canvas");
                if (c.getContext && c.getContext("2d")) i.fn.sparkline.canvas = function(t, e, n, i) {
                    return new A(t, e, n, i)
                };
                else {
                    if (!t.namespaces || t.namespaces.v) return i.fn.sparkline.canvas = !1, !1;
                    t.namespaces.add("v", "urn:schemas-microsoft-com:vml", "#default#VML"), i.fn.sparkline.canvas = function(t, e, n, i) {
                        return new L(t, e, n)
                    }
                }
            }
            return e === n && (e = i(this).innerWidth()), o === n && (o = i(this).innerHeight()), a = i.fn.sparkline.canvas(e, o, this, s), l = i(this).data("_jqs_mhandler"), l && l.registerCanvas(a), a
        }, i.fn.cleardraw = function() {
            var t = this.data("_jqs_vcanvas");
            t && t.reset()
        }, i.RangeMapClass = y = r({
            init: function(t) {
                var e, n, i = [];
                for (e in t) t.hasOwnProperty(e) && "string" == typeof e && e.indexOf(":") > -1 && (n = e.split(":"), n[0] = 0 === n[0].length ? -(1 / 0) : parseFloat(n[0]), n[1] = 0 === n[1].length ? 1 / 0 : parseFloat(n[1]), n[2] = t[e], i.push(n));
                this.map = t, this.rangelist = i || !1
            },
            get: function(t) {
                var e, i, o, r = this.rangelist;
                if ((o = this.map[t]) !== n) return o;
                if (r)
                    for (e = r.length; e--;)
                        if (i = r[e], i[0] <= t && i[1] >= t) return i[2];
                return n
            }
        }), i.range_map = function(t) {
            return new y(t)
        }, w = r({
            init: function(t, e) {
                var n = i(t);
                this.$el = n, this.options = e, this.currentPageX = 0, this.currentPageY = 0, this.el = t, this.splist = [], this.tooltip = null, this.over = !1, this.displayTooltips = !e.get("disableTooltips"), this.highlightEnabled = !e.get("disableHighlight")
            },
            registerSparkline: function(t) {
                this.splist.push(t), this.over && this.updateDisplay()
            },
            registerCanvas: function(t) {
                var e = i(t.canvas);
                this.canvas = t, this.$canvas = e, e.mouseenter(i.proxy(this.mouseenter, this)), e.mouseleave(i.proxy(this.mouseleave, this)), e.click(i.proxy(this.mouseclick, this))
            },
            reset: function(t) {
                this.splist = [], this.tooltip && t && (this.tooltip.remove(), this.tooltip = n)
            },
            mouseclick: function(t) {
                var e = i.Event("sparklineClick");
                e.originalEvent = t, e.sparklines = this.splist, this.$el.trigger(e)
            },
            mouseenter: function(e) {
                i(t.body).unbind("mousemove.jqs"), i(t.body).bind("mousemove.jqs", i.proxy(this.mousemove, this)), this.over = !0, this.currentPageX = e.pageX, this.currentPageY = e.pageY, this.currentEl = e.target, !this.tooltip && this.displayTooltips && (this.tooltip = new b(this.options), this.tooltip.updatePosition(e.pageX, e.pageY)), this.updateDisplay()
            },
            mouseleave: function() {
                i(t.body).unbind("mousemove.jqs");
                var e, n, o = this.splist,
                    r = o.length,
                    s = !1;
                for (this.over = !1, this.currentEl = null, this.tooltip && (this.tooltip.remove(), this.tooltip = null), n = 0; r > n; n++) e = o[n], e.clearRegionHighlight() && (s = !0);
                s && this.canvas.render()
            },
            mousemove: function(t) {
                this.currentPageX = t.pageX, this.currentPageY = t.pageY, this.currentEl = t.target, this.tooltip && this.tooltip.updatePosition(t.pageX, t.pageY), this.updateDisplay()
            },
            updateDisplay: function() {
                var t, e, n, o, r, s = this.splist,
                    a = s.length,
                    l = !1,
                    c = this.$canvas.offset(),
                    u = this.currentPageX - c.left,
                    d = this.currentPageY - c.top;
                if (this.over) {
                    for (n = 0; a > n; n++) e = s[n], o = e.setRegionHighlight(this.currentEl, u, d), o && (l = !0);
                    if (l) {
                        if (r = i.Event("sparklineRegionChange"), r.sparklines = this.splist, this.$el.trigger(r), this.tooltip) {
                            for (t = "", n = 0; a > n; n++) e = s[n], t += e.getCurrentRegionTooltip();
                            this.tooltip.setContent(t)
                        }
                        this.disableHighlight || this.canvas.render()
                    }
                    null === o && this.mouseleave()
                }
            }
        }), b = r({
            sizeStyle: "position: static !important;display: block !important;visibility: hidden !important;float: left !important;",
            init: function(e) {
                var n, o = e.get("tooltipClassname", "jqstooltip"),
                    r = this.sizeStyle;
                this.container = e.get("tooltipContainer") || t.body, this.tooltipOffsetX = e.get("tooltipOffsetX", 10), this.tooltipOffsetY = e.get("tooltipOffsetY", 12), i("#jqssizetip").remove(), i("#jqstooltip").remove(), this.sizetip = i("<div/>", {
                    id: "jqssizetip",
                    style: r,
                    "class": o
                }), this.tooltip = i("<div/>", {
                    id: "jqstooltip",
                    "class": o
                }).appendTo(this.container), n = this.tooltip.offset(), this.offsetLeft = n.left, this.offsetTop = n.top, this.hidden = !0, i(window).unbind("resize.jqs scroll.jqs"), i(window).bind("resize.jqs scroll.jqs", i.proxy(this.updateWindowDims, this)), this.updateWindowDims()
            },
            updateWindowDims: function() {
                this.scrollTop = i(window).scrollTop(), this.scrollLeft = i(window).scrollLeft(), this.scrollRight = this.scrollLeft + i(window).width(), this.updatePosition()
            },
            getSize: function(t) {
                this.sizetip.html(t).appendTo(this.container), this.width = this.sizetip.width() + 1, this.height = this.sizetip.height(), this.sizetip.remove()
            },
            setContent: function(t) {
                return t ? (this.getSize(t), this.tooltip.html(t).css({
                    width: this.width,
                    height: this.height,
                    visibility: "visible"
                }), this.hidden && (this.hidden = !1, this.updatePosition()), void 0) : (this.tooltip.css("visibility", "hidden"), void(this.hidden = !0))
            },
            updatePosition: function(t, e) {
                if (t === n) {
                    if (this.mousex === n) return;
                    t = this.mousex - this.offsetLeft, e = this.mousey - this.offsetTop
                } else this.mousex = t -= this.offsetLeft, this.mousey = e -= this.offsetTop;
                this.height && this.width && !this.hidden && (e -= this.height + this.tooltipOffsetY, t += this.tooltipOffsetX, e < this.scrollTop && (e = this.scrollTop), t < this.scrollLeft ? t = this.scrollLeft : t + this.width > this.scrollRight && (t = this.scrollRight - this.width), this.tooltip.css({
                    left: t,
                    top: e
                }))
            },
            remove: function() {
                this.tooltip.remove(), this.sizetip.remove(), this.sizetip = this.tooltip = n, i(window).unbind("resize.jqs scroll.jqs")
            }
        }), R = function() {
            g(_)
        }, i(R), N = [], i.fn.sparkline = function(e, o) {
            return this.each(function() {
                var r, s, a = new i.fn.sparkline.options(this, o),
                    l = i(this);
                if (r = function() {
                    var o, r, s, c, u, d, h;
                    return "html" === e || e === n ? (h = this.getAttribute(a.get("tagValuesAttribute")), (h === n || null === h) && (h = l.html()), o = h.replace(/(^\s*<!--)|(-->\s*$)|\s+/g, "").split(",")) : o = e, r = "auto" === a.get("width") ? o.length * a.get("defaultPixelsPerValue") : a.get("width"), "auto" === a.get("height") ? a.get("composite") && i.data(this, "_jqs_vcanvas") || (c = t.createElement("span"), c.innerHTML = "a", l.html(c), s = i(c).innerHeight() || i(c).height(), i(c).remove(), c = null) : s = a.get("height"), a.get("disableInteraction") ? u = !1 : (u = i.data(this, "_jqs_mhandler"), u ? a.get("composite") || u.reset() : (u = new w(this, a), i.data(this, "_jqs_mhandler", u))), a.get("composite") && !i.data(this, "_jqs_vcanvas") ? void(i.data(this, "_jqs_errnotify") || (alert("Attempted to attach a composite sparkline to an element with no existing sparkline"), i.data(this, "_jqs_errnotify", !0))) : (d = new(i.fn.sparkline[a.get("type")])(this, o, a, r, s), d.render(), u && u.registerSparkline(d), void 0)
                }, i(this).html() && !a.get("disableHiddenCheck") && i(this).is(":hidden") || !i(this).parents("body").length) {
                    if (!a.get("composite") && i.data(this, "_jqs_pending"))
                        for (s = N.length; s; s--) N[s - 1][0] == this && N.splice(s - 1, 1);
                    N.push([this, r]), i.data(this, "_jqs_pending", !0)
                } else r.call(this)
            })
        }, i.fn.sparkline.defaults = o(), i.sparkline_display_visible = function() {
            var t, e, n, o = [];
            for (e = 0, n = N.length; n > e; e++) t = N[e][0], i(t).is(":visible") && !i(t).parents().is(":hidden") ? (N[e][1].call(t), i.data(N[e][0], "_jqs_pending", !1), o.push(e)) : !i(t).closest("html").length && !i.data(t, "_jqs_pending") && (i.data(N[e][0], "_jqs_pending", !1), o.push(e));
            for (e = o.length; e; e--) N.splice(o[e - 1], 1)
        }, i.fn.sparkline.options = r({
            init: function(t, e) {
                var n, o, r, s;
                this.userOptions = e = e || {}, this.tag = t, this.tagValCache = {}, o = i.fn.sparkline.defaults, r = o.common, this.tagOptionsPrefix = e.enableTagOptions && (e.tagOptionsPrefix || r.tagOptionsPrefix), s = this.getTagSetting("type"), n = s === O ? o[e.type || r.type] : o[s], this.mergedOptions = i.extend({}, r, n, e)
            },
            getTagSetting: function(t) {
                var e, i, o, r, s = this.tagOptionsPrefix;
                if (s === !1 || s === n) return O;
                if (this.tagValCache.hasOwnProperty(t)) e = this.tagValCache.key;
                else {
                    if (e = this.tag.getAttribute(s + t), e === n || null === e) e = O;
                    else if ("[" === e.substr(0, 1))
                        for (e = e.substr(1, e.length - 2).split(","), i = e.length; i--;) e[i] = c(e[i].replace(/(^\s*)|(\s*$)/g, ""));
                    else if ("{" === e.substr(0, 1))
                        for (o = e.substr(1, e.length - 2).split(","), e = {}, i = o.length; i--;) r = o[i].split(":", 2), e[r[0].replace(/(^\s*)|(\s*$)/g, "")] = c(r[1].replace(/(^\s*)|(\s*$)/g, ""));
                    else e = c(e);
                    this.tagValCache.key = e
                }
                return e
            },
            get: function(t, e) {
                var i, o = this.getTagSetting(t);
                return o !== O ? o : (i = this.mergedOptions[t]) === n ? e : i
            }
        }), i.fn.sparkline._base = r({
            disabled: !1,
            init: function(t, e, o, r, s) {
                this.el = t, this.$el = i(t), this.values = e, this.options = o, this.width = r, this.height = s, this.currentRegion = n
            },
            initTarget: function() {
                var t = !this.options.get("disableInteraction");
                (this.target = this.$el.simpledraw(this.width, this.height, this.options.get("composite"), t)) ? (this.canvasWidth = this.target.pixelWidth, this.canvasHeight = this.target.pixelHeight) : this.disabled = !0
            },
            render: function() {
                return this.disabled ? (this.el.innerHTML = "", !1) : !0
            },
            getRegion: function(t, e) {},
            setRegionHighlight: function(t, e, i) {
                var o, r = this.currentRegion,
                    s = !this.options.get("disableHighlight");
                return e > this.canvasWidth || i > this.canvasHeight || 0 > e || 0 > i ? null : (o = this.getRegion(t, e, i), r !== o ? (r !== n && s && this.removeHighlight(), this.currentRegion = o, o !== n && s && this.renderHighlight(), !0) : !1)
            },
            clearRegionHighlight: function() {
                return this.currentRegion !== n ? (this.removeHighlight(), this.currentRegion = n, !0) : !1
            },
            renderHighlight: function() {
                this.changeHighlight(!0)
            },
            removeHighlight: function() {
                this.changeHighlight(!1)
            },
            changeHighlight: function(t) {},
            getCurrentRegionTooltip: function() {
                var t, e, o, r, a, l, c, u, d, h, f, p, g, m, v = this.options,
                    y = "",
                    w = [];
                if (this.currentRegion === n) return "";
                if (t = this.getCurrentRegionFields(), f = v.get("tooltipFormatter")) return f(this, v, t);
                if (v.get("tooltipChartTitle") && (y += '<div class="jqs jqstitle">' + v.get("tooltipChartTitle") + "</div>\n"), e = this.options.get("tooltipFormat"), !e) return "";
                if (i.isArray(e) || (e = [e]), i.isArray(t) || (t = [t]), c = this.options.get("tooltipFormatFieldlist"), u = this.options.get("tooltipFormatFieldlistKey"), c && u) {
                    for (d = [], l = t.length; l--;) h = t[l][u], -1 != (m = i.inArray(h, c)) && (d[m] = t[l]);
                    t = d
                }
                for (o = e.length, g = t.length, l = 0; o > l; l++)
                    for (p = e[l], "string" == typeof p && (p = new s(p)), r = p.fclass || "jqsfield", m = 0; g > m; m++) t[m].isNull && v.get("tooltipSkipNull") || (i.extend(t[m], {
                        prefix: v.get("tooltipPrefix"),
                        suffix: v.get("tooltipSuffix")
                    }), a = p.render(t[m], v.get("tooltipValueLookups"), v), w.push('<div class="' + r + '">' + a + "</div>"));
                return w.length ? y + w.join("\n") : ""
            },
            getCurrentRegionFields: function() {},
            calcHighlightColor: function(t, n) {
                var i, o, r, s, l = n.get("highlightColor"),
                    c = n.get("highlightLighten");
                if (l) return l;
                if (c && (i = /^#([0-9a-f])([0-9a-f])([0-9a-f])$/i.exec(t) || /^#([0-9a-f]{2})([0-9a-f]{2})([0-9a-f]{2})$/i.exec(t))) {
                    for (r = [], o = 4 === t.length ? 16 : 1, s = 0; 3 > s; s++) r[s] = a(e.round(parseInt(i[s + 1], 16) * o * c), 0, 255);
                    return "rgb(" + r.join(",") + ")"
                }
                return t
            }
        }), x = {
            changeHighlight: function(t) {
                var e, n = this.currentRegion,
                    o = this.target,
                    r = this.regionShapes[n];
                r && (e = this.renderRegion(n, t), i.isArray(e) || i.isArray(r) ? (o.replaceWithShapes(r, e), this.regionShapes[n] = i.map(e, function(t) {
                    return t.id
                })) : (o.replaceWithShape(r, e), this.regionShapes[n] = e.id))
            },
            render: function() {
                var t, e, n, o, r = this.values,
                    s = this.target,
                    a = this.regionShapes;
                if (this.cls._super.render.call(this)) {
                    for (n = r.length; n--;)
                        if (t = this.renderRegion(n))
                            if (i.isArray(t)) {
                                for (e = [], o = t.length; o--;) t[o].append(), e.push(t[o].id);
                                a[n] = e
                            } else t.append(), a[n] = t.id;
                    else a[n] = null;
                    s.render()
                }
            }
        }, i.fn.sparkline.line = S = r(i.fn.sparkline._base, {
            type: "line",
            init: function(t, e, n, i, o) {
                S._super.init.call(this, t, e, n, i, o), this.vertices = [], this.regionMap = [], this.xvalues = [], this.yvalues = [], this.yminmax = [], this.hightlightSpotId = null, this.lastShapeId = null, this.initTarget()
            },
            getRegion: function(t, e, i) {
                var o, r = this.regionMap;
                for (o = r.length; o--;)
                    if (null !== r[o] && e >= r[o][0] && e <= r[o][1]) return r[o][2];
                return n
            },
            getCurrentRegionFields: function() {
                var t = this.currentRegion;
                return {
                    isNull: null === this.yvalues[t],
                    x: this.xvalues[t],
                    y: this.yvalues[t],
                    color: this.options.get("lineColor"),
                    fillColor: this.options.get("fillColor"),
                    offset: t
                }
            },
            renderHighlight: function() {
                var t, e, i = this.currentRegion,
                    o = this.target,
                    r = this.vertices[i],
                    s = this.options,
                    a = s.get("spotRadius"),
                    l = s.get("highlightSpotColor"),
                    c = s.get("highlightLineColor");
                r && (a && l && (t = o.drawCircle(r[0], r[1], a, n, l), this.highlightSpotId = t.id, o.insertAfterShape(this.lastShapeId, t)), c && (e = o.drawLine(r[0], this.canvasTop, r[0], this.canvasTop + this.canvasHeight, c), this.highlightLineId = e.id, o.insertAfterShape(this.lastShapeId, e)))
            },
            removeHighlight: function() {
                var t = this.target;
                this.highlightSpotId && (t.removeShapeId(this.highlightSpotId), this.highlightSpotId = null), this.highlightLineId && (t.removeShapeId(this.highlightLineId), this.highlightLineId = null)
            },
            scanValues: function() {
                var t, n, i, o, r, s = this.values,
                    a = s.length,
                    l = this.xvalues,
                    c = this.yvalues,
                    u = this.yminmax;
                for (t = 0; a > t; t++) n = s[t], i = "string" == typeof s[t], o = "object" == typeof s[t] && s[t] instanceof Array, r = i && s[t].split(":"), i && 2 === r.length ? (l.push(Number(r[0])), c.push(Number(r[1])), u.push(Number(r[1]))) : o ? (l.push(n[0]), c.push(n[1]), u.push(n[1])) : (l.push(t), null === s[t] || "null" === s[t] ? c.push(null) : (c.push(Number(n)), u.push(Number(n))));
                this.options.get("xvalues") && (l = this.options.get("xvalues")), this.maxy = this.maxyorg = e.max.apply(e, u), this.miny = this.minyorg = e.min.apply(e, u), this.maxx = e.max.apply(e, l), this.minx = e.min.apply(e, l), this.xvalues = l, this.yvalues = c, this.yminmax = u
            },
            processRangeOptions: function() {
                var t = this.options,
                    e = t.get("normalRangeMin"),
                    i = t.get("normalRangeMax");
                e !== n && (e < this.miny && (this.miny = e), i > this.maxy && (this.maxy = i)), t.get("chartRangeMin") !== n && (t.get("chartRangeClip") || t.get("chartRangeMin") < this.miny) && (this.miny = t.get("chartRangeMin")), t.get("chartRangeMax") !== n && (t.get("chartRangeClip") || t.get("chartRangeMax") > this.maxy) && (this.maxy = t.get("chartRangeMax")), t.get("chartRangeMinX") !== n && (t.get("chartRangeClipX") || t.get("chartRangeMinX") < this.minx) && (this.minx = t.get("chartRangeMinX")), t.get("chartRangeMaxX") !== n && (t.get("chartRangeClipX") || t.get("chartRangeMaxX") > this.maxx) && (this.maxx = t.get("chartRangeMaxX"))
            },
            drawNormalRange: function(t, i, o, r, s) {
                var a = this.options.get("normalRangeMin"),
                    l = this.options.get("normalRangeMax"),
                    c = i + e.round(o - o * ((l - this.miny) / s)),
                    u = e.round(o * (l - a) / s);
                this.target.drawRect(t, c, r, u, n, this.options.get("normalRangeColor")).append()
            },
            render: function() {
                var t, o, r, s, a, l, c, u, d, h, f, p, g, m, v, w, b, x, T, C, k, E, D, M, _, R = this.options,
                    H = this.target,
                    z = this.canvasWidth,
                    A = this.canvasHeight,
                    L = this.vertices,
                    N = R.get("spotRadius"),
                    O = this.regionMap;
                if (S._super.render.call(this) && (this.scanValues(), this.processRangeOptions(), D = this.xvalues, M = this.yvalues, this.yminmax.length && !(this.yvalues.length < 2))) {
                    for (s = a = 0, t = this.maxx - this.minx === 0 ? 1 : this.maxx - this.minx, o = this.maxy - this.miny === 0 ? 1 : this.maxy - this.miny, r = this.yvalues.length - 1, N && (4 * N > z || 4 * N > A) && (N = 0), N && (k = R.get("highlightSpotColor") && !R.get("disableInteraction"), (k || R.get("minSpotColor") || R.get("spotColor") && M[r] === this.miny) && (A -= e.ceil(N)), (k || R.get("maxSpotColor") || R.get("spotColor") && M[r] === this.maxy) && (A -= e.ceil(N), s += e.ceil(N)), (k || (R.get("minSpotColor") || R.get("maxSpotColor")) && (M[0] === this.miny || M[0] === this.maxy)) && (a += e.ceil(N), z -= e.ceil(N)), (k || R.get("spotColor") || R.get("minSpotColor") || R.get("maxSpotColor") && (M[r] === this.miny || M[r] === this.maxy)) && (z -= e.ceil(N))), A--, R.get("normalRangeMin") !== n && !R.get("drawNormalOnTop") && this.drawNormalRange(a, s, A, z, o), c = [], u = [c], m = v = null, w = M.length, _ = 0; w > _; _++) d = D[_], f = D[_ + 1], h = M[_], p = a + e.round((d - this.minx) * (z / t)), g = w - 1 > _ ? a + e.round((f - this.minx) * (z / t)) : z, v = p + (g - p) / 2, O[_] = [m || 0, v, _], m = v, null === h ? _ && (null !== M[_ - 1] && (c = [], u.push(c)), L.push(null)) : (h < this.miny && (h = this.miny), h > this.maxy && (h = this.maxy), c.length || c.push([p, s + A]), l = [p, s + e.round(A - A * ((h - this.miny) / o))], c.push(l), L.push(l));
                    for (b = [], x = [], T = u.length, _ = 0; T > _; _++) c = u[_], c.length && (R.get("fillColor") && (c.push([c[c.length - 1][0], s + A]), x.push(c.slice(0)), c.pop()), c.length > 2 && (c[0] = [c[0][0], c[1][1]]), b.push(c));
                    for (T = x.length, _ = 0; T > _; _++) H.drawShape(x[_], R.get("fillColor"), R.get("fillColor")).append();
                    for (R.get("normalRangeMin") !== n && R.get("drawNormalOnTop") && this.drawNormalRange(a, s, A, z, o), T = b.length, _ = 0; T > _; _++) H.drawShape(b[_], R.get("lineColor"), n, R.get("lineWidth")).append();
                    if (N && R.get("valueSpots"))
                        for (C = R.get("valueSpots"), C.get === n && (C = new y(C)), _ = 0; w > _; _++) E = C.get(M[_]), E && H.drawCircle(a + e.round((D[_] - this.minx) * (z / t)), s + e.round(A - A * ((M[_] - this.miny) / o)), N, n, E).append();
                    N && R.get("spotColor") && null !== M[r] && H.drawCircle(a + e.round((D[D.length - 1] - this.minx) * (z / t)), s + e.round(A - A * ((M[r] - this.miny) / o)), N, n, R.get("spotColor")).append(), this.maxy !== this.minyorg && (N && R.get("minSpotColor") && (d = D[i.inArray(this.minyorg, M)], H.drawCircle(a + e.round((d - this.minx) * (z / t)), s + e.round(A - A * ((this.minyorg - this.miny) / o)), N, n, R.get("minSpotColor")).append()), N && R.get("maxSpotColor") && (d = D[i.inArray(this.maxyorg, M)], H.drawCircle(a + e.round((d - this.minx) * (z / t)), s + e.round(A - A * ((this.maxyorg - this.miny) / o)), N, n, R.get("maxSpotColor")).append())), this.lastShapeId = H.getLastShapeId(), this.canvasTop = s, H.render()
                }
            }
        }), i.fn.sparkline.bar = T = r(i.fn.sparkline._base, x, {
            type: "bar",
            init: function(t, o, r, s, l) {
                var h, f, p, g, m, v, w, b, x, S, C, k, E, D, M, _, R, H, z, A, L, N, O = parseInt(r.get("barWidth"), 10),
                    I = parseInt(r.get("barSpacing"), 10),
                    P = r.get("chartRangeMin"),
                    F = r.get("chartRangeMax"),
                    W = r.get("chartRangeClip"),
                    j = 1 / 0,
                    Y = -(1 / 0);
                for (T._super.init.call(this, t, o, r, s, l), v = 0, w = o.length; w > v; v++) A = o[v], h = "string" == typeof A && A.indexOf(":") > -1, (h || i.isArray(A)) && (M = !0, h && (A = o[v] = u(A.split(":"))), A = d(A, null), f = e.min.apply(e, A), p = e.max.apply(e, A), j > f && (j = f), p > Y && (Y = p));
                this.stacked = M, this.regionShapes = {}, this.barWidth = O, this.barSpacing = I, this.totalBarWidth = O + I, this.width = s = o.length * O + (o.length - 1) * I, this.initTarget(), W && (E = P === n ? -(1 / 0) : P, D = F === n ? 1 / 0 : F), m = [], g = M ? [] : m;
                var $ = [],
                    q = [];
                for (v = 0, w = o.length; w > v; v++)
                    if (M)
                        for (_ = o[v], o[v] = z = [], $[v] = 0, g[v] = q[v] = 0, R = 0, H = _.length; H > R; R++) A = z[R] = W ? a(_[R], E, D) : _[R], null !== A && (A > 0 && ($[v] += A), 0 > j && Y > 0 ? 0 > A ? q[v] += e.abs(A) : g[v] += A : g[v] += e.abs(A - (0 > A ? Y : j)), m.push(A));
                    else A = W ? a(o[v], E, D) : o[v], A = o[v] = c(A), null !== A && m.push(A);
                this.max = k = e.max.apply(e, m), this.min = C = e.min.apply(e, m), this.stackMax = Y = M ? e.max.apply(e, $) : k, this.stackMin = j = M ? e.min.apply(e, m) : C, r.get("chartRangeMin") !== n && (r.get("chartRangeClip") || r.get("chartRangeMin") < C) && (C = r.get("chartRangeMin")), r.get("chartRangeMax") !== n && (r.get("chartRangeClip") || r.get("chartRangeMax") > k) && (k = r.get("chartRangeMax")), this.zeroAxis = x = r.get("zeroAxis", !0), S = 0 >= C && k >= 0 && x ? 0 : 0 == x ? C : C > 0 ? C : k, this.xaxisOffset = S, b = M ? e.max.apply(e, g) + e.max.apply(e, q) : k - C, this.canvasHeightEf = x && 0 > C ? this.canvasHeight - 2 : this.canvasHeight - 1, S > C ? (N = M && k >= 0 ? Y : k, L = (N - S) / b * this.canvasHeight, L !== e.ceil(L) && (this.canvasHeightEf -= 2, L = e.ceil(L))) : L = this.canvasHeight, this.yoffset = L, i.isArray(r.get("colorMap")) ? (this.colorMapByIndex = r.get("colorMap"), this.colorMapByValue = null) : (this.colorMapByIndex = null, this.colorMapByValue = r.get("colorMap"), this.colorMapByValue && this.colorMapByValue.get === n && (this.colorMapByValue = new y(this.colorMapByValue))), this.range = b
            },
            getRegion: function(t, i, o) {
                var r = e.floor(i / this.totalBarWidth);
                return 0 > r || r >= this.values.length ? n : r
            },
            getCurrentRegionFields: function() {
                var t, e, n = this.currentRegion,
                    i = m(this.values[n]),
                    o = [];
                for (e = i.length; e--;) t = i[e], o.push({
                    isNull: null === t,
                    value: t,
                    color: this.calcColor(e, t, n),
                    offset: n
                });
                return o
            },
            calcColor: function(t, e, o) {
                var r, s, a = this.colorMapByIndex,
                    l = this.colorMapByValue,
                    c = this.options;
                return r = c.get(this.stacked ? "stackedBarColor" : 0 > e ? "negBarColor" : "barColor"), 0 === e && c.get("zeroColor") !== n && (r = c.get("zeroColor")), l && (s = l.get(e)) ? r = s : a && a.length > o && (r = a[o]), i.isArray(r) ? r[t % r.length] : r
            },
            renderRegion: function(t, o) {
                var r, s, a, l, c, u, d, h, p, g, m = this.values[t],
                    v = this.options,
                    y = this.xaxisOffset,
                    w = [],
                    b = this.range,
                    x = this.stacked,
                    S = this.target,
                    T = t * this.totalBarWidth,
                    C = this.canvasHeightEf,
                    k = this.yoffset;
                if (m = i.isArray(m) ? m : [m], d = m.length, h = m[0], l = f(null, m), g = f(y, m, !0), l) return v.get("nullColor") ? (a = o ? v.get("nullColor") : this.calcHighlightColor(v.get("nullColor"), v), r = k > 0 ? k - 1 : k, S.drawRect(T, r, this.barWidth - 1, 0, a, a)) : n;
                for (c = k, u = 0; d > u; u++) {
                    if (h = m[u], x && h === y) {
                        if (!g || p) continue;
                        p = !0
                    }
                    s = b > 0 ? e.floor(C * (e.abs(h - y) / b)) + 1 : 1, y > h || h === y && 0 === k ? (r = c, c += s) : (r = k - s, k -= s), a = this.calcColor(u, h, t), o && (a = this.calcHighlightColor(a, v)), w.push(S.drawRect(T, r, this.barWidth - 1, s - 1, a, a))
                }
                return 1 === w.length ? w[0] : w
            }
        }), i.fn.sparkline.tristate = C = r(i.fn.sparkline._base, x, {
            type: "tristate",
            init: function(t, e, o, r, s) {
                var a = parseInt(o.get("barWidth"), 10),
                    l = parseInt(o.get("barSpacing"), 10);
                C._super.init.call(this, t, e, o, r, s), this.regionShapes = {}, this.barWidth = a, this.barSpacing = l, this.totalBarWidth = a + l, this.values = i.map(e, Number), this.width = r = e.length * a + (e.length - 1) * l, i.isArray(o.get("colorMap")) ? (this.colorMapByIndex = o.get("colorMap"), this.colorMapByValue = null) : (this.colorMapByIndex = null, this.colorMapByValue = o.get("colorMap"), this.colorMapByValue && this.colorMapByValue.get === n && (this.colorMapByValue = new y(this.colorMapByValue))), this.initTarget()
            },
            getRegion: function(t, n, i) {
                return e.floor(n / this.totalBarWidth)
            },
            getCurrentRegionFields: function() {
                var t = this.currentRegion;
                return {
                    isNull: this.values[t] === n,
                    value: this.values[t],
                    color: this.calcColor(this.values[t], t),
                    offset: t
                }
            },
            calcColor: function(t, e) {
                var n, i, o = this.values,
                    r = this.options,
                    s = this.colorMapByIndex,
                    a = this.colorMapByValue;
                return n = a && (i = a.get(t)) ? i : s && s.length > e ? s[e] : r.get(o[e] < 0 ? "negBarColor" : o[e] > 0 ? "posBarColor" : "zeroBarColor")
            },
            renderRegion: function(t, n) {
                var i, o, r, s, a, l, c = this.values,
                    u = this.options,
                    d = this.target;
                return i = d.pixelHeight, r = e.round(i / 2), s = t * this.totalBarWidth, c[t] < 0 ? (a = r, o = r - 1) : c[t] > 0 ? (a = 0, o = r - 1) : (a = r - 1, o = 2), l = this.calcColor(c[t], t), null !== l ? (n && (l = this.calcHighlightColor(l, u)), d.drawRect(s, a, this.barWidth - 1, o - 1, l, l)) : void 0
            }
        }), i.fn.sparkline.discrete = k = r(i.fn.sparkline._base, x, {
            type: "discrete",
            init: function(t, o, r, s, a) {
                k._super.init.call(this, t, o, r, s, a), this.regionShapes = {}, this.values = o = i.map(o, Number), this.min = e.min.apply(e, o), this.max = e.max.apply(e, o), this.range = this.max - this.min, this.width = s = "auto" === r.get("width") ? 2 * o.length : this.width, this.interval = e.floor(s / o.length), this.itemWidth = s / o.length, r.get("chartRangeMin") !== n && (r.get("chartRangeClip") || r.get("chartRangeMin") < this.min) && (this.min = r.get("chartRangeMin")), r.get("chartRangeMax") !== n && (r.get("chartRangeClip") || r.get("chartRangeMax") > this.max) && (this.max = r.get("chartRangeMax")), this.initTarget(), this.target && (this.lineHeight = "auto" === r.get("lineHeight") ? e.round(.3 * this.canvasHeight) : r.get("lineHeight"))
            },
            getRegion: function(t, n, i) {
                return e.floor(n / this.itemWidth)
            },
            getCurrentRegionFields: function() {
                var t = this.currentRegion;
                return {
                    isNull: this.values[t] === n,
                    value: this.values[t],
                    offset: t
                }
            },
            renderRegion: function(t, n) {
                var i, o, r, s, l = this.values,
                    c = this.options,
                    u = this.min,
                    d = this.max,
                    h = this.range,
                    f = this.interval,
                    p = this.target,
                    g = this.canvasHeight,
                    m = this.lineHeight,
                    v = g - m;
                return o = a(l[t], u, d), s = t * f, i = e.round(v - v * ((o - u) / h)), r = c.get(c.get("thresholdColor") && o < c.get("thresholdValue") ? "thresholdColor" : "lineColor"), n && (r = this.calcHighlightColor(r, c)), p.drawLine(s, i, s, i + m, r)
            }
        }), i.fn.sparkline.bullet = E = r(i.fn.sparkline._base, {
            type: "bullet",
            init: function(t, i, o, r, s) {
                var a, l, c;
                E._super.init.call(this, t, i, o, r, s), this.values = i = u(i), c = i.slice(), c[0] = null === c[0] ? c[2] : c[0], c[1] = null === i[1] ? c[2] : c[1], a = e.min.apply(e, i), l = e.max.apply(e, i), a = o.get("base") === n ? 0 > a ? a : 0 : o.get("base"), this.min = a, this.max = l, this.range = l - a, this.shapes = {}, this.valueShapes = {}, this.regiondata = {}, this.width = r = "auto" === o.get("width") ? "4.0em" : r, this.target = this.$el.simpledraw(r, s, o.get("composite")), i.length || (this.disabled = !0), this.initTarget()
            },
            getRegion: function(t, e, i) {
                var o = this.target.getShapeAt(t, e, i);
                return o !== n && this.shapes[o] !== n ? this.shapes[o] : n
            },
            getCurrentRegionFields: function() {
                var t = this.currentRegion;
                return {
                    fieldkey: t.substr(0, 1),
                    value: this.values[t.substr(1)],
                    region: t
                }
            },
            changeHighlight: function(t) {
                var e, n = this.currentRegion,
                    i = this.valueShapes[n];
                switch (delete this.shapes[i], n.substr(0, 1)) {
                    case "r":
                        e = this.renderRange(n.substr(1), t);
                        break;
                    case "p":
                        e = this.renderPerformance(t);
                        break;
                    case "t":
                        e = this.renderTarget(t)
                }
                this.valueShapes[n] = e.id, this.shapes[e.id] = n, this.target.replaceWithShape(i, e)
            },
            renderRange: function(t, n) {
                var i = this.values[t],
                    o = e.round(this.canvasWidth * ((i - this.min) / this.range)),
                    r = this.options.get("rangeColors")[t - 2];
                return n && (r = this.calcHighlightColor(r, this.options)), this.target.drawRect(0, 0, o - 1, this.canvasHeight - 1, r, r)
            },
            renderPerformance: function(t) {
                var n = this.values[1],
                    i = e.round(this.canvasWidth * ((n - this.min) / this.range)),
                    o = this.options.get("performanceColor");
                return t && (o = this.calcHighlightColor(o, this.options)), this.target.drawRect(0, e.round(.3 * this.canvasHeight), i - 1, e.round(.4 * this.canvasHeight) - 1, o, o);

            },
            renderTarget: function(t) {
                var n = this.values[0],
                    i = e.round(this.canvasWidth * ((n - this.min) / this.range) - this.options.get("targetWidth") / 2),
                    o = e.round(.1 * this.canvasHeight),
                    r = this.canvasHeight - 2 * o,
                    s = this.options.get("targetColor");
                return t && (s = this.calcHighlightColor(s, this.options)), this.target.drawRect(i, o, this.options.get("targetWidth") - 1, r - 1, s, s)
            },
            render: function() {
                var t, e, n = this.values.length,
                    i = this.target;
                if (E._super.render.call(this)) {
                    for (t = 2; n > t; t++) e = this.renderRange(t).append(), this.shapes[e.id] = "r" + t, this.valueShapes["r" + t] = e.id;
                    null !== this.values[1] && (e = this.renderPerformance().append(), this.shapes[e.id] = "p1", this.valueShapes.p1 = e.id), null !== this.values[0] && (e = this.renderTarget().append(), this.shapes[e.id] = "t0", this.valueShapes.t0 = e.id), i.render()
                }
            }
        }), i.fn.sparkline.pie = D = r(i.fn.sparkline._base, {
            type: "pie",
            init: function(t, n, o, r, s) {
                var a, l = 0;
                if (D._super.init.call(this, t, n, o, r, s), this.shapes = {}, this.valueShapes = {}, this.values = n = i.map(n, Number), "auto" === o.get("width") && (this.width = this.height), n.length > 0)
                    for (a = n.length; a--;) l += n[a];
                this.total = l, this.initTarget(), this.radius = e.floor(e.min(this.canvasWidth, this.canvasHeight) / 2)
            },
            getRegion: function(t, e, i) {
                var o = this.target.getShapeAt(t, e, i);
                return o !== n && this.shapes[o] !== n ? this.shapes[o] : n
            },
            getCurrentRegionFields: function() {
                var t = this.currentRegion;
                return {
                    isNull: this.values[t] === n,
                    value: this.values[t],
                    percent: this.values[t] / this.total * 100,
                    color: this.options.get("sliceColors")[t % this.options.get("sliceColors").length],
                    offset: t
                }
            },
            changeHighlight: function(t) {
                var e = this.currentRegion,
                    n = this.renderSlice(e, t),
                    i = this.valueShapes[e];
                delete this.shapes[i], this.target.replaceWithShape(i, n), this.valueShapes[e] = n.id, this.shapes[n.id] = e
            },
            renderSlice: function(t, i) {
                var o, r, s, a, l, c = this.target,
                    u = this.options,
                    d = this.radius,
                    h = u.get("borderWidth"),
                    f = u.get("offset"),
                    p = 2 * e.PI,
                    g = this.values,
                    m = this.total,
                    v = f ? 2 * e.PI * (f / 360) : 0;
                for (a = g.length, s = 0; a > s; s++) {
                    if (o = v, r = v, m > 0 && (r = v + p * (g[s] / m)), t === s) return l = u.get("sliceColors")[s % u.get("sliceColors").length], i && (l = this.calcHighlightColor(l, u)), c.drawPieSlice(d, d, d - h, o, r, n, l);
                    v = r
                }
            },
            render: function() {
                var t, i, o = this.target,
                    r = this.values,
                    s = this.options,
                    a = this.radius,
                    l = s.get("borderWidth");
                if (D._super.render.call(this)) {
                    for (l && o.drawCircle(a, a, e.floor(a - l / 2), s.get("borderColor"), n, l).append(), i = r.length; i--;) r[i] && (t = this.renderSlice(i).append(), this.valueShapes[i] = t.id, this.shapes[t.id] = i);
                    o.render()
                }
            }
        }), i.fn.sparkline.box = M = r(i.fn.sparkline._base, {
            type: "box",
            init: function(t, e, n, o, r) {
                M._super.init.call(this, t, e, n, o, r), this.values = i.map(e, Number), this.width = "auto" === n.get("width") ? "4.0em" : o, this.initTarget(), this.values.length || (this.disabled = 1)
            },
            getRegion: function() {
                return 1
            },
            getCurrentRegionFields: function() {
                var t = [{
                    field: "lq",
                    value: this.quartiles[0]
                }, {
                    field: "med",
                    value: this.quartiles[1]
                }, {
                    field: "uq",
                    value: this.quartiles[2]
                }];
                return this.loutlier !== n && t.push({
                    field: "lo",
                    value: this.loutlier
                }), this.routlier !== n && t.push({
                    field: "ro",
                    value: this.routlier
                }), this.lwhisker !== n && t.push({
                    field: "lw",
                    value: this.lwhisker
                }), this.rwhisker !== n && t.push({
                    field: "rw",
                    value: this.rwhisker
                }), t
            },
            render: function() {
                var t, i, o, r, s, a, c, u, d, h, f, p = this.target,
                    g = this.values,
                    m = g.length,
                    v = this.options,
                    y = this.canvasWidth,
                    w = this.canvasHeight,
                    b = v.get("chartRangeMin") === n ? e.min.apply(e, g) : v.get("chartRangeMin"),
                    x = v.get("chartRangeMax") === n ? e.max.apply(e, g) : v.get("chartRangeMax"),
                    S = 0;
                if (M._super.render.call(this)) {
                    if (v.get("raw")) v.get("showOutliers") && g.length > 5 ? (i = g[0], t = g[1], r = g[2], s = g[3], a = g[4], c = g[5], u = g[6]) : (t = g[0], r = g[1], s = g[2], a = g[3], c = g[4]);
                    else if (g.sort(function(t, e) {
                        return t - e
                    }), r = l(g, 1), s = l(g, 2), a = l(g, 3), o = a - r, v.get("showOutliers")) {
                        for (t = c = n, d = 0; m > d; d++) t === n && g[d] > r - o * v.get("outlierIQR") && (t = g[d]), g[d] < a + o * v.get("outlierIQR") && (c = g[d]);
                        i = g[0], u = g[m - 1]
                    } else t = g[0], c = g[m - 1];
                    this.quartiles = [r, s, a], this.lwhisker = t, this.rwhisker = c, this.loutlier = i, this.routlier = u, f = y / (x - b + 1), v.get("showOutliers") && (S = e.ceil(v.get("spotRadius")), y -= 2 * e.ceil(v.get("spotRadius")), f = y / (x - b + 1), t > i && p.drawCircle((i - b) * f + S, w / 2, v.get("spotRadius"), v.get("outlierLineColor"), v.get("outlierFillColor")).append(), u > c && p.drawCircle((u - b) * f + S, w / 2, v.get("spotRadius"), v.get("outlierLineColor"), v.get("outlierFillColor")).append()), p.drawRect(e.round((r - b) * f + S), e.round(.1 * w), e.round((a - r) * f), e.round(.8 * w), v.get("boxLineColor"), v.get("boxFillColor")).append(), p.drawLine(e.round((t - b) * f + S), e.round(w / 2), e.round((r - b) * f + S), e.round(w / 2), v.get("lineColor")).append(), p.drawLine(e.round((t - b) * f + S), e.round(w / 4), e.round((t - b) * f + S), e.round(w - w / 4), v.get("whiskerColor")).append(), p.drawLine(e.round((c - b) * f + S), e.round(w / 2), e.round((a - b) * f + S), e.round(w / 2), v.get("lineColor")).append(), p.drawLine(e.round((c - b) * f + S), e.round(w / 4), e.round((c - b) * f + S), e.round(w - w / 4), v.get("whiskerColor")).append(), p.drawLine(e.round((s - b) * f + S), e.round(.1 * w), e.round((s - b) * f + S), e.round(.9 * w), v.get("medianColor")).append(), v.get("target") && (h = e.ceil(v.get("spotRadius")), p.drawLine(e.round((v.get("target") - b) * f + S), e.round(w / 2 - h), e.round((v.get("target") - b) * f + S), e.round(w / 2 + h), v.get("targetColor")).append(), p.drawLine(e.round((v.get("target") - b) * f + S - h), e.round(w / 2), e.round((v.get("target") - b) * f + S + h), e.round(w / 2), v.get("targetColor")).append()), p.render()
                }
            }
        }), H = r({
            init: function(t, e, n, i) {
                this.target = t, this.id = e, this.type = n, this.args = i
            },
            append: function() {
                return this.target.appendShape(this), this
            }
        }), z = r({
            _pxregex: /(\d+)(px)?\s*$/i,
            init: function(t, e, n) {
                t && (this.width = t, this.height = e, this.target = n, this.lastShapeId = null, n[0] && (n = n[0]), i.data(n, "_jqs_vcanvas", this))
            },
            drawLine: function(t, e, n, i, o, r) {
                return this.drawShape([
                    [t, e],
                    [n, i]
                ], o, r)
            },
            drawShape: function(t, e, n, i) {
                return this._genShape("Shape", [t, e, n, i])
            },
            drawCircle: function(t, e, n, i, o, r) {
                return this._genShape("Circle", [t, e, n, i, o, r])
            },
            drawPieSlice: function(t, e, n, i, o, r, s) {
                return this._genShape("PieSlice", [t, e, n, i, o, r, s])
            },
            drawRect: function(t, e, n, i, o, r) {
                return this._genShape("Rect", [t, e, n, i, o, r])
            },
            getElement: function() {
                return this.canvas
            },
            getLastShapeId: function() {
                return this.lastShapeId
            },
            reset: function() {
                alert("reset not implemented")
            },
            _insert: function(t, e) {
                i(e).html(t)
            },
            _calculatePixelDims: function(t, e, n) {
                var o;
                o = this._pxregex.exec(e), this.pixelHeight = o ? o[1] : i(n).height(), o = this._pxregex.exec(t), this.pixelWidth = o ? o[1] : i(n).width()
            },
            _genShape: function(t, e) {
                var n = I++;
                return e.unshift(n), new H(this, n, t, e)
            },
            appendShape: function(t) {
                alert("appendShape not implemented")
            },
            replaceWithShape: function(t, e) {
                alert("replaceWithShape not implemented")
            },
            insertAfterShape: function(t, e) {
                alert("insertAfterShape not implemented")
            },
            removeShapeId: function(t) {
                alert("removeShapeId not implemented")
            },
            getShapeAt: function(t, e, n) {
                alert("getShapeAt not implemented")
            },
            render: function() {
                alert("render not implemented")
            }
        }), A = r(z, {
            init: function(e, o, r, s) {
                A._super.init.call(this, e, o, r), this.canvas = t.createElement("canvas"), r[0] && (r = r[0]), i.data(r, "_jqs_vcanvas", this), i(this.canvas).css({
                    display: "inline-block",
                    width: e,
                    height: o,
                    verticalAlign: "top"
                }), this._insert(this.canvas, r), this._calculatePixelDims(e, o, this.canvas), this.canvas.width = this.pixelWidth, this.canvas.height = this.pixelHeight, this.interact = s, this.shapes = {}, this.shapeseq = [], this.currentTargetShapeId = n, i(this.canvas).css({
                    width: this.pixelWidth,
                    height: this.pixelHeight
                })
            },
            _getContext: function(t, e, i) {
                var o = this.canvas.getContext("2d");
                return t !== n && (o.strokeStyle = t), o.lineWidth = i === n ? 1 : i, e !== n && (o.fillStyle = e), o
            },
            reset: function() {
                var t = this._getContext();
                t.clearRect(0, 0, this.pixelWidth, this.pixelHeight), this.shapes = {}, this.shapeseq = [], this.currentTargetShapeId = n
            },
            _drawShape: function(t, e, i, o, r) {
                var s, a, l = this._getContext(i, o, r);
                for (l.beginPath(), l.moveTo(e[0][0] + .5, e[0][1] + .5), s = 1, a = e.length; a > s; s++) l.lineTo(e[s][0] + .5, e[s][1] + .5);
                i !== n && l.stroke(), o !== n && l.fill(), this.targetX !== n && this.targetY !== n && l.isPointInPath(this.targetX, this.targetY) && (this.currentTargetShapeId = t)
            },
            _drawCircle: function(t, i, o, r, s, a, l) {
                var c = this._getContext(s, a, l);
                c.beginPath(), c.arc(i, o, r, 0, 2 * e.PI, !1), this.targetX !== n && this.targetY !== n && c.isPointInPath(this.targetX, this.targetY) && (this.currentTargetShapeId = t), s !== n && c.stroke(), a !== n && c.fill()
            },
            _drawPieSlice: function(t, e, i, o, r, s, a, l) {
                var c = this._getContext(a, l);
                c.beginPath(), c.moveTo(e, i), c.arc(e, i, o, r, s, !1), c.lineTo(e, i), c.closePath(), a !== n && c.stroke(), l && c.fill(), this.targetX !== n && this.targetY !== n && c.isPointInPath(this.targetX, this.targetY) && (this.currentTargetShapeId = t)
            },
            _drawRect: function(t, e, n, i, o, r, s) {
                return this._drawShape(t, [
                    [e, n],
                    [e + i, n],
                    [e + i, n + o],
                    [e, n + o],
                    [e, n]
                ], r, s)
            },
            appendShape: function(t) {
                return this.shapes[t.id] = t, this.shapeseq.push(t.id), this.lastShapeId = t.id, t.id
            },
            replaceWithShape: function(t, e) {
                var n, i = this.shapeseq;
                for (this.shapes[e.id] = e, n = i.length; n--;) i[n] == t && (i[n] = e.id);
                delete this.shapes[t]
            },
            replaceWithShapes: function(t, e) {
                var n, i, o, r = this.shapeseq,
                    s = {};
                for (i = t.length; i--;) s[t[i]] = !0;
                for (i = r.length; i--;) n = r[i], s[n] && (r.splice(i, 1), delete this.shapes[n], o = i);
                for (i = e.length; i--;) r.splice(o, 0, e[i].id), this.shapes[e[i].id] = e[i]
            },
            insertAfterShape: function(t, e) {
                var n, i = this.shapeseq;
                for (n = i.length; n--;)
                    if (i[n] === t) return i.splice(n + 1, 0, e.id), void(this.shapes[e.id] = e)
            },
            removeShapeId: function(t) {
                var e, n = this.shapeseq;
                for (e = n.length; e--;)
                    if (n[e] === t) {
                        n.splice(e, 1);
                        break
                    }
                delete this.shapes[t]
            },
            getShapeAt: function(t, e, n) {
                return this.targetX = e, this.targetY = n, this.render(), this.currentTargetShapeId
            },
            render: function() {
                var t, e, n, i = this.shapeseq,
                    o = this.shapes,
                    r = i.length,
                    s = this._getContext();
                for (s.clearRect(0, 0, this.pixelWidth, this.pixelHeight), n = 0; r > n; n++) t = i[n], e = o[t], this["_draw" + e.type].apply(this, e.args);
                this.interact || (this.shapes = {}, this.shapeseq = [])
            }
        }), L = r(z, {
            init: function(e, n, o) {
                var r;
                L._super.init.call(this, e, n, o), o[0] && (o = o[0]), i.data(o, "_jqs_vcanvas", this), this.canvas = t.createElement("span"), i(this.canvas).css({
                    display: "inline-block",
                    position: "relative",
                    overflow: "hidden",
                    width: e,
                    height: n,
                    margin: "0px",
                    padding: "0px",
                    verticalAlign: "top"
                }), this._insert(this.canvas, o), this._calculatePixelDims(e, n, this.canvas), this.canvas.width = this.pixelWidth, this.canvas.height = this.pixelHeight, r = '<v:group coordorigin="0 0" coordsize="' + this.pixelWidth + " " + this.pixelHeight + '" style="position:absolute;top:0;left:0;width:' + this.pixelWidth + "px;height=" + this.pixelHeight + 'px;"></v:group>', this.canvas.insertAdjacentHTML("beforeEnd", r), this.group = i(this.canvas).children()[0], this.rendered = !1, this.prerender = ""
            },
            _drawShape: function(t, e, i, o, r) {
                var s, a, l, c, u, d, h, f = [];
                for (h = 0, d = e.length; d > h; h++) f[h] = "" + e[h][0] + "," + e[h][1];
                return s = f.splice(0, 1), r = r === n ? 1 : r, a = i === n ? ' stroked="false" ' : ' strokeWeight="' + r + 'px" strokeColor="' + i + '" ', l = o === n ? ' filled="false"' : ' fillColor="' + o + '" filled="true" ', c = f[0] === f[f.length - 1] ? "x " : "", u = '<v:shape coordorigin="0 0" coordsize="' + this.pixelWidth + " " + this.pixelHeight + '"  id="jqsshape' + t + '" ' + a + l + ' style="position:absolute;left:0px;top:0px;height:' + this.pixelHeight + "px;width:" + this.pixelWidth + 'px;padding:0px;margin:0px;"  path="m ' + s + " l " + f.join(", ") + " " + c + 'e"> </v:shape>'
            },
            _drawCircle: function(t, e, i, o, r, s, a) {
                var l, c, u;
                return e -= o, i -= o, l = r === n ? ' stroked="false" ' : ' strokeWeight="' + a + 'px" strokeColor="' + r + '" ', c = s === n ? ' filled="false"' : ' fillColor="' + s + '" filled="true" ', u = '<v:oval  id="jqsshape' + t + '" ' + l + c + ' style="position:absolute;top:' + i + "px; left:" + e + "px; width:" + 2 * o + "px; height:" + 2 * o + 'px"></v:oval>'
            },
            _drawPieSlice: function(t, i, o, r, s, a, l, c) {
                var u, d, h, f, p, g, m, v;
                if (s === a) return "";
                if (a - s === 2 * e.PI && (s = 0, a = 2 * e.PI), d = i + e.round(e.cos(s) * r), h = o + e.round(e.sin(s) * r), f = i + e.round(e.cos(a) * r), p = o + e.round(e.sin(a) * r), d === f && h === p) {
                    if (a - s < e.PI) return "";
                    d = f = i + r, h = p = o
                }
                return d === f && h === p && a - s < e.PI ? "" : (u = [i - r, o - r, i + r, o + r, d, h, f, p], g = l === n ? ' stroked="false" ' : ' strokeWeight="1px" strokeColor="' + l + '" ', m = c === n ? ' filled="false"' : ' fillColor="' + c + '" filled="true" ', v = '<v:shape coordorigin="0 0" coordsize="' + this.pixelWidth + " " + this.pixelHeight + '"  id="jqsshape' + t + '" ' + g + m + ' style="position:absolute;left:0px;top:0px;height:' + this.pixelHeight + "px;width:" + this.pixelWidth + 'px;padding:0px;margin:0px;"  path="m ' + i + "," + o + " wa " + u.join(", ") + ' x e"> </v:shape>')
            },
            _drawRect: function(t, e, n, i, o, r, s) {
                return this._drawShape(t, [
                    [e, n],
                    [e, n + o],
                    [e + i, n + o],
                    [e + i, n],
                    [e, n]
                ], r, s)
            },
            reset: function() {
                this.group.innerHTML = ""
            },
            appendShape: function(t) {
                var e = this["_draw" + t.type].apply(this, t.args);
                return this.rendered ? this.group.insertAdjacentHTML("beforeEnd", e) : this.prerender += e, this.lastShapeId = t.id, t.id
            },
            replaceWithShape: function(t, e) {
                var n = i("#jqsshape" + t),
                    o = this["_draw" + e.type].apply(this, e.args);
                n[0].outerHTML = o
            },
            replaceWithShapes: function(t, e) {
                var n, o = i("#jqsshape" + t[0]),
                    r = "",
                    s = e.length;
                for (n = 0; s > n; n++) r += this["_draw" + e[n].type].apply(this, e[n].args);
                for (o[0].outerHTML = r, n = 1; n < t.length; n++) i("#jqsshape" + t[n]).remove()
            },
            insertAfterShape: function(t, e) {
                var n = i("#jqsshape" + t),
                    o = this["_draw" + e.type].apply(this, e.args);
                n[0].insertAdjacentHTML("afterEnd", o)
            },
            removeShapeId: function(t) {
                var e = i("#jqsshape" + t);
                this.group.removeChild(e[0])
            },
            getShapeAt: function(t, e, n) {
                var i = t.id.substr(8);
                return i
            },
            render: function() {
                this.rendered || (this.group.innerHTML = this.prerender, this.rendered = !0)
            }
        })
    })
}(document, Math), ! function(t, e) {
    "object" == typeof exports ? module.exports = e(require("jquery")) : "function" == typeof define && define.amd ? define(["jquery"], e) : e(t.jQuery)
}(this, function(t) {
    var e = function(t, e) {
            var n, i = document.createElement("canvas");
            t.appendChild(i), "undefined" != typeof G_vmlCanvasManager && G_vmlCanvasManager.initElement(i);
            var o = i.getContext("2d");
            i.width = i.height = e.size;
            var r = 1;
            window.devicePixelRatio > 1 && (r = window.devicePixelRatio, i.style.width = i.style.height = [e.size, "px"].join(""), i.width = i.height = e.size * r, o.scale(r, r)), o.translate(e.size / 2, e.size / 2), o.rotate((-.5 + e.rotate / 180) * Math.PI);
            var s = (e.size - e.lineWidth) / 2;
            e.scaleColor && e.scaleLength && (s -= e.scaleLength + 2), Date.now = Date.now || function() {
                return +new Date
            };
            var a = function(t, e, n) {
                    n = Math.min(Math.max(-1, n || 0), 1);
                    var i = 0 >= n ? !0 : !1;
                    o.beginPath(), o.arc(0, 0, s, 0, 2 * Math.PI * n, i), o.strokeStyle = t, o.lineWidth = e, o.stroke()
                },
                l = function() {
                    var t, n;
                    o.lineWidth = 1, o.fillStyle = e.scaleColor, o.save();
                    for (var i = 24; i > 0; --i) i % 6 === 0 ? (n = e.scaleLength, t = 0) : (n = .6 * e.scaleLength, t = e.scaleLength - n), o.fillRect(-e.size / 2 + t, 0, n, 1), o.rotate(Math.PI / 12);
                    o.restore()
                },
                c = function() {
                    return window.requestAnimationFrame || window.webkitRequestAnimationFrame || window.mozRequestAnimationFrame || function(t) {
                        window.setTimeout(t, 1e3 / 60)
                    }
                }(),
                u = function() {
                    e.scaleColor && l(), e.trackColor && a(e.trackColor, e.trackWidth || e.lineWidth, 1)
                };
            this.getCanvas = function() {
                return i
            }, this.getCtx = function() {
                return o
            }, this.clear = function() {
                o.clearRect(e.size / -2, e.size / -2, e.size, e.size)
            }, this.draw = function(t) {
                e.scaleColor || e.trackColor ? o.getImageData && o.putImageData ? n ? o.putImageData(n, 0, 0) : (u(), n = o.getImageData(0, 0, e.size * r, e.size * r)) : (this.clear(), u()) : this.clear(), o.lineCap = e.lineCap;
                var i;
                i = "function" == typeof e.barColor ? e.barColor(t) : e.barColor, a(i, e.lineWidth, t / 100)
            }.bind(this), this.animate = function(t, n) {
                var i = Date.now();
                e.onStart(t, n);
                var o = function() {
                    var r = Math.min(Date.now() - i, e.animate.duration),
                        s = e.easing(this, r, t, n - t, e.animate.duration);
                    this.draw(s), e.onStep(t, n, s), r >= e.animate.duration ? e.onStop(t, n) : c(o)
                }.bind(this);
                c(o)
            }.bind(this)
        },
        n = function(t, n) {
            var i = {
                barColor: "#ef1e25",
                trackColor: "#f9f9f9",
                scaleColor: "#dfe0e0",
                scaleLength: 5,
                lineCap: "round",
                lineWidth: 3,
                trackWidth: void 0,
                size: 110,
                rotate: 0,
                animate: {
                    duration: 1e3,
                    enabled: !0
                },
                easing: function(t, e, n, i, o) {
                    return e /= o / 2, 1 > e ? i / 2 * e * e + n : -i / 2 * (--e * (e - 2) - 1) + n
                },
                onStart: function() {},
                onStep: function() {},
                onStop: function() {}
            };
            if ("undefined" != typeof e) i.renderer = e;
            else {
                if ("undefined" == typeof SVGRenderer) throw new Error("Please load either the SVG- or the CanvasRenderer");
                i.renderer = SVGRenderer
            }
            var o = {},
                r = 0,
                s = function() {
                    this.el = t, this.options = o;
                    for (var e in i) i.hasOwnProperty(e) && (o[e] = n && "undefined" != typeof n[e] ? n[e] : i[e], "function" == typeof o[e] && (o[e] = o[e].bind(this)));
                    o.easing = "string" == typeof o.easing && "undefined" != typeof jQuery && jQuery.isFunction(jQuery.easing[o.easing]) ? jQuery.easing[o.easing] : i.easing, "number" == typeof o.animate && (o.animate = {
                        duration: o.animate,
                        enabled: !0
                    }), "boolean" != typeof o.animate || o.animate || (o.animate = {
                        duration: 1e3,
                        enabled: o.animate
                    }), this.renderer = new o.renderer(t, o), this.renderer.draw(r), t.dataset && t.dataset.percent ? this.update(parseFloat(t.dataset.percent)) : t.getAttribute && t.getAttribute("data-percent") && this.update(parseFloat(t.getAttribute("data-percent")))
                }.bind(this);
            this.update = function(t) {
                return t = parseFloat(t), o.animate.enabled ? this.renderer.animate(r, t) : this.renderer.draw(t), r = t, this
            }.bind(this), this.disableAnimation = function() {
                return o.animate.enabled = !1, this
            }, this.enableAnimation = function() {
                return o.animate.enabled = !0, this
            }, s()
        };
    t.fn.easyPieChart = function(e) {
        return this.each(function() {
            var i;
            t.data(this, "easyPieChart") || (i = t.extend({}, e, t(this).data()), t.data(this, "easyPieChart", new n(this, i)))
        })
    }
}), ! function(t, e) {
    "object" == typeof exports && "undefined" != typeof module ? module.exports = e() : "function" == typeof define && define.amd ? define(e) : t.moment = e()
}(this, function() {
    "use strict";

    function t() {
        return Rn.apply(null, arguments)
    }

    function e(t) {
        Rn = t
    }

    function n(t) {
        return "[object Array]" === Object.prototype.toString.call(t)
    }

    function i(t) {
        return t instanceof Date || "[object Date]" === Object.prototype.toString.call(t)
    }

    function o(t, e) {
        var n, i = [];
        for (n = 0; n < t.length; ++n) i.push(e(t[n], n));
        return i
    }

    function r(t, e) {
        return Object.prototype.hasOwnProperty.call(t, e)
    }

    function s(t, e) {
        for (var n in e) r(e, n) && (t[n] = e[n]);
        return r(e, "toString") && (t.toString = e.toString), r(e, "valueOf") && (t.valueOf = e.valueOf), t
    }

    function a(t, e, n, i) {
        return kt(t, e, n, i, !0).utc()
    }

    function l() {
        return {
            empty: !1,
            unusedTokens: [],
            unusedInput: [],
            overflow: -2,
            charsLeftOver: 0,
            nullInput: !1,
            invalidMonth: null,
            invalidFormat: !1,
            userInvalidated: !1,
            iso: !1
        }
    }

    function c(t) {
        return null == t._pf && (t._pf = l()), t._pf
    }

    function u(t) {
        if (null == t._isValid) {
            var e = c(t);
            t._isValid = !isNaN(t._d.getTime()) && e.overflow < 0 && !e.empty && !e.invalidMonth && !e.nullInput && !e.invalidFormat && !e.userInvalidated, t._strict && (t._isValid = t._isValid && 0 === e.charsLeftOver && 0 === e.unusedTokens.length && void 0 === e.bigHour)
        }
        return t._isValid
    }

    function d(t) {
        var e = a(0 / 0);
        return null != t ? s(c(e), t) : c(e).userInvalidated = !0, e
    }

    function h(t, e) {
        var n, i, o;
        if ("undefined" != typeof e._isAMomentObject && (t._isAMomentObject = e._isAMomentObject), "undefined" != typeof e._i && (t._i = e._i), "undefined" != typeof e._f && (t._f = e._f), "undefined" != typeof e._l && (t._l = e._l), "undefined" != typeof e._strict && (t._strict = e._strict), "undefined" != typeof e._tzm && (t._tzm = e._tzm), "undefined" != typeof e._isUTC && (t._isUTC = e._isUTC), "undefined" != typeof e._offset && (t._offset = e._offset), "undefined" != typeof e._pf && (t._pf = c(e)), "undefined" != typeof e._locale && (t._locale = e._locale), zn.length > 0)
            for (n in zn) i = zn[n], o = e[i], "undefined" != typeof o && (t[i] = o);
        return t
    }

    function f(e) {
        h(this, e), this._d = new Date(+e._d), An === !1 && (An = !0, t.updateOffset(this), An = !1)
    }

    function p(t) {
        return t instanceof f || null != t && null != t._isAMomentObject
    }

    function g(t) {
        var e = +t,
            n = 0;
        return 0 !== e && isFinite(e) && (n = e >= 0 ? Math.floor(e) : Math.ceil(e)), n
    }

    function m(t, e, n) {
        var i, o = Math.min(t.length, e.length),
            r = Math.abs(t.length - e.length),
            s = 0;
        for (i = 0; o > i; i++)(n && t[i] !== e[i] || !n && g(t[i]) !== g(e[i])) && s++;
        return s + r
    }

    function v() {}

    function y(t) {
        return t ? t.toLowerCase().replace("_", "-") : t
    }

    function w(t) {
        for (var e, n, i, o, r = 0; r < t.length;) {
            for (o = y(t[r]).split("-"), e = o.length, n = y(t[r + 1]), n = n ? n.split("-") : null; e > 0;) {
                if (i = b(o.slice(0, e).join("-"))) return i;
                if (n && n.length >= e && m(o, n, !0) >= e - 1) break;
                e--
            }
            r++
        }
        return null
    }

    function b(t) {
        var e = null;
        if (!Ln[t] && "undefined" != typeof module && module && module.exports) try {
            e = Hn._abbr, require("./locale/" + t), x(e)
        } catch (n) {}
        return Ln[t]
    }

    function x(t, e) {
        var n;
        return t && (n = "undefined" == typeof e ? T(t) : S(t, e), n && (Hn = n)), Hn._abbr
    }

    function S(t, e) {
        return null !== e ? (e.abbr = t, Ln[t] || (Ln[t] = new v), Ln[t].set(e), x(t), Ln[t]) : (delete Ln[t], null)
    }

    function T(t) {
        var e;
        if (t && t._locale && t._locale._abbr && (t = t._locale._abbr), !t) return Hn;
        if (!n(t)) {
            if (e = b(t)) return e;
            t = [t]
        }
        return w(t)
    }

    function C(t, e) {
        var n = t.toLowerCase();
        Nn[n] = Nn[n + "s"] = Nn[e] = t
    }

    function k(t) {
        return "string" == typeof t ? Nn[t] || Nn[t.toLowerCase()] : void 0
    }

    function E(t) {
        var e, n, i = {};
        for (n in t) r(t, n) && (e = k(n), e && (i[e] = t[n]));
        return i
    }

    function D(e, n) {
        return function(i) {
            return null != i ? (_(this, e, i), t.updateOffset(this, n), this) : M(this, e)
        }
    }

    function M(t, e) {
        return t._d["get" + (t._isUTC ? "UTC" : "") + e]()
    }

    function _(t, e, n) {
        return t._d["set" + (t._isUTC ? "UTC" : "") + e](n)
    }

    function R(t, e) {
        var n;
        if ("object" == typeof t)
            for (n in t) this.set(n, t[n]);
        else if (t = k(t), "function" == typeof this[t]) return this[t](e);
        return this
    }

    function H(t, e, n) {
        for (var i = "" + Math.abs(t), o = t >= 0; i.length < e;) i = "0" + i;
        return (o ? n ? "+" : "" : "-") + i
    }

    function z(t, e, n, i) {
        var o = i;
        "string" == typeof i && (o = function() {
            return this[i]()
        }), t && (Fn[t] = o), e && (Fn[e[0]] = function() {
            return H(o.apply(this, arguments), e[1], e[2])
        }), n && (Fn[n] = function() {
            return this.localeData().ordinal(o.apply(this, arguments), t)
        })
    }

    function A(t) {
        return t.match(/\[[\s\S]/) ? t.replace(/^\[|\]$/g, "") : t.replace(/\\/g, "")
    }

    function L(t) {
        var e, n, i = t.match(On);
        for (e = 0, n = i.length; n > e; e++) i[e] = Fn[i[e]] ? Fn[i[e]] : A(i[e]);
        return function(o) {
            var r = "";
            for (e = 0; n > e; e++) r += i[e] instanceof Function ? i[e].call(o, t) : i[e];
            return r
        }
    }

    function N(t, e) {
        return t.isValid() ? (e = O(e, t.localeData()), Pn[e] || (Pn[e] = L(e)), Pn[e](t)) : t.localeData().invalidDate()
    }

    function O(t, e) {
        function n(t) {
            return e.longDateFormat(t) || t
        }
        var i = 5;
        for (In.lastIndex = 0; i >= 0 && In.test(t);) t = t.replace(In, n), In.lastIndex = 0, i -= 1;
        return t
    }

    function I(t, e, n) {
        ti[t] = "function" == typeof e ? e : function(t) {
            return t && n ? n : e
        }
    }

    function P(t, e) {
        return r(ti, t) ? ti[t](e._strict, e._locale) : new RegExp(F(t))
    }

    function F(t) {
        return t.replace("\\", "").replace(/\\(\[)|\\(\])|\[([^\]\[]*)\]|\\(.)/g, function(t, e, n, i, o) {
            return e || n || i || o
        }).replace(/[-\/\\^$*+?.()|[\]{}]/g, "\\$&")
    }

    function W(t, e) {
        var n, i = e;
        for ("string" == typeof t && (t = [t]), "number" == typeof e && (i = function(t, n) {
            n[e] = g(t)
        }), n = 0; n < t.length; n++) ei[t[n]] = i
    }

    function j(t, e) {
        W(t, function(t, n, i, o) {
            i._w = i._w || {}, e(t, i._w, i, o)
        })
    }

    function Y(t, e, n) {
        null != e && r(ei, t) && ei[t](e, n._a, n, t)
    }

    function $(t, e) {
        return new Date(Date.UTC(t, e + 1, 0)).getUTCDate()
    }

    function q(t) {
        return this._months[t.month()]
    }

    function B(t) {
        return this._monthsShort[t.month()]
    }

    function G(t, e, n) {
        var i, o, r;
        for (this._monthsParse || (this._monthsParse = [], this._longMonthsParse = [], this._shortMonthsParse = []), i = 0; 12 > i; i++) {
            if (o = a([2e3, i]), n && !this._longMonthsParse[i] && (this._longMonthsParse[i] = new RegExp("^" + this.months(o, "").replace(".", "") + "$", "i"), this._shortMonthsParse[i] = new RegExp("^" + this.monthsShort(o, "").replace(".", "") + "$", "i")), n || this._monthsParse[i] || (r = "^" + this.months(o, "") + "|^" + this.monthsShort(o, ""), this._monthsParse[i] = new RegExp(r.replace(".", ""), "i")), n && "MMMM" === e && this._longMonthsParse[i].test(t)) return i;
            if (n && "MMM" === e && this._shortMonthsParse[i].test(t)) return i;
            if (!n && this._monthsParse[i].test(t)) return i
        }
    }

    function V(t, e) {
        var n;
        return "string" == typeof e && (e = t.localeData().monthsParse(e), "number" != typeof e) ? t : (n = Math.min(t.date(), $(t.year(), e)), t._d["set" + (t._isUTC ? "UTC" : "") + "Month"](e, n), t)
    }

    function U(e) {
        return null != e ? (V(this, e), t.updateOffset(this, !0), this) : M(this, "Month")
    }

    function X() {
        return $(this.year(), this.month())
    }

    function Z(t) {
        var e, n = t._a;
        return n && -2 === c(t).overflow && (e = n[ii] < 0 || n[ii] > 11 ? ii : n[oi] < 1 || n[oi] > $(n[ni], n[ii]) ? oi : n[ri] < 0 || n[ri] > 24 || 24 === n[ri] && (0 !== n[si] || 0 !== n[ai] || 0 !== n[li]) ? ri : n[si] < 0 || n[si] > 59 ? si : n[ai] < 0 || n[ai] > 59 ? ai : n[li] < 0 || n[li] > 999 ? li : -1, c(t)._overflowDayOfYear && (ni > e || e > oi) && (e = oi), c(t).overflow = e), t
    }

    function Q(e) {
        t.suppressDeprecationWarnings === !1 && "undefined" != typeof console && console.warn && console.warn("Deprecation warning: " + e)
    }

    function K(t, e) {
        var n = !0,
            i = t + "\n" + (new Error).stack;
        return s(function() {
            return n && (Q(i), n = !1), e.apply(this, arguments)
        }, e)
    }

    function J(t, e) {
        di[t] || (Q(e), di[t] = !0)
    }

    function tt(t) {
        var e, n, i = t._i,
            o = hi.exec(i);
        if (o) {
            for (c(t).iso = !0, e = 0, n = fi.length; n > e; e++)
                if (fi[e][1].exec(i)) {
                    t._f = fi[e][0] + (o[6] || " ");
                    break
                }
            for (e = 0, n = pi.length; n > e; e++)
                if (pi[e][1].exec(i)) {
                    t._f += pi[e][0];
                    break
                }
            i.match(Qn) && (t._f += "Z"), wt(t)
        } else t._isValid = !1
    }

    function et(e) {
        var n = gi.exec(e._i);
        return null !== n ? void(e._d = new Date(+n[1])) : (tt(e), void(e._isValid === !1 && (delete e._isValid, t.createFromInputFallback(e))))
    }

    function nt(t, e, n, i, o, r, s) {
        var a = new Date(t, e, n, i, o, r, s);
        return 1970 > t && a.setFullYear(t), a
    }

    function it(t) {
        var e = new Date(Date.UTC.apply(null, arguments));
        return 1970 > t && e.setUTCFullYear(t), e
    }

    function ot(t) {
        return rt(t) ? 366 : 365
    }

    function rt(t) {
        return t % 4 === 0 && t % 100 !== 0 || t % 400 === 0
    }

    function st() {
        return rt(this.year())
    }

    function at(t, e, n) {
        var i, o = n - e,
            r = n - t.day();
        return r > o && (r -= 7), o - 7 > r && (r += 7), i = Et(t).add(r, "d"), {
            week: Math.ceil(i.dayOfYear() / 7),
            year: i.year()
        }
    }

    function lt(t) {
        return at(t, this._week.dow, this._week.doy).week
    }

    function ct() {
        return this._week.dow
    }

    function ut() {
        return this._week.doy
    }

    function dt(t) {
        var e = this.localeData().week(this);
        return null == t ? e : this.add(7 * (t - e), "d")
    }

    function ht(t) {
        var e = at(this, 1, 4).week;
        return null == t ? e : this.add(7 * (t - e), "d")
    }

    function ft(t, e, n, i, o) {
        var r, s, a = it(t, 0, 1).getUTCDay();
        return a = 0 === a ? 7 : a, n = null != n ? n : o, r = o - a + (a > i ? 7 : 0) - (o > a ? 7 : 0), s = 7 * (e - 1) + (n - o) + r + 1, {
            year: s > 0 ? t : t - 1,
            dayOfYear: s > 0 ? s : ot(t - 1) + s
        }
    }

    function pt(t) {
        var e = Math.round((this.clone().startOf("day") - this.clone().startOf("year")) / 864e5) + 1;
        return null == t ? e : this.add(t - e, "d")
    }

    function gt(t, e, n) {
        return null != t ? t : null != e ? e : n
    }

    function mt(t) {
        var e = new Date;
        return t._useUTC ? [e.getUTCFullYear(), e.getUTCMonth(), e.getUTCDate()] : [e.getFullYear(), e.getMonth(), e.getDate()]
    }

    function vt(t) {
        var e, n, i, o, r = [];
        if (!t._d) {
            for (i = mt(t), t._w && null == t._a[oi] && null == t._a[ii] && yt(t), t._dayOfYear && (o = gt(t._a[ni], i[ni]), t._dayOfYear > ot(o) && (c(t)._overflowDayOfYear = !0), n = it(o, 0, t._dayOfYear), t._a[ii] = n.getUTCMonth(), t._a[oi] = n.getUTCDate()), e = 0; 3 > e && null == t._a[e]; ++e) t._a[e] = r[e] = i[e];
            for (; 7 > e; e++) t._a[e] = r[e] = null == t._a[e] ? 2 === e ? 1 : 0 : t._a[e];
            24 === t._a[ri] && 0 === t._a[si] && 0 === t._a[ai] && 0 === t._a[li] && (t._nextDay = !0, t._a[ri] = 0), t._d = (t._useUTC ? it : nt).apply(null, r), null != t._tzm && t._d.setUTCMinutes(t._d.getUTCMinutes() - t._tzm), t._nextDay && (t._a[ri] = 24)
        }
    }

    function yt(t) {
        var e, n, i, o, r, s, a;
        e = t._w, null != e.GG || null != e.W || null != e.E ? (r = 1, s = 4, n = gt(e.GG, t._a[ni], at(Et(), 1, 4).year), i = gt(e.W, 1), o = gt(e.E, 1)) : (r = t._locale._week.dow, s = t._locale._week.doy, n = gt(e.gg, t._a[ni], at(Et(), r, s).year), i = gt(e.w, 1), null != e.d ? (o = e.d, r > o && ++i) : o = null != e.e ? e.e + r : r), a = ft(n, i, o, s, r), t._a[ni] = a.year, t._dayOfYear = a.dayOfYear
    }

    function wt(e) {
        if (e._f === t.ISO_8601) return void tt(e);
        e._a = [], c(e).empty = !0;
        var n, i, o, r, s, a = "" + e._i,
            l = a.length,
            u = 0;
        for (o = O(e._f, e._locale).match(On) || [], n = 0; n < o.length; n++) r = o[n], i = (a.match(P(r, e)) || [])[0], i && (s = a.substr(0, a.indexOf(i)), s.length > 0 && c(e).unusedInput.push(s), a = a.slice(a.indexOf(i) + i.length), u += i.length), Fn[r] ? (i ? c(e).empty = !1 : c(e).unusedTokens.push(r), Y(r, i, e)) : e._strict && !i && c(e).unusedTokens.push(r);
        c(e).charsLeftOver = l - u, a.length > 0 && c(e).unusedInput.push(a), c(e).bigHour === !0 && e._a[ri] <= 12 && e._a[ri] > 0 && (c(e).bigHour = void 0), e._a[ri] = bt(e._locale, e._a[ri], e._meridiem), vt(e), Z(e)
    }

    function bt(t, e, n) {
        var i;
        return null == n ? e : null != t.meridiemHour ? t.meridiemHour(e, n) : null != t.isPM ? (i = t.isPM(n), i && 12 > e && (e += 12), i || 12 !== e || (e = 0), e) : e
    }

    function xt(t) {
        var e, n, i, o, r;
        if (0 === t._f.length) return c(t).invalidFormat = !0, void(t._d = new Date(0 / 0));
        for (o = 0; o < t._f.length; o++) r = 0, e = h({}, t), null != t._useUTC && (e._useUTC = t._useUTC), e._f = t._f[o], wt(e), u(e) && (r += c(e).charsLeftOver, r += 10 * c(e).unusedTokens.length, c(e).score = r, (null == i || i > r) && (i = r, n = e));
        s(t, n || e)
    }

    function St(t) {
        if (!t._d) {
            var e = E(t._i);
            t._a = [e.year, e.month, e.day || e.date, e.hour, e.minute, e.second, e.millisecond], vt(t)
        }
    }

    function Tt(t) {
        var e, o = t._i,
            r = t._f;
        return t._locale = t._locale || T(t._l), null === o || void 0 === r && "" === o ? d({
            nullInput: !0
        }) : ("string" == typeof o && (t._i = o = t._locale.preparse(o)), p(o) ? new f(Z(o)) : (n(r) ? xt(t) : r ? wt(t) : i(o) ? t._d = o : Ct(t), e = new f(Z(t)), e._nextDay && (e.add(1, "d"), e._nextDay = void 0), e))
    }

    function Ct(e) {
        var r = e._i;
        void 0 === r ? e._d = new Date : i(r) ? e._d = new Date(+r) : "string" == typeof r ? et(e) : n(r) ? (e._a = o(r.slice(0), function(t) {
            return parseInt(t, 10)
        }), vt(e)) : "object" == typeof r ? St(e) : "number" == typeof r ? e._d = new Date(r) : t.createFromInputFallback(e)
    }

    function kt(t, e, n, i, o) {
        var r = {};
        return "boolean" == typeof n && (i = n, n = void 0), r._isAMomentObject = !0, r._useUTC = r._isUTC = o, r._l = n, r._i = t, r._f = e, r._strict = i, Tt(r)
    }

    function Et(t, e, n, i) {
        return kt(t, e, n, i, !1)
    }

    function Dt(t, e) {
        var i, o;
        if (1 === e.length && n(e[0]) && (e = e[0]), !e.length) return Et();
        for (i = e[0], o = 1; o < e.length; ++o) e[o][t](i) && (i = e[o]);
        return i
    }

    function Mt() {
        var t = [].slice.call(arguments, 0);
        return Dt("isBefore", t)
    }

    function _t() {
        var t = [].slice.call(arguments, 0);
        return Dt("isAfter", t)
    }

    function Rt(t) {
        var e = E(t),
            n = e.year || 0,
            i = e.quarter || 0,
            o = e.month || 0,
            r = e.week || 0,
            s = e.day || 0,
            a = e.hour || 0,
            l = e.minute || 0,
            c = e.second || 0,
            u = e.millisecond || 0;
        this._milliseconds = +u + 1e3 * c + 6e4 * l + 36e5 * a, this._days = +s + 7 * r, this._months = +o + 3 * i + 12 * n, this._data = {}, this._locale = T(), this._bubble()
    }

    function Ht(t) {
        return t instanceof Rt
    }

    function zt(t, e) {
        z(t, 0, 0, function() {
            var t = this.utcOffset(),
                n = "+";
            return 0 > t && (t = -t, n = "-"), n + H(~~(t / 60), 2) + e + H(~~t % 60, 2)
        })
    }

    function At(t) {
        var e = (t || "").match(Qn) || [],
            n = e[e.length - 1] || [],
            i = (n + "").match(bi) || ["-", 0, 0],
            o = +(60 * i[1]) + g(i[2]);
        return "+" === i[0] ? o : -o
    }

    function Lt(e, n) {
        var o, r;
        return n._isUTC ? (o = n.clone(), r = (p(e) || i(e) ? +e : +Et(e)) - +o, o._d.setTime(+o._d + r), t.updateOffset(o, !1), o) : Et(e).local()
    }

    function Nt(t) {
        return 15 * -Math.round(t._d.getTimezoneOffset() / 15)
    }

    function Ot(e, n) {
        var i, o = this._offset || 0;
        return null != e ? ("string" == typeof e && (e = At(e)), Math.abs(e) < 16 && (e = 60 * e), !this._isUTC && n && (i = Nt(this)), this._offset = e, this._isUTC = !0, null != i && this.add(i, "m"), o !== e && (!n || this._changeInProgress ? Kt(this, Vt(e - o, "m"), 1, !1) : this._changeInProgress || (this._changeInProgress = !0, t.updateOffset(this, !0), this._changeInProgress = null)), this) : this._isUTC ? o : Nt(this)
    }

    function It(t, e) {
        return null != t ? ("string" != typeof t && (t = -t), this.utcOffset(t, e), this) : -this.utcOffset()
    }

    function Pt(t) {
        return this.utcOffset(0, t)
    }

    function Ft(t) {
        return this._isUTC && (this.utcOffset(0, t), this._isUTC = !1, t && this.subtract(Nt(this), "m")), this
    }

    function Wt() {
        return this._tzm ? this.utcOffset(this._tzm) : "string" == typeof this._i && this.utcOffset(At(this._i)), this
    }

    function jt(t) {
        return t = t ? Et(t).utcOffset() : 0, (this.utcOffset() - t) % 60 === 0
    }

    function Yt() {
        return this.utcOffset() > this.clone().month(0).utcOffset() || this.utcOffset() > this.clone().month(5).utcOffset()
    }

    function $t() {
        if (this._a) {
            var t = this._isUTC ? a(this._a) : Et(this._a);
            return this.isValid() && m(this._a, t.toArray()) > 0
        }
        return !1
    }

    function qt() {
        return !this._isUTC
    }

    function Bt() {
        return this._isUTC
    }

    function Gt() {
        return this._isUTC && 0 === this._offset
    }

    function Vt(t, e) {
        var n, i, o, s = t,
            a = null;
        return Ht(t) ? s = {
            ms: t._milliseconds,
            d: t._days,
            M: t._months
        } : "number" == typeof t ? (s = {}, e ? s[e] = t : s.milliseconds = t) : (a = xi.exec(t)) ? (n = "-" === a[1] ? -1 : 1, s = {
            y: 0,
            d: g(a[oi]) * n,
            h: g(a[ri]) * n,
            m: g(a[si]) * n,
            s: g(a[ai]) * n,
            ms: g(a[li]) * n
        }) : (a = Si.exec(t)) ? (n = "-" === a[1] ? -1 : 1, s = {
            y: Ut(a[2], n),
            M: Ut(a[3], n),
            d: Ut(a[4], n),
            h: Ut(a[5], n),
            m: Ut(a[6], n),
            s: Ut(a[7], n),
            w: Ut(a[8], n)
        }) : null == s ? s = {} : "object" == typeof s && ("from" in s || "to" in s) && (o = Zt(Et(s.from), Et(s.to)), s = {}, s.ms = o.milliseconds, s.M = o.months), i = new Rt(s), Ht(t) && r(t, "_locale") && (i._locale = t._locale), i
    }

    function Ut(t, e) {
        var n = t && parseFloat(t.replace(",", "."));
        return (isNaN(n) ? 0 : n) * e
    }

    function Xt(t, e) {
        var n = {
            milliseconds: 0,
            months: 0
        };
        return n.months = e.month() - t.month() + 12 * (e.year() - t.year()), t.clone().add(n.months, "M").isAfter(e) && --n.months, n.milliseconds = +e - +t.clone().add(n.months, "M"), n
    }

    function Zt(t, e) {
        var n;
        return e = Lt(e, t), t.isBefore(e) ? n = Xt(t, e) : (n = Xt(e, t), n.milliseconds = -n.milliseconds, n.months = -n.months), n
    }

    function Qt(t, e) {
        return function(n, i) {
            var o, r;
            return null === i || isNaN(+i) || (J(e, "moment()." + e + "(period, number) is deprecated. Please use moment()." + e + "(number, period)."), r = n, n = i, i = r), n = "string" == typeof n ? +n : n, o = Vt(n, i), Kt(this, o, t), this
        }
    }

    function Kt(e, n, i, o) {
        var r = n._milliseconds,
            s = n._days,
            a = n._months;
        o = null == o ? !0 : o, r && e._d.setTime(+e._d + r * i), s && _(e, "Date", M(e, "Date") + s * i), a && V(e, M(e, "Month") + a * i), o && t.updateOffset(e, s || a)
    }

    function Jt(t) {
        var e = t || Et(),
            n = Lt(e, this).startOf("day"),
            i = this.diff(n, "days", !0),
            o = -6 > i ? "sameElse" : -1 > i ? "lastWeek" : 0 > i ? "lastDay" : 1 > i ? "sameDay" : 2 > i ? "nextDay" : 7 > i ? "nextWeek" : "sameElse";

        return this.format(this.localeData().calendar(o, this, Et(e)))
    }

    function te() {
        return new f(this)
    }

    function ee(t, e) {
        var n;
        return e = k("undefined" != typeof e ? e : "millisecond"), "millisecond" === e ? (t = p(t) ? t : Et(t), +this > +t) : (n = p(t) ? +t : +Et(t), n < +this.clone().startOf(e))
    }

    function ne(t, e) {
        var n;
        return e = k("undefined" != typeof e ? e : "millisecond"), "millisecond" === e ? (t = p(t) ? t : Et(t), +t > +this) : (n = p(t) ? +t : +Et(t), +this.clone().endOf(e) < n)
    }

    function ie(t, e, n) {
        return this.isAfter(t, n) && this.isBefore(e, n)
    }

    function oe(t, e) {
        var n;
        return e = k(e || "millisecond"), "millisecond" === e ? (t = p(t) ? t : Et(t), +this === +t) : (n = +Et(t), +this.clone().startOf(e) <= n && n <= +this.clone().endOf(e))
    }

    function re(t) {
        return 0 > t ? Math.ceil(t) : Math.floor(t)
    }

    function se(t, e, n) {
        var i, o, r = Lt(t, this),
            s = 6e4 * (r.utcOffset() - this.utcOffset());
        return e = k(e), "year" === e || "month" === e || "quarter" === e ? (o = ae(this, r), "quarter" === e ? o /= 3 : "year" === e && (o /= 12)) : (i = this - r, o = "second" === e ? i / 1e3 : "minute" === e ? i / 6e4 : "hour" === e ? i / 36e5 : "day" === e ? (i - s) / 864e5 : "week" === e ? (i - s) / 6048e5 : i), n ? o : re(o)
    }

    function ae(t, e) {
        var n, i, o = 12 * (e.year() - t.year()) + (e.month() - t.month()),
            r = t.clone().add(o, "months");
        return 0 > e - r ? (n = t.clone().add(o - 1, "months"), i = (e - r) / (r - n)) : (n = t.clone().add(o + 1, "months"), i = (e - r) / (n - r)), -(o + i)
    }

    function le() {
        return this.clone().locale("en").format("ddd MMM DD YYYY HH:mm:ss [GMT]ZZ")
    }

    function ce() {
        var t = this.clone().utc();
        return 0 < t.year() && t.year() <= 9999 ? "function" == typeof Date.prototype.toISOString ? this.toDate().toISOString() : N(t, "YYYY-MM-DD[T]HH:mm:ss.SSS[Z]") : N(t, "YYYYYY-MM-DD[T]HH:mm:ss.SSS[Z]")
    }

    function ue(e) {
        var n = N(this, e || t.defaultFormat);
        return this.localeData().postformat(n)
    }

    function de(t, e) {
        return this.isValid() ? Vt({
            to: this,
            from: t
        }).locale(this.locale()).humanize(!e) : this.localeData().invalidDate()
    }

    function he(t) {
        return this.from(Et(), t)
    }

    function fe(t, e) {
        return this.isValid() ? Vt({
            from: this,
            to: t
        }).locale(this.locale()).humanize(!e) : this.localeData().invalidDate()
    }

    function pe(t) {
        return this.to(Et(), t)
    }

    function ge(t) {
        var e;
        return void 0 === t ? this._locale._abbr : (e = T(t), null != e && (this._locale = e), this)
    }

    function me() {
        return this._locale
    }

    function ve(t) {
        switch (t = k(t)) {
            case "year":
                this.month(0);
            case "quarter":
            case "month":
                this.date(1);
            case "week":
            case "isoWeek":
            case "day":
                this.hours(0);
            case "hour":
                this.minutes(0);
            case "minute":
                this.seconds(0);
            case "second":
                this.milliseconds(0)
        }
        return "week" === t && this.weekday(0), "isoWeek" === t && this.isoWeekday(1), "quarter" === t && this.month(3 * Math.floor(this.month() / 3)), this
    }

    function ye(t) {
        return t = k(t), void 0 === t || "millisecond" === t ? this : this.startOf(t).add(1, "isoWeek" === t ? "week" : t).subtract(1, "ms")
    }

    function we() {
        return +this._d - 6e4 * (this._offset || 0)
    }

    function be() {
        return Math.floor(+this / 1e3)
    }

    function xe() {
        return this._offset ? new Date(+this) : this._d
    }

    function Se() {
        var t = this;
        return [t.year(), t.month(), t.date(), t.hour(), t.minute(), t.second(), t.millisecond()]
    }

    function Te() {
        return u(this)
    }

    function Ce() {
        return s({}, c(this))
    }

    function ke() {
        return c(this).overflow
    }

    function Ee(t, e) {
        z(0, [t, t.length], 0, e)
    }

    function De(t, e, n) {
        return at(Et([t, 11, 31 + e - n]), e, n).week
    }

    function Me(t) {
        var e = at(this, this.localeData()._week.dow, this.localeData()._week.doy).year;
        return null == t ? e : this.add(t - e, "y")
    }

    function _e(t) {
        var e = at(this, 1, 4).year;
        return null == t ? e : this.add(t - e, "y")
    }

    function Re() {
        return De(this.year(), 1, 4)
    }

    function He() {
        var t = this.localeData()._week;
        return De(this.year(), t.dow, t.doy)
    }

    function ze(t) {
        return null == t ? Math.ceil((this.month() + 1) / 3) : this.month(3 * (t - 1) + this.month() % 3)
    }

    function Ae(t, e) {
        if ("string" == typeof t)
            if (isNaN(t)) {
                if (t = e.weekdaysParse(t), "number" != typeof t) return null
            } else t = parseInt(t, 10);
        return t
    }

    function Le(t) {
        return this._weekdays[t.day()]
    }

    function Ne(t) {
        return this._weekdaysShort[t.day()]
    }

    function Oe(t) {
        return this._weekdaysMin[t.day()]
    }

    function Ie(t) {
        var e, n, i;
        for (this._weekdaysParse || (this._weekdaysParse = []), e = 0; 7 > e; e++)
            if (this._weekdaysParse[e] || (n = Et([2e3, 1]).day(e), i = "^" + this.weekdays(n, "") + "|^" + this.weekdaysShort(n, "") + "|^" + this.weekdaysMin(n, ""), this._weekdaysParse[e] = new RegExp(i.replace(".", ""), "i")), this._weekdaysParse[e].test(t)) return e
    }

    function Pe(t) {
        var e = this._isUTC ? this._d.getUTCDay() : this._d.getDay();
        return null != t ? (t = Ae(t, this.localeData()), this.add(t - e, "d")) : e
    }

    function Fe(t) {
        var e = (this.day() + 7 - this.localeData()._week.dow) % 7;
        return null == t ? e : this.add(t - e, "d")
    }

    function We(t) {
        return null == t ? this.day() || 7 : this.day(this.day() % 7 ? t : t - 7)
    }

    function je(t, e) {
        z(t, 0, 0, function() {
            return this.localeData().meridiem(this.hours(), this.minutes(), e)
        })
    }

    function Ye(t, e) {
        return e._meridiemParse
    }

    function $e(t) {
        return "p" === (t + "").toLowerCase().charAt(0)
    }

    function qe(t, e, n) {
        return t > 11 ? n ? "pm" : "PM" : n ? "am" : "AM"
    }

    function Be(t) {
        z(0, [t, 3], 0, "millisecond")
    }

    function Ge() {
        return this._isUTC ? "UTC" : ""
    }

    function Ve() {
        return this._isUTC ? "Coordinated Universal Time" : ""
    }

    function Ue(t) {
        return Et(1e3 * t)
    }

    function Xe() {
        return Et.apply(null, arguments).parseZone()
    }

    function Ze(t, e, n) {
        var i = this._calendar[t];
        return "function" == typeof i ? i.call(e, n) : i
    }

    function Qe(t) {
        var e = this._longDateFormat[t];
        return !e && this._longDateFormat[t.toUpperCase()] && (e = this._longDateFormat[t.toUpperCase()].replace(/MMMM|MM|DD|dddd/g, function(t) {
            return t.slice(1)
        }), this._longDateFormat[t] = e), e
    }

    function Ke() {
        return this._invalidDate
    }

    function Je(t) {
        return this._ordinal.replace("%d", t)
    }

    function tn(t) {
        return t
    }

    function en(t, e, n, i) {
        var o = this._relativeTime[n];
        return "function" == typeof o ? o(t, e, n, i) : o.replace(/%d/i, t)
    }

    function nn(t, e) {
        var n = this._relativeTime[t > 0 ? "future" : "past"];
        return "function" == typeof n ? n(e) : n.replace(/%s/i, e)
    }

    function on(t) {
        var e, n;
        for (n in t) e = t[n], "function" == typeof e ? this[n] = e : this["_" + n] = e;
        this._ordinalParseLenient = new RegExp(this._ordinalParse.source + "|" + /\d{1,2}/.source)
    }

    function rn(t, e, n, i) {
        var o = T(),
            r = a().set(i, e);
        return o[n](r, t)
    }

    function sn(t, e, n, i, o) {
        if ("number" == typeof t && (e = t, t = void 0), t = t || "", null != e) return rn(t, e, n, o);
        var r, s = [];
        for (r = 0; i > r; r++) s[r] = rn(t, r, n, o);
        return s
    }

    function an(t, e) {
        return sn(t, e, "months", 12, "month")
    }

    function ln(t, e) {
        return sn(t, e, "monthsShort", 12, "month")
    }

    function cn(t, e) {
        return sn(t, e, "weekdays", 7, "day")
    }

    function un(t, e) {
        return sn(t, e, "weekdaysShort", 7, "day")
    }

    function dn(t, e) {
        return sn(t, e, "weekdaysMin", 7, "day")
    }

    function hn() {
        var t = this._data;
        return this._milliseconds = qi(this._milliseconds), this._days = qi(this._days), this._months = qi(this._months), t.milliseconds = qi(t.milliseconds), t.seconds = qi(t.seconds), t.minutes = qi(t.minutes), t.hours = qi(t.hours), t.months = qi(t.months), t.years = qi(t.years), this
    }

    function fn(t, e, n, i) {
        var o = Vt(e, n);
        return t._milliseconds += i * o._milliseconds, t._days += i * o._days, t._months += i * o._months, t._bubble()
    }

    function pn(t, e) {
        return fn(this, t, e, 1)
    }

    function gn(t, e) {
        return fn(this, t, e, -1)
    }

    function mn() {
        var t, e, n, i = this._milliseconds,
            o = this._days,
            r = this._months,
            s = this._data,
            a = 0;
        return s.milliseconds = i % 1e3, t = re(i / 1e3), s.seconds = t % 60, e = re(t / 60), s.minutes = e % 60, n = re(e / 60), s.hours = n % 24, o += re(n / 24), a = re(vn(o)), o -= re(yn(a)), r += re(o / 30), o %= 30, a += re(r / 12), r %= 12, s.days = o, s.months = r, s.years = a, this
    }

    function vn(t) {
        return 400 * t / 146097
    }

    function yn(t) {
        return 146097 * t / 400
    }

    function wn(t) {
        var e, n, i = this._milliseconds;
        if (t = k(t), "month" === t || "year" === t) return e = this._days + i / 864e5, n = this._months + 12 * vn(e), "month" === t ? n : n / 12;
        switch (e = this._days + Math.round(yn(this._months / 12)), t) {
            case "week":
                return e / 7 + i / 6048e5;
            case "day":
                return e + i / 864e5;
            case "hour":
                return 24 * e + i / 36e5;
            case "minute":
                return 1440 * e + i / 6e4;
            case "second":
                return 86400 * e + i / 1e3;
            case "millisecond":
                return Math.floor(864e5 * e) + i;
            default:
                throw new Error("Unknown unit " + t)
        }
    }

    function bn() {
        return this._milliseconds + 864e5 * this._days + this._months % 12 * 2592e6 + 31536e6 * g(this._months / 12)
    }

    function xn(t) {
        return function() {
            return this.as(t)
        }
    }

    function Sn(t) {
        return t = k(t), this[t + "s"]()
    }

    function Tn(t) {
        return function() {
            return this._data[t]
        }
    }

    function Cn() {
        return re(this.days() / 7)
    }

    function kn(t, e, n, i, o) {
        return o.relativeTime(e || 1, !!n, t, i)
    }

    function En(t, e, n) {
        var i = Vt(t).abs(),
            o = so(i.as("s")),
            r = so(i.as("m")),
            s = so(i.as("h")),
            a = so(i.as("d")),
            l = so(i.as("M")),
            c = so(i.as("y")),
            u = o < ao.s && ["s", o] || 1 === r && ["m"] || r < ao.m && ["mm", r] || 1 === s && ["h"] || s < ao.h && ["hh", s] || 1 === a && ["d"] || a < ao.d && ["dd", a] || 1 === l && ["M"] || l < ao.M && ["MM", l] || 1 === c && ["y"] || ["yy", c];
        return u[2] = e, u[3] = +t > 0, u[4] = n, kn.apply(null, u)
    }

    function Dn(t, e) {
        return void 0 === ao[t] ? !1 : void 0 === e ? ao[t] : (ao[t] = e, !0)
    }

    function Mn(t) {
        var e = this.localeData(),
            n = En(this, !t, e);
        return t && (n = e.pastFuture(+this, n)), e.postformat(n)
    }

    function _n() {
        var t = lo(this.years()),
            e = lo(this.months()),
            n = lo(this.days()),
            i = lo(this.hours()),
            o = lo(this.minutes()),
            r = lo(this.seconds() + this.milliseconds() / 1e3),
            s = this.asSeconds();
        return s ? (0 > s ? "-" : "") + "P" + (t ? t + "Y" : "") + (e ? e + "M" : "") + (n ? n + "D" : "") + (i || o || r ? "T" : "") + (i ? i + "H" : "") + (o ? o + "M" : "") + (r ? r + "S" : "") : "P0D"
    }
    var Rn, Hn, zn = t.momentProperties = [],
        An = !1,
        Ln = {},
        Nn = {},
        On = /(\[[^\[]*\])|(\\)?(Mo|MM?M?M?|Do|DDDo|DD?D?D?|ddd?d?|do?|w[o|w]?|W[o|W]?|Q|YYYYYY|YYYYY|YYYY|YY|gg(ggg?)?|GG(GGG?)?|e|E|a|A|hh?|HH?|mm?|ss?|S{1,4}|x|X|zz?|ZZ?|.)/g,
        In = /(\[[^\[]*\])|(\\)?(LTS|LT|LL?L?L?|l{1,4})/g,
        Pn = {},
        Fn = {},
        Wn = /\d/,
        jn = /\d\d/,
        Yn = /\d{3}/,
        $n = /\d{4}/,
        qn = /[+-]?\d{6}/,
        Bn = /\d\d?/,
        Gn = /\d{1,3}/,
        Vn = /\d{1,4}/,
        Un = /[+-]?\d{1,6}/,
        Xn = /\d+/,
        Zn = /[+-]?\d+/,
        Qn = /Z|[+-]\d\d:?\d\d/gi,
        Kn = /[+-]?\d+(\.\d{1,3})?/,
        Jn = /[0-9]*['a-z\u00A0-\u05FF\u0700-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+|[\u0600-\u06FF\/]+(\s*?[\u0600-\u06FF]+){1,2}/i,
        ti = {},
        ei = {},
        ni = 0,
        ii = 1,
        oi = 2,
        ri = 3,
        si = 4,
        ai = 5,
        li = 6;
    z("M", ["MM", 2], "Mo", function() {
        return this.month() + 1
    }), z("MMM", 0, 0, function(t) {
        return this.localeData().monthsShort(this, t)
    }), z("MMMM", 0, 0, function(t) {
        return this.localeData().months(this, t)
    }), C("month", "M"), I("M", Bn), I("MM", Bn, jn), I("MMM", Jn), I("MMMM", Jn), W(["M", "MM"], function(t, e) {
        e[ii] = g(t) - 1
    }), W(["MMM", "MMMM"], function(t, e, n, i) {
        var o = n._locale.monthsParse(t, i, n._strict);
        null != o ? e[ii] = o : c(n).invalidMonth = t
    });
    var ci = "January_February_March_April_May_June_July_August_September_October_November_December".split("_"),
        ui = "Jan_Feb_Mar_Apr_May_Jun_Jul_Aug_Sep_Oct_Nov_Dec".split("_"),
        di = {};
    t.suppressDeprecationWarnings = !1;
    var hi = /^\s*(?:[+-]\d{6}|\d{4})-(?:(\d\d-\d\d)|(W\d\d$)|(W\d\d-\d)|(\d\d\d))((T| )(\d\d(:\d\d(:\d\d(\.\d+)?)?)?)?([\+\-]\d\d(?::?\d\d)?|\s*Z)?)?$/,
        fi = [
            ["YYYYYY-MM-DD", /[+-]\d{6}-\d{2}-\d{2}/],
            ["YYYY-MM-DD", /\d{4}-\d{2}-\d{2}/],
            ["GGGG-[W]WW-E", /\d{4}-W\d{2}-\d/],
            ["GGGG-[W]WW", /\d{4}-W\d{2}/],
            ["YYYY-DDD", /\d{4}-\d{3}/]
        ],
        pi = [
            ["HH:mm:ss.SSSS", /(T| )\d\d:\d\d:\d\d\.\d+/],
            ["HH:mm:ss", /(T| )\d\d:\d\d:\d\d/],
            ["HH:mm", /(T| )\d\d:\d\d/],
            ["HH", /(T| )\d\d/]
        ],
        gi = /^\/?Date\((\-?\d+)/i;
    t.createFromInputFallback = K("moment construction falls back to js Date. This is discouraged and will be removed in upcoming major release. Please refer to https://github.com/moment/moment/issues/1407 for more info.", function(t) {
        t._d = new Date(t._i + (t._useUTC ? " UTC" : ""))
    }), z(0, ["YY", 2], 0, function() {
        return this.year() % 100
    }), z(0, ["YYYY", 4], 0, "year"), z(0, ["YYYYY", 5], 0, "year"), z(0, ["YYYYYY", 6, !0], 0, "year"), C("year", "y"), I("Y", Zn), I("YY", Bn, jn), I("YYYY", Vn, $n), I("YYYYY", Un, qn), I("YYYYYY", Un, qn), W(["YYYY", "YYYYY", "YYYYYY"], ni), W("YY", function(e, n) {
        n[ni] = t.parseTwoDigitYear(e)
    }), t.parseTwoDigitYear = function(t) {
        return g(t) + (g(t) > 68 ? 1900 : 2e3)
    };
    var mi = D("FullYear", !1);
    z("w", ["ww", 2], "wo", "week"), z("W", ["WW", 2], "Wo", "isoWeek"), C("week", "w"), C("isoWeek", "W"), I("w", Bn), I("ww", Bn, jn), I("W", Bn), I("WW", Bn, jn), j(["w", "ww", "W", "WW"], function(t, e, n, i) {
        e[i.substr(0, 1)] = g(t)
    });
    var vi = {
        dow: 0,
        doy: 6
    };
    z("DDD", ["DDDD", 3], "DDDo", "dayOfYear"), C("dayOfYear", "DDD"), I("DDD", Gn), I("DDDD", Yn), W(["DDD", "DDDD"], function(t, e, n) {
        n._dayOfYear = g(t)
    }), t.ISO_8601 = function() {};
    var yi = K("moment().min is deprecated, use moment.min instead. https://github.com/moment/moment/issues/1548", function() {
            var t = Et.apply(null, arguments);
            return this > t ? this : t
        }),
        wi = K("moment().max is deprecated, use moment.max instead. https://github.com/moment/moment/issues/1548", function() {
            var t = Et.apply(null, arguments);
            return t > this ? this : t
        });
    zt("Z", ":"), zt("ZZ", ""), I("Z", Qn), I("ZZ", Qn), W(["Z", "ZZ"], function(t, e, n) {
        n._useUTC = !0, n._tzm = At(t)
    });
    var bi = /([\+\-]|\d\d)/gi;
    t.updateOffset = function() {};
    var xi = /(\-)?(?:(\d*)\.)?(\d+)\:(\d+)(?:\:(\d+)\.?(\d{3})?)?/,
        Si = /^(-)?P(?:(?:([0-9,.]*)Y)?(?:([0-9,.]*)M)?(?:([0-9,.]*)D)?(?:T(?:([0-9,.]*)H)?(?:([0-9,.]*)M)?(?:([0-9,.]*)S)?)?|([0-9,.]*)W)$/;
    Vt.fn = Rt.prototype;
    var Ti = Qt(1, "add"),
        Ci = Qt(-1, "subtract");
    t.defaultFormat = "YYYY-MM-DDTHH:mm:ssZ";
    var ki = K("moment().lang() is deprecated. Instead, use moment().localeData() to get the language configuration. Use moment().locale() to change languages.", function(t) {
        return void 0 === t ? this.localeData() : this.locale(t)
    });
    z(0, ["gg", 2], 0, function() {
        return this.weekYear() % 100
    }), z(0, ["GG", 2], 0, function() {
        return this.isoWeekYear() % 100
    }), Ee("gggg", "weekYear"), Ee("ggggg", "weekYear"), Ee("GGGG", "isoWeekYear"), Ee("GGGGG", "isoWeekYear"), C("weekYear", "gg"), C("isoWeekYear", "GG"), I("G", Zn), I("g", Zn), I("GG", Bn, jn), I("gg", Bn, jn), I("GGGG", Vn, $n), I("gggg", Vn, $n), I("GGGGG", Un, qn), I("ggggg", Un, qn), j(["gggg", "ggggg", "GGGG", "GGGGG"], function(t, e, n, i) {
        e[i.substr(0, 2)] = g(t)
    }), j(["gg", "GG"], function(e, n, i, o) {
        n[o] = t.parseTwoDigitYear(e)
    }), z("Q", 0, 0, "quarter"), C("quarter", "Q"), I("Q", Wn), W("Q", function(t, e) {
        e[ii] = 3 * (g(t) - 1)
    }), z("D", ["DD", 2], "Do", "date"), C("date", "D"), I("D", Bn), I("DD", Bn, jn), I("Do", function(t, e) {
        return t ? e._ordinalParse : e._ordinalParseLenient
    }), W(["D", "DD"], oi), W("Do", function(t, e) {
        e[oi] = g(t.match(Bn)[0], 10)
    });
    var Ei = D("Date", !0);
    z("d", 0, "do", "day"), z("dd", 0, 0, function(t) {
        return this.localeData().weekdaysMin(this, t)
    }), z("ddd", 0, 0, function(t) {
        return this.localeData().weekdaysShort(this, t)
    }), z("dddd", 0, 0, function(t) {
        return this.localeData().weekdays(this, t)
    }), z("e", 0, 0, "weekday"), z("E", 0, 0, "isoWeekday"), C("day", "d"), C("weekday", "e"), C("isoWeekday", "E"), I("d", Bn), I("e", Bn), I("E", Bn), I("dd", Jn), I("ddd", Jn), I("dddd", Jn), j(["dd", "ddd", "dddd"], function(t, e, n) {
        var i = n._locale.weekdaysParse(t);
        null != i ? e.d = i : c(n).invalidWeekday = t
    }), j(["d", "e", "E"], function(t, e, n, i) {
        e[i] = g(t)
    });
    var Di = "Sunday_Monday_Tuesday_Wednesday_Thursday_Friday_Saturday".split("_"),
        Mi = "Sun_Mon_Tue_Wed_Thu_Fri_Sat".split("_"),
        _i = "Su_Mo_Tu_We_Th_Fr_Sa".split("_");
    z("H", ["HH", 2], 0, "hour"), z("h", ["hh", 2], 0, function() {
        return this.hours() % 12 || 12
    }), je("a", !0), je("A", !1), C("hour", "h"), I("a", Ye), I("A", Ye), I("H", Bn), I("h", Bn), I("HH", Bn, jn), I("hh", Bn, jn), W(["H", "HH"], ri), W(["a", "A"], function(t, e, n) {
        n._isPm = n._locale.isPM(t), n._meridiem = t
    }), W(["h", "hh"], function(t, e, n) {
        e[ri] = g(t), c(n).bigHour = !0
    });
    var Ri = /[ap]\.?m?\.?/i,
        Hi = D("Hours", !0);
    z("m", ["mm", 2], 0, "minute"), C("minute", "m"), I("m", Bn), I("mm", Bn, jn), W(["m", "mm"], si);
    var zi = D("Minutes", !1);
    z("s", ["ss", 2], 0, "second"), C("second", "s"), I("s", Bn), I("ss", Bn, jn), W(["s", "ss"], ai);
    var Ai = D("Seconds", !1);
    z("S", 0, 0, function() {
        return~~ (this.millisecond() / 100)
    }), z(0, ["SS", 2], 0, function() {
        return~~ (this.millisecond() / 10)
    }), Be("SSS"), Be("SSSS"), C("millisecond", "ms"), I("S", Gn, Wn), I("SS", Gn, jn), I("SSS", Gn, Yn), I("SSSS", Xn), W(["S", "SS", "SSS", "SSSS"], function(t, e) {
        e[li] = g(1e3 * ("0." + t))
    });
    var Li = D("Milliseconds", !1);
    z("z", 0, 0, "zoneAbbr"), z("zz", 0, 0, "zoneName");
    var Ni = f.prototype;
    Ni.add = Ti, Ni.calendar = Jt, Ni.clone = te, Ni.diff = se, Ni.endOf = ye, Ni.format = ue, Ni.from = de, Ni.fromNow = he, Ni.to = fe, Ni.toNow = pe, Ni.get = R, Ni.invalidAt = ke, Ni.isAfter = ee, Ni.isBefore = ne, Ni.isBetween = ie, Ni.isSame = oe, Ni.isValid = Te, Ni.lang = ki, Ni.locale = ge, Ni.localeData = me, Ni.max = wi, Ni.min = yi, Ni.parsingFlags = Ce, Ni.set = R, Ni.startOf = ve, Ni.subtract = Ci, Ni.toArray = Se, Ni.toDate = xe, Ni.toISOString = ce, Ni.toJSON = ce, Ni.toString = le, Ni.unix = be, Ni.valueOf = we, Ni.year = mi, Ni.isLeapYear = st, Ni.weekYear = Me, Ni.isoWeekYear = _e, Ni.quarter = Ni.quarters = ze, Ni.month = U, Ni.daysInMonth = X, Ni.week = Ni.weeks = dt, Ni.isoWeek = Ni.isoWeeks = ht, Ni.weeksInYear = He, Ni.isoWeeksInYear = Re, Ni.date = Ei, Ni.day = Ni.days = Pe, Ni.weekday = Fe, Ni.isoWeekday = We, Ni.dayOfYear = pt, Ni.hour = Ni.hours = Hi, Ni.minute = Ni.minutes = zi, Ni.second = Ni.seconds = Ai, Ni.millisecond = Ni.milliseconds = Li, Ni.utcOffset = Ot, Ni.utc = Pt, Ni.local = Ft, Ni.parseZone = Wt, Ni.hasAlignedHourOffset = jt, Ni.isDST = Yt, Ni.isDSTShifted = $t, Ni.isLocal = qt, Ni.isUtcOffset = Bt, Ni.isUtc = Gt, Ni.isUTC = Gt, Ni.zoneAbbr = Ge, Ni.zoneName = Ve, Ni.dates = K("dates accessor is deprecated. Use date instead.", Ei), Ni.months = K("months accessor is deprecated. Use month instead", U), Ni.years = K("years accessor is deprecated. Use year instead", mi), Ni.zone = K("moment().zone is deprecated, use moment().utcOffset instead. https://github.com/moment/moment/issues/1779", It);
    var Oi = Ni,
        Ii = {
            sameDay: "[Today at] LT",
            nextDay: "[Tomorrow at] LT",
            nextWeek: "dddd [at] LT",
            lastDay: "[Yesterday at] LT",
            lastWeek: "[Last] dddd [at] LT",
            sameElse: "L"
        },
        Pi = {
            LTS: "h:mm:ss A",
            LT: "h:mm A",
            L: "MM/DD/YYYY",
            LL: "MMMM D, YYYY",
            LLL: "MMMM D, YYYY LT",
            LLLL: "dddd, MMMM D, YYYY LT"
        },
        Fi = "Invalid date",
        Wi = "%d",
        ji = /\d{1,2}/,
        Yi = {
            future: "in %s",
            past: "%s ago",
            s: "a few seconds",
            m: "a minute",
            mm: "%d minutes",
            h: "an hour",
            hh: "%d hours",
            d: "a day",
            dd: "%d days",
            M: "a month",
            MM: "%d months",
            y: "a year",
            yy: "%d years"
        },
        $i = v.prototype;
    $i._calendar = Ii, $i.calendar = Ze, $i._longDateFormat = Pi, $i.longDateFormat = Qe, $i._invalidDate = Fi, $i.invalidDate = Ke, $i._ordinal = Wi, $i.ordinal = Je, $i._ordinalParse = ji, $i.preparse = tn, $i.postformat = tn, $i._relativeTime = Yi, $i.relativeTime = en, $i.pastFuture = nn, $i.set = on, $i.months = q, $i._months = ci, $i.monthsShort = B, $i._monthsShort = ui, $i.monthsParse = G, $i.week = lt, $i._week = vi, $i.firstDayOfYear = ut, $i.firstDayOfWeek = ct, $i.weekdays = Le, $i._weekdays = Di, $i.weekdaysMin = Oe, $i._weekdaysMin = _i, $i.weekdaysShort = Ne, $i._weekdaysShort = Mi, $i.weekdaysParse = Ie, $i.isPM = $e, $i._meridiemParse = Ri, $i.meridiem = qe, x("en", {
        ordinalParse: /\d{1,2}(th|st|nd|rd)/,
        ordinal: function(t) {
            var e = t % 10,
                n = 1 === g(t % 100 / 10) ? "th" : 1 === e ? "st" : 2 === e ? "nd" : 3 === e ? "rd" : "th";
            return t + n
        }
    }), t.lang = K("moment.lang is deprecated. Use moment.locale instead.", x), t.langData = K("moment.langData is deprecated. Use moment.localeData instead.", T);
    var qi = Math.abs,
        Bi = xn("ms"),
        Gi = xn("s"),
        Vi = xn("m"),
        Ui = xn("h"),
        Xi = xn("d"),
        Zi = xn("w"),
        Qi = xn("M"),
        Ki = xn("y"),
        Ji = Tn("milliseconds"),
        to = Tn("seconds"),
        eo = Tn("minutes"),
        no = Tn("hours"),
        io = Tn("days"),
        oo = Tn("months"),
        ro = Tn("years"),
        so = Math.round,
        ao = {
            s: 45,
            m: 45,
            h: 22,
            d: 26,
            M: 11
        },
        lo = Math.abs,
        co = Rt.prototype;
    co.abs = hn, co.add = pn, co.subtract = gn, co.as = wn, co.asMilliseconds = Bi, co.asSeconds = Gi, co.asMinutes = Vi, co.asHours = Ui, co.asDays = Xi, co.asWeeks = Zi, co.asMonths = Qi, co.asYears = Ki, co.valueOf = bn, co._bubble = mn, co.get = Sn, co.milliseconds = Ji, co.seconds = to, co.minutes = eo, co.hours = no, co.days = io, co.weeks = Cn, co.months = oo, co.years = ro, co.humanize = Mn, co.toISOString = _n, co.toString = _n, co.toJSON = _n, co.locale = ge, co.localeData = me, co.toIsoString = K("toIsoString() is deprecated. Please use toISOString() instead (notice the capitals)", _n), co.lang = ki, z("X", 0, 0, "unix"), z("x", 0, 0, "valueOf"), I("x", Zn), I("X", Kn), W("X", function(t, e, n) {
        n._d = new Date(1e3 * parseFloat(t, 10))
    }), W("x", function(t, e, n) {
        n._d = new Date(g(t))
    }), t.version = "2.10.3", e(Et), t.fn = Oi, t.min = Mt, t.max = _t, t.utc = a, t.unix = Ue, t.months = an, t.isDate = i, t.locale = x, t.invalid = d, t.duration = Vt, t.isMoment = p, t.weekdays = cn, t.parseZone = Xe, t.localeData = T, t.isDuration = Ht, t.monthsShort = ln, t.weekdaysMin = dn, t.defineLocale = S, t.weekdaysShort = un, t.normalizeUnits = k, t.relativeTimeThreshold = Dn;
    var uo = t;
    return uo
}),
function(t) {
    "function" == typeof define && define.amd ? define(["jquery", "moment"], t) : "object" == typeof exports ? module.exports = t(require("jquery"), require("moment")) : t(jQuery, moment)
}(function(t, e) {
    function n() {
        var e, n, i, o, r, s = Array.prototype.slice.call(arguments),
            a = {};
        for (e = 0; Ot.length > e; e++) {
            for (n = Ot[e], i = null, o = 0; s.length > o; o++) r = s[o][n], t.isPlainObject(r) ? i = t.extend(i || {}, r) : null != r && (i = null);
            null !== i && (a[n] = i)
        }
        return s.unshift({}), s.push(a), t.extend.apply(t, s)
    }

    function i(e) {
        var n, i = {
            views: e.views || {}
        };
        return t.each(e, function(e, o) {
            "views" != e && (t.isPlainObject(o) && !/(time|duration|interval)$/i.test(e) && -1 == t.inArray(e, Ot) ? (n = null, t.each(o, function(t, o) {
                /^(month|week|day|default|basic(Week|Day)?|agenda(Week|Day)?)$/.test(t) ? (i.views[t] || (i.views[t] = {}), i.views[t][e] = o) : (n || (n = {}), n[t] = o)
            }), n && (i[e] = n)) : i[e] = o)
        }), i
    }

    function o(t, e) {
        e.left && t.css({
            "border-left-width": 1,
            "margin-left": e.left - 1
        }), e.right && t.css({
            "border-right-width": 1,
            "margin-right": e.right - 1
        })
    }

    function r(t) {
        t.css({
            "margin-left": "",
            "margin-right": "",
            "border-left-width": "",
            "border-right-width": ""
        })
    }

    function s() {
        t("body").addClass("fc-not-allowed")
    }

    function a() {
        t("body").removeClass("fc-not-allowed")
    }

    function l(e, n, i) {
        var o = Math.floor(n / e.length),
            r = Math.floor(n - o * (e.length - 1)),
            s = [],
            a = [],
            l = [],
            u = 0;
        c(e), e.each(function(n, i) {
            var c = n === e.length - 1 ? r : o,
                d = t(i).outerHeight(!0);
            c > d ? (s.push(i), a.push(d), l.push(t(i).height())) : u += d
        }), i && (n -= u, o = Math.floor(n / s.length), r = Math.floor(n - o * (s.length - 1))), t(s).each(function(e, n) {
            var i = e === s.length - 1 ? r : o,
                c = a[e],
                u = l[e],
                d = i - (c - u);
            i > c && t(n).height(d)
        })
    }

    function c(t) {
        t.height("")
    }

    function u(e) {
        var n = 0;
        return e.find("> *").each(function(e, i) {
            var o = t(i).outerWidth();
            o > n && (n = o)
        }), n++, e.width(n), n
    }

    function d(t, e) {
        return t.height(e).addClass("fc-scroller"), t[0].scrollHeight - 1 > t[0].clientHeight ? !0 : (h(t), !1)
    }

    function h(t) {
        t.height("").removeClass("fc-scroller")
    }

    function f(e) {
        var n = e.css("position"),
            i = e.parents().filter(function() {
                var e = t(this);
                return /(auto|scroll)/.test(e.css("overflow") + e.css("overflow-y") + e.css("overflow-x"))
            }).eq(0);
        return "fixed" !== n && i.length ? i : t(e[0].ownerDocument || document)
    }

    function p(t) {
        var e = t.offset();
        return {
            left: e.left,
            right: e.left + t.outerWidth(),
            top: e.top,
            bottom: e.top + t.outerHeight()
        }
    }

    function g(t) {
        var e = t.offset(),
            n = v(t),
            i = e.left + b(t, "border-left-width") + n.left,
            o = e.top + b(t, "border-top-width") + n.top;
        return {
            left: i,
            right: i + t[0].clientWidth,
            top: o,
            bottom: o + t[0].clientHeight
        }
    }

    function m(t) {
        var e = t.offset(),
            n = e.left + b(t, "border-left-width") + b(t, "padding-left"),
            i = e.top + b(t, "border-top-width") + b(t, "padding-top");
        return {
            left: n,
            right: n + t.width(),
            top: i,
            bottom: i + t.height()
        }
    }

    function v(t) {
        var e = t.innerWidth() - t[0].clientWidth,
            n = {
                left: 0,
                right: 0,
                top: 0,
                bottom: t.innerHeight() - t[0].clientHeight
            };
        return y() && "rtl" == t.css("direction") ? n.left = e : n.right = e, n
    }

    function y() {
        return null === It && (It = w()), It
    }

    function w() {
        var e = t("<div><div/></div>").css({
                position: "absolute",
                top: -1e3,
                left: 0,
                border: 0,
                padding: 0,
                overflow: "scroll",
                direction: "rtl"
            }).appendTo("body"),
            n = e.children(),
            i = n.offset().left > e.offset().left;
        return e.remove(), i
    }

    function b(t, e) {
        return parseFloat(t.css(e)) || 0
    }

    function x(t) {
        return 1 == t.which && !t.ctrlKey
    }

    function S(t, e) {
        var n = {
            left: Math.max(t.left, e.left),
            right: Math.min(t.right, e.right),
            top: Math.max(t.top, e.top),
            bottom: Math.min(t.bottom, e.bottom)
        };
        return n.left < n.right && n.top < n.bottom ? n : !1
    }

    function T(t, e) {
        return {
            left: Math.min(Math.max(t.left, e.left), e.right),
            top: Math.min(Math.max(t.top, e.top), e.bottom)
        }
    }

    function C(t) {
        return {
            left: (t.left + t.right) / 2,
            top: (t.top + t.bottom) / 2
        }
    }

    function k(t, e) {
        return {
            left: t.left - e.left,
            top: t.top - e.top
        }
    }

    function E(t, e) {
        var n, i, o, r, s = t.start,
            a = t.end,
            l = e.start,
            c = e.end;
        return a > l && c > s ? (s >= l ? (n = s.clone(), o = !0) : (n = l.clone(), o = !1), c >= a ? (i = a.clone(), r = !0) : (i = c.clone(), r = !1), {
            start: n,
            end: i,
            isStart: o,
            isEnd: r
        }) : void 0
    }

    function D(t, n) {
        return e.duration({
            days: t.clone().stripTime().diff(n.clone().stripTime(), "days"),
            ms: t.time() - n.time()
        })
    }

    function M(t, n) {
        return e.duration({
            days: t.clone().stripTime().diff(n.clone().stripTime(), "days")
        })
    }

    function _(t, n, i) {
        return e.duration(Math.round(t.diff(n, i, !0)), i)
    }

    function R(t, e) {
        var n, i, o;
        for (n = 0; Yt.length > n && (i = Yt[n], o = H(i, t, e), !(o >= 1 && V(o))); n++);
        return i
    }

    function H(t, n, i) {
        return null != i ? i.diff(n, t, !0) : e.isDuration(n) ? n.as(t) : n.end.diff(n.start, t, !0)
    }

    function z(t) {
        return Boolean(t.hours() || t.minutes() || t.seconds() || t.milliseconds())
    }

    function A(t) {
        return "[object Date]" === Object.prototype.toString.call(t) || t instanceof Date
    }

    function L(t) {
        return /^\d+\:\d+(?:\:\d+\.?(?:\d{3})?)?$/.test(t)
    }

    function N(t) {
        var e = function() {};
        return e.prototype = t, new e
    }

    function O(t, e) {
        for (var n in t) P(t, n) && (e[n] = t[n])
    }

    function I(t, e) {
        var n, i, o = ["constructor", "toString", "valueOf"];
        for (n = 0; o.length > n; n++) i = o[n], t[i] !== Object.prototype[i] && (e[i] = t[i])
    }

    function P(t, e) {
        return $t.call(t, e)
    }

    function F(e) {
        return /undefined|null|boolean|number|string/.test(t.type(e))
    }

    function W(e, n, i) {
        if (t.isFunction(e) && (e = [e]), e) {
            var o, r;
            for (o = 0; e.length > o; o++) r = e[o].apply(n, i) || r;
            return r
        }
    }

    function j() {
        for (var t = 0; arguments.length > t; t++)
            if (void 0 !== arguments[t]) return arguments[t]
    }

    function Y(t) {
        return (t + "").replace(/&/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;").replace(/'/g, "&#039;").replace(/"/g, "&quot;").replace(/\n/g, "<br />")
    }

    function $(t) {
        return t.replace(/&.*?;/g, "")
    }

    function q(e) {
        var n = [];
        return t.each(e, function(t, e) {
            null != e && n.push(t + ":" + e)
        }), n.join(";")
    }

    function B(t) {
        return t.charAt(0).toUpperCase() + t.slice(1)
    }

    function G(t, e) {
        return t - e
    }

    function V(t) {
        return 0 === t % 1
    }

    function U(t, e) {
        var n = t[e];
        return function() {
            return n.apply(t, arguments)
        }
    }

    function X(t, e) {
        var n, i, o, r, s = function() {
            var a = +new Date - r;
            e > a && a > 0 ? n = setTimeout(s, e - a) : (n = null, t.apply(o, i), n || (o = i = null))
        };
        return function() {
            o = this, i = arguments, r = +new Date, n || (n = setTimeout(s, e))
        }
    }

    function Z(n, i, o) {
        var r, s, a, l, c = n[0],
            u = 1 == n.length && "string" == typeof c;
        return e.isMoment(c) ? (l = e.apply(null, n), K(c, l)) : A(c) || void 0 === c ? l = e.apply(null, n) : (r = !1, s = !1, u ? qt.test(c) ? (c += "-01", n = [c], r = !0, s = !0) : (a = Bt.exec(c)) && (r = !a[5], s = !0) : t.isArray(c) && (s = !0), l = i || r ? e.utc.apply(e, n) : e.apply(null, n), r ? (l._ambigTime = !0, l._ambigZone = !0) : o && (s ? l._ambigZone = !0 : u && (l.utcOffset ? l.utcOffset(c) : l.zone(c)))), l._fullCalendar = !0, l
    }

    function Q(t, n) {
        var i, o, r = !1,
            s = !1,
            a = t.length,
            l = [];
        for (i = 0; a > i; i++) o = t[i], e.isMoment(o) || (o = Lt.moment.parseZone(o)), r = r || o._ambigTime, s = s || o._ambigZone, l.push(o);
        for (i = 0; a > i; i++) o = l[i], n || !r || o._ambigTime ? s && !o._ambigZone && (l[i] = o.clone().stripZone()) : l[i] = o.clone().stripTime();
        return l
    }

    function K(t, e) {
        t._ambigTime ? e._ambigTime = !0 : e._ambigTime && (e._ambigTime = !1), t._ambigZone ? e._ambigZone = !0 : e._ambigZone && (e._ambigZone = !1)
    }

    function J(t, e) {
        t.year(e[0] || 0).month(e[1] || 0).date(e[2] || 0).hours(e[3] || 0).minutes(e[4] || 0).seconds(e[5] || 0).milliseconds(e[6] || 0)
    }

    function tt(t, e) {
        return Vt.format.call(t, e)
    }

    function et(t, e) {
        return nt(t, at(e))
    }

    function nt(t, e) {
        var n, i = "";
        for (n = 0; e.length > n; n++) i += it(t, e[n]);
        return i
    }

    function it(t, e) {
        var n, i;
        return "string" == typeof e ? e : (n = e.token) ? Ut[n] ? Ut[n](t) : tt(t, n) : e.maybe && (i = nt(t, e.maybe), i.match(/[1-9]/)) ? i : ""
    }

    function ot(t, e, n, i, o) {
        var r;
        return t = Lt.moment.parseZone(t), e = Lt.moment.parseZone(e), r = (t.localeData || t.lang).call(t), n = r.longDateFormat(n) || n, i = i || " - ", rt(t, e, at(n), i, o)
    }

    function rt(t, e, n, i, o) {
        var r, s, a, l, c = "",
            u = "",
            d = "",
            h = "",
            f = "";
        for (s = 0; n.length > s && (r = st(t, e, n[s]), r !== !1); s++) c += r;
        for (a = n.length - 1; a > s && (r = st(t, e, n[a]), r !== !1); a--) u = r + u;
        for (l = s; a >= l; l++) d += it(t, n[l]), h += it(e, n[l]);
        return (d || h) && (f = o ? h + i + d : d + i + h), c + f + u
    }

    function st(t, e, n) {
        var i, o;
        return "string" == typeof n ? n : (i = n.token) && (o = Xt[i.charAt(0)], o && t.isSame(e, o)) ? tt(t, i) : !1
    }

    function at(t) {
        return t in Zt ? Zt[t] : Zt[t] = lt(t)
    }

    function lt(t) {
        for (var e, n = [], i = /\[([^\]]*)\]|\(([^\)]*)\)|(LTS|LT|(\w)\4*o?)|([^\w\[\(]+)/g; e = i.exec(t);) e[1] ? n.push(e[1]) : e[2] ? n.push({
            maybe: lt(e[2])
        }) : e[3] ? n.push({
            token: e[3]
        }) : e[5] && n.push(e[5]);
        return n
    }

    function ct() {}

    function ut(t, e) {
        return t || e ? t && e ? t.grid === e.grid && t.row === e.row && t.col === e.col : !1 : !0
    }

    function dt(t) {
        var e = ft(t);
        return "background" === e || "inverse-background" === e
    }

    function ht(t) {
        return "inverse-background" === ft(t)
    }

    function ft(t) {
        return j((t.source || {}).rendering, t.rendering)
    }

    function pt(t) {
        var e, n, i = {};
        for (e = 0; t.length > e; e++) n = t[e], (i[n._id] || (i[n._id] = [])).push(n);
        return i
    }

    function gt(t, e) {
        return t.eventStartMS - e.eventStartMS
    }

    function mt(t, e) {
        return t.eventStartMS - e.eventStartMS || e.eventDurationMS - t.eventDurationMS || e.event.allDay - t.event.allDay || (t.event.title || "").localeCompare(e.event.title)
    }

    function vt(n) {
        var i, o, r, s, a = Lt.dataAttrPrefix;
        return a && (a += "-"), i = n.data(a + "event") || null, i && (i = "object" == typeof i ? t.extend({}, i) : {}, o = i.start, null == o && (o = i.time), r = i.duration, s = i.stick, delete i.start, delete i.time, delete i.duration, delete i.stick), null == o && (o = n.data(a + "start")), null == o && (o = n.data(a + "time")), null == r && (r = n.data(a + "duration")), null == s && (s = n.data(a + "stick")), o = null != o ? e.duration(o) : null, r = null != r ? e.duration(r) : null, s = Boolean(s), {
            eventProps: i,
            startTime: o,
            duration: r,
            stick: s
        }
    }

    function yt(t, e) {
        var n, i;
        for (n = 0; e.length > n; n++)
            if (i = e[n], i.leftCol <= t.rightCol && i.rightCol >= t.leftCol) return !0;
        return !1
    }

    function wt(t, e) {
        return t.leftCol - e.leftCol
    }

    function bt(t) {
        var e, n, i;
        if (t.sort(mt), e = xt(t), St(e), n = e[0]) {
            for (i = 0; n.length > i; i++) Tt(n[i]);
            for (i = 0; n.length > i; i++) Ct(n[i], 0, 0)
        }
    }

    function xt(t) {
        var e, n, i, o = [];
        for (e = 0; t.length > e; e++) {
            for (n = t[e], i = 0; o.length > i && kt(n, o[i]).length; i++);
            n.level = i, (o[i] || (o[i] = [])).push(n)
        }
        return o
    }

    function St(t) {
        var e, n, i, o, r;
        for (e = 0; t.length > e; e++)
            for (n = t[e], i = 0; n.length > i; i++)
                for (o = n[i], o.forwardSegs = [], r = e + 1; t.length > r; r++) kt(o, t[r], o.forwardSegs)
    }

    function Tt(t) {
        var e, n, i = t.forwardSegs,
            o = 0;
        if (void 0 === t.forwardPressure) {
            for (e = 0; i.length > e; e++) n = i[e], Tt(n), o = Math.max(o, 1 + n.forwardPressure);
            t.forwardPressure = o
        }
    }

    function Ct(t, e, n) {
        var i, o = t.forwardSegs;
        if (void 0 === t.forwardCoord)
            for (o.length ? (o.sort(Dt), Ct(o[0], e + 1, n), t.forwardCoord = o[0].backwardCoord) : t.forwardCoord = 1, t.backwardCoord = t.forwardCoord - (t.forwardCoord - n) / (e + 1), i = 0; o.length > i; i++) Ct(o[i], 0, t.forwardCoord)
    }

    function kt(t, e, n) {
        n = n || [];
        for (var i = 0; e.length > i; i++) Et(t, e[i]) && n.push(e[i]);
        return n
    }

    function Et(t, e) {
        return t.bottom > e.top && t.top < e.bottom
    }

    function Dt(t, e) {
        return e.forwardPressure - t.forwardPressure || (t.backwardCoord || 0) - (e.backwardCoord || 0) || mt(t, e)
    }

    function Mt(n, i) {
        function o() {
            G ? a() && (u(), l()) : r()
        }

        function r() {
            V = j.theme ? "ui" : "fc", n.addClass("fc"), n.addClass(j.isRTL ? "fc-rtl" : "fc-ltr"), n.addClass(j.theme ? "ui-widget" : "fc-unthemed"), G = t("<div class='fc-view-container'/>").prependTo(n), q = W.header = new Ht(W, j), B = q.render(), B && n.prepend(B), l(j.defaultView), j.handleWindowResize && (Q = X(h, j.windowResizeDelay), t(window).resize(Q))
        }

        function s() {
            U && U.removeElement(), q.destroy(), G.remove(), n.removeClass("fc fc-ltr fc-rtl fc-unthemed ui-widget"), Q && t(window).unbind("resize", Q)
        }

        function a() {
            return n.is(":visible")
        }

        function l(e) {
            it++, U && e && U.type !== e && (q.deactivateButton(U.type), A(), U.removeElement(), U = W.view = null), !U && e && (U = W.view = nt[e] || (nt[e] = W.instantiateView(e)), U.setElement(t("<div class='fc-view fc-" + e + "-view' />").appendTo(G)), q.activateButton(e)), U && (K = U.massageCurrentDate(K), U.isDisplayed && K.isWithin(U.intervalStart, U.intervalEnd) || a() && (A(), U.display(K), L(), b(), x(), m())), L(), it--
        }

        function c(t) {
            return a() ? (t && d(), it++, U.updateSize(!0), it--, !0) : void 0
        }

        function u() {
            a() && d()
        }

        function d() {
            Z = "number" == typeof j.contentHeight ? j.contentHeight : "number" == typeof j.height ? j.height - (B ? B.outerHeight(!0) : 0) : Math.round(G.width() / Math.max(j.aspectRatio, .5))
        }

        function h(t) {
            !it && t.target === window && U.start && c(!0) && U.trigger("windowResize", et)
        }

        function f() {
            g(), v()
        }

        function p() {
            a() && (A(), U.displayEvents(ot), L())
        }

        function g() {
            A(), U.clearEvents(), L()
        }

        function m() {
            !j.lazyFetching || J(U.start, U.end) ? v() : p()
        }

        function v() {
            tt(U.start, U.end)
        }

        function y(t) {
            ot = t, p()
        }

        function w() {
            p()
        }

        function b() {
            q.updateTitle(U.title)
        }

        function x() {
            var t = W.getNow();
            t.isWithin(U.intervalStart, U.intervalEnd) ? q.disableButton("today") : q.enableButton("today")
        }

        function S(t, e) {
            t = W.moment(t), e = e ? W.moment(e) : t.clone().add(t.hasTime() ? W.defaultTimedEventDuration : W.defaultAllDayEventDuration), U.select({
                start: t,
                end: e
            })
        }

        function T() {
            U && U.unselect()
        }

        function C() {
            K = U.computePrevDate(K), l()
        }

        function k() {
            K = U.computeNextDate(K), l()
        }

        function E() {
            K.add(-1, "years"), l()
        }

        function D() {
            K.add(1, "years"), l()
        }

        function M() {
            K = W.getNow(), l()
        }

        function _(t) {
            K = W.moment(t), l()
        }

        function R(t) {
            K.add(e.duration(t)), l()
        }

        function H(t, e) {
            var n;
            e = e || "day", n = W.getViewSpec(e) || W.getUnitViewSpec(e), K = t, l(n ? n.type : null)
        }

        function z() {
            return K.clone()
        }

        function A() {
            G.css({
                width: "100%",
                height: G.height(),
                overflow: "hidden"
            })
        }

        function L() {
            G.css({
                width: "",
                height: "",
                overflow: ""
            })
        }

        function O() {
            return W
        }

        function I() {
            return U
        }

        function P(t, e) {
            return void 0 === e ? j[t] : void(("height" == t || "contentHeight" == t || "aspectRatio" == t) && (j[t] = e, c(!0)))
        }

        function F(t, e) {
            return j[t] ? j[t].apply(e || et, Array.prototype.slice.call(arguments, 2)) : void 0
        }
        var W = this;
        W.initOptions(i || {});
        var j = this.options;
        W.render = o, W.destroy = s, W.refetchEvents = f, W.reportEvents = y, W.reportEventChange = w, W.rerenderEvents = p, W.changeView = l, W.select = S, W.unselect = T, W.prev = C, W.next = k, W.prevYear = E, W.nextYear = D, W.today = M,
        W.gotoDate = _, W.incrementDate = R, W.zoomTo = H, W.getDate = z, W.getCalendar = O, W.getView = I, W.option = P, W.trigger = F;
        var Y = N(Rt(j.lang));
        if (j.monthNames && (Y._months = j.monthNames), j.monthNamesShort && (Y._monthsShort = j.monthNamesShort), j.dayNames && (Y._weekdays = j.dayNames), j.dayNamesShort && (Y._weekdaysShort = j.dayNamesShort), null != j.firstDay) {
            var $ = N(Y._week);
            $.dow = j.firstDay, Y._week = $
        }
        Y._fullCalendar_weekCalc = function(t) {
            return "function" == typeof t ? t : "local" === t ? t : "iso" === t || "ISO" === t ? "ISO" : void 0
        }(j.weekNumberCalculation), W.defaultAllDayEventDuration = e.duration(j.defaultAllDayEventDuration), W.defaultTimedEventDuration = e.duration(j.defaultTimedEventDuration), W.moment = function() {
            var t;
            return "local" === j.timezone ? (t = Lt.moment.apply(null, arguments), t.hasTime() && t.local()) : t = "UTC" === j.timezone ? Lt.moment.utc.apply(null, arguments) : Lt.moment.parseZone.apply(null, arguments), "_locale" in t ? t._locale = Y : t._lang = Y, t
        }, W.getIsAmbigTimezone = function() {
            return "local" !== j.timezone && "UTC" !== j.timezone
        }, W.rezoneDate = function(t) {
            return W.moment(t.toArray())
        }, W.getNow = function() {
            var t = j.now;
            return "function" == typeof t && (t = t()), W.moment(t)
        }, W.getEventEnd = function(t) {
            return t.end ? t.end.clone() : W.getDefaultEventEnd(t.allDay, t.start)
        }, W.getDefaultEventEnd = function(t, e) {
            var n = e.clone();
            return t ? n.stripTime().add(W.defaultAllDayEventDuration) : n.add(W.defaultTimedEventDuration), W.getIsAmbigTimezone() && n.stripZone(), n
        }, W.humanizeDuration = function(t) {
            return (t.locale || t.lang).call(t, j.lang).humanize()
        }, zt.call(W, j);
        var q, B, G, V, U, Z, Q, K, J = W.isFetchNeeded,
            tt = W.fetchEvents,
            et = n[0],
            nt = {},
            it = 0,
            ot = [];
        K = null != j.defaultDate ? W.moment(j.defaultDate) : W.getNow(), W.getSuggestedViewHeight = function() {
            return void 0 === Z && u(), Z
        }, W.isHeightAuto = function() {
            return "auto" === j.contentHeight || "auto" === j.height
        }
    }

    function _t(e) {
        t.each(he, function(t, n) {
            null == e[t] && (e[t] = n(e))
        })
    }

    function Rt(t) {
        var n = e.localeData || e.langData;
        return n.call(e, t) || n.call(e, "en")
    }

    function Ht(e, n) {
        function i() {
            var e = n.header;
            return f = n.theme ? "ui" : "fc", e ? p = t("<div class='fc-toolbar'/>").append(r("left")).append(r("right")).append(r("center")).append('<div class="fc-clear"/>') : void 0
        }

        function o() {
            p.remove()
        }

        function r(i) {
            var o = t('<div class="fc-' + i + '"/>'),
                r = n.header[i];
            return r && t.each(r.split(" "), function() {
                var i, r = t(),
                    s = !0;
                t.each(this.split(","), function(i, o) {
                    var a, l, c, u, d, h, p, m, v;
                    "title" == o ? (r = r.add(t("<h2>&nbsp;</h2>")), s = !1) : (a = e.getViewSpec(o), a ? (l = function() {
                        e.changeView(o)
                    }, g.push(o), c = a.buttonTextOverride, u = a.buttonTextDefault) : e[o] && (l = function() {
                        e[o]()
                    }, c = (e.overrides.buttonText || {})[o], u = n.buttonText[o]), l && (d = n.themeButtonIcons[o], h = n.buttonIcons[o], p = c ? Y(c) : d && n.theme ? "<span class='ui-icon ui-icon-" + d + "'></span>" : h && !n.theme ? "<span class='fc-icon fc-icon-" + h + "'></span>" : Y(u), m = ["fc-" + o + "-button", f + "-button", f + "-state-default"], v = t('<button type="button" class="' + m.join(" ") + '">' + p + "</button>").click(function() {
                        v.hasClass(f + "-state-disabled") || (l(), (v.hasClass(f + "-state-active") || v.hasClass(f + "-state-disabled")) && v.removeClass(f + "-state-hover"))
                    }).mousedown(function() {
                        v.not("." + f + "-state-active").not("." + f + "-state-disabled").addClass(f + "-state-down")
                    }).mouseup(function() {
                        v.removeClass(f + "-state-down")
                    }).hover(function() {
                        v.not("." + f + "-state-active").not("." + f + "-state-disabled").addClass(f + "-state-hover")
                    }, function() {
                        v.removeClass(f + "-state-hover").removeClass(f + "-state-down")
                    }), r = r.add(v)))
                }), s && r.first().addClass(f + "-corner-left").end().last().addClass(f + "-corner-right").end(), r.length > 1 ? (i = t("<div/>"), s && i.addClass("fc-button-group"), i.append(r), o.append(i)) : o.append(r)
            }), o
        }

        function s(t) {
            p.find("h2").text(t)
        }

        function a(t) {
            p.find(".fc-" + t + "-button").addClass(f + "-state-active")
        }

        function l(t) {
            p.find(".fc-" + t + "-button").removeClass(f + "-state-active")
        }

        function c(t) {
            p.find(".fc-" + t + "-button").attr("disabled", "disabled").addClass(f + "-state-disabled")
        }

        function u(t) {
            p.find(".fc-" + t + "-button").removeAttr("disabled").removeClass(f + "-state-disabled")
        }

        function d() {
            return g
        }
        var h = this;
        h.render = i, h.destroy = o, h.updateTitle = s, h.activateButton = a, h.deactivateButton = l, h.disableButton = c, h.enableButton = u, h.getViewsWithButtons = d;
        var f, p = t(),
            g = []
    }

    function zt(n) {
        function i(t, e) {
            return !q || t.clone().stripZone() < q.clone().stripZone() || e.clone().stripZone() > B.clone().stripZone()
        }

        function o(t, e) {
            q = t, B = e, tt = [];
            var n = ++Q,
                i = Z.length;
            K = i;
            for (var o = 0; i > o; o++) r(Z[o], n)
        }

        function r(e, n) {
            s(e, function(i) {
                var o, r, s, a = t.isArray(e.events);
                if (n == Q) {
                    if (i)
                        for (o = 0; i.length > o; o++) r = i[o], s = a ? r : b(r, e), s && tt.push.apply(tt, k(s));
                    K--, K || U(tt)
                }
            })
        }

        function s(e, i) {
            var o, r, a = Lt.sourceFetchers;
            for (o = 0; a.length > o; o++) {
                if (r = a[o].call($, e, q.clone(), B.clone(), n.timezone, i), r === !0) return;
                if ("object" == typeof r) return void s(r, i)
            }
            var l = e.events;
            if (l) t.isFunction(l) ? (y(), l.call($, q.clone(), B.clone(), n.timezone, function(t) {
                i(t), w()
            })) : t.isArray(l) ? i(l) : i();
            else {
                var c = e.url;
                if (c) {
                    var u, d = e.success,
                        h = e.error,
                        f = e.complete;
                    u = t.isFunction(e.data) ? e.data() : e.data;
                    var p = t.extend({}, u || {}),
                        g = j(e.startParam, n.startParam),
                        m = j(e.endParam, n.endParam),
                        v = j(e.timezoneParam, n.timezoneParam);
                    g && (p[g] = q.format()), m && (p[m] = B.format()), n.timezone && "local" != n.timezone && (p[v] = n.timezone), y(), t.ajax(t.extend({}, fe, e, {
                        data: p,
                        success: function(e) {
                            e = e || [];
                            var n = W(d, this, arguments);
                            t.isArray(n) && (e = n), i(e)
                        },
                        error: function() {
                            W(h, this, arguments), i()
                        },
                        complete: function() {
                            W(f, this, arguments), w()
                        }
                    }))
                } else i()
            }
        }

        function a(t) {
            var e = l(t);
            e && (Z.push(e), K++, r(e, Q))
        }

        function l(e) {
            var n, i, o = Lt.sourceNormalizers;
            if (t.isFunction(e) || t.isArray(e) ? n = {
                events: e
            } : "string" == typeof e ? n = {
                url: e
            } : "object" == typeof e && (n = t.extend({}, e)), n) {
                for (n.className ? "string" == typeof n.className && (n.className = n.className.split(/\s+/)) : n.className = [], t.isArray(n.events) && (n.origArray = n.events, n.events = t.map(n.events, function(t) {
                    return b(t, n)
                })), i = 0; o.length > i; i++) o[i].call($, n);
                return n
            }
        }

        function c(e) {
            Z = t.grep(Z, function(t) {
                return !u(t, e)
            }), tt = t.grep(tt, function(t) {
                return !u(t.source, e)
            }), U(tt)
        }

        function u(t, e) {
            return t && e && d(t) == d(e)
        }

        function d(t) {
            return ("object" == typeof t ? t.origArray || t.googleCalendarId || t.url || t.events : null) || t
        }

        function h(t) {
            t.start = $.moment(t.start), t.end = t.end ? $.moment(t.end) : null, E(t, f(t)), U(tt)
        }

        function f(e) {
            var n = {};
            return t.each(e, function(t, e) {
                p(t) && void 0 !== e && F(e) && (n[t] = e)
            }), n
        }

        function p(t) {
            return !/^_|^(id|allDay|start|end)$/.test(t)
        }

        function g(t, e) {
            var n, i, o, r = b(t);
            if (r) {
                for (n = k(r), i = 0; n.length > i; i++) o = n[i], o.source || (e && (X.events.push(o), o.source = X), tt.push(o));
                return U(tt), n
            }
            return []
        }

        function m(e) {
            var n, i;
            for (null == e ? e = function() {
                return !0
            } : t.isFunction(e) || (n = e + "", e = function(t) {
                return t._id == n
            }), tt = t.grep(tt, e, !0), i = 0; Z.length > i; i++) t.isArray(Z[i].events) && (Z[i].events = t.grep(Z[i].events, e, !0));
            U(tt)
        }

        function v(e) {
            return t.isFunction(e) ? t.grep(tt, e) : null != e ? (e += "", t.grep(tt, function(t) {
                return t._id == e
            })) : tt
        }

        function y() {
            J++ || G("loading", null, !0, V())
        }

        function w() {
            --J || G("loading", null, !1, V())
        }

        function b(i, o) {
            var r, s, a, l = {};
            if (n.eventDataTransform && (i = n.eventDataTransform(i)), o && o.eventDataTransform && (i = o.eventDataTransform(i)), t.extend(l, i), o && (l.source = o), l._id = i._id || (void 0 === i.id ? "_fc" + pe++ : i.id + ""), l.className = i.className ? "string" == typeof i.className ? i.className.split(/\s+/) : i.className : [], r = i.start || i.date, s = i.end, L(r) && (r = e.duration(r)), L(s) && (s = e.duration(s)), i.dow || e.isDuration(r) || e.isDuration(s)) l.start = r ? e.duration(r) : null, l.end = s ? e.duration(s) : null, l._recurring = !0;
            else {
                if (r && (r = $.moment(r), !r.isValid())) return !1;
                s && (s = $.moment(s), s.isValid() || (s = null)), a = i.allDay, void 0 === a && (a = j(o ? o.allDayDefault : void 0, n.allDayDefault)), x(r, s, a, l)
            }
            return l
        }

        function x(t, e, n, i) {
            i.start = t, i.end = e, i.allDay = n, S(i), At(i)
        }

        function S(t) {
            T(t), t.end && !t.end.isAfter(t.start) && (t.end = null), t.end || (t.end = n.forceEventDuration ? $.getDefaultEventEnd(t.allDay, t.start) : null)
        }

        function T(t) {
            null == t.allDay && (t.allDay = !(t.start.hasTime() || t.end && t.end.hasTime())), t.allDay ? (t.start.stripTime(), t.end && t.end.stripTime()) : (t.start.hasTime() || (t.start = $.rezoneDate(t.start)), t.end && !t.end.hasTime() && (t.end = $.rezoneDate(t.end)))
        }

        function C(e) {
            var n;
            return e.end || (n = e.allDay, null == n && (n = !e.start.hasTime()), e = t.extend({}, e), e.end = $.getDefaultEventEnd(n, e.start)), e
        }

        function k(e, n, i) {
            var o, r, s, a, l, c, u, d, h, f = [];
            if (n = n || q, i = i || B, e)
                if (e._recurring) {
                    if (r = e.dow)
                        for (o = {}, s = 0; r.length > s; s++) o[r[s]] = !0;
                    for (a = n.clone().stripTime(); a.isBefore(i);)(!o || o[a.day()]) && (l = e.start, c = e.end, u = a.clone(), d = null, l && (u = u.time(l)), c && (d = a.clone().time(c)), h = t.extend({}, e), x(u, d, !l && !c, h), f.push(h)), a.add(1, "days")
                } else f.push(e);
            return f
        }

        function E(e, n, i) {
            function o(t, e) {
                return i ? _(t, e, i) : n.allDay ? M(t, e) : D(t, e)
            }
            var r, s, a, l, c, u, d = {};
            return n = n || {}, n.start || (n.start = e.start.clone()), void 0 === n.end && (n.end = e.end ? e.end.clone() : null), null == n.allDay && (n.allDay = e.allDay), S(n), r = {
                start: e._start.clone(),
                end: e._end ? e._end.clone() : $.getDefaultEventEnd(e._allDay, e._start),
                allDay: n.allDay
            }, S(r), s = null !== e._end && null === n.end, a = o(n.start, r.start), n.end ? (l = o(n.end, r.end), c = l.subtract(a)) : c = null, t.each(n, function(t, e) {
                p(t) && void 0 !== e && (d[t] = e)
            }), u = R(v(e._id), s, n.allDay, a, c, d), {
                dateDelta: a,
                durationDelta: c,
                undo: u
            }
        }

        function R(e, n, i, o, r, s) {
            var a = $.getIsAmbigTimezone(),
                l = [];
            return o && !o.valueOf() && (o = null), r && !r.valueOf() && (r = null), t.each(e, function(e, c) {
                    var u, d;
                    u = {
                        start: c.start.clone(),
                        end: c.end ? c.end.clone() : null,
                        allDay: c.allDay
                    }, t.each(s, function(t) {
                        u[t] = c[t]
                    }), d = {
                        start: c._start,
                        end: c._end,
                        allDay: i
                    }, S(d), n ? d.end = null : r && !d.end && (d.end = $.getDefaultEventEnd(d.allDay, d.start)), o && (d.start.add(o), d.end && d.end.add(o)), r && d.end.add(r), a && !d.allDay && (o || r) && (d.start.stripZone(), d.end && d.end.stripZone()), t.extend(c, s, d), At(c), l.push(function() {
                        t.extend(c, u), At(c)
                    })
                }),
                function() {
                    for (var t = 0; l.length > t; t++) l[t]()
                }
        }

        function H(e) {
            var i, o = n.businessHours,
                r = {
                    className: "fc-nonbusiness",
                    start: "09:00",
                    end: "17:00",
                    dow: [1, 2, 3, 4, 5],
                    rendering: "inverse-background"
                },
                s = $.getView();
            return o && (i = t.extend({}, r, "object" == typeof o ? o : {})), i ? (e && (i.start = null, i.end = null), k(b(i), s.start, s.end)) : []
        }

        function z(t, e) {
            var i = e.source || {},
                o = j(e.constraint, i.constraint, n.eventConstraint),
                r = j(e.overlap, i.overlap, n.eventOverlap);
            return t = C(t), O(t, o, r, e)
        }

        function A(t) {
            return O(t, n.selectConstraint, n.selectOverlap)
        }

        function N(e, n) {
            var i, o;
            return n && (i = t.extend({}, n, e), o = k(b(i))[0]), o ? z(e, o) : (e = C(e), A(e))
        }

        function O(e, n, i, o) {
            var r, s, a, l, c, u;
            if (e = t.extend({}, e), e.start = e.start.clone().stripZone(), e.end = e.end.clone().stripZone(), null != n) {
                for (r = I(n), s = !1, l = 0; r.length > l; l++)
                    if (P(r[l], e)) {
                        s = !0;
                        break
                    }
                if (!s) return !1
            }
            for (a = $.getPeerEvents(o, e), l = 0; a.length > l; l++)
                if (c = a[l], Y(c, e)) {
                    if (i === !1) return !1;
                    if ("function" == typeof i && !i(c, o)) return !1;
                    if (o) {
                        if (u = j(c.overlap, (c.source || {}).overlap), u === !1) return !1;
                        if ("function" == typeof u && !u(o, c)) return !1
                    }
                }
            return !0
        }

        function I(t) {
            return "businessHours" === t ? H() : "object" == typeof t ? k(b(t)) : v(t)
        }

        function P(t, e) {
            var n = t.start.clone().stripZone(),
                i = $.getEventEnd(t).stripZone();
            return e.start >= n && i >= e.end
        }

        function Y(t, e) {
            var n = t.start.clone().stripZone(),
                i = $.getEventEnd(t).stripZone();
            return i > e.start && e.end > n
        }
        var $ = this;
        $.isFetchNeeded = i, $.fetchEvents = o, $.addEventSource = a, $.removeEventSource = c, $.updateEvent = h, $.renderEvent = g, $.removeEvents = m, $.clientEvents = v, $.mutateEvent = E, $.normalizeEventRange = S, $.normalizeEventRangeTimes = T, $.ensureVisibleEventRange = C;
        var q, B, G = $.trigger,
            V = $.getView,
            U = $.reportEvents,
            X = {
                events: []
            },
            Z = [X],
            Q = 0,
            K = 0,
            J = 0,
            tt = [];
        t.each((n.events ? [n.events] : []).concat(n.eventSources || []), function(t, e) {
            var n = l(e);
            n && Z.push(n)
        }), $.getBusinessHoursEvents = H, $.isEventRangeAllowed = z, $.isSelectionRangeAllowed = A, $.isExternalDropRangeAllowed = N, $.getEventCache = function() {
            return tt
        }
    }

    function At(t) {
        t._allDay = t.allDay, t._start = t.start.clone(), t._end = t.end ? t.end.clone() : null
    }
    var Lt = t.fullCalendar = {
            version: "2.3.1"
        },
        Nt = Lt.views = {};
    t.fn.fullCalendar = function(e) {
        var n = Array.prototype.slice.call(arguments, 1),
            i = this;
        return this.each(function(o, r) {
            var s, a = t(r),
                l = a.data("fullCalendar");
            "string" == typeof e ? l && t.isFunction(l[e]) && (s = l[e].apply(l, n), o || (i = s), "destroy" === e && a.removeData("fullCalendar")) : l || (l = new Lt.CalendarBase(a, e), a.data("fullCalendar", l), l.render())
        }), i
    };
    var Ot = ["header", "buttonText", "buttonIcons", "themeButtonIcons"];
    Lt.intersectionToSeg = E, Lt.applyAll = W, Lt.debounce = X, Lt.isInt = V, Lt.htmlEscape = Y, Lt.cssToStr = q, Lt.proxy = U, Lt.getClientRect = g, Lt.getContentRect = m, Lt.getScrollbarWidths = v;
    var It = null;
    Lt.computeIntervalUnit = R, Lt.durationHasTime = z;
    var Pt, Ft, Wt, jt = ["sun", "mon", "tue", "wed", "thu", "fri", "sat"],
        Yt = ["year", "month", "week", "day", "hour", "minute", "second", "millisecond"],
        $t = {}.hasOwnProperty,
        qt = /^\s*\d{4}-\d\d$/,
        Bt = /^\s*\d{4}-(?:(\d\d-\d\d)|(W\d\d$)|(W\d\d-\d)|(\d\d\d))((T| )(\d\d(:\d\d(:\d\d(\.\d+)?)?)?)?)?$/,
        Gt = e.fn,
        Vt = t.extend({}, Gt);
    Lt.moment = function() {
        return Z(arguments)
    }, Lt.moment.utc = function() {
        var t = Z(arguments, !0);
        return t.hasTime() && t.utc(), t
    }, Lt.moment.parseZone = function() {
        return Z(arguments, !0, !0)
    }, Gt.clone = function() {
        var t = Vt.clone.apply(this, arguments);
        return K(this, t), this._fullCalendar && (t._fullCalendar = !0), t
    }, Gt.week = Gt.weeks = function(t) {
        var e = (this._locale || this._lang)._fullCalendar_weekCalc;
        return null == t && "function" == typeof e ? e(this) : "ISO" === e ? Vt.isoWeek.apply(this, arguments) : Vt.week.apply(this, arguments)
    }, Gt.time = function(t) {
        if (!this._fullCalendar) return Vt.time.apply(this, arguments);
        if (null == t) return e.duration({
            hours: this.hours(),
            minutes: this.minutes(),
            seconds: this.seconds(),
            milliseconds: this.milliseconds()
        });
        this._ambigTime = !1, e.isDuration(t) || e.isMoment(t) || (t = e.duration(t));
        var n = 0;
        return e.isDuration(t) && (n = 24 * Math.floor(t.asDays())), this.hours(n + t.hours()).minutes(t.minutes()).seconds(t.seconds()).milliseconds(t.milliseconds())
    }, Gt.stripTime = function() {
        var t;
        return this._ambigTime || (t = this.toArray(), this.utc(), Ft(this, t.slice(0, 3)), this._ambigTime = !0, this._ambigZone = !0), this
    }, Gt.hasTime = function() {
        return !this._ambigTime
    }, Gt.stripZone = function() {
        var t, e;
        return this._ambigZone || (t = this.toArray(), e = this._ambigTime, this.utc(), Ft(this, t), this._ambigTime = e || !1, this._ambigZone = !0), this
    }, Gt.hasZone = function() {
        return !this._ambigZone
    }, Gt.local = function() {
        var t = this.toArray(),
            e = this._ambigZone;
        return Vt.local.apply(this, arguments), this._ambigTime = !1, this._ambigZone = !1, e && Wt(this, t), this
    }, Gt.utc = function() {
        return Vt.utc.apply(this, arguments), this._ambigTime = !1, this._ambigZone = !1, this
    }, t.each(["zone", "utcOffset"], function(t, e) {
        Vt[e] && (Gt[e] = function(t) {
            return null != t && (this._ambigTime = !1, this._ambigZone = !1), Vt[e].apply(this, arguments)
        })
    }), Gt.format = function() {
        return this._fullCalendar && arguments[0] ? et(this, arguments[0]) : this._ambigTime ? tt(this, "YYYY-MM-DD") : this._ambigZone ? tt(this, "YYYY-MM-DD[T]HH:mm:ss") : Vt.format.apply(this, arguments)
    }, Gt.toISOString = function() {
        return this._ambigTime ? tt(this, "YYYY-MM-DD") : this._ambigZone ? tt(this, "YYYY-MM-DD[T]HH:mm:ss") : Vt.toISOString.apply(this, arguments)
    }, Gt.isWithin = function(t, e) {
        var n = Q([this, t, e]);
        return n[0] >= n[1] && n[0] < n[2]
    }, Gt.isSame = function(t, e) {
        var n;
        return this._fullCalendar ? e ? (n = Q([this, t], !0), Vt.isSame.call(n[0], n[1], e)) : (t = Lt.moment.parseZone(t), Vt.isSame.call(this, t) && Boolean(this._ambigTime) === Boolean(t._ambigTime) && Boolean(this._ambigZone) === Boolean(t._ambigZone)) : Vt.isSame.apply(this, arguments)
    }, t.each(["isBefore", "isAfter"], function(t, e) {
        Gt[e] = function(t, n) {
            var i;
            return this._fullCalendar ? (i = Q([this, t]), Vt[e].call(i[0], i[1], n)) : Vt[e].apply(this, arguments)
        }
    }), Pt = "_d" in e() && "updateOffset" in e, Ft = Pt ? function(t, n) {
        t._d.setTime(Date.UTC.apply(Date, n)), e.updateOffset(t, !1)
    } : J, Wt = Pt ? function(t, n) {
        t._d.setTime(+new Date(n[0] || 0, n[1] || 0, n[2] || 0, n[3] || 0, n[4] || 0, n[5] || 0, n[6] || 0)), e.updateOffset(t, !1)
    } : J;
    var Ut = {
        t: function(t) {
            return tt(t, "a").charAt(0)
        },
        T: function(t) {
            return tt(t, "A").charAt(0)
        }
    };
    Lt.formatRange = ot;
    var Xt = {
            Y: "year",
            M: "month",
            D: "day",
            d: "day",
            A: "second",
            a: "second",
            T: "second",
            t: "second",
            H: "second",
            h: "second",
            m: "second",
            s: "second"
        },
        Zt = {};
    Lt.Class = ct, ct.extend = function(t) {
        var e, n = this;
        return t = t || {}, P(t, "constructor") && (e = t.constructor), "function" != typeof e && (e = t.constructor = function() {
            n.apply(this, arguments)
        }), e.prototype = N(n.prototype), O(t, e.prototype), I(t, e.prototype), O(n, e), e
    }, ct.mixin = function(t) {
        O(t.prototype || t, this.prototype)
    };
    var Qt = ct.extend({
            isHidden: !0,
            options: null,
            el: null,
            documentMousedownProxy: null,
            margin: 10,
            constructor: function(t) {
                this.options = t || {}
            },
            show: function() {
                this.isHidden && (this.el || this.render(), this.el.show(), this.position(), this.isHidden = !1, this.trigger("show"))
            },
            hide: function() {
                this.isHidden || (this.el.hide(), this.isHidden = !0, this.trigger("hide"))
            },
            render: function() {
                var e = this,
                    n = this.options;
                this.el = t('<div class="fc-popover"/>').addClass(n.className || "").css({
                    top: 0,
                    left: 0
                }).append(n.content).appendTo(n.parentEl), this.el.on("click", ".fc-close", function() {
                    e.hide()
                }), n.autoHide && t(document).on("mousedown", this.documentMousedownProxy = U(this, "documentMousedown"))
            },
            documentMousedown: function(e) {
                this.el && !t(e.target).closest(this.el).length && this.hide()
            },
            destroy: function() {
                this.hide(), this.el && (this.el.remove(), this.el = null), t(document).off("mousedown", this.documentMousedownProxy)
            },
            position: function() {
                var e, n, i, o, r, s = this.options,
                    a = this.el.offsetParent().offset(),
                    l = this.el.outerWidth(),
                    c = this.el.outerHeight(),
                    u = t(window),
                    d = f(this.el);
                o = s.top || 0, r = void 0 !== s.left ? s.left : void 0 !== s.right ? s.right - l : 0, d.is(window) || d.is(document) ? (d = u, e = 0, n = 0) : (i = d.offset(), e = i.top, n = i.left), e += u.scrollTop(), n += u.scrollLeft(), s.viewportConstrain !== !1 && (o = Math.min(o, e + d.outerHeight() - c - this.margin), o = Math.max(o, e + this.margin), r = Math.min(r, n + d.outerWidth() - l - this.margin), r = Math.max(r, n + this.margin)), this.el.css({
                    top: o - a.top,
                    left: r - a.left
                })
            },
            trigger: function(t) {
                this.options[t] && this.options[t].apply(this, Array.prototype.slice.call(arguments, 1))
            }
        }),
        Kt = ct.extend({
            grid: null,
            rowCoords: null,
            colCoords: null,
            containerEl: null,
            bounds: null,
            constructor: function(t) {
                this.grid = t
            },
            build: function() {
                this.rowCoords = this.grid.computeRowCoords(), this.colCoords = this.grid.computeColCoords(), this.computeBounds()
            },
            clear: function() {
                this.rowCoords = null, this.colCoords = null
            },
            getCell: function(e, n) {
                var i, o, r, s = this.rowCoords,
                    a = s.length,
                    l = this.colCoords,
                    c = l.length,
                    u = null,
                    d = null;
                if (this.inBounds(e, n)) {
                    for (i = 0; a > i; i++)
                        if (o = s[i], n >= o.top && o.bottom > n) {
                            u = i;
                            break
                        }
                    for (i = 0; c > i; i++)
                        if (o = l[i], e >= o.left && o.right > e) {
                            d = i;
                            break
                        }
                    if (null !== u && null !== d) return r = this.grid.getCell(u, d), r.grid = this.grid, t.extend(r, s[u], l[d]), r
                }
                return null
            },
            computeBounds: function() {
                this.bounds = this.containerEl ? g(this.containerEl) : null
            },
            inBounds: function(t, e) {
                var n = this.bounds;
                return n ? t >= n.left && n.right > t && e >= n.top && n.bottom > e : !0
            }
        }),
        Jt = ct.extend({
            coordMaps: null,
            constructor: function(t) {
                this.coordMaps = t
            },
            build: function() {
                var t, e = this.coordMaps;
                for (t = 0; e.length > t; t++) e[t].build()
            },
            getCell: function(t, e) {
                var n, i = this.coordMaps,
                    o = null;
                for (n = 0; i.length > n && !o; n++) o = i[n].getCell(t, e);
                return o
            },
            clear: function() {
                var t, e = this.coordMaps;
                for (t = 0; e.length > t; t++) e[t].clear()
            }
        }),
        te = Lt.DragListener = ct.extend({
            options: null,
            isListening: !1,
            isDragging: !1,
            originX: null,
            originY: null,
            mousemoveProxy: null,
            mouseupProxy: null,
            subjectEl: null,
            subjectHref: null,
            scrollEl: null,
            scrollBounds: null,
            scrollTopVel: null,
            scrollLeftVel: null,
            scrollIntervalId: null,
            scrollHandlerProxy: null,
            scrollSensitivity: 30,
            scrollSpeed: 200,
            scrollIntervalMs: 50,
            constructor: function(t) {
                t = t || {}, this.options = t, this.subjectEl = t.subjectEl
            },
            mousedown: function(t) {
                x(t) && (t.preventDefault(), this.startListening(t), this.options.distance || this.startDrag(t))
            },
            startListening: function(e) {
                var n;
                this.isListening || (e && this.options.scroll && (n = f(t(e.target)), n.is(window) || n.is(document) || (this.scrollEl = n, this.scrollHandlerProxy = X(U(this, "scrollHandler"), 100), this.scrollEl.on("scroll", this.scrollHandlerProxy))), t(document).on("mousemove", this.mousemoveProxy = U(this, "mousemove")).on("mouseup", this.mouseupProxy = U(this, "mouseup")).on("selectstart", this.preventDefault), e ? (this.originX = e.pageX, this.originY = e.pageY) : (this.originX = 0, this.originY = 0), this.isListening = !0, this.listenStart(e))
            },
            listenStart: function(t) {
                this.trigger("listenStart", t)
            },
            mousemove: function(t) {
                var e, n, i = t.pageX - this.originX,
                    o = t.pageY - this.originY;
                this.isDragging || (e = this.options.distance || 1, n = i * i + o * o, n >= e * e && this.startDrag(t)), this.isDragging && this.drag(i, o, t)
            },
            startDrag: function(t) {
                this.isListening || this.startListening(), this.isDragging || (this.isDragging = !0, this.dragStart(t))
            },
            dragStart: function(t) {
                var e = this.subjectEl;
                this.trigger("dragStart", t), (this.subjectHref = e ? e.attr("href") : null) && e.removeAttr("href")
            },
            drag: function(t, e, n) {
                this.trigger("drag", t, e, n), this.updateScroll(n)
            },
            mouseup: function(t) {
                this.stopListening(t)
            },
            stopDrag: function(t) {
                this.isDragging && (this.stopScrolling(), this.dragStop(t), this.isDragging = !1)
            },
            dragStop: function(t) {
                var e = this;
                this.trigger("dragStop", t), setTimeout(function() {
                    e.subjectHref && e.subjectEl.attr("href", e.subjectHref)
                }, 0)
            },
            stopListening: function(e) {
                this.stopDrag(e), this.isListening && (this.scrollEl && (this.scrollEl.off("scroll", this.scrollHandlerProxy), this.scrollHandlerProxy = null), t(document).off("mousemove", this.mousemoveProxy).off("mouseup", this.mouseupProxy).off("selectstart", this.preventDefault), this.mousemoveProxy = null, this.mouseupProxy = null, this.isListening = !1, this.listenStop(e))
            },
            listenStop: function(t) {
                this.trigger("listenStop", t)
            },
            trigger: function(t) {
                this.options[t] && this.options[t].apply(this, Array.prototype.slice.call(arguments, 1))
            },
            preventDefault: function(t) {
                t.preventDefault()
            },
            computeScrollBounds: function() {
                var t = this.scrollEl;
                this.scrollBounds = t ? p(t) : null
            },
            updateScroll: function(t) {
                var e, n, i, o, r = this.scrollSensitivity,
                    s = this.scrollBounds,
                    a = 0,
                    l = 0;
                s && (e = (r - (t.pageY - s.top)) / r, n = (r - (s.bottom - t.pageY)) / r, i = (r - (t.pageX - s.left)) / r, o = (r - (s.right - t.pageX)) / r, e >= 0 && 1 >= e ? a = -1 * e * this.scrollSpeed : n >= 0 && 1 >= n && (a = n * this.scrollSpeed), i >= 0 && 1 >= i ? l = -1 * i * this.scrollSpeed : o >= 0 && 1 >= o && (l = o * this.scrollSpeed)), this.setScrollVel(a, l)
            },
            setScrollVel: function(t, e) {
                this.scrollTopVel = t, this.scrollLeftVel = e, this.constrainScrollVel(), !this.scrollTopVel && !this.scrollLeftVel || this.scrollIntervalId || (this.scrollIntervalId = setInterval(U(this, "scrollIntervalFunc"), this.scrollIntervalMs))
            },
            constrainScrollVel: function() {
                var t = this.scrollEl;
                0 > this.scrollTopVel ? 0 >= t.scrollTop() && (this.scrollTopVel = 0) : this.scrollTopVel > 0 && t.scrollTop() + t[0].clientHeight >= t[0].scrollHeight && (this.scrollTopVel = 0), 0 > this.scrollLeftVel ? 0 >= t.scrollLeft() && (this.scrollLeftVel = 0) : this.scrollLeftVel > 0 && t.scrollLeft() + t[0].clientWidth >= t[0].scrollWidth && (this.scrollLeftVel = 0)
            },
            scrollIntervalFunc: function() {
                var t = this.scrollEl,
                    e = this.scrollIntervalMs / 1e3;
                this.scrollTopVel && t.scrollTop(t.scrollTop() + this.scrollTopVel * e), this.scrollLeftVel && t.scrollLeft(t.scrollLeft() + this.scrollLeftVel * e), this.constrainScrollVel(), this.scrollTopVel || this.scrollLeftVel || this.stopScrolling()
            },
            stopScrolling: function() {
                this.scrollIntervalId && (clearInterval(this.scrollIntervalId), this.scrollIntervalId = null, this.scrollStop())
            },
            scrollHandler: function() {
                this.scrollIntervalId || this.scrollStop()
            },
            scrollStop: function() {}
        }),
        ee = te.extend({
            coordMap: null,
            origCell: null,
            cell: null,
            coordAdjust: null,
            constructor: function(t, e) {
                te.prototype.constructor.call(this, e), this.coordMap = t
            },
            listenStart: function(t) {
                var e, n, i, o = this.subjectEl;
                te.prototype.listenStart.apply(this, arguments), this.computeCoords(), t ? (n = {
                    left: t.pageX,
                    top: t.pageY
                }, i = n, o && (e = p(o), i = T(i, e)), this.origCell = this.getCell(i.left, i.top), o && this.options.subjectCenter && (this.origCell && (e = S(this.origCell, e) || e), i = C(e)), this.coordAdjust = k(i, n)) : (this.origCell = null, this.coordAdjust = null)
            },
            computeCoords: function() {
                this.coordMap.build(), this.computeScrollBounds()
            },
            dragStart: function(t) {
                var e;
                te.prototype.dragStart.apply(this, arguments), e = this.getCell(t.pageX, t.pageY), e && this.cellOver(e)
            },
            drag: function(t, e, n) {
                var i;
                te.prototype.drag.apply(this, arguments), i = this.getCell(n.pageX, n.pageY), ut(i, this.cell) || (this.cell && this.cellOut(), i && this.cellOver(i))
            },
            dragStop: function() {
                this.cellDone(), te.prototype.dragStop.apply(this, arguments)
            },
            cellOver: function(t) {
                this.cell = t, this.trigger("cellOver", t, ut(t, this.origCell), this.origCell)
            },
            cellOut: function() {
                this.cell && (this.trigger("cellOut", this.cell), this.cellDone(), this.cell = null)
            },
            cellDone: function() {
                this.cell && this.trigger("cellDone", this.cell)
            },
            listenStop: function() {
                te.prototype.listenStop.apply(this, arguments), this.origCell = this.cell = null, this.coordMap.clear()
            },
            scrollStop: function() {
                te.prototype.scrollStop.apply(this, arguments), this.computeCoords()
            },
            getCell: function(t, e) {
                return this.coordAdjust && (t += this.coordAdjust.left, e += this.coordAdjust.top), this.coordMap.getCell(t, e)
            }
        }),
        ne = ct.extend({
            options: null,
            sourceEl: null,
            el: null,
            parentEl: null,
            top0: null,
            left0: null,
            mouseY0: null,
            mouseX0: null,
            topDelta: null,
            leftDelta: null,
            mousemoveProxy: null,
            isFollowing: !1,
            isHidden: !1,
            isAnimating: !1,
            constructor: function(e, n) {
                this.options = n = n || {}, this.sourceEl = e, this.parentEl = n.parentEl ? t(n.parentEl) : e.parent()
            },
            start: function(e) {
                this.isFollowing || (this.isFollowing = !0, this.mouseY0 = e.pageY, this.mouseX0 = e.pageX, this.topDelta = 0, this.leftDelta = 0, this.isHidden || this.updatePosition(), t(document).on("mousemove", this.mousemoveProxy = U(this, "mousemove")))
            },
            stop: function(e, n) {
                function i() {
                    this.isAnimating = !1, o.destroyEl(), this.top0 = this.left0 = null, n && n()
                }
                var o = this,
                    r = this.options.revertDuration;
                this.isFollowing && !this.isAnimating && (this.isFollowing = !1, t(document).off("mousemove", this.mousemoveProxy), e && r && !this.isHidden ? (this.isAnimating = !0, this.el.animate({
                    top: this.top0,
                    left: this.left0
                }, {
                    duration: r,
                    complete: i
                })) : i())
            },
            getEl: function() {
                var t = this.el;
                return t || (this.sourceEl.width(), t = this.el = this.sourceEl.clone().css({
                    position: "absolute",
                    visibility: "",
                    display: this.isHidden ? "none" : "",
                    margin: 0,
                    right: "auto",
                    bottom: "auto",
                    width: this.sourceEl.width(),
                    height: this.sourceEl.height(),
                    opacity: this.options.opacity || "",
                    zIndex: this.options.zIndex
                }).appendTo(this.parentEl)), t
            },
            destroyEl: function() {
                this.el && (this.el.remove(), this.el = null)
            },
            updatePosition: function() {
                var t, e;
                this.getEl(), null === this.top0 && (this.sourceEl.width(), t = this.sourceEl.offset(), e = this.el.offsetParent().offset(), this.top0 = t.top - e.top, this.left0 = t.left - e.left), this.el.css({
                    top: this.top0 + this.topDelta,
                    left: this.left0 + this.leftDelta
                })
            },
            mousemove: function(t) {
                this.topDelta = t.pageY - this.mouseY0, this.leftDelta = t.pageX - this.mouseX0, this.isHidden || this.updatePosition()
            },
            hide: function() {
                this.isHidden || (this.isHidden = !0, this.el && this.el.hide())
            },
            show: function() {
                this.isHidden && (this.isHidden = !1, this.updatePosition(), this.getEl().show())
            }
        }),
        ie = ct.extend({
            view: null,
            isRTL: null,
            cellHtml: "<td/>",
            constructor: function(t) {
                this.view = t, this.isRTL = t.opt("isRTL")
            },
            rowHtml: function(t, e) {
                var n, i, o = this.getHtmlRenderer("cell", t),
                    r = "";
                for (e = e || 0, n = 0; this.colCnt > n; n++) i = this.getCell(e, n), r += o(i);
                return r = this.bookendCells(r, t, e), "<tr>" + r + "</tr>"
            },
            bookendCells: function(t, e, n) {
                var i = this.getHtmlRenderer("intro", e)(n || 0),
                    o = this.getHtmlRenderer("outro", e)(n || 0),
                    r = this.isRTL ? o : i,
                    s = this.isRTL ? i : o;
                return "string" == typeof t ? r + t + s : t.prepend(r).append(s)
            },
            getHtmlRenderer: function(t, e) {
                var n, i, o, r, s = this.view;
                return n = t + "Html", e && (i = e + B(t) + "Html"), i && (r = s[i]) ? o = s : i && (r = this[i]) ? o = this : (r = s[n]) ? o = s : (r = this[n]) && (o = this), "function" == typeof r ? function() {
                    return r.apply(o, arguments) || ""
                } : function() {
                    return r || ""
                }
            }
        }),
        oe = Lt.Grid = ie.extend({
            start: null,
            end: null,
            rowCnt: 0,
            colCnt: 0,
            rowData: null,
            colData: null,
            el: null,
            coordMap: null,
            elsByFill: null,
            externalDragStartProxy: null,
            colHeadFormat: null,
            eventTimeFormat: null,
            displayEventTime: null,
            displayEventEnd: null,
            cellDuration: null,
            largeUnit: null,
            constructor: function() {
                ie.apply(this, arguments), this.coordMap = new Kt(this), this.elsByFill = {}, this.externalDragStartProxy = U(this, "externalDragStart")
            },
            computeColHeadFormat: function() {},
            computeEventTimeFormat: function() {
                return this.view.opt("smallTimeFormat")
            },
            computeDisplayEventTime: function() {
                return !0
            },
            computeDisplayEventEnd: function() {
                return !0
            },
            setRange: function(t) {
                var e, n, i = this.view;
                this.start = t.start.clone(), this.end = t.end.clone(), this.rowData = [], this.colData = [], this.updateCells(), this.colHeadFormat = i.opt("columnFormat") || this.computeColHeadFormat(), this.eventTimeFormat = i.opt("eventTimeFormat") || i.opt("timeFormat") || this.computeEventTimeFormat(), e = i.opt("displayEventTime"), null == e && (e = this.computeDisplayEventTime()), n = i.opt("displayEventEnd"), null == n && (n = this.computeDisplayEventEnd()), this.displayEventTime = e, this.displayEventEnd = n
            },
            updateCells: function() {},
            rangeToSegs: function() {},
            diffDates: function(t, e) {
                return this.largeUnit ? _(t, e, this.largeUnit) : D(t, e)
            },
            getCell: function(e, n) {
                var i;
                return null == n && ("number" == typeof e ? (n = e % this.colCnt, e = Math.floor(e / this.colCnt)) : (n = e.col, e = e.row)), i = {
                    row: e,
                    col: n
                }, t.extend(i, this.getRowData(e), this.getColData(n)), t.extend(i, this.computeCellRange(i)), i
            },
            computeCellRange: function(t) {
                var e = this.computeCellDate(t);
                return {
                    start: e,
                    end: e.clone().add(this.cellDuration)
                }
            },
            computeCellDate: function() {},
            getRowData: function(t) {
                return this.rowData[t] || {}
            },
            getColData: function(t) {
                return this.colData[t] || {}
            },
            getRowEl: function() {},
            getColEl: function() {},
            getCellDayEl: function(t) {
                return this.getColEl(t.col) || this.getRowEl(t.row)
            },
            computeRowCoords: function() {
                var t, e, n, i = [];
                for (t = 0; this.rowCnt > t; t++) e = this.getRowEl(t), n = e.offset().top, i.push({
                    top: n,
                    bottom: n + e.outerHeight()
                });
                return i
            },
            computeColCoords: function() {
                var t, e, n, i = [];
                for (t = 0; this.colCnt > t; t++) e = this.getColEl(t), n = e.offset().left, i.push({
                    left: n,
                    right: n + e.outerWidth()
                });
                return i
            },
            setElement: function(e) {
                var n = this;
                this.el = e, e.on("mousedown", function(e) {
                    t(e.target).is(".fc-event-container *, .fc-more") || t(e.target).closest(".fc-popover").length || n.dayMousedown(e)
                }), this.bindSegHandlers(), this.bindGlobalHandlers()
            },
            removeElement: function() {
                this.unbindGlobalHandlers(), this.el.remove()
            },
            renderSkeleton: function() {},
            renderDates: function() {},
            destroyDates: function() {},
            bindGlobalHandlers: function() {
                t(document).on("dragstart sortstart", this.externalDragStartProxy)
            },
            unbindGlobalHandlers: function() {
                t(document).off("dragstart sortstart", this.externalDragStartProxy)
            },
            dayMousedown: function(t) {
                var e, n, i = this,
                    o = this.view,
                    r = o.opt("selectable"),
                    l = new ee(this.coordMap, {
                        scroll: o.opt("dragScroll"),
                        dragStart: function() {
                            o.unselect()
                        },
                        cellOver: function(t, o, a) {
                            a && (e = o ? t : null, r && (n = i.computeSelection(a, t), n ? i.renderSelection(n) : s()))
                        },
                        cellOut: function() {
                            e = null, n = null, i.destroySelection(), a()
                        },
                        listenStop: function(t) {
                            e && o.trigger("dayClick", i.getCellDayEl(e), e.start, t), n && o.reportSelection(n, t), a()
                        }
                    });
                l.mousedown(t)
            },
            renderRangeHelper: function(t, e) {
                var n = this.fabricateHelperEvent(t, e);
                this.renderHelper(n, e)
            },
            fabricateHelperEvent: function(t, e) {
                var n = e ? N(e.event) : {};
                return n.start = t.start.clone(), n.end = t.end ? t.end.clone() : null, n.allDay = null, this.view.calendar.normalizeEventRange(n), n.className = (n.className || []).concat("fc-helper"), e || (n.editable = !1), n
            },
            renderHelper: function() {},
            destroyHelper: function() {},
            renderSelection: function(t) {
                this.renderHighlight(t)
            },
            destroySelection: function() {
                this.destroyHighlight()
            },
            computeSelection: function(t, e) {
                var n, i = [t.start, t.end, e.start, e.end];
                return i.sort(G), n = {
                    start: i[0].clone(),
                    end: i[3].clone()
                }, this.view.calendar.isSelectionRangeAllowed(n) ? n : null
            },
            renderHighlight: function(t) {
                this.renderFill("highlight", this.rangeToSegs(t))
            },
            destroyHighlight: function() {
                this.destroyFill("highlight")
            },
            highlightSegClasses: function() {
                return ["fc-highlight"]
            },
            renderFill: function() {},
            destroyFill: function(t) {
                var e = this.elsByFill[t];
                e && (e.remove(), delete this.elsByFill[t])
            },
            renderFillSegEls: function(e, n) {
                var i, o = this,
                    r = this[e + "SegEl"],
                    s = "",
                    a = [];
                if (n.length) {
                    for (i = 0; n.length > i; i++) s += this.fillSegHtml(e, n[i]);
                    t(s).each(function(e, i) {
                        var s = n[e],
                            l = t(i);
                        r && (l = r.call(o, s, l)), l && (l = t(l), l.is(o.fillSegTag) && (s.el = l, a.push(s)))
                    })
                }
                return a
            },
            fillSegTag: "div",
            fillSegHtml: function(t, e) {
                var n = this[t + "SegClasses"],
                    i = this[t + "SegCss"],
                    o = n ? n.call(this, e) : [],
                    r = q(i ? i.call(this, e) : {});
                return "<" + this.fillSegTag + (o.length ? ' class="' + o.join(" ") + '"' : "") + (r ? ' style="' + r + '"' : "") + " />"
            },
            headHtml: function() {
                return '<div class="fc-row ' + this.view.widgetHeaderClass + '"><table><thead>' + this.rowHtml("head") + "</thead></table></div>"
            },
            headCellHtml: function(t) {
                var e = this.view,
                    n = t.start;
                return '<th class="fc-day-header ' + e.widgetHeaderClass + " fc-" + jt[n.day()] + '">' + Y(n.format(this.colHeadFormat)) + "</th>"
            },
            bgCellHtml: function(t) {
                var e = this.view,
                    n = t.start,
                    i = this.getDayClasses(n);
                return i.unshift("fc-day", e.widgetContentClass), '<td class="' + i.join(" ") + '" data-date="' + n.format("YYYY-MM-DD") + '"></td>'
            },
            getDayClasses: function(t) {
                var e = this.view,
                    n = e.calendar.getNow().stripTime(),
                    i = ["fc-" + jt[t.day()]];
                return 1 == e.intervalDuration.as("months") && t.month() != e.intervalStart.month() && i.push("fc-other-month"), t.isSame(n, "day") ? i.push("fc-today", e.highlightStateClass) : i.push(n > t ? "fc-past" : "fc-future"), i
            }
        });
    oe.mixin({
        mousedOverSeg: null,
        isDraggingSeg: !1,
        isResizingSeg: !1,
        isDraggingExternal: !1,
        segs: null,
        renderEvents: function(t) {
            var e, n, i = this.eventsToSegs(t),
                o = [],
                r = [];
            for (e = 0; i.length > e; e++) n = i[e], dt(n.event) ? o.push(n) : r.push(n);
            o = this.renderBgSegs(o) || o, r = this.renderFgSegs(r) || r, this.segs = o.concat(r)
        },
        destroyEvents: function() {
            this.triggerSegMouseout(), this.destroyFgSegs(), this.destroyBgSegs(), this.segs = null
        },
        getEventSegs: function() {
            return this.segs || []
        },
        renderFgSegs: function() {},
        destroyFgSegs: function() {},
        renderFgSegEls: function(e, n) {
            var i, o = this.view,
                r = "",
                s = [];
            if (e.length) {
                for (i = 0; e.length > i; i++) r += this.fgSegHtml(e[i], n);
                t(r).each(function(n, i) {
                    var r = e[n],
                        a = o.resolveEventEl(r.event, t(i));
                    a && (a.data("fc-seg", r), r.el = a, s.push(r))
                })
            }
            return s
        },
        fgSegHtml: function() {},
        renderBgSegs: function(t) {
            return this.renderFill("bgEvent", t)
        },
        destroyBgSegs: function() {
            this.destroyFill("bgEvent")
        },
        bgEventSegEl: function(t, e) {
            return this.view.resolveEventEl(t.event, e)
        },
        bgEventSegClasses: function(t) {
            var e = t.event,
                n = e.source || {};
            return ["fc-bgevent"].concat(e.className, n.className || [])
        },
        bgEventSegCss: function(t) {
            var e = this.view,
                n = t.event,
                i = n.source || {};
            return {
                "background-color": n.backgroundColor || n.color || i.backgroundColor || i.color || e.opt("eventBackgroundColor") || e.opt("eventColor")
            }
        },
        businessHoursSegClasses: function() {
            return ["fc-nonbusiness", "fc-bgevent"]
        },
        bindSegHandlers: function() {
            var e = this,
                n = this.view;
            t.each({
                mouseenter: function(t, n) {
                    e.triggerSegMouseover(t, n)
                },
                mouseleave: function(t, n) {
                    e.triggerSegMouseout(t, n)
                },
                click: function(t, e) {
                    return n.trigger("eventClick", this, t.event, e)
                },
                mousedown: function(i, o) {
                    t(o.target).is(".fc-resizer") && n.isEventResizable(i.event) ? e.segResizeMousedown(i, o, t(o.target).is(".fc-start-resizer")) : n.isEventDraggable(i.event) && e.segDragMousedown(i, o)
                }
            }, function(n, i) {
                e.el.on(n, ".fc-event-container > *", function(n) {
                    var o = t(this).data("fc-seg");
                    return !o || e.isDraggingSeg || e.isResizingSeg ? void 0 : i.call(this, o, n)
                })
            })
        },
        triggerSegMouseover: function(t, e) {
            this.mousedOverSeg || (this.mousedOverSeg = t, this.view.trigger("eventMouseover", t.el[0], t.event, e))
        },
        triggerSegMouseout: function(t, e) {
            e = e || {}, this.mousedOverSeg && (t = t || this.mousedOverSeg, this.mousedOverSeg = null, this.view.trigger("eventMouseout", t.el[0], t.event, e))
        },
        segDragMousedown: function(t, e) {
            var n, i = this,
                o = this.view,
                r = o.calendar,
                l = t.el,
                c = t.event,
                u = new ne(t.el, {
                    parentEl: o.el,
                    opacity: o.opt("dragOpacity"),
                    revertDuration: o.opt("dragRevertDuration"),
                    zIndex: 2
                }),
                d = new ee(o.coordMap, {
                    distance: 5,
                    scroll: o.opt("dragScroll"),
                    subjectEl: l,
                    subjectCenter: !0,
                    listenStart: function(t) {
                        u.hide(), u.start(t)
                    },
                    dragStart: function(e) {
                        i.triggerSegMouseout(t, e), i.segDragStart(t, e), o.hideEvent(c)
                    },
                    cellOver: function(e, a, l) {
                        t.cell && (l = t.cell), n = i.computeEventDrop(l, e, c), n && !r.isEventRangeAllowed(n, c) && (s(), n = null), n && o.renderDrag(n, t) ? u.hide() : u.show(), a && (n = null)
                    },
                    cellOut: function() {
                        o.destroyDrag(), u.show(), n = null
                    },
                    cellDone: function() {
                        a()
                    },
                    dragStop: function(e) {
                        u.stop(!n, function() {
                            o.destroyDrag(), o.showEvent(c), i.segDragStop(t, e), n && o.reportEventDrop(c, n, this.largeUnit, l, e)
                        })
                    },
                    listenStop: function() {
                        u.stop()
                    }
                });
            d.mousedown(e)
        },
        segDragStart: function(t, e) {
            this.isDraggingSeg = !0, this.view.trigger("eventDragStart", t.el[0], t.event, e, {})
        },
        segDragStop: function(t, e) {
            this.isDraggingSeg = !1, this.view.trigger("eventDragStop", t.el[0], t.event, e, {})
        },
        computeEventDrop: function(t, e, n) {
            var i, o, r = this.view.calendar,
                s = t.start,
                a = e.start;
            return s.hasTime() === a.hasTime() ? (i = this.diffDates(a, s), n.allDay && z(i) ? (o = {
                start: n.start.clone(),
                end: r.getEventEnd(n),
                allDay: !1
            }, r.normalizeEventRangeTimes(o)) : o = {
                start: n.start.clone(),
                end: n.end ? n.end.clone() : null,
                allDay: n.allDay
            }, o.start.add(i), o.end && o.end.add(i)) : o = {
                start: a.clone(),
                end: null,
                allDay: !a.hasTime()
            }, o
        },
        applyDragOpacity: function(t) {
            var e = this.view.opt("dragOpacity");
            null != e && t.each(function(t, n) {
                n.style.opacity = e
            })
        },
        externalDragStart: function(e, n) {
            var i, o, r = this.view;
            r.opt("droppable") && (i = t((n ? n.item : null) || e.target), o = r.opt("dropAccept"), (t.isFunction(o) ? o.call(i[0], i) : i.is(o)) && (this.isDraggingExternal || this.listenToExternalDrag(i, e, n)))
        },
        listenToExternalDrag: function(t, e, n) {
            var i, o, r = this,
                l = vt(t);
            i = new ee(this.coordMap, {
                listenStart: function() {
                    r.isDraggingExternal = !0
                },
                cellOver: function(t) {
                    o = r.computeExternalDrop(t, l), o ? r.renderDrag(o) : s()
                },
                cellOut: function() {
                    o = null, r.destroyDrag(), a()
                },
                dragStop: function() {
                    r.destroyDrag(), a(), o && r.view.reportExternalDrop(l, o, t, e, n)
                },
                listenStop: function() {
                    r.isDraggingExternal = !1
                }
            }), i.startDrag(e)
        },
        computeExternalDrop: function(t, e) {
            var n = {
                start: t.start.clone(),
                end: null
            };
            return e.startTime && !n.start.hasTime() && n.start.time(e.startTime), e.duration && (n.end = n.start.clone().add(e.duration)), this.view.calendar.isExternalDropRangeAllowed(n, e.eventProps) ? n : null
        },
        renderDrag: function() {},
        destroyDrag: function() {},
        segResizeMousedown: function(t, e, n) {
            var i, o, r = this,
                l = this.view,
                c = l.calendar,
                u = t.el,
                d = t.event,
                h = c.getEventEnd(d);
            i = new ee(this.coordMap, {
                distance: 5,
                scroll: l.opt("dragScroll"),
                subjectEl: u,
                dragStart: function(e) {
                    r.triggerSegMouseout(t, e), r.segResizeStart(t, e)
                },
                cellOver: function(e, i, a) {
                    o = n ? r.computeEventStartResize(a, e, d) : r.computeEventEndResize(a, e, d), o && (c.isEventRangeAllowed(o, d) ? o.start.isSame(d.start) && o.end.isSame(h) && (o = null) : (s(), o = null)), o && (l.hideEvent(d), r.renderEventResize(o, t))
                },
                cellOut: function() {
                    o = null
                },
                cellDone: function() {
                    r.destroyEventResize(), l.showEvent(d), a()
                },
                dragStop: function(e) {
                    r.segResizeStop(t, e), o && l.reportEventResize(d, o, this.largeUnit, u, e)
                }
            }), i.mousedown(e)
        },
        segResizeStart: function(t, e) {
            this.isResizingSeg = !0, this.view.trigger("eventResizeStart", t.el[0], t.event, e, {})
        },
        segResizeStop: function(t, e) {
            this.isResizingSeg = !1, this.view.trigger("eventResizeStop", t.el[0], t.event, e, {})
        },
        computeEventStartResize: function(t, e, n) {
            return this.computeEventResize("start", t, e, n)
        },
        computeEventEndResize: function(t, e, n) {
            return this.computeEventResize("end", t, e, n)
        },
        computeEventResize: function(t, e, n, i) {
            var o, r, s = this.view.calendar,
                a = this.diffDates(n[t], e[t]);
            return o = {
                start: i.start.clone(),
                end: s.getEventEnd(i),
                allDay: i.allDay
            }, o.allDay && z(a) && (o.allDay = !1, s.normalizeEventRangeTimes(o)), o[t].add(a), o.start.isBefore(o.end) || (r = i.allDay ? s.defaultAllDayEventDuration : s.defaultTimedEventDuration, this.cellDuration && r > this.cellDuration && (r = this.cellDuration), "start" == t ? o.start = o.end.clone().subtract(r) : o.end = o.start.clone().add(r)), o
        },
        renderEventResize: function() {},
        destroyEventResize: function() {},
        getEventTimeText: function(t, e, n) {
            return null == e && (e = this.eventTimeFormat), null == n && (n = this.displayEventEnd), this.displayEventTime && t.start.hasTime() ? n && t.end ? this.view.formatRange(t, e) : t.start.format(e) : ""
        },
        getSegClasses: function(t, e, n) {
            var i = t.event,
                o = ["fc-event", t.isStart ? "fc-start" : "fc-not-start", t.isEnd ? "fc-end" : "fc-not-end"].concat(i.className, i.source ? i.source.className : []);
            return e && o.push("fc-draggable"), n && o.push("fc-resizable"), o
        },
        getEventSkinCss: function(t) {
            var e = this.view,
                n = t.source || {},
                i = t.color,
                o = n.color,
                r = e.opt("eventColor");
            return {
                "background-color": t.backgroundColor || i || n.backgroundColor || o || e.opt("eventBackgroundColor") || r,
                "border-color": t.borderColor || i || n.borderColor || o || e.opt("eventBorderColor") || r,
                color: t.textColor || n.textColor || e.opt("eventTextColor")
            }
        },
        eventsToSegs: function(t, e) {
            var n, i = this.eventsToRanges(t),
                o = [];
            for (n = 0; i.length > n; n++) o.push.apply(o, this.eventRangeToSegs(i[n], e));
            return o
        },
        eventsToRanges: function(e) {
            var n = this,
                i = pt(e),
                o = [];
            return t.each(i, function(t, e) {
                e.length && o.push.apply(o, ht(e[0]) ? n.eventsToInverseRanges(e) : n.eventsToNormalRanges(e))
            }), o
        },
        eventsToNormalRanges: function(t) {
            var e, n, i, o, r = this.view.calendar,
                s = [];
            for (e = 0; t.length > e; e++) n = t[e], i = n.start.clone().stripZone(), o = r.getEventEnd(n).stripZone(), s.push({
                event: n,
                start: i,
                end: o,
                eventStartMS: +i,
                eventDurationMS: o - i
            });
            return s
        },
        eventsToInverseRanges: function(t) {
            var e, n, i = this.view,
                o = i.start.clone().stripZone(),
                r = i.end.clone().stripZone(),
                s = this.eventsToNormalRanges(t),
                a = [],
                l = t[0],
                c = o;
            for (s.sort(gt), e = 0; s.length > e; e++) n = s[e], n.start > c && a.push({
                event: l,
                start: c,
                end: n.start
            }), c = n.end;
            return r > c && a.push({
                event: l,
                start: c,
                end: r
            }), a
        },
        eventRangeToSegs: function(t, e) {
            var n, i, o;
            for (n = e ? e(t) : this.rangeToSegs(t), i = 0; n.length > i; i++) o = n[i], o.event = t.event, o.eventStartMS = t.eventStartMS, o.eventDurationMS = t.eventDurationMS;
            return n
        }
    }), Lt.compareSegs = mt, Lt.dataAttrPrefix = "";
    var re = oe.extend({
        numbersVisible: !1,
        bottomCoordPadding: 0,
        breakOnWeeks: null,
        cellDates: null,
        dayToCellOffsets: null,
        rowEls: null,
        dayEls: null,
        helperEls: null,
        constructor: function() {
            oe.apply(this, arguments), this.cellDuration = e.duration(1, "day")
        },
        renderDates: function(t) {
            var e, n, i, o = this.view,
                r = this.rowCnt,
                s = this.colCnt,
                a = r * s,
                l = "";
            for (e = 0; r > e; e++) l += this.dayRowHtml(e, t);
            for (this.el.html(l), this.rowEls = this.el.find(".fc-row"), this.dayEls = this.el.find(".fc-day"), n = 0; a > n; n++) i = this.getCell(n), o.trigger("dayRender", null, i.start, this.dayEls.eq(n))
        },
        destroyDates: function() {
            this.destroySegPopover()
        },
        renderBusinessHours: function() {
            var t = this.view.calendar.getBusinessHoursEvents(!0),
                e = this.eventsToSegs(t);
            this.renderFill("businessHours", e, "bgevent")
        },
        dayRowHtml: function(t, e) {
            var n = this.view,
                i = ["fc-row", "fc-week", n.widgetContentClass];
            return e && i.push("fc-rigid"), '<div class="' + i.join(" ") + '"><div class="fc-bg"><table>' + this.rowHtml("day", t) + '</table></div><div class="fc-content-skeleton"><table>' + (this.numbersVisible ? "<thead>" + this.rowHtml("number", t) + "</thead>" : "") + "</table></div></div>"
        },
        dayCellHtml: function(t) {
            return this.bgCellHtml(t)
        },
        computeColHeadFormat: function() {
            return this.rowCnt > 1 ? "ddd" : this.colCnt > 1 ? this.view.opt("dayOfMonthFormat") : "dddd"
        },
        computeEventTimeFormat: function() {
            return this.view.opt("extraSmallTimeFormat")
        },
        computeDisplayEventEnd: function() {
            return 1 == this.colCnt
        },
        updateCells: function() {
            var t, e, n, i;
            if (this.updateCellDates(), t = this.cellDates, this.breakOnWeeks) {
                for (e = t[0].day(), i = 1; t.length > i && t[i].day() != e; i++);
                n = Math.ceil(t.length / i)
            } else n = 1, i = t.length;
            this.rowCnt = n, this.colCnt = i
        },
        updateCellDates: function() {
            for (var t = this.view, e = this.start.clone(), n = [], i = -1, o = []; e.isBefore(this.end);) t.isHiddenDay(e) ? o.push(i + .5) : (i++, o.push(i), n.push(e.clone())), e.add(1, "days");
            this.cellDates = n, this.dayToCellOffsets = o
        },
        computeCellDate: function(t) {
            var e = this.colCnt,
                n = t.row * e + (this.isRTL ? e - t.col - 1 : t.col);
            return this.cellDates[n].clone()
        },
        getRowEl: function(t) {
            return this.rowEls.eq(t)
        },
        getColEl: function(t) {
            return this.dayEls.eq(t)
        },
        getCellDayEl: function(t) {
            return this.dayEls.eq(t.row * this.colCnt + t.col)
        },
        computeRowCoords: function() {
            var t = oe.prototype.computeRowCoords.call(this);
            return t[t.length - 1].bottom += this.bottomCoordPadding, t
        },
        rangeToSegs: function(t) {
            var e, n, i, o, r, s, a, l, c, u, d = this.isRTL,
                h = this.rowCnt,
                f = this.colCnt,
                p = [];
            for (t = this.view.computeDayRange(t), e = this.dateToCellOffset(t.start), n = this.dateToCellOffset(t.end.subtract(1, "days")), i = 0; h > i; i++) o = i * f, r = o + f - 1, l = Math.max(o, e), c = Math.min(r, n), l = Math.ceil(l), c = Math.floor(c), c >= l && (s = l === e, a = c === n, l -= o, c -= o, u = {
                row: i,
                isStart: s,
                isEnd: a
            }, d ? (u.leftCol = f - c - 1, u.rightCol = f - l - 1) : (u.leftCol = l, u.rightCol = c), p.push(u));
            return p
        },
        dateToCellOffset: function(t) {
            var e = this.dayToCellOffsets,
                n = t.diff(this.start, "days");
            return 0 > n ? e[0] - 1 : n >= e.length ? e[e.length - 1] + 1 : e[n]
        },
        renderDrag: function(t, e) {
            return this.renderHighlight(this.view.calendar.ensureVisibleEventRange(t)), e && !e.el.closest(this.el).length ? (this.renderRangeHelper(t, e), this.applyDragOpacity(this.helperEls), !0) : void 0
        },
        destroyDrag: function() {
            this.destroyHighlight(), this.destroyHelper()
        },
        renderEventResize: function(t, e) {
            this.renderHighlight(t), this.renderRangeHelper(t, e)
        },
        destroyEventResize: function() {
            this.destroyHighlight(), this.destroyHelper()
        },
        renderHelper: function(e, n) {
            var i, o = [],
                r = this.eventsToSegs([e]);
            r = this.renderFgSegEls(r), i = this.renderSegRows(r), this.rowEls.each(function(e, r) {
                var s, a = t(r),
                    l = t('<div class="fc-helper-skeleton"><table/></div>');
                s = n && n.row === e ? n.el.position().top : a.find(".fc-content-skeleton tbody").position().top, l.css("top", s).find("table").append(i[e].tbodyEl), a.append(l), o.push(l[0])
            }), this.helperEls = t(o)
        },
        destroyHelper: function() {
            this.helperEls && (this.helperEls.remove(), this.helperEls = null)
        },
        fillSegTag: "td",
        renderFill: function(e, n, i) {
            var o, r, s, a = [];
            for (n = this.renderFillSegEls(e, n), o = 0; n.length > o; o++) r = n[o], s = this.renderFillRow(e, r, i), this.rowEls.eq(r.row).append(s), a.push(s[0]);
            return this.elsByFill[e] = t(a), n
        },
        renderFillRow: function(e, n, i) {
            var o, r, s = this.colCnt,
                a = n.leftCol,
                l = n.rightCol + 1;
            return i = i || e.toLowerCase(), o = t('<div class="fc-' + i + '-skeleton"><table><tr/></table></div>'), r = o.find("tr"), a > 0 && r.append('<td colspan="' + a + '"/>'), r.append(n.el.attr("colspan", l - a)), s > l && r.append('<td colspan="' + (s - l) + '"/>'), this.bookendCells(r, e), o
        }
    });
    re.mixin({
        rowStructs: null,
        destroyEvents: function() {
            this.destroySegPopover(), oe.prototype.destroyEvents.apply(this, arguments)
        },
        getEventSegs: function() {
            return oe.prototype.getEventSegs.call(this).concat(this.popoverSegs || [])
        },
        renderBgSegs: function(e) {
            var n = t.grep(e, function(t) {
                return t.event.allDay
            });
            return oe.prototype.renderBgSegs.call(this, n)
        },
        renderFgSegs: function(e) {
            var n;
            return e = this.renderFgSegEls(e), n = this.rowStructs = this.renderSegRows(e), this.rowEls.each(function(e, i) {
                t(i).find(".fc-content-skeleton > table").append(n[e].tbodyEl)
            }), e
        },
        destroyFgSegs: function() {
            for (var t, e = this.rowStructs || []; t = e.pop();) t.tbodyEl.remove();
            this.rowStructs = null
        },
        renderSegRows: function(t) {
            var e, n, i = [];
            for (e = this.groupSegRows(t), n = 0; e.length > n; n++) i.push(this.renderSegRow(n, e[n]));
            return i
        },
        fgSegHtml: function(t, e) {
            var n, i, o = this.view,
                r = t.event,
                s = o.isEventDraggable(r),
                a = !e && r.allDay && t.isStart && o.isEventResizableFromStart(r),
                l = !e && r.allDay && t.isEnd && o.isEventResizableFromEnd(r),
                c = this.getSegClasses(t, s, a || l),
                u = q(this.getEventSkinCss(r)),
                d = "";
            return c.unshift("fc-day-grid-event", "fc-h-event"), t.isStart && (n = this.getEventTimeText(r), n && (d = '<span class="fc-time">' + Y(n) + "</span>")), i = '<span class="fc-title">' + (Y(r.title || "") || "&nbsp;") + "</span>", '<a class="' + c.join(" ") + '"' + (r.url ? ' href="' + Y(r.url) + '"' : "") + (u ? ' style="' + u + '"' : "") + '><div class="fc-content">' + (this.isRTL ? i + " " + d : d + " " + i) + "</div>" + (a ? '<div class="fc-resizer fc-start-resizer" />' : "") + (l ? '<div class="fc-resizer fc-end-resizer" />' : "") + "</a>"
        },
        renderSegRow: function(e, n) {
            function i(e) {
                for (; e > s;) u = (v[o - 1] || [])[s], u ? u.attr("rowspan", parseInt(u.attr("rowspan") || 1, 10) + 1) : (u = t("<td/>"), a.append(u)), m[o][s] = u, v[o][s] = u, s++
            }
            var o, r, s, a, l, c, u, d = this.colCnt,
                h = this.buildSegLevels(n),
                f = Math.max(1, h.length),
                p = t("<tbody/>"),
                g = [],
                m = [],
                v = [];
            for (o = 0; f > o; o++) {
                if (r = h[o], s = 0, a = t("<tr/>"), g.push([]), m.push([]), v.push([]), r)
                    for (l = 0; r.length > l; l++) {
                        for (c = r[l], i(c.leftCol), u = t('<td class="fc-event-container"/>').append(c.el), c.leftCol != c.rightCol ? u.attr("colspan", c.rightCol - c.leftCol + 1) : v[o][s] = u; c.rightCol >= s;) m[o][s] = u, g[o][s] = c, s++;
                        a.append(u)
                    }
                i(d), this.bookendCells(a, "eventSkeleton"), p.append(a)
            }
            return {
                row: e,
                tbodyEl: p,
                cellMatrix: m,
                segMatrix: g,
                segLevels: h,
                segs: n
            }
        },
        buildSegLevels: function(t) {
            var e, n, i, o = [];
            for (t.sort(mt), e = 0; t.length > e; e++) {
                for (n = t[e], i = 0; o.length > i && yt(n, o[i]); i++);
                n.level = i, (o[i] || (o[i] = [])).push(n)
            }
            for (i = 0; o.length > i; i++) o[i].sort(wt);
            return o
        },
        groupSegRows: function(t) {
            var e, n = [];
            for (e = 0; this.rowCnt > e; e++) n.push([]);
            for (e = 0; t.length > e; e++) n[t[e].row].push(t[e]);
            return n
        }
    }), re.mixin({
        segPopover: null,
        popoverSegs: null,
        destroySegPopover: function() {
            this.segPopover && this.segPopover.hide()
        },
        limitRows: function(t) {
            var e, n, i = this.rowStructs || [];
            for (e = 0; i.length > e; e++) this.unlimitRow(e), n = t ? "number" == typeof t ? t : this.computeRowLevelLimit(e) : !1, n !== !1 && this.limitRow(e, n)
        },
        computeRowLevelLimit: function(e) {
            function n(e, n) {
                r = Math.max(r, t(n).outerHeight())
            }
            var i, o, r, s = this.rowEls.eq(e),
                a = s.height(),
                l = this.rowStructs[e].tbodyEl.children();
            for (i = 0; l.length > i; i++)
                if (o = l.eq(i).removeClass("fc-limited"), r = 0, o.find("> td > :first-child").each(n), o.position().top + r > a) return i;
            return !1
        },
        limitRow: function(e, n) {
            function i(i) {
                for (; i > T;) o = b.getCell(e, T), u = b.getCellSegs(o, n), u.length && (f = s[n - 1][T], w = b.renderMoreLink(o, u), y = t("<div/>").append(w), f.append(y), S.push(y[0])), T++
            }
            var o, r, s, a, l, c, u, d, h, f, p, g, m, v, y, w, b = this,
                x = this.rowStructs[e],
                S = [],
                T = 0;
            if (n && x.segLevels.length > n) {
                for (r = x.segLevels[n - 1], s = x.cellMatrix, a = x.tbodyEl.children().slice(n).addClass("fc-limited").get(), l = 0; r.length > l; l++) {
                    for (c = r[l], i(c.leftCol), h = [], d = 0; c.rightCol >= T;) o = this.getCell(e, T), u = this.getCellSegs(o, n), h.push(u), d += u.length, T++;
                    if (d) {
                        for (f = s[n - 1][c.leftCol], p = f.attr("rowspan") || 1, g = [], m = 0; h.length > m; m++) v = t('<td class="fc-more-cell"/>').attr("rowspan", p), u = h[m], o = this.getCell(e, c.leftCol + m), w = this.renderMoreLink(o, [c].concat(u)), y = t("<div/>").append(w), v.append(y), g.push(v[0]), S.push(v[0]);
                        f.addClass("fc-limited").after(t(g)), a.push(f[0])
                    }
                }
                i(this.colCnt), x.moreEls = t(S), x.limitedEls = t(a)
            }
        },
        unlimitRow: function(t) {
            var e = this.rowStructs[t];
            e.moreEls && (e.moreEls.remove(), e.moreEls = null), e.limitedEls && (e.limitedEls.removeClass("fc-limited"), e.limitedEls = null)
        },
        renderMoreLink: function(e, n) {
            var i = this,
                o = this.view;
            return t('<a class="fc-more"/>').text(this.getMoreLinkText(n.length)).on("click", function(r) {
                var s = o.opt("eventLimitClick"),
                    a = e.start,
                    l = t(this),
                    c = i.getCellDayEl(e),
                    u = i.getCellSegs(e),
                    d = i.resliceDaySegs(u, a),
                    h = i.resliceDaySegs(n, a);
                "function" == typeof s && (s = o.trigger("eventLimitClick", null, {
                    date: a,
                    dayEl: c,
                    moreEl: l,
                    segs: d,
                    hiddenSegs: h
                }, r)), "popover" === s ? i.showSegPopover(e, l, d) : "string" == typeof s && o.calendar.zoomTo(a, s)
            })
        },
        showSegPopover: function(t, e, n) {
            var i, o, r = this,
                s = this.view,
                a = e.parent();
            i = 1 == this.rowCnt ? s.el : this.rowEls.eq(t.row), o = {
                className: "fc-more-popover",
                content: this.renderSegPopoverContent(t, n),
                parentEl: this.el,
                top: i.offset().top,
                autoHide: !0,
                viewportConstrain: s.opt("popoverViewportConstrain"),
                hide: function() {
                    r.segPopover.destroy(), r.segPopover = null, r.popoverSegs = null
                }
            }, this.isRTL ? o.right = a.offset().left + a.outerWidth() + 1 : o.left = a.offset().left - 1, this.segPopover = new Qt(o), this.segPopover.show()
        },
        renderSegPopoverContent: function(e, n) {
            var i, o = this.view,
                r = o.opt("theme"),
                s = e.start.format(o.opt("dayPopoverFormat")),
                a = t('<div class="fc-header ' + o.widgetHeaderClass + '"><span class="fc-close ' + (r ? "ui-icon ui-icon-closethick" : "fc-icon fc-icon-x") + '"></span><span class="fc-title">' + Y(s) + '</span><div class="fc-clear"/></div><div class="fc-body ' + o.widgetContentClass + '"><div class="fc-event-container"></div></div>'),
                l = a.find(".fc-event-container");
            for (n = this.renderFgSegEls(n, !0), this.popoverSegs = n, i = 0; n.length > i; i++) n[i].cell = e, l.append(n[i].el);
            return a
        },
        resliceDaySegs: function(e, n) {
            var i = t.map(e, function(t) {
                    return t.event
                }),
                o = n.clone().stripTime(),
                r = o.clone().add(1, "days"),
                s = {
                    start: o,
                    end: r
                };
            return e = this.eventsToSegs(i, function(t) {
                var e = E(t, s);
                return e ? [e] : []
            }), e.sort(mt), e
        },
        getMoreLinkText: function(t) {
            var e = this.view.opt("eventLimitText");
            return "function" == typeof e ? e(t) : "+" + t + " " + e
        },
        getCellSegs: function(t, e) {
            for (var n, i = this.rowStructs[t.row].segMatrix, o = e || 0, r = []; i.length > o;) n = i[o][t.col], n && r.push(n), o++;
            return r
        }
    });
    var se = oe.extend({
        slotDuration: null,
        snapDuration: null,
        minTime: null,
        maxTime: null,
        axisFormat: null,
        dayEls: null,
        slatEls: null,
        slatTops: null,
        helperEl: null,
        businessHourSegs: null,
        constructor: function() {
            oe.apply(this, arguments), this.processOptions()
        },
        renderDates: function() {
            this.el.html(this.renderHtml()), this.dayEls = this.el.find(".fc-day"), this.slatEls = this.el.find(".fc-slats tr")
        },
        renderBusinessHours: function() {
            var t = this.view.calendar.getBusinessHoursEvents();
            this.businessHourSegs = this.renderFill("businessHours", this.eventsToSegs(t), "bgevent")
        },
        renderHtml: function() {
            return '<div class="fc-bg"><table>' + this.rowHtml("slotBg") + '</table></div><div class="fc-slats"><table>' + this.slatRowHtml() + "</table></div>"
        },
        slotBgCellHtml: function(t) {
            return this.bgCellHtml(t)
        },
        slatRowHtml: function() {
            for (var t, n, i, o = this.view, r = this.isRTL, s = "", a = 0 === this.slotDuration.asMinutes() % 15, l = e.duration(+this.minTime); this.maxTime > l;) t = this.start.clone().time(l), n = t.minutes(), i = '<td class="fc-axis fc-time ' + o.widgetContentClass + '" ' + o.axisStyleAttr() + ">" + (a && n ? "" : "<span>" + Y(t.format(this.axisFormat)) + "</span>") + "</td>", s += "<tr " + (n ? 'class="fc-minor"' : "") + ">" + (r ? "" : i) + '<td class="' + o.widgetContentClass + '"/>' + (r ? i : "") + "</tr>", l.add(this.slotDuration);
            return s
        },
        processOptions: function() {
            var t = this.view,
                n = t.opt("slotDuration"),
                i = t.opt("snapDuration");
            n = e.duration(n), i = i ? e.duration(i) : n, this.slotDuration = n, this.snapDuration = i, this.cellDuration = i, this.minTime = e.duration(t.opt("minTime")), this.maxTime = e.duration(t.opt("maxTime")), this.axisFormat = t.opt("axisFormat") || t.opt("smallTimeFormat")
        },
        computeColHeadFormat: function() {
            return this.colCnt > 1 ? this.view.opt("dayOfMonthFormat") : "dddd"
        },
        computeEventTimeFormat: function() {
            return this.view.opt("noMeridiemTimeFormat")
        },
        computeDisplayEventEnd: function() {
            return !0
        },
        updateCells: function() {
            var t, e = this.view,
                n = [];
            for (t = this.start.clone(); t.isBefore(this.end);) n.push({
                day: t.clone()
            }), t.add(1, "day"), t = e.skipHiddenDays(t);
            this.isRTL && n.reverse(), this.colData = n, this.colCnt = n.length, this.rowCnt = Math.ceil((this.maxTime - this.minTime) / this.snapDuration)
        },
        computeCellDate: function(t) {
            var e = this.computeSnapTime(t.row);
            return this.view.calendar.rezoneDate(t.day).time(e)
        },
        getColEl: function(t) {
            return this.dayEls.eq(t)
        },
        computeSnapTime: function(t) {
            return e.duration(this.minTime + this.snapDuration * t)
        },
        rangeToSegs: function(t) {
            var e, n, i, o, r = this.colCnt,
                s = [];
            for (t = {
                start: t.start.clone().stripZone(),
                end: t.end.clone().stripZone()
            }, n = 0; r > n; n++) i = this.colData[n].day, o = {
                start: i.clone().time(this.minTime),
                end: i.clone().time(this.maxTime)
            }, e = E(t, o), e && (e.col = n, s.push(e));
            return s
        },
        updateSize: function(t) {
            this.computeSlatTops(), t && this.updateSegVerticals()
        },
        computeRowCoords: function() {
            var t, e, n = this.el.offset().top,
                i = [];
            for (t = 0; this.rowCnt > t; t++) e = {
                top: n + this.computeTimeTop(this.computeSnapTime(t))
            }, t > 0 && (i[t - 1].bottom = e.top), i.push(e);
            return e.bottom = e.top + this.computeTimeTop(this.computeSnapTime(t)), i
        },
        computeDateTop: function(t, n) {
            return this.computeTimeTop(e.duration(t.clone().stripZone() - n.clone().stripTime()))
        },
        computeTimeTop: function(t) {
            var e, n, i, o, r = (t - this.minTime) / this.slotDuration;
            return r = Math.max(0, r), r = Math.min(this.slatEls.length, r), e = Math.floor(r), n = r - e, i = this.slatTops[e], n ? (o = this.slatTops[e + 1], i + (o - i) * n) : i
        },
        computeSlatTops: function() {
            var e, n = [];
            this.slatEls.each(function(i, o) {
                e = t(o).position().top, n.push(e)
            }), n.push(e + this.slatEls.last().outerHeight()), this.slatTops = n
        },
        renderDrag: function(t, e) {
            return e ? (this.renderRangeHelper(t, e), this.applyDragOpacity(this.helperEl), !0) : void this.renderHighlight(this.view.calendar.ensureVisibleEventRange(t))
        },
        destroyDrag: function() {
            this.destroyHelper(), this.destroyHighlight()
        },
        renderEventResize: function(t, e) {
            this.renderRangeHelper(t, e)
        },
        destroyEventResize: function() {
            this.destroyHelper()
        },
        renderHelper: function(e, n) {
            var i, o, r, s, a = this.eventsToSegs([e]);
            for (a = this.renderFgSegEls(a), i = this.renderSegTable(a), o = 0; a.length > o; o++) r = a[o], n && n.col === r.col && (s = n.el, r.el.css({
                left: s.css("left"),
                right: s.css("right"),
                "margin-left": s.css("margin-left"),
                "margin-right": s.css("margin-right")
            }));
            this.helperEl = t('<div class="fc-helper-skeleton"/>').append(i).appendTo(this.el)
        },
        destroyHelper: function() {
            this.helperEl && (this.helperEl.remove(), this.helperEl = null)
        },
        renderSelection: function(t) {
            this.view.opt("selectHelper") ? this.renderRangeHelper(t) : this.renderHighlight(t)
        },
        destroySelection: function() {
            this.destroyHelper(), this.destroyHighlight()
        },
        renderFill: function(e, n, i) {
            var o, r, s, a, l, c, u, d, h, f;
            if (n.length) {
                for (n = this.renderFillSegEls(e, n), o = this.groupSegCols(n), i = i || e.toLowerCase(), r = t('<div class="fc-' + i + '-skeleton"><table><tr/></table></div>'), s = r.find("tr"), a = 0; o.length > a; a++)
                    if (l = o[a], c = t("<td/>").appendTo(s), l.length)
                        for (u = t('<div class="fc-' + i + '-container"/>').appendTo(c), d = this.colData[a].day, h = 0; l.length > h; h++) f = l[h], u.append(f.el.css({
                            top: this.computeDateTop(f.start, d),
                            bottom: -this.computeDateTop(f.end, d)
                        }));
                this.bookendCells(s, e), this.el.append(r), this.elsByFill[e] = r
            }
            return n
        }
    });
    se.mixin({
        eventSkeletonEl: null,
        renderFgSegs: function(e) {
            return e = this.renderFgSegEls(e), this.el.append(this.eventSkeletonEl = t('<div class="fc-content-skeleton"/>').append(this.renderSegTable(e))), e
        },
        destroyFgSegs: function() {
            this.eventSkeletonEl && (this.eventSkeletonEl.remove(), this.eventSkeletonEl = null)
        },
        renderSegTable: function(e) {
            var n, i, o, r, s, a, l = t("<table><tr/></table>"),
                c = l.find("tr");
            for (n = this.groupSegCols(e), this.computeSegVerticals(e), r = 0; n.length > r; r++) {
                for (s = n[r], bt(s), a = t('<div class="fc-event-container"/>'), i = 0; s.length > i; i++) o = s[i], o.el.css(this.generateSegPositionCss(o)), 30 > o.bottom - o.top && o.el.addClass("fc-short"), a.append(o.el);
                c.append(t("<td/>").append(a))
            }
            return this.bookendCells(c, "eventSkeleton"), l
        },
        updateSegVerticals: function() {
            var t, e = (this.segs || []).concat(this.businessHourSegs || []);
            for (this.computeSegVerticals(e), t = 0; e.length > t; t++) e[t].el.css(this.generateSegVerticalCss(e[t]))
        },
        computeSegVerticals: function(t) {
            var e, n;
            for (e = 0; t.length > e; e++) n = t[e], n.top = this.computeDateTop(n.start, n.start), n.bottom = this.computeDateTop(n.end, n.start)
        },
        fgSegHtml: function(t, e) {
            var n, i, o, r = this.view,
                s = t.event,
                a = r.isEventDraggable(s),
                l = !e && t.isStart && r.isEventResizableFromStart(s),
                c = !e && t.isEnd && r.isEventResizableFromEnd(s),
                u = this.getSegClasses(t, a, l || c),
                d = q(this.getEventSkinCss(s));
            return u.unshift("fc-time-grid-event", "fc-v-event"), r.isMultiDayEvent(s) ? (t.isStart || t.isEnd) && (n = this.getEventTimeText(t), i = this.getEventTimeText(t, "LT"), o = this.getEventTimeText(t, null, !1)) : (n = this.getEventTimeText(s), i = this.getEventTimeText(s, "LT"), o = this.getEventTimeText(s, null, !1)), '<a class="' + u.join(" ") + '"' + (s.url ? ' href="' + Y(s.url) + '"' : "") + (d ? ' style="' + d + '"' : "") + '><div class="fc-content">' + (n ? '<div class="fc-time" data-start="' + Y(o) + '" data-full="' + Y(i) + '"><span>' + Y(n) + "</span></div>" : "") + (s.title ? '<div class="fc-title">' + Y(s.title) + "</div>" : "") + '</div><div class="fc-bg"/>' + (c ? '<div class="fc-resizer fc-end-resizer" />' : "") + "</a>"
        },
        generateSegPositionCss: function(t) {
            var e, n, i = this.view.opt("slotEventOverlap"),
                o = t.backwardCoord,
                r = t.forwardCoord,
                s = this.generateSegVerticalCss(t);
            return i && (r = Math.min(1, o + 2 * (r - o))), this.isRTL ? (e = 1 - r, n = o) : (e = o, n = 1 - r), s.zIndex = t.level + 1, s.left = 100 * e + "%", s.right = 100 * n + "%", i && t.forwardPressure && (s[this.isRTL ? "marginLeft" : "marginRight"] = 20), s
        },
        generateSegVerticalCss: function(t) {
            return {
                top: t.top,
                bottom: -t.bottom
            }
        },
        groupSegCols: function(t) {
            var e, n = [];
            for (e = 0; this.colCnt > e; e++) n.push([]);
            for (e = 0; t.length > e; e++) n[t[e].col].push(t[e]);
            return n
        }
    });
    var ae = Lt.View = ct.extend({
            type: null,
            name: null,
            title: null,
            calendar: null,
            options: null,
            coordMap: null,
            el: null,
            isDisplayed: !1,
            isSkeletonRendered: !1,
            isEventsRendered: !1,
            start: null,
            end: null,
            intervalStart: null,
            intervalEnd: null,
            intervalDuration: null,
            intervalUnit: null,
            isSelected: !1,
            scrollerEl: null,
            scrollTop: null,
            widgetHeaderClass: null,
            widgetContentClass: null,
            highlightStateClass: null,
            nextDayThreshold: null,
            isHiddenDayHash: null,
            documentMousedownProxy: null,
            constructor: function(t, n, i, o) {
                this.calendar = t, this.type = this.name = n, this.options = i, this.intervalDuration = o || e.duration(1, "day"), this.nextDayThreshold = e.duration(this.opt("nextDayThreshold")), this.initThemingProps(), this.initHiddenDays(), this.documentMousedownProxy = U(this, "documentMousedown"), this.initialize()
            },
            initialize: function() {},
            opt: function(t) {
                return this.options[t]
            },
            trigger: function(t, e) {
                var n = this.calendar;
                return n.trigger.apply(n, [t, e || this].concat(Array.prototype.slice.call(arguments, 2), [this]))
            },
            setDate: function(t) {
                this.setRange(this.computeRange(t))
            },
            setRange: function(e) {
                t.extend(this, e), this.updateTitle()
            },
            computeRange: function(t) {
                var e, n, i = R(this.intervalDuration),
                    o = t.clone().startOf(i),
                    r = o.clone().add(this.intervalDuration);
                return /year|month|week|day/.test(i) ? (o.stripTime(), r.stripTime()) : (o.hasTime() || (o = this.calendar.rezoneDate(o)), r.hasTime() || (r = this.calendar.rezoneDate(r))), e = o.clone(), e = this.skipHiddenDays(e), n = r.clone(), n = this.skipHiddenDays(n, -1, !0), {
                    intervalUnit: i,
                    intervalStart: o,
                    intervalEnd: r,
                    start: e,
                    end: n
                }
            },
            computePrevDate: function(t) {
                return this.massageCurrentDate(t.clone().startOf(this.intervalUnit).subtract(this.intervalDuration), -1)
            },
            computeNextDate: function(t) {
                return this.massageCurrentDate(t.clone().startOf(this.intervalUnit).add(this.intervalDuration))
            },
            massageCurrentDate: function(t, e) {
                return 1 >= this.intervalDuration.as("days") && this.isHiddenDay(t) && (t = this.skipHiddenDays(t, e), t.startOf("day")), t
            },
            updateTitle: function() {
                this.title = this.computeTitle()
            },
            computeTitle: function() {
                return this.formatRange({
                    start: this.intervalStart,
                    end: this.intervalEnd
                }, this.opt("titleFormat") || this.computeTitleFormat(), this.opt("titleRangeSeparator"))
            },
            computeTitleFormat: function() {
                return "year" == this.intervalUnit ? "YYYY" : "month" == this.intervalUnit ? this.opt("monthYearFormat") : this.intervalDuration.as("days") > 1 ? "ll" : "LL"
            },
            formatRange: function(t, e, n) {
                var i = t.end;
                return i.hasTime() || (i = i.clone().subtract(1)), ot(t.start, i, e, n, this.opt("isRTL"))
            },
            setElement: function(t) {
                this.el = t, this.bindGlobalHandlers()
            },
            removeElement: function() {
                this.clear(), this.isSkeletonRendered && (this.destroySkeleton(), this.isSkeletonRendered = !1), this.unbindGlobalHandlers(), this.el.remove()
            },
            display: function(t) {
                var e = null;
                this.isDisplayed && (e = this.queryScroll()), this.clear(), this.setDate(t), this.render(), this.updateSize(), this.renderBusinessHours(), this.isDisplayed = !0, e = this.computeInitialScroll(e), this.forceScroll(e), this.triggerRender()
            },
            clear: function() {
                this.isDisplayed && (this.unselect(), this.clearEvents(), this.triggerDestroy(), this.destroyBusinessHours(), this.destroy(), this.isDisplayed = !1)
            },
            render: function() {
                this.isSkeletonRendered || (this.renderSkeleton(), this.isSkeletonRendered = !0), this.renderDates()
            },
            destroy: function() {
                this.destroyDates()
            },
            renderSkeleton: function() {},
            destroySkeleton: function() {},
            renderDates: function() {},
            destroyDates: function() {},
            renderBusinessHours: function() {},
            destroyBusinessHours: function() {},
            triggerRender: function() {
                this.trigger("viewRender", this, this, this.el)
            },
            triggerDestroy: function() {
                this.trigger("viewDestroy", this, this, this.el)
            },
            bindGlobalHandlers: function() {
                t(document).on("mousedown", this.documentMousedownProxy)
            },
            unbindGlobalHandlers: function() {
                t(document).off("mousedown", this.documentMousedownProxy)
            },
            initThemingProps: function() {
                var t = this.opt("theme") ? "ui" : "fc";
                this.widgetHeaderClass = t + "-widget-header", this.widgetContentClass = t + "-widget-content", this.highlightStateClass = t + "-state-highlight"
            },
            updateSize: function(t) {
                var e;
                t && (e = this.queryScroll()), this.updateHeight(), this.updateWidth(), t && this.setScroll(e)
            },
            updateWidth: function() {},
            updateHeight: function() {
                var t = this.calendar;
                this.setHeight(t.getSuggestedViewHeight(), t.isHeightAuto())
            },
            setHeight: function() {},
            computeScrollerHeight: function(t) {
                var e, n, i = this.scrollerEl;
                return e = this.el.add(i), e.css({
                    position: "relative",
                    left: -1
                }), n = this.el.outerHeight() - i.height(), e.css({
                    position: "",
                    left: ""
                }), t - n
            },
            computeInitialScroll: function() {
                return 0
            },
            queryScroll: function() {
                return this.scrollerEl ? this.scrollerEl.scrollTop() : void 0
            },
            setScroll: function(t) {
                return this.scrollerEl ? this.scrollerEl.scrollTop(t) : void 0
            },
            forceScroll: function(t) {
                var e = this;
                this.setScroll(t), setTimeout(function() {
                    e.setScroll(t)
                }, 0)
            },
            displayEvents: function(t) {
                var e = this.queryScroll();
                this.clearEvents(), this.renderEvents(t), this.isEventsRendered = !0, this.setScroll(e), this.triggerEventRender()
            },
            clearEvents: function() {
                this.isEventsRendered && (this.triggerEventDestroy(), this.destroyEvents(), this.isEventsRendered = !1)
            },
            renderEvents: function() {},
            destroyEvents: function() {},
            triggerEventRender: function() {
                this.renderedEventSegEach(function(t) {
                    this.trigger("eventAfterRender", t.event, t.event, t.el)
                }), this.trigger("eventAfterAllRender")
            },
            triggerEventDestroy: function() {
                this.renderedEventSegEach(function(t) {
                    this.trigger("eventDestroy", t.event, t.event, t.el);

                })
            },
            resolveEventEl: function(e, n) {
                var i = this.trigger("eventRender", e, e, n);
                return i === !1 ? n = null : i && i !== !0 && (n = t(i)), n
            },
            showEvent: function(t) {
                this.renderedEventSegEach(function(t) {
                    t.el.css("visibility", "")
                }, t)
            },
            hideEvent: function(t) {
                this.renderedEventSegEach(function(t) {
                    t.el.css("visibility", "hidden")
                }, t)
            },
            renderedEventSegEach: function(t, e) {
                var n, i = this.getEventSegs();
                for (n = 0; i.length > n; n++) e && i[n].event._id !== e._id || i[n].el && t.call(this, i[n])
            },
            getEventSegs: function() {
                return []
            },
            isEventDraggable: function(t) {
                var e = t.source || {};
                return j(t.startEditable, e.startEditable, this.opt("eventStartEditable"), t.editable, e.editable, this.opt("editable"))
            },
            reportEventDrop: function(t, e, n, i, o) {
                var r = this.calendar,
                    s = r.mutateEvent(t, e, n),
                    a = function() {
                        s.undo(), r.reportEventChange()
                    };
                this.triggerEventDrop(t, s.dateDelta, a, i, o), r.reportEventChange()
            },
            triggerEventDrop: function(t, e, n, i, o) {
                this.trigger("eventDrop", i[0], t, e, n, o, {})
            },
            reportExternalDrop: function(e, n, i, o, r) {
                var s, a, l = e.eventProps;
                l && (s = t.extend({}, l, n), a = this.calendar.renderEvent(s, e.stick)[0]), this.triggerExternalDrop(a, n, i, o, r)
            },
            triggerExternalDrop: function(t, e, n, i, o) {
                this.trigger("drop", n[0], e.start, i, o), t && this.trigger("eventReceive", null, t)
            },
            renderDrag: function() {},
            destroyDrag: function() {},
            isEventResizableFromStart: function(t) {
                return this.opt("eventResizableFromStart") && this.isEventResizable(t)
            },
            isEventResizableFromEnd: function(t) {
                return this.isEventResizable(t)
            },
            isEventResizable: function(t) {
                var e = t.source || {};
                return j(t.durationEditable, e.durationEditable, this.opt("eventDurationEditable"), t.editable, e.editable, this.opt("editable"))
            },
            reportEventResize: function(t, e, n, i, o) {
                var r = this.calendar,
                    s = r.mutateEvent(t, e, n),
                    a = function() {
                        s.undo(), r.reportEventChange()
                    };
                this.triggerEventResize(t, s.durationDelta, a, i, o), r.reportEventChange()
            },
            triggerEventResize: function(t, e, n, i, o) {
                this.trigger("eventResize", i[0], t, e, n, o, {})
            },
            select: function(t, e) {
                this.unselect(e), this.renderSelection(t), this.reportSelection(t, e)
            },
            renderSelection: function() {},
            reportSelection: function(t, e) {
                this.isSelected = !0, this.trigger("select", null, t.start, t.end, e)
            },
            unselect: function(t) {
                this.isSelected && (this.isSelected = !1, this.destroySelection(), this.trigger("unselect", null, t))
            },
            destroySelection: function() {},
            documentMousedown: function(e) {
                var n;
                this.isSelected && this.opt("unselectAuto") && x(e) && (n = this.opt("unselectCancel"), n && t(e.target).closest(n).length || this.unselect(e))
            },
            initHiddenDays: function() {
                var e, n = this.opt("hiddenDays") || [],
                    i = [],
                    o = 0;
                for (this.opt("weekends") === !1 && n.push(0, 6), e = 0; 7 > e; e++)(i[e] = -1 !== t.inArray(e, n)) || o++;
                if (!o) throw "invalid hiddenDays";
                this.isHiddenDayHash = i
            },
            isHiddenDay: function(t) {
                return e.isMoment(t) && (t = t.day()), this.isHiddenDayHash[t]
            },
            skipHiddenDays: function(t, e, n) {
                var i = t.clone();
                for (e = e || 1; this.isHiddenDayHash[(i.day() + (n ? e : 0) + 7) % 7];) i.add(e, "days");
                return i
            },
            computeDayRange: function(t) {
                var e, n = t.start.clone().stripTime(),
                    i = t.end,
                    o = null;
                return i && (o = i.clone().stripTime(), e = +i.time(), e && e >= this.nextDayThreshold && o.add(1, "days")), (!i || n >= o) && (o = n.clone().add(1, "days")), {
                    start: n,
                    end: o
                }
            },
            isMultiDayEvent: function(t) {
                var e = this.computeDayRange(t);
                return e.end.diff(e.start, "days") > 1
            }
        }),
        le = Lt.Calendar = Lt.CalendarBase = ct.extend({
            dirDefaults: null,
            langDefaults: null,
            overrides: null,
            options: null,
            viewSpecCache: null,
            view: null,
            header: null,
            constructor: Mt,
            initOptions: function(t) {
                var e, o, r, s;
                t = i(t), e = t.lang, o = ce[e], o || (e = le.defaults.lang, o = ce[e] || {}), r = j(t.isRTL, o.isRTL, le.defaults.isRTL), s = r ? le.rtlDefaults : {}, this.dirDefaults = s, this.langDefaults = o, this.overrides = t, this.options = n(le.defaults, s, o, t), _t(this.options), this.viewSpecCache = {}
            },
            getViewSpec: function(t) {
                var e = this.viewSpecCache;
                return e[t] || (e[t] = this.buildViewSpec(t))
            },
            getUnitViewSpec: function(e) {
                var n, i, o;
                if (-1 != t.inArray(e, Yt))
                    for (n = this.header.getViewsWithButtons(), t.each(Lt.views, function(t) {
                        n.push(t)
                    }), i = 0; n.length > i; i++)
                        if (o = this.getViewSpec(n[i]), o && o.singleUnit == e) return o
            },
            buildViewSpec: function(t) {
                for (var i, o, r, s, a, l, c = this.overrides.views || {}, u = [], d = [], h = t; h && !i;) o = Nt[h] || {}, r = c[h] || {}, s = s || r.duration || o.duration, h = r.type || o.type, "function" == typeof o ? (i = o, u.unshift(i.defaults || {})) : u.unshift(o), d.unshift(r);
                return i ? (l = {
                    "class": i,
                    type: t
                }, s && (s = e.duration(s), s.valueOf() || (s = null)), s && (l.duration = s, a = R(s), 1 === s.as(a) && (l.singleUnit = a, d.unshift(c[a] || {}))), l.defaults = n.apply(null, u), l.overrides = n.apply(null, d), this.buildViewSpecOptions(l), this.buildViewSpecButtonText(l, t), l) : void 0
            },
            buildViewSpecOptions: function(t) {
                t.options = n(le.defaults, t.defaults, this.dirDefaults, this.langDefaults, this.overrides, t.overrides), _t(t.options)
            },
            buildViewSpecButtonText: function(t, e) {
                function n(n) {
                    var i = n.buttonText || {};
                    return i[e] || (t.singleUnit ? i[t.singleUnit] : null)
                }
                t.buttonTextOverride = n(this.overrides) || t.overrides.buttonText, t.buttonTextDefault = n(this.langDefaults) || n(this.dirDefaults) || t.defaults.buttonText || n(le.defaults) || (t.duration ? this.humanizeDuration(t.duration) : null) || e
            },
            instantiateView: function(t) {
                var e = this.getViewSpec(t);
                return new e["class"](this, t, e.options, e.duration)
            },
            isValidViewType: function(t) {
                return Boolean(this.getViewSpec(t))
            }
        });
    le.defaults = {
        titleRangeSeparator: " — ",
        monthYearFormat: "MMMM YYYY",
        defaultTimedEventDuration: "02:00:00",
        defaultAllDayEventDuration: {
            days: 1
        },
        forceEventDuration: !1,
        nextDayThreshold: "09:00:00",
        defaultView: "month",
        aspectRatio: 1.35,
        header: {
            left: "title",
            center: "",
            right: "today prev,next"
        },
        weekends: !0,
        weekNumbers: !1,
        weekNumberTitle: "W",
        weekNumberCalculation: "local",
        lazyFetching: !0,
        startParam: "start",
        endParam: "end",
        timezoneParam: "timezone",
        timezone: !1,
        isRTL: !1,
        buttonText: {
            prev: "prev",
            next: "next",
            prevYear: "prev year",
            nextYear: "next year",
            year: "year",
            today: "today",
            month: "month",
            week: "week",
            day: "day"
        },
        buttonIcons: {
            prev: "left-single-arrow",
            next: "right-single-arrow",
            prevYear: "left-double-arrow",
            nextYear: "right-double-arrow"
        },
        theme: !1,
        themeButtonIcons: {
            prev: "circle-triangle-w",
            next: "circle-triangle-e",
            prevYear: "seek-prev",
            nextYear: "seek-next"
        },
        dragOpacity: .75,
        dragRevertDuration: 500,
        dragScroll: !0,
        unselectAuto: !0,
        dropAccept: "*",
        eventLimit: !1,
        eventLimitText: "more",
        eventLimitClick: "popover",
        dayPopoverFormat: "LL",
        handleWindowResize: !0,
        windowResizeDelay: 200
    }, le.englishDefaults = {
        dayPopoverFormat: "dddd, MMMM D"
    }, le.rtlDefaults = {
        header: {
            left: "next,prev today",
            center: "",
            right: "title"
        },
        buttonIcons: {
            prev: "right-single-arrow",
            next: "left-single-arrow",
            prevYear: "right-double-arrow",
            nextYear: "left-double-arrow"
        },
        themeButtonIcons: {
            prev: "circle-triangle-e",
            next: "circle-triangle-w",
            nextYear: "seek-prev",
            prevYear: "seek-next"
        }
    };
    var ce = Lt.langs = {};
    Lt.datepickerLang = function(e, n, i) {
        var o = ce[e] || (ce[e] = {});
        o.isRTL = i.isRTL, o.weekNumberTitle = i.weekHeader, t.each(ue, function(t, e) {
            o[t] = e(i)
        }), t.datepicker && (t.datepicker.regional[n] = t.datepicker.regional[e] = i, t.datepicker.regional.en = t.datepicker.regional[""], t.datepicker.setDefaults(i))
    }, Lt.lang = function(e, i) {
        var o, r;
        o = ce[e] || (ce[e] = {}), i && (o = ce[e] = n(o, i)), r = Rt(e), t.each(de, function(t, e) {
            null == o[t] && (o[t] = e(r, o))
        }), le.defaults.lang = e
    };
    var ue = {
            buttonText: function(t) {
                return {
                    prev: $(t.prevText),
                    next: $(t.nextText),
                    today: $(t.currentText)
                }
            },
            monthYearFormat: function(t) {
                return t.showMonthAfterYear ? "YYYY[" + t.yearSuffix + "] MMMM" : "MMMM YYYY[" + t.yearSuffix + "]"
            }
        },
        de = {
            dayOfMonthFormat: function(t, e) {
                var n = t.longDateFormat("l");
                return n = n.replace(/^Y+[^\w\s]*|[^\w\s]*Y+$/g, ""), e.isRTL ? n += " ddd" : n = "ddd " + n, n
            },
            mediumTimeFormat: function(t) {
                return t.longDateFormat("LT").replace(/\s*a$/i, "a")
            },
            smallTimeFormat: function(t) {
                return t.longDateFormat("LT").replace(":mm", "(:mm)").replace(/(\Wmm)$/, "($1)").replace(/\s*a$/i, "a")
            },
            extraSmallTimeFormat: function(t) {
                return t.longDateFormat("LT").replace(":mm", "(:mm)").replace(/(\Wmm)$/, "($1)").replace(/\s*a$/i, "t")
            },
            hourFormat: function(t) {
                return t.longDateFormat("LT").replace(":mm", "").replace(/(\Wmm)$/, "").replace(/\s*a$/i, "a")
            },
            noMeridiemTimeFormat: function(t) {
                return t.longDateFormat("LT").replace(/\s*a$/i, "")
            }
        },
        he = {
            smallDayDateFormat: function(t) {
                return t.isRTL ? "D dd" : "dd D"
            },
            weekFormat: function(t) {
                return t.isRTL ? "w[ " + t.weekNumberTitle + "]" : "[" + t.weekNumberTitle + " ]w"
            },
            smallWeekFormat: function(t) {
                return t.isRTL ? "w[" + t.weekNumberTitle + "]" : "[" + t.weekNumberTitle + "]w"
            }
        };
    Lt.lang("en", le.englishDefaults), Lt.sourceNormalizers = [], Lt.sourceFetchers = [];
    var fe = {
            dataType: "json",
            cache: !1
        },
        pe = 1;
    le.prototype.getPeerEvents = function(t) {
        var e, n, i = this.getEventCache(),
            o = [];
        for (e = 0; i.length > e; e++) n = i[e], t && t._id === n._id || o.push(n);
        return o
    };
    var ge = Nt.basic = ae.extend({
            dayGrid: null,
            dayNumbersVisible: !1,
            weekNumbersVisible: !1,
            weekNumberWidth: null,
            headRowEl: null,
            initialize: function() {
                this.dayGrid = new re(this), this.coordMap = this.dayGrid.coordMap
            },
            setRange: function(t) {
                ae.prototype.setRange.call(this, t), this.dayGrid.breakOnWeeks = /year|month|week/.test(this.intervalUnit), this.dayGrid.setRange(t)
            },
            computeRange: function(t) {
                var e = ae.prototype.computeRange.call(this, t);
                return /year|month/.test(e.intervalUnit) && (e.start.startOf("week"), e.start = this.skipHiddenDays(e.start), e.end.weekday() && (e.end.add(1, "week").startOf("week"), e.end = this.skipHiddenDays(e.end, -1, !0))), e
            },
            render: function() {
                this.dayNumbersVisible = this.dayGrid.rowCnt > 1, this.weekNumbersVisible = this.opt("weekNumbers"), this.dayGrid.numbersVisible = this.dayNumbersVisible || this.weekNumbersVisible, this.el.addClass("fc-basic-view").html(this.renderHtml()), this.headRowEl = this.el.find("thead .fc-row"), this.scrollerEl = this.el.find(".fc-day-grid-container"), this.dayGrid.coordMap.containerEl = this.scrollerEl, this.dayGrid.setElement(this.el.find(".fc-day-grid")), this.dayGrid.renderDates(this.hasRigidRows())
            },
            destroy: function() {
                this.dayGrid.destroyDates(), this.dayGrid.removeElement()
            },
            renderBusinessHours: function() {
                this.dayGrid.renderBusinessHours()
            },
            renderHtml: function() {
                return '<table><thead class="fc-head"><tr><td class="' + this.widgetHeaderClass + '">' + this.dayGrid.headHtml() + '</td></tr></thead><tbody class="fc-body"><tr><td class="' + this.widgetContentClass + '"><div class="fc-day-grid-container"><div class="fc-day-grid"/></div></td></tr></tbody></table>'
            },
            headIntroHtml: function() {
                return this.weekNumbersVisible ? '<th class="fc-week-number ' + this.widgetHeaderClass + '" ' + this.weekNumberStyleAttr() + "><span>" + Y(this.opt("weekNumberTitle")) + "</span></th>" : void 0
            },
            numberIntroHtml: function(t) {
                return this.weekNumbersVisible ? '<td class="fc-week-number" ' + this.weekNumberStyleAttr() + "><span>" + this.dayGrid.getCell(t, 0).start.format("w") + "</span></td>" : void 0
            },
            dayIntroHtml: function() {
                return this.weekNumbersVisible ? '<td class="fc-week-number ' + this.widgetContentClass + '" ' + this.weekNumberStyleAttr() + "></td>" : void 0
            },
            introHtml: function() {
                return this.weekNumbersVisible ? '<td class="fc-week-number" ' + this.weekNumberStyleAttr() + "></td>" : void 0
            },
            numberCellHtml: function(t) {
                var e, n = t.start;
                return this.dayNumbersVisible ? (e = this.dayGrid.getDayClasses(n), e.unshift("fc-day-number"), '<td class="' + e.join(" ") + '" data-date="' + n.format() + '">' + n.date() + "</td>") : "<td/>"
            },
            weekNumberStyleAttr: function() {
                return null !== this.weekNumberWidth ? 'style="width:' + this.weekNumberWidth + 'px"' : ""
            },
            hasRigidRows: function() {
                var t = this.opt("eventLimit");
                return t && "number" != typeof t
            },
            updateWidth: function() {
                this.weekNumbersVisible && (this.weekNumberWidth = u(this.el.find(".fc-week-number")))
            },
            setHeight: function(t, e) {
                var n, i = this.opt("eventLimit");
                h(this.scrollerEl), r(this.headRowEl), this.dayGrid.destroySegPopover(), i && "number" == typeof i && this.dayGrid.limitRows(i), n = this.computeScrollerHeight(t), this.setGridHeight(n, e), i && "number" != typeof i && this.dayGrid.limitRows(i), !e && d(this.scrollerEl, n) && (o(this.headRowEl, v(this.scrollerEl)), n = this.computeScrollerHeight(t), this.scrollerEl.height(n))
            },
            setGridHeight: function(t, e) {
                e ? c(this.dayGrid.rowEls) : l(this.dayGrid.rowEls, t, !0)
            },
            renderEvents: function(t) {
                this.dayGrid.renderEvents(t), this.updateHeight()
            },
            getEventSegs: function() {
                return this.dayGrid.getEventSegs()
            },
            destroyEvents: function() {
                this.dayGrid.destroyEvents()
            },
            renderDrag: function(t, e) {
                return this.dayGrid.renderDrag(t, e)
            },
            destroyDrag: function() {
                this.dayGrid.destroyDrag()
            },
            renderSelection: function(t) {
                this.dayGrid.renderSelection(t)
            },
            destroySelection: function() {
                this.dayGrid.destroySelection()
            }
        }),
        me = Nt.month = ge.extend({
            computeRange: function(t) {
                var e, n = ge.prototype.computeRange.call(this, t);
                return this.isFixedWeeks() && (e = Math.ceil(n.end.diff(n.start, "weeks", !0)), n.end.add(6 - e, "weeks")), n
            },
            setGridHeight: function(t, e) {
                e = e || "variable" === this.opt("weekMode"), e && (t *= this.rowCnt / 6), l(this.dayGrid.rowEls, t, !e)
            },
            isFixedWeeks: function() {
                var t = this.opt("weekMode");
                return t ? "fixed" === t : this.opt("fixedWeekCount")
            }
        });
    me.duration = {
        months: 1
    }, me.defaults = {
        fixedWeekCount: !0
    }, Nt.basicWeek = {
        type: "basic",
        duration: {
            weeks: 1
        }
    }, Nt.basicDay = {
        type: "basic",
        duration: {
            days: 1
        }
    };
    var ve = {
            allDaySlot: !0,
            allDayText: "all-day",
            scrollTime: "06:00:00",
            slotDuration: "00:30:00",
            minTime: "00:00:00",
            maxTime: "24:00:00",
            slotEventOverlap: !0
        },
        ye = 5,
        we = Nt.agenda = ae.extend({
            timeGrid: null,
            dayGrid: null,
            axisWidth: null,
            noScrollRowEls: null,
            bottomRuleEl: null,
            bottomRuleHeight: null,
            initialize: function() {
                this.timeGrid = new se(this), this.opt("allDaySlot") ? (this.dayGrid = new re(this), this.coordMap = new Jt([this.dayGrid.coordMap, this.timeGrid.coordMap])) : this.coordMap = this.timeGrid.coordMap
            },
            setRange: function(t) {
                ae.prototype.setRange.call(this, t), this.timeGrid.setRange(t), this.dayGrid && this.dayGrid.setRange(t)
            },
            render: function() {
                this.el.addClass("fc-agenda-view").html(this.renderHtml()), this.scrollerEl = this.el.find(".fc-time-grid-container"), this.timeGrid.coordMap.containerEl = this.scrollerEl, this.timeGrid.setElement(this.el.find(".fc-time-grid")), this.timeGrid.renderDates(), this.bottomRuleEl = t('<hr class="fc-divider ' + this.widgetHeaderClass + '"/>').appendTo(this.timeGrid.el), this.dayGrid && (this.dayGrid.setElement(this.el.find(".fc-day-grid")), this.dayGrid.renderDates(), this.dayGrid.bottomCoordPadding = this.dayGrid.el.next("hr").outerHeight()), this.noScrollRowEls = this.el.find(".fc-row:not(.fc-scroller *)")
            },
            destroy: function() {
                this.timeGrid.destroyDates(), this.timeGrid.removeElement(), this.dayGrid && (this.dayGrid.destroyDates(), this.dayGrid.removeElement())
            },
            renderBusinessHours: function() {
                this.timeGrid.renderBusinessHours(), this.dayGrid && this.dayGrid.renderBusinessHours()
            },
            renderHtml: function() {
                return '<table><thead class="fc-head"><tr><td class="' + this.widgetHeaderClass + '">' + this.timeGrid.headHtml() + '</td></tr></thead><tbody class="fc-body"><tr><td class="' + this.widgetContentClass + '">' + (this.dayGrid ? '<div class="fc-day-grid"/><hr class="fc-divider ' + this.widgetHeaderClass + '"/>' : "") + '<div class="fc-time-grid-container"><div class="fc-time-grid"/></div></td></tr></tbody></table>'
            },
            headIntroHtml: function() {
                var t, e;
                return this.opt("weekNumbers") ? (t = this.timeGrid.getCell(0).start, e = t.format(this.opt("smallWeekFormat")), '<th class="fc-axis fc-week-number ' + this.widgetHeaderClass + '" ' + this.axisStyleAttr() + "><span>" + Y(e) + "</span></th>") : '<th class="fc-axis ' + this.widgetHeaderClass + '" ' + this.axisStyleAttr() + "></th>"
            },
            dayIntroHtml: function() {
                return '<td class="fc-axis ' + this.widgetContentClass + '" ' + this.axisStyleAttr() + "><span>" + (this.opt("allDayHtml") || Y(this.opt("allDayText"))) + "</span></td>"
            },
            slotBgIntroHtml: function() {
                return '<td class="fc-axis ' + this.widgetContentClass + '" ' + this.axisStyleAttr() + "></td>"
            },
            introHtml: function() {
                return '<td class="fc-axis" ' + this.axisStyleAttr() + "></td>"
            },
            axisStyleAttr: function() {
                return null !== this.axisWidth ? 'style="width:' + this.axisWidth + 'px"' : ""
            },
            updateSize: function(t) {
                this.timeGrid.updateSize(t), ae.prototype.updateSize.call(this, t)
            },
            updateWidth: function() {
                this.axisWidth = u(this.el.find(".fc-axis"))
            },
            setHeight: function(t, e) {
                var n, i;
                null === this.bottomRuleHeight && (this.bottomRuleHeight = this.bottomRuleEl.outerHeight()), this.bottomRuleEl.hide(), this.scrollerEl.css("overflow", ""), h(this.scrollerEl), r(this.noScrollRowEls), this.dayGrid && (this.dayGrid.destroySegPopover(), n = this.opt("eventLimit"), n && "number" != typeof n && (n = ye), n && this.dayGrid.limitRows(n)), e || (i = this.computeScrollerHeight(t), d(this.scrollerEl, i) ? (o(this.noScrollRowEls, v(this.scrollerEl)), i = this.computeScrollerHeight(t), this.scrollerEl.height(i)) : (this.scrollerEl.height(i).css("overflow", "hidden"), this.bottomRuleEl.show()))
            },
            computeInitialScroll: function() {
                var t = e.duration(this.opt("scrollTime")),
                    n = this.timeGrid.computeTimeTop(t);
                return n = Math.ceil(n), n && n++, n
            },
            renderEvents: function(t) {
                var e, n, i = [],
                    o = [],
                    r = [];
                for (n = 0; t.length > n; n++) t[n].allDay ? i.push(t[n]) : o.push(t[n]);
                e = this.timeGrid.renderEvents(o), this.dayGrid && (r = this.dayGrid.renderEvents(i)), this.updateHeight()
            },
            getEventSegs: function() {
                return this.timeGrid.getEventSegs().concat(this.dayGrid ? this.dayGrid.getEventSegs() : [])
            },
            destroyEvents: function() {
                this.timeGrid.destroyEvents(), this.dayGrid && this.dayGrid.destroyEvents()
            },
            renderDrag: function(t, e) {
                return t.start.hasTime() ? this.timeGrid.renderDrag(t, e) : this.dayGrid ? this.dayGrid.renderDrag(t, e) : void 0
            },
            destroyDrag: function() {
                this.timeGrid.destroyDrag(), this.dayGrid && this.dayGrid.destroyDrag()
            },
            renderSelection: function(t) {
                t.start.hasTime() || t.end.hasTime() ? this.timeGrid.renderSelection(t) : this.dayGrid && this.dayGrid.renderSelection(t)
            },
            destroySelection: function() {
                this.timeGrid.destroySelection(), this.dayGrid && this.dayGrid.destroySelection()
            }
        });
    return we.defaults = ve, Nt.agendaWeek = {
        type: "agenda",
        duration: {
            weeks: 1
        }
    }, Nt.agendaDay = {
        type: "agenda",
        duration: {
            days: 1
        }
    }, Lt
}), ! function(t) {
    "use strict";

    function e(t, e) {
        return Math.round("f" === t ? 5 / 9 * (e - 32) : 1.8 * e + 32)
    }
    t.extend({
        simpleWeather: function(n) {
            n = t.extend({
                location: "",
                woeid: "",
                unit: "f",
                success: function() {},
                error: function() {}
            }, n);
            var i = new Date,
                o = "https://query.yahooapis.com/v1/public/yql?format=json&rnd=" + i.getFullYear() + i.getMonth() + i.getDay() + i.getHours() + "&diagnostics=true&callback=?&q=";
            if ("" !== n.location) o += 'select * from weather.forecast where woeid in (select woeid from geo.placefinder where text="' + n.location + '" and gflags="R" limit 1) and u="' + n.unit + '"';
            else {
                if ("" === n.woeid) return n.error({
                    message: "Could not retrieve weather due to an invalid location."
                }), !1;
                o += "select * from weather.forecast where woeid=" + n.woeid + ' and u="' + n.unit + '"'
            }
            return t.getJSON(encodeURI(o), function(t) {
                if (null !== t && null !== t.query && null !== t.query.results && "Yahoo! Weather Error" !== t.query.results.channel.description) {
                    var i, o = t.query.results.channel,
                        r = {},
                        s = ["N", "NNE", "NE", "ENE", "E", "ESE", "SE", "SSE", "S", "SSW", "SW", "WSW", "W", "WNW", "NW", "NNW", "N"],
                        a = "https://s.yimg.com/os/mit/media/m/weather/images/icons/l/44d-100567.png";
                    r.title = o.item.title, r.temp = o.item.condition.temp, r.code = o.item.condition.code, r.todayCode = o.item.forecast[0].code, r.currently = o.item.condition.text, r.high = o.item.forecast[0].high, r.low = o.item.forecast[0].low, r.text = o.item.forecast[0].text, r.humidity = o.atmosphere.humidity, r.pressure = o.atmosphere.pressure, r.rising = o.atmosphere.rising, r.visibility = o.atmosphere.visibility, r.sunrise = o.astronomy.sunrise, r.sunset = o.astronomy.sunset, r.description = o.item.description, r.city = o.location.city, r.country = o.location.country, r.region = o.location.region, r.updated = o.item.pubDate, r.link = o.item.link, r.units = {
                        temp: o.units.temperature,
                        distance: o.units.distance,
                        pressure: o.units.pressure,
                        speed: o.units.speed
                    }, r.wind = {
                        chill: o.wind.chill,
                        direction: s[Math.round(o.wind.direction / 22.5)],
                        speed: o.wind.speed
                    }, r.heatindex = o.item.condition.temp < 80 && o.atmosphere.humidity < 40 ? -42.379 + 2.04901523 * o.item.condition.temp + 10.14333127 * o.atmosphere.humidity - .22475541 * o.item.condition.temp * o.atmosphere.humidity - 6.83783 * Math.pow(10, -3) * Math.pow(o.item.condition.temp, 2) - 5.481717 * Math.pow(10, -2) * Math.pow(o.atmosphere.humidity, 2) + 1.22874 * Math.pow(10, -3) * Math.pow(o.item.condition.temp, 2) * o.atmosphere.humidity + 8.5282 * Math.pow(10, -4) * o.item.condition.temp * Math.pow(o.atmosphere.humidity, 2) - 1.99 * Math.pow(10, -6) * Math.pow(o.item.condition.temp, 2) * Math.pow(o.atmosphere.humidity, 2) : o.item.condition.temp, "3200" == o.item.condition.code ? (r.thumbnail = a, r.image = a) : (r.thumbnail = "https://s.yimg.com/zz/combo?a/i/us/nws/weather/gr/" + o.item.condition.code + "ds.png", r.image = "https://s.yimg.com/zz/combo?a/i/us/nws/weather/gr/" + o.item.condition.code + "d.png"), r.alt = {
                        temp: e(n.unit, o.item.condition.temp),
                        high: e(n.unit, o.item.forecast[0].high),
                        low: e(n.unit, o.item.forecast[0].low)
                    }, r.alt.unit = "f" === n.unit ? "c" : "f", r.forecast = [];
                    for (var l = 0; l < o.item.forecast.length; l++) i = o.item.forecast[l], i.alt = {
                        high: e(n.unit, o.item.forecast[l].high),
                        low: e(n.unit, o.item.forecast[l].low)
                    }, "3200" == o.item.forecast[l].code ? (i.thumbnail = a, i.image = a) : (i.thumbnail = "https://s.yimg.com/zz/combo?a/i/us/nws/weather/gr/" + o.item.forecast[l].code + "ds.png", i.image = "https://s.yimg.com/zz/combo?a/i/us/nws/weather/gr/" + o.item.forecast[l].code + "d.png"), r.forecast.push(i);
                    n.success(r)
                } else n.error({
                    message: "There was an error retrieving the latest weather information. Please try again.",
                    error: t.query.results.channel.item.title
                })
            }), this
        }
    })
}(jQuery),
function(t) {
    "function" == typeof define && define.amd ? define(["jquery"], t) : t(jQuery)
}(function(t) {
    var e = !1,
        n = !1,
        i = 0,
        o = 2e3,
        r = 0,
        s = ["webkit", "ms", "moz", "o"],
        a = window.requestAnimationFrame || !1,
        l = window.cancelAnimationFrame || !1;
    if (!a)
        for (var c in s) {
            var u = s[c];
            a || (a = window[u + "RequestAnimationFrame"]), l || (l = window[u + "CancelAnimationFrame"] || window[u + "CancelRequestAnimationFrame"])
        }
    var d = window.MutationObserver || window.WebKitMutationObserver || !1,
        h = {
            zindex: "auto",
            cursoropacitymin: 0,
            cursoropacitymax: 1,
            cursorcolor: "#424242",
            cursorwidth: "5px",
            cursorborder: "1px solid #fff",
            cursorborderradius: "5px",
            scrollspeed: 60,
            mousescrollstep: 24,
            touchbehavior: !1,
            hwacceleration: !0,
            usetransition: !0,
            boxzoom: !1,
            dblclickzoom: !0,
            gesturezoom: !0,
            grabcursorenabled: !0,
            autohidemode: !0,
            background: "",
            iframeautoresize: !0,
            cursorminheight: 32,
            preservenativescrolling: !0,
            railoffset: !1,
            railhoffset: !1,
            bouncescroll: !0,
            spacebarenabled: !0,
            railpadding: {
                top: 0,
                right: 0,
                left: 0,
                bottom: 0
            },
            disableoutline: !0,
            horizrailenabled: !0,
            railalign: "right",
            railvalign: "bottom",
            enabletranslate3d: !0,
            enablemousewheel: !0,
            enablekeyboard: !0,
            smoothscroll: !0,
            sensitiverail: !0,
            enablemouselockapi: !0,
            cursorfixedheight: !1,
            directionlockdeadzone: 6,
            hidecursordelay: 400,
            nativeparentscrolling: !0,
            enablescrollonselection: !0,
            overflowx: !0,
            overflowy: !0,
            cursordragspeed: .3,
            rtlmode: "auto",
            cursordragontouch: !1,
            oneaxismousemode: "auto",
            scriptpath: function() {
                var t = document.getElementsByTagName("script"),
                    t = t[t.length - 1].src.split("?")[0];
                return 0 < t.split("/").length ? t.split("/").slice(0, -1).join("/") + "/" : ""
            }(),
            preventmultitouchscrolling: !0
        },
        f = !1,
        p = function() {
            if (f) return f;
            var t = document.createElement("DIV"),
                e = t.style,
                n = navigator.userAgent,
                i = navigator.platform,
                o = {
                    haspointerlock: "pointerLockElement" in document || "webkitPointerLockElement" in document || "mozPointerLockElement" in document
                };
            o.isopera = "opera" in window, o.isopera12 = o.isopera && "getUserMedia" in navigator, o.isoperamini = "[object OperaMini]" === Object.prototype.toString.call(window.operamini), o.isie = "all" in document && "attachEvent" in t && !o.isopera, o.isieold = o.isie && !("msInterpolationMode" in e), o.isie7 = !(!o.isie || o.isieold || "documentMode" in document && 7 != document.documentMode), o.isie8 = o.isie && "documentMode" in document && 8 == document.documentMode, o.isie9 = o.isie && "performance" in window && 9 <= document.documentMode, o.isie10 = o.isie && "performance" in window && 10 == document.documentMode, o.isie11 = "msRequestFullscreen" in t && 11 <= document.documentMode, o.isie9mobile = /iemobile.9/i.test(n), o.isie9mobile && (o.isie9 = !1), o.isie7mobile = !o.isie9mobile && o.isie7 && /iemobile/i.test(n), o.ismozilla = "MozAppearance" in e, o.iswebkit = "WebkitAppearance" in e, o.ischrome = "chrome" in window, o.ischrome22 = o.ischrome && o.haspointerlock, o.ischrome26 = o.ischrome && "transition" in e, o.cantouch = "ontouchstart" in document.documentElement || "ontouchstart" in window, o.hasmstouch = window.MSPointerEvent || !1, o.hasw3ctouch = window.PointerEvent || !1, o.ismac = /^mac$/i.test(i), o.isios = o.cantouch && /iphone|ipad|ipod/i.test(i), o.isios4 = o.isios && !("seal" in Object), o.isios7 = o.isios && "webkitHidden" in document, o.isandroid = /android/i.test(n), o.haseventlistener = "addEventListener" in t, o.trstyle = !1, o.hastransform = !1, o.hastranslate3d = !1, o.transitionstyle = !1, o.hastransition = !1, o.transitionend = !1, i = ["transform", "msTransform", "webkitTransform", "MozTransform", "OTransform"];
            for (n = 0; n < i.length; n++)
                if ("undefined" != typeof e[i[n]]) {
                    o.trstyle = i[n];
                    break
                }
            o.hastransform = !!o.trstyle, o.hastransform && (e[o.trstyle] = "translate3d(1px,2px,3px)", o.hastranslate3d = /translate3d/.test(e[o.trstyle])), o.transitionstyle = !1, o.prefixstyle = "", o.transitionend = !1;
            for (var i = "transition webkitTransition msTransition MozTransition OTransition OTransition KhtmlTransition".split(" "), r = " -webkit- -ms- -moz- -o- -o -khtml-".split(" "), s = "transitionend webkitTransitionEnd msTransitionEnd transitionend otransitionend oTransitionEnd KhtmlTransitionEnd".split(" "), n = 0; n < i.length; n++)
                if (i[n] in e) {
                    o.transitionstyle = i[n], o.prefixstyle = r[n], o.transitionend = s[n];
                    break
                }
            o.ischrome26 && (o.prefixstyle = r[1]), o.hastransition = o.transitionstyle;
            t: {
                for (n = ["-webkit-grab", "-moz-grab", "grab"], (o.ischrome && !o.ischrome22 || o.isie) && (n = []), i = 0; i < n.length; i++)
                    if (r = n[i], e.cursor = r, e.cursor == r) {
                        e = r;
                        break t
                    }
                e = "url(//mail.google.com/mail/images/2/openhand.cur),n-resize"
            }
            return o.cursorgrabvalue = e, o.hasmousecapture = "setCapture" in t, o.hasMutationObserver = !1 !== d, f = o
        },
        g = function(s, c) {
            function u() {
                var t = w.doc.css(x.trstyle);
                return t && "matrix" == t.substr(0, 6) ? t.replace(/^.*\((.*)\)$/g, "$1").replace(/px/g, "").split(/, +/) : !1
            }

            function f() {
                var t = w.win;
                if ("zIndex" in t) return t.zIndex();
                for (; 0 < t.length && 9 != t[0].nodeType;) {
                    var e = t.css("zIndex");
                    if (!isNaN(e) && 0 != e) return parseInt(e);
                    t = t.parent()
                }
                return !1
            }

            function g(t, e, n) {
                return e = t.css(e), t = parseFloat(e), isNaN(t) ? (t = k[e] || 0, n = 3 == t ? n ? w.win.outerHeight() - w.win.innerHeight() : w.win.outerWidth() - w.win.innerWidth() : 1, w.isie8 && t && (t += 1), n ? t : 0) : t
            }

            function v(t, e, n, i) {
                w._bind(t, e, function(i) {
                    i = i ? i : window.event;
                    var o = {
                        original: i,
                        target: i.target || i.srcElement,
                        type: "wheel",
                        deltaMode: "MozMousePixelScroll" == i.type ? 0 : 1,
                        deltaX: 0,
                        deltaZ: 0,
                        preventDefault: function() {
                            return i.preventDefault ? i.preventDefault() : i.returnValue = !1, !1
                        },
                        stopImmediatePropagation: function() {
                            i.stopImmediatePropagation ? i.stopImmediatePropagation() : i.cancelBubble = !0
                        }
                    };
                    return "mousewheel" == e ? (o.deltaY = -.025 * i.wheelDelta, i.wheelDeltaX && (o.deltaX = -.025 * i.wheelDeltaX)) : o.deltaY = i.detail, n.call(t, o)
                }, i)
            }

            function y(t, e, n) {
                var i, o;
                if (0 == t.deltaMode ? (i = -Math.floor(w.opt.mousescrollstep / 54 * t.deltaX), o = -Math.floor(w.opt.mousescrollstep / 54 * t.deltaY)) : 1 == t.deltaMode && (i = -Math.floor(t.deltaX * w.opt.mousescrollstep), o = -Math.floor(t.deltaY * w.opt.mousescrollstep)), e && w.opt.oneaxismousemode && 0 == i && o && (i = o, o = 0, n && (0 > i ? w.getScrollLeft() >= w.page.maxw : 0 >= w.getScrollLeft()) && (o = i, i = 0)), i && (w.scrollmom && w.scrollmom.stop(), w.lastdeltax += i, w.debounced("mousewheelx", function() {
                    var t = w.lastdeltax;
                    w.lastdeltax = 0, w.rail.drag || w.doScrollLeftBy(t)
                }, 15)), o) {
                    if (w.opt.nativeparentscrolling && n && !w.ispage && !w.zoomactive)
                        if (0 > o) {
                            if (w.getScrollTop() >= w.page.maxh) return !0
                        } else if (0 >= w.getScrollTop()) return !0;
                    w.scrollmom && w.scrollmom.stop(), w.lastdeltay += o, w.debounced("mousewheely", function() {
                        var t = w.lastdeltay;
                        w.lastdeltay = 0, w.rail.drag || w.doScrollBy(t)
                    }, 15)
                }
                return t.stopImmediatePropagation(), t.preventDefault()
            }
            var w = this;
            if (this.version = "3.6.0", this.name = "nicescroll", this.me = c, this.opt = {
                doc: t("body"),
                win: !1
            }, t.extend(this.opt, h), this.opt.snapbackspeed = 80, s)
                for (var b in w.opt) "undefined" != typeof s[b] && (w.opt[b] = s[b]);
            this.iddoc = (this.doc = w.opt.doc) && this.doc[0] ? this.doc[0].id || "" : "", this.ispage = /^BODY|HTML/.test(w.opt.win ? w.opt.win[0].nodeName : this.doc[0].nodeName), this.haswrapper = !1 !== w.opt.win, this.win = w.opt.win || (this.ispage ? t(window) : this.doc), this.docscroll = this.ispage && !this.haswrapper ? t(window) : this.win, this.body = t("body"), this.iframe = this.isfixed = this.viewport = !1, this.isiframe = "IFRAME" == this.doc[0].nodeName && "IFRAME" == this.win[0].nodeName, this.istextarea = "TEXTAREA" == this.win[0].nodeName, this.forcescreen = !1, this.canshowonmouseevent = "scroll" != w.opt.autohidemode, this.page = this.view = this.onzoomout = this.onzoomin = this.onscrollcancel = this.onscrollend = this.onscrollstart = this.onclick = this.ongesturezoom = this.onkeypress = this.onmousewheel = this.onmousemove = this.onmouseup = this.onmousedown = !1, this.scroll = {
                x: 0,
                y: 0
            }, this.scrollratio = {
                x: 0,
                y: 0
            }, this.cursorheight = 20, this.scrollvaluemax = 0, this.isrtlmode = "auto" == this.opt.rtlmode ? "rtl" == (this.win[0] == window ? this.body : this.win).css("direction") : !0 === this.opt.rtlmode, this.observerbody = this.observerremover = this.observer = this.scrollmom = this.scrollrunning = !1;
            do this.id = "ascrail" + o++; while (document.getElementById(this.id));
            this.hasmousefocus = this.hasfocus = this.zoomactive = this.zoom = this.selectiondrag = this.cursorfreezed = this.cursor = this.rail = !1, this.visibility = !0, this.hidden = this.locked = this.railslocked = !1, this.cursoractive = !0, this.wheelprevented = !1, this.overflowx = w.opt.overflowx, this.overflowy = w.opt.overflowy, this.nativescrollingarea = !1, this.checkarea = 0, this.events = [], this.saved = {}, this.delaylist = {}, this.synclist = {}, this.lastdeltay = this.lastdeltax = 0, this.detected = p();
            var x = t.extend({}, this.detected);
            this.ishwscroll = (this.canhwscroll = x.hastransform && w.opt.hwacceleration) && w.haswrapper, this.hasreversehr = this.isrtlmode && !x.iswebkit, this.istouchcapable = !1, !x.cantouch || x.isios || x.isandroid || !x.iswebkit && !x.ismozilla || (this.istouchcapable = !0, x.cantouch = !1), w.opt.enablemouselockapi || (x.hasmousecapture = !1, x.haspointerlock = !1), this.debounced = function(t, e, n) {
                var i = w.delaylist[t];
                w.delaylist[t] = e, i || setTimeout(function() {
                    var e = w.delaylist[t];
                    w.delaylist[t] = !1, e.call(w)
                }, n)
            };
            var S = !1;
            this.synched = function(t, e) {
                return w.synclist[t] = e,
                    function() {
                        S || (a(function() {
                            S = !1;
                            for (var t in w.synclist) {
                                var e = w.synclist[t];
                                e && e.call(w), w.synclist[t] = !1
                            }
                        }), S = !0)
                    }(), t
            }, this.unsynched = function(t) {
                w.synclist[t] && (w.synclist[t] = !1)
            }, this.css = function(t, e) {
                for (var n in e) w.saved.css.push([t, n, t.css(n)]), t.css(n, e[n])
            }, this.scrollTop = function(t) {
                return "undefined" == typeof t ? w.getScrollTop() : w.setScrollTop(t)
            }, this.scrollLeft = function(t) {
                return "undefined" == typeof t ? w.getScrollLeft() : w.setScrollLeft(t)
            };
            var T = function(t, e, n, i, o, r, s) {
                this.st = t, this.ed = e, this.spd = n, this.p1 = i || 0, this.p2 = o || 1, this.p3 = r || 0, this.p4 = s || 1, this.ts = (new Date).getTime(), this.df = this.ed - this.st
            };
            if (T.prototype = {
                B2: function(t) {
                    return 3 * t * t * (1 - t)
                },
                B3: function(t) {
                    return 3 * t * (1 - t) * (1 - t)
                },
                B4: function(t) {
                    return (1 - t) * (1 - t) * (1 - t)
                },
                getNow: function() {
                    var t = 1 - ((new Date).getTime() - this.ts) / this.spd,
                        e = this.B2(t) + this.B3(t) + this.B4(t);
                    return 0 > t ? this.ed : this.st + Math.round(this.df * e)
                },
                update: function(t, e) {
                    return this.st = this.getNow(), this.ed = t, this.spd = e, this.ts = (new Date).getTime(), this.df = this.ed - this.st, this
                }
            }, this.ishwscroll) {
                this.doc.translate = {
                    x: 0,
                    y: 0,
                    tx: "0px",
                    ty: "0px"
                }, x.hastranslate3d && x.isios && this.doc.css("-webkit-backface-visibility", "hidden"), this.getScrollTop = function(t) {
                    if (!t) {
                        if (t = u()) return 16 == t.length ? -t[13] : -t[5];
                        if (w.timerscroll && w.timerscroll.bz) return w.timerscroll.bz.getNow()
                    }
                    return w.doc.translate.y
                }, this.getScrollLeft = function(t) {
                    if (!t) {
                        if (t = u()) return 16 == t.length ? -t[12] : -t[4];
                        if (w.timerscroll && w.timerscroll.bh) return w.timerscroll.bh.getNow()
                    }
                    return w.doc.translate.x
                }, this.notifyScrollEvent = function(t) {
                    var e = document.createEvent("UIEvents");
                    e.initUIEvent("scroll", !1, !0, window, 1), e.niceevent = !0, t.dispatchEvent(e)
                };
                var C = this.isrtlmode ? 1 : -1;
                x.hastranslate3d && w.opt.enabletranslate3d ? (this.setScrollTop = function(t, e) {
                    w.doc.translate.y = t, w.doc.translate.ty = -1 * t + "px", w.doc.css(x.trstyle, "translate3d(" + w.doc.translate.tx + "," + w.doc.translate.ty + ",0px)"), e || w.notifyScrollEvent(w.win[0])
                }, this.setScrollLeft = function(t, e) {
                    w.doc.translate.x = t, w.doc.translate.tx = t * C + "px", w.doc.css(x.trstyle, "translate3d(" + w.doc.translate.tx + "," + w.doc.translate.ty + ",0px)"), e || w.notifyScrollEvent(w.win[0])
                }) : (this.setScrollTop = function(t, e) {
                    w.doc.translate.y = t, w.doc.translate.ty = -1 * t + "px", w.doc.css(x.trstyle, "translate(" + w.doc.translate.tx + "," + w.doc.translate.ty + ")"), e || w.notifyScrollEvent(w.win[0])
                }, this.setScrollLeft = function(t, e) {
                    w.doc.translate.x = t, w.doc.translate.tx = t * C + "px", w.doc.css(x.trstyle, "translate(" + w.doc.translate.tx + "," + w.doc.translate.ty + ")"), e || w.notifyScrollEvent(w.win[0])
                })
            } else this.getScrollTop = function() {
                return w.docscroll.scrollTop()
            }, this.setScrollTop = function(t) {
                return w.docscroll.scrollTop(t)
            }, this.getScrollLeft = function() {
                return w.detected.ismozilla && w.isrtlmode ? Math.abs(w.docscroll.scrollLeft()) : w.docscroll.scrollLeft()
            }, this.setScrollLeft = function(t) {
                return w.docscroll.scrollLeft(w.detected.ismozilla && w.isrtlmode ? -t : t)
            };
            this.getTarget = function(t) {
                return t ? t.target ? t.target : t.srcElement ? t.srcElement : !1 : !1
            }, this.hasParent = function(t, e) {
                if (!t) return !1;
                for (var n = t.target || t.srcElement || t || !1; n && n.id != e;) n = n.parentNode || !1;
                return !1 !== n
            };
            var k = {
                thin: 1,
                medium: 3,
                thick: 5
            };
            this.getDocumentScrollOffset = function() {
                return {
                    top: window.pageYOffset || document.documentElement.scrollTop,
                    left: window.pageXOffset || document.documentElement.scrollLeft
                }
            }, this.getOffset = function() {
                if (w.isfixed) {
                    var t = w.win.offset(),
                        e = w.getDocumentScrollOffset();
                    return t.top -= e.top, t.left -= e.left, t
                }
                return t = w.win.offset(), w.viewport ? (e = w.viewport.offset(), {
                    top: t.top - e.top,
                    left: t.left - e.left
                }) : t
            }, this.updateScrollBar = function(t) {
                if (w.ishwscroll) w.rail.css({
                    height: w.win.innerHeight() - (w.opt.railpadding.top + w.opt.railpadding.bottom)
                }), w.railh && w.railh.css({
                    width: w.win.innerWidth() - (w.opt.railpadding.left + w.opt.railpadding.right)
                });
                else {
                    var e = w.getOffset(),
                        n = e.top,
                        i = e.left - (w.opt.railpadding.left + w.opt.railpadding.right),
                        n = n + g(w.win, "border-top-width", !0),
                        i = i + (w.rail.align ? w.win.outerWidth() - g(w.win, "border-right-width") - w.rail.width : g(w.win, "border-left-width")),
                        o = w.opt.railoffset;
                    o && (o.top && (n += o.top), w.rail.align && o.left && (i += o.left)), w.railslocked || w.rail.css({
                        top: n,
                        left: i,
                        height: (t ? t.h : w.win.innerHeight()) - (w.opt.railpadding.top + w.opt.railpadding.bottom)
                    }), w.zoom && w.zoom.css({
                        top: n + 1,
                        left: 1 == w.rail.align ? i - 20 : i + w.rail.width + 4
                    }), w.railh && !w.railslocked && (n = e.top, i = e.left, (o = w.opt.railhoffset) && (o.top && (n += o.top), o.left && (i += o.left)), t = w.railh.align ? n + g(w.win, "border-top-width", !0) + w.win.innerHeight() - w.railh.height : n + g(w.win, "border-top-width", !0), i += g(w.win, "border-left-width"), w.railh.css({
                        top: t - (w.opt.railpadding.top + w.opt.railpadding.bottom),
                        left: i,
                        width: w.railh.width
                    }))
                }
            }, this.doRailClick = function(t, e, n) {
                var i;
                w.railslocked || (w.cancelEvent(t), e ? (e = n ? w.doScrollLeft : w.doScrollTop, i = n ? (t.pageX - w.railh.offset().left - w.cursorwidth / 2) * w.scrollratio.x : (t.pageY - w.rail.offset().top - w.cursorheight / 2) * w.scrollratio.y, e(i)) : (e = n ? w.doScrollLeftBy : w.doScrollBy, i = n ? w.scroll.x : w.scroll.y, t = n ? t.pageX - w.railh.offset().left : t.pageY - w.rail.offset().top, n = n ? w.view.w : w.view.h, e(i >= t ? n : -n)))
            }, w.hasanimationframe = a, w.hascancelanimationframe = l, w.hasanimationframe ? w.hascancelanimationframe || (l = function() {
                w.cancelAnimationFrame = !0
            }) : (a = function(t) {
                return setTimeout(t, 15 - Math.floor(+new Date / 1e3) % 16)
            }, l = clearInterval), this.init = function() {
                if (w.saved.css = [], x.isie7mobile || x.isoperamini) return !0;
                if (x.hasmstouch && w.css(w.ispage ? t("html") : w.win, {
                    "-ms-touch-action": "none"
                }), w.zindex = "auto", w.zindex = w.ispage || "auto" != w.opt.zindex ? w.opt.zindex : f() || "auto", !w.ispage && "auto" != w.zindex && w.zindex > r && (r = w.zindex), w.isie && 0 == w.zindex && "auto" == w.opt.zindex && (w.zindex = "auto"), !w.ispage || !x.cantouch && !x.isieold && !x.isie9mobile) {
                    var o = w.docscroll;
                    w.ispage && (o = w.haswrapper ? w.win : w.doc), x.isie9mobile || w.css(o, {
                        "overflow-y": "hidden"
                    }), w.ispage && x.isie7 && ("BODY" == w.doc[0].nodeName ? w.css(t("html"), {
                        "overflow-y": "hidden"
                    }) : "HTML" == w.doc[0].nodeName && w.css(t("body"), {
                        "overflow-y": "hidden"
                    })), !x.isios || w.ispage || w.haswrapper || w.css(t("body"), {
                        "-webkit-overflow-scrolling": "touch"
                    });
                    var s = t(document.createElement("div"));
                    s.css({
                        position: "relative",
                        top: 0,
                        "float": "right",
                        width: w.opt.cursorwidth,
                        height: "0px",
                        "background-color": w.opt.cursorcolor,
                        border: w.opt.cursorborder,
                        "background-clip": "padding-box",
                        "-webkit-border-radius": w.opt.cursorborderradius,
                        "-moz-border-radius": w.opt.cursorborderradius,
                        "border-radius": w.opt.cursorborderradius
                    }), s.hborder = parseFloat(s.outerHeight() - s.innerHeight()), s.addClass("nicescroll-cursors"), w.cursor = s;
                    var a = t(document.createElement("div"));
                    a.attr("id", w.id), a.addClass("nicescroll-rails nicescroll-rails-vr");
                    var l, c, u, h = ["left", "right", "top", "bottom"];
                    for (u in h) c = h[u], (l = w.opt.railpadding[c]) ? a.css("padding-" + c, l + "px") : w.opt.railpadding[c] = 0;
                    a.append(s), a.width = Math.max(parseFloat(w.opt.cursorwidth), s.outerWidth()), a.css({
                        width: a.width + "px",
                        zIndex: w.zindex,
                        background: w.opt.background,
                        cursor: "default"
                    }), a.visibility = !0, a.scrollable = !0, a.align = "left" == w.opt.railalign ? 0 : 1, w.rail = a, s = w.rail.drag = !1, !w.opt.boxzoom || w.ispage || x.isieold || (s = document.createElement("div"), w.bind(s, "click", w.doZoom), w.bind(s, "mouseenter", function() {
                        w.zoom.css("opacity", w.opt.cursoropacitymax)
                    }), w.bind(s, "mouseleave", function() {
                        w.zoom.css("opacity", w.opt.cursoropacitymin)
                    }), w.zoom = t(s), w.zoom.css({
                        cursor: "pointer",
                        "z-index": w.zindex,
                        backgroundImage: "url(" + w.opt.scriptpath + "zoomico.png)",
                        height: 18,
                        width: 18,
                        backgroundPosition: "0px 0px"
                    }), w.opt.dblclickzoom && w.bind(w.win, "dblclick", w.doZoom), x.cantouch && w.opt.gesturezoom && (w.ongesturezoom = function(t) {
                        return 1.5 < t.scale && w.doZoomIn(t), .8 > t.scale && w.doZoomOut(t), w.cancelEvent(t)
                    }, w.bind(w.win, "gestureend", w.ongesturezoom))), w.railh = !1;
                    var p;
                    if (w.opt.horizrailenabled && (w.css(o, {
                        "overflow-x": "hidden"
                    }), s = t(document.createElement("div")), s.css({
                        position: "absolute",
                        top: 0,
                        height: w.opt.cursorwidth,
                        width: "0px",
                        "background-color": w.opt.cursorcolor,
                        border: w.opt.cursorborder,
                        "background-clip": "padding-box",
                        "-webkit-border-radius": w.opt.cursorborderradius,
                        "-moz-border-radius": w.opt.cursorborderradius,
                        "border-radius": w.opt.cursorborderradius
                    }), x.isieold && s.css({
                        overflow: "hidden"
                    }), s.wborder = parseFloat(s.outerWidth() - s.innerWidth()), s.addClass("nicescroll-cursors"), w.cursorh = s, p = t(document.createElement("div")), p.attr("id", w.id + "-hr"), p.addClass("nicescroll-rails nicescroll-rails-hr"), p.height = Math.max(parseFloat(w.opt.cursorwidth), s.outerHeight()), p.css({
                        height: p.height + "px",
                        zIndex: w.zindex,
                        background: w.opt.background
                    }), p.append(s), p.visibility = !0, p.scrollable = !0, p.align = "top" == w.opt.railvalign ? 0 : 1, w.railh = p, w.railh.drag = !1), w.ispage ? (a.css({
                        position: "fixed",
                        top: "0px",
                        height: "100%"
                    }), a.css(a.align ? {
                        right: "0px"
                    } : {
                        left: "0px"
                    }), w.body.append(a), w.railh && (p.css({
                        position: "fixed",
                        left: "0px",
                        width: "100%"
                    }), p.css(p.align ? {
                        bottom: "0px"
                    } : {
                        top: "0px"
                    }), w.body.append(p))) : (w.ishwscroll ? ("static" == w.win.css("position") && w.css(w.win, {
                        position: "relative"
                    }), o = "HTML" == w.win[0].nodeName ? w.body : w.win, t(o).scrollTop(0).scrollLeft(0), w.zoom && (w.zoom.css({
                        position: "absolute",
                        top: 1,
                        right: 0,
                        "margin-right": a.width + 4
                    }), o.append(w.zoom)), a.css({
                        position: "absolute",
                        top: 0
                    }), a.css(a.align ? {
                        right: 0
                    } : {
                        left: 0
                    }), o.append(a), p && (p.css({
                        position: "absolute",
                        left: 0,
                        bottom: 0
                    }), p.css(p.align ? {
                        bottom: 0
                    } : {
                        top: 0
                    }), o.append(p))) : (w.isfixed = "fixed" == w.win.css("position"), o = w.isfixed ? "fixed" : "absolute", w.isfixed || (w.viewport = w.getViewport(w.win[0])), w.viewport && (w.body = w.viewport, 0 == /fixed|absolute/.test(w.viewport.css("position")) && w.css(w.viewport, {
                        position: "relative"
                    })), a.css({
                        position: o
                    }), w.zoom && w.zoom.css({
                        position: o
                    }), w.updateScrollBar(), w.body.append(a), w.zoom && w.body.append(w.zoom), w.railh && (p.css({
                        position: o
                    }), w.body.append(p))), x.isios && w.css(w.win, {
                        "-webkit-tap-highlight-color": "rgba(0,0,0,0)",
                        "-webkit-touch-callout": "none"
                    }), x.isie && w.opt.disableoutline && w.win.attr("hideFocus", "true"), x.iswebkit && w.opt.disableoutline && w.win.css({
                        outline: "none"
                    })), !1 === w.opt.autohidemode ? (w.autohidedom = !1, w.rail.css({
                        opacity: w.opt.cursoropacitymax
                    }), w.railh && w.railh.css({
                        opacity: w.opt.cursoropacitymax
                    })) : !0 === w.opt.autohidemode || "leave" === w.opt.autohidemode ? (w.autohidedom = t().add(w.rail), x.isie8 && (w.autohidedom = w.autohidedom.add(w.cursor)), w.railh && (w.autohidedom = w.autohidedom.add(w.railh)), w.railh && x.isie8 && (w.autohidedom = w.autohidedom.add(w.cursorh))) : "scroll" == w.opt.autohidemode ? (w.autohidedom = t().add(w.rail), w.railh && (w.autohidedom = w.autohidedom.add(w.railh))) : "cursor" == w.opt.autohidemode ? (w.autohidedom = t().add(w.cursor), w.railh && (w.autohidedom = w.autohidedom.add(w.cursorh))) : "hidden" == w.opt.autohidemode && (w.autohidedom = !1, w.hide(), w.railslocked = !1), x.isie9mobile) w.scrollmom = new m(w), w.onmangotouch = function() {
                        var t = w.getScrollTop(),
                            e = w.getScrollLeft();
                        if (t == w.scrollmom.lastscrolly && e == w.scrollmom.lastscrollx) return !0;
                        var n = t - w.mangotouch.sy,
                            i = e - w.mangotouch.sx;
                        if (0 != Math.round(Math.sqrt(Math.pow(i, 2) + Math.pow(n, 2)))) {
                            var o = 0 > n ? -1 : 1,
                                r = 0 > i ? -1 : 1,
                                s = +new Date;
                            w.mangotouch.lazy && clearTimeout(w.mangotouch.lazy), 80 < s - w.mangotouch.tm || w.mangotouch.dry != o || w.mangotouch.drx != r ? (w.scrollmom.stop(), w.scrollmom.reset(e, t), w.mangotouch.sy = t, w.mangotouch.ly = t, w.mangotouch.sx = e, w.mangotouch.lx = e, w.mangotouch.dry = o, w.mangotouch.drx = r, w.mangotouch.tm = s) : (w.scrollmom.stop(), w.scrollmom.update(w.mangotouch.sx - i, w.mangotouch.sy - n), w.mangotouch.tm = s, n = Math.max(Math.abs(w.mangotouch.ly - t), Math.abs(w.mangotouch.lx - e)), w.mangotouch.ly = t, w.mangotouch.lx = e, n > 2 && (w.mangotouch.lazy = setTimeout(function() {
                                w.mangotouch.lazy = !1, w.mangotouch.dry = 0, w.mangotouch.drx = 0, w.mangotouch.tm = 0, w.scrollmom.doMomentum(30)
                            }, 100)))
                        }
                    }, a = w.getScrollTop(), p = w.getScrollLeft(), w.mangotouch = {
                        sy: a,
                        ly: a,
                        dry: 0,
                        sx: p,
                        lx: p,
                        drx: 0,
                        lazy: !1,
                        tm: 0
                    }, w.bind(w.docscroll, "scroll", w.onmangotouch);
                    else {
                        if (x.cantouch || w.istouchcapable || w.opt.touchbehavior || x.hasmstouch) {
                            w.scrollmom = new m(w), w.ontouchstart = function(e) {
                                if (e.pointerType && 2 != e.pointerType && "touch" != e.pointerType) return !1;
                                if (w.hasmoving = !1, !w.railslocked) {
                                    var n;
                                    if (x.hasmstouch)
                                        for (n = e.target ? e.target : !1; n;) {
                                            var i = t(n).getNiceScroll();
                                            if (0 < i.length && i[0].me == w.me) break;
                                            if (0 < i.length) return !1;
                                            if ("DIV" == n.nodeName && n.id == w.id) break;
                                            n = n.parentNode ? n.parentNode : !1
                                        }
                                    if (w.cancelScroll(), (n = w.getTarget(e)) && /INPUT/i.test(n.nodeName) && /range/i.test(n.type)) return w.stopPropagation(e);
                                    if (!("clientX" in e) && "changedTouches" in e && (e.clientX = e.changedTouches[0].clientX, e.clientY = e.changedTouches[0].clientY), w.forcescreen && (i = e, e = {
                                        original: e.original ? e.original : e
                                    }, e.clientX = i.screenX, e.clientY = i.screenY), w.rail.drag = {
                                        x: e.clientX,
                                        y: e.clientY,
                                        sx: w.scroll.x,
                                        sy: w.scroll.y,
                                        st: w.getScrollTop(),
                                        sl: w.getScrollLeft(),
                                        pt: 2,
                                        dl: !1
                                    }, w.ispage || !w.opt.directionlockdeadzone) w.rail.drag.dl = "f";
                                    else {
                                        var i = t(window).width(),
                                            o = t(window).height(),
                                            r = Math.max(document.body.scrollWidth, document.documentElement.scrollWidth),
                                            s = Math.max(document.body.scrollHeight, document.documentElement.scrollHeight),
                                            o = Math.max(0, s - o),
                                            i = Math.max(0, r - i);
                                        w.rail.drag.ck = !w.rail.scrollable && w.railh.scrollable ? o > 0 ? "v" : !1 : w.rail.scrollable && !w.railh.scrollable && i > 0 ? "h" : !1, w.rail.drag.ck || (w.rail.drag.dl = "f")
                                    }
                                    if (w.opt.touchbehavior && w.isiframe && x.isie && (i = w.win.position(), w.rail.drag.x += i.left, w.rail.drag.y += i.top), w.hasmoving = !1, w.lastmouseup = !1, w.scrollmom.reset(e.clientX, e.clientY), !x.cantouch && !this.istouchcapable && !e.pointerType) {
                                        if (!n || !/INPUT|SELECT|TEXTAREA/i.test(n.nodeName)) return !w.ispage && x.hasmousecapture && n.setCapture(), w.opt.touchbehavior ? (n.onclick && !n._onclick && (n._onclick = n.onclick, n.onclick = function(t) {
                                            return w.hasmoving ? !1 : void n._onclick.call(this, t)
                                        }), w.cancelEvent(e)) : w.stopPropagation(e);
                                        /SUBMIT|CANCEL|BUTTON/i.test(t(n).attr("type")) && (pc = {
                                            tg: n,
                                            click: !1
                                        }, w.preventclick = pc)
                                    }
                                }
                            }, w.ontouchend = function(t) {
                                if (!w.rail.drag) return !0;
                                if (2 == w.rail.drag.pt) {
                                    if (t.pointerType && 2 != t.pointerType && "touch" != t.pointerType) return !1;
                                    if (w.scrollmom.doMomentum(), w.rail.drag = !1, w.hasmoving && (w.lastmouseup = !0, w.hideCursor(), x.hasmousecapture && document.releaseCapture(), !x.cantouch)) return w.cancelEvent(t)
                                } else if (1 == w.rail.drag.pt) return w.onmouseup(t)
                            };
                            var g = w.opt.touchbehavior && w.isiframe && !x.hasmousecapture;
                            w.ontouchmove = function(e, n) {
                                if (!w.rail.drag || e.targetTouches && w.opt.preventmultitouchscrolling && 1 < e.targetTouches.length || e.pointerType && 2 != e.pointerType && "touch" != e.pointerType) return !1;
                                if (2 == w.rail.drag.pt) {
                                    if (x.cantouch && x.isios && "undefined" == typeof e.original) return !0;
                                    if (w.hasmoving = !0, w.preventclick && !w.preventclick.click && (w.preventclick.click = w.preventclick.tg.onclick || !1, w.preventclick.tg.onclick = w.onpreventclick), e = t.extend({
                                        original: e
                                    }, e), "changedTouches" in e && (e.clientX = e.changedTouches[0].clientX, e.clientY = e.changedTouches[0].clientY), w.forcescreen) {
                                        var i = e;
                                        e = {
                                            original: e.original ? e.original : e
                                        }, e.clientX = i.screenX, e.clientY = i.screenY
                                    }
                                    var o, i = o = 0;
                                    g && !n && (o = w.win.position(), i = -o.left, o = -o.top);
                                    var r = e.clientY + o;
                                    o = r - w.rail.drag.y;
                                    var s = e.clientX + i,
                                        a = s - w.rail.drag.x,
                                        l = w.rail.drag.st - o;
                                    w.ishwscroll && w.opt.bouncescroll ? 0 > l ? l = Math.round(l / 2) : l > w.page.maxh && (l = w.page.maxh + Math.round((l - w.page.maxh) / 2)) : (0 > l && (r = l = 0), l > w.page.maxh && (l = w.page.maxh, r = 0));
                                    var c;
                                    if (w.railh && w.railh.scrollable && (c = w.isrtlmode ? a - w.rail.drag.sl : w.rail.drag.sl - a, w.ishwscroll && w.opt.bouncescroll ? 0 > c ? c = Math.round(c / 2) : c > w.page.maxw && (c = w.page.maxw + Math.round((c - w.page.maxw) / 2)) : (0 > c && (s = c = 0), c > w.page.maxw && (c = w.page.maxw, s = 0))), i = !1, w.rail.drag.dl) i = !0, "v" == w.rail.drag.dl ? c = w.rail.drag.sl : "h" == w.rail.drag.dl && (l = w.rail.drag.st);
                                    else {
                                        o = Math.abs(o);
                                        var a = Math.abs(a),
                                            u = w.opt.directionlockdeadzone;
                                        if ("v" == w.rail.drag.ck) {
                                            if (o > u && .3 * o >= a) return w.rail.drag = !1, !0;
                                            a > u && (w.rail.drag.dl = "f", t("body").scrollTop(t("body").scrollTop()))
                                        } else if ("h" == w.rail.drag.ck) {
                                            if (a > u && .3 * a >= o) return w.rail.drag = !1, !0;
                                            o > u && (w.rail.drag.dl = "f", t("body").scrollLeft(t("body").scrollLeft()))
                                        }
                                    }
                                    if (w.synched("touchmove", function() {
                                        w.rail.drag && 2 == w.rail.drag.pt && (w.prepareTransition && w.prepareTransition(0), w.rail.scrollable && w.setScrollTop(l), w.scrollmom.update(s, r), w.railh && w.railh.scrollable ? (w.setScrollLeft(c), w.showCursor(l, c)) : w.showCursor(l), x.isie10 && document.selection.clear())
                                    }), x.ischrome && w.istouchcapable && (i = !1), i) return w.cancelEvent(e)
                                } else if (1 == w.rail.drag.pt) return w.onmousemove(e)
                            }
                        }
                        if (w.onmousedown = function(t, e) {
                            if (!w.rail.drag || 1 == w.rail.drag.pt) {
                                if (w.railslocked) return w.cancelEvent(t);
                                w.cancelScroll(), w.rail.drag = {
                                    x: t.clientX,
                                    y: t.clientY,
                                    sx: w.scroll.x,
                                    sy: w.scroll.y,
                                    pt: 1,
                                    hr: !!e
                                };
                                var n = w.getTarget(t);
                                return !w.ispage && x.hasmousecapture && n.setCapture(), w.isiframe && !x.hasmousecapture && (w.saved.csspointerevents = w.doc.css("pointer-events"), w.css(w.doc, {
                                    "pointer-events": "none"
                                })), w.hasmoving = !1, w.cancelEvent(t)
                            }
                        }, w.onmouseup = function(t) {
                            return w.rail.drag ? 1 != w.rail.drag.pt ? !0 : (x.hasmousecapture && document.releaseCapture(), w.isiframe && !x.hasmousecapture && w.doc.css("pointer-events", w.saved.csspointerevents), w.rail.drag = !1, w.hasmoving && w.triggerScrollEnd(), w.cancelEvent(t)) : void 0
                        }, w.onmousemove = function(t) {
                            if (w.rail.drag && 1 == w.rail.drag.pt) {
                                if (x.ischrome && 0 == t.which) return w.onmouseup(t);
                                if (w.cursorfreezed = !0, w.hasmoving = !0, w.rail.drag.hr) {
                                    w.scroll.x = w.rail.drag.sx + (t.clientX - w.rail.drag.x), 0 > w.scroll.x && (w.scroll.x = 0);
                                    var e = w.scrollvaluemaxw;
                                    w.scroll.x > e && (w.scroll.x = e)
                                } else w.scroll.y = w.rail.drag.sy + (t.clientY - w.rail.drag.y), 0 > w.scroll.y && (w.scroll.y = 0), e = w.scrollvaluemax, w.scroll.y > e && (w.scroll.y = e);
                                return w.synched("mousemove", function() {
                                    w.rail.drag && 1 == w.rail.drag.pt && (w.showCursor(), w.rail.drag.hr ? w.hasreversehr ? w.doScrollLeft(w.scrollvaluemaxw - Math.round(w.scroll.x * w.scrollratio.x), w.opt.cursordragspeed) : w.doScrollLeft(Math.round(w.scroll.x * w.scrollratio.x), w.opt.cursordragspeed) : w.doScrollTop(Math.round(w.scroll.y * w.scrollratio.y), w.opt.cursordragspeed))
                                }), w.cancelEvent(t)
                            }
                        }, x.cantouch || w.opt.touchbehavior) w.onpreventclick = function(t) {
                            return w.preventclick ? (w.preventclick.tg.onclick = w.preventclick.click, w.preventclick = !1, w.cancelEvent(t)) : void 0
                        }, w.bind(w.win, "mousedown", w.ontouchstart), w.onclick = x.isios ? !1 : function(t) {
                            return w.lastmouseup ? (w.lastmouseup = !1, w.cancelEvent(t)) : !0
                        }, w.opt.grabcursorenabled && x.cursorgrabvalue && (w.css(w.ispage ? w.doc : w.win, {
                            cursor: x.cursorgrabvalue
                        }), w.css(w.rail, {
                            cursor: x.cursorgrabvalue
                        }));
                        else {
                            var v = function(t) {
                                if (w.selectiondrag) {
                                    if (t) {
                                        var e = w.win.outerHeight();
                                        t = t.pageY - w.selectiondrag.top, t > 0 && e > t && (t = 0), t >= e && (t -= e), w.selectiondrag.df = t
                                    }
                                    0 != w.selectiondrag.df && (w.doScrollBy(2 * -Math.floor(w.selectiondrag.df / 6)), w.debounced("doselectionscroll", function() {
                                        v()
                                    }, 50))
                                }
                            };
                            w.hasTextSelected = "getSelection" in document ? function() {
                                return 0 < document.getSelection().rangeCount
                            } : "selection" in document ? function() {
                                return "None" != document.selection.type
                            } : function() {
                                return !1
                            }, w.onselectionstart = function(t) {
                                w.ispage || (w.selectiondrag = w.win.offset())
                            }, w.onselectionend = function(t) {
                                w.selectiondrag = !1
                            }, w.onselectiondrag = function(t) {
                                w.selectiondrag && w.hasTextSelected() && w.debounced("selectionscroll", function() {
                                    v(t)
                                }, 250)
                            }
                        }
                        x.hasw3ctouch ? (w.css(w.rail, {
                            "touch-action": "none"
                        }), w.css(w.cursor, {
                            "touch-action": "none"
                        }), w.bind(w.win, "pointerdown", w.ontouchstart), w.bind(document, "pointerup", w.ontouchend), w.bind(document, "pointermove", w.ontouchmove)) : x.hasmstouch ? (w.css(w.rail, {
                            "-ms-touch-action": "none"
                        }), w.css(w.cursor, {
                            "-ms-touch-action": "none"
                        }), w.bind(w.win, "MSPointerDown", w.ontouchstart), w.bind(document, "MSPointerUp", w.ontouchend), w.bind(document, "MSPointerMove", w.ontouchmove), w.bind(w.cursor, "MSGestureHold", function(t) {
                            t.preventDefault()
                        }), w.bind(w.cursor, "contextmenu", function(t) {
                            t.preventDefault()
                        })) : this.istouchcapable && (w.bind(w.win, "touchstart", w.ontouchstart), w.bind(document, "touchend", w.ontouchend), w.bind(document, "touchcancel", w.ontouchend), w.bind(document, "touchmove", w.ontouchmove)), (w.opt.cursordragontouch || !x.cantouch && !w.opt.touchbehavior) && (w.rail.css({
                            cursor: "default"
                        }), w.railh && w.railh.css({
                            cursor: "default"
                        }), w.jqbind(w.rail, "mouseenter", function() {
                            return w.ispage || w.win.is(":visible") ? (w.canshowonmouseevent && w.showCursor(), void(w.rail.active = !0)) : !1
                        }), w.jqbind(w.rail, "mouseleave", function() {
                            w.rail.active = !1, w.rail.drag || w.hideCursor()
                        }), w.opt.sensitiverail && (w.bind(w.rail, "click", function(t) {
                            w.doRailClick(t, !1, !1)
                        }), w.bind(w.rail, "dblclick", function(t) {
                            w.doRailClick(t, !0, !1)
                        }), w.bind(w.cursor, "click", function(t) {
                            w.cancelEvent(t)
                        }), w.bind(w.cursor, "dblclick", function(t) {
                            w.cancelEvent(t)
                        })), w.railh && (w.jqbind(w.railh, "mouseenter", function() {
                            return w.ispage || w.win.is(":visible") ? (w.canshowonmouseevent && w.showCursor(), void(w.rail.active = !0)) : !1
                        }), w.jqbind(w.railh, "mouseleave", function() {
                            w.rail.active = !1, w.rail.drag || w.hideCursor()
                        }), w.opt.sensitiverail && (w.bind(w.railh, "click", function(t) {
                            w.doRailClick(t, !1, !0)
                        }), w.bind(w.railh, "dblclick", function(t) {
                            w.doRailClick(t, !0, !0)
                        }), w.bind(w.cursorh, "click", function(t) {
                            w.cancelEvent(t)
                        }), w.bind(w.cursorh, "dblclick", function(t) {
                            w.cancelEvent(t)
                        })))), x.cantouch || w.opt.touchbehavior ? (w.bind(x.hasmousecapture ? w.win : document, "mouseup", w.ontouchend), w.bind(document, "mousemove", w.ontouchmove), w.onclick && w.bind(document, "click", w.onclick), w.opt.cursordragontouch && (w.bind(w.cursor, "mousedown", w.onmousedown), w.bind(w.cursor, "mouseup", w.onmouseup), w.cursorh && w.bind(w.cursorh, "mousedown", function(t) {
                            w.onmousedown(t, !0)
                        }), w.cursorh && w.bind(w.cursorh, "mouseup", w.onmouseup))) : (w.bind(x.hasmousecapture ? w.win : document, "mouseup", w.onmouseup), w.bind(document, "mousemove", w.onmousemove), w.onclick && w.bind(document, "click", w.onclick), w.bind(w.cursor, "mousedown", w.onmousedown), w.bind(w.cursor, "mouseup", w.onmouseup), w.railh && (w.bind(w.cursorh, "mousedown", function(t) {
                            w.onmousedown(t, !0)
                        }), w.bind(w.cursorh, "mouseup", w.onmouseup)), !w.ispage && w.opt.enablescrollonselection && (w.bind(w.win[0], "mousedown", w.onselectionstart), w.bind(document, "mouseup", w.onselectionend), w.bind(w.cursor, "mouseup", w.onselectionend), w.cursorh && w.bind(w.cursorh, "mouseup", w.onselectionend), w.bind(document, "mousemove", w.onselectiondrag)), w.zoom && (w.jqbind(w.zoom, "mouseenter", function() {
                            w.canshowonmouseevent && w.showCursor(), w.rail.active = !0
                        }), w.jqbind(w.zoom, "mouseleave", function() {
                            w.rail.active = !1, w.rail.drag || w.hideCursor()
                        }))), w.opt.enablemousewheel && (w.isiframe || w.bind(x.isie && w.ispage ? document : w.win, "mousewheel", w.onmousewheel), w.bind(w.rail, "mousewheel", w.onmousewheel), w.railh && w.bind(w.railh, "mousewheel", w.onmousewheelhr)), w.ispage || x.cantouch || /HTML|^BODY/.test(w.win[0].nodeName) || (w.win.attr("tabindex") || w.win.attr({
                            tabindex: i++
                        }), w.jqbind(w.win, "focus", function(t) {
                            e = w.getTarget(t).id || !0, w.hasfocus = !0, w.canshowonmouseevent && w.noticeCursor()
                        }), w.jqbind(w.win, "blur", function(t) {
                            e = !1, w.hasfocus = !1
                        }), w.jqbind(w.win, "mouseenter", function(t) {
                            n = w.getTarget(t).id || !0, w.hasmousefocus = !0, w.canshowonmouseevent && w.noticeCursor()
                        }), w.jqbind(w.win, "mouseleave", function() {
                            n = !1, w.hasmousefocus = !1, w.rail.drag || w.hideCursor()
                        }))
                    }
                    if (w.onkeypress = function(i) {
                        if (w.railslocked && 0 == w.page.maxh) return !0;
                        i = i ? i : window.e;
                        var o = w.getTarget(i);
                        if (o && /INPUT|TEXTAREA|SELECT|OPTION/.test(o.nodeName) && (!o.getAttribute("type") && !o.type || !/submit|button|cancel/i.tp) || t(o).attr("contenteditable")) return !0;
                        if (w.hasfocus || w.hasmousefocus && !e || w.ispage && !e && !n) {
                            if (o = i.keyCode, w.railslocked && 27 != o) return w.cancelEvent(i);
                            var r = i.ctrlKey || !1,
                                s = i.shiftKey || !1,
                                a = !1;
                            switch (o) {
                                case 38:
                                case 63233:
                                    w.doScrollBy(72), a = !0;
                                    break;
                                case 40:
                                case 63235:
                                    w.doScrollBy(-72), a = !0;
                                    break;
                                case 37:
                                case 63232:
                                    w.railh && (r ? w.doScrollLeft(0) : w.doScrollLeftBy(72), a = !0);
                                    break;
                                case 39:
                                case 63234:
                                    w.railh && (r ? w.doScrollLeft(w.page.maxw) : w.doScrollLeftBy(-72), a = !0);
                                    break;
                                case 33:
                                case 63276:
                                    w.doScrollBy(w.view.h), a = !0;
                                    break;
                                case 34:
                                case 63277:
                                    w.doScrollBy(-w.view.h), a = !0;
                                    break;
                                case 36:
                                case 63273:
                                    w.railh && r ? w.doScrollPos(0, 0) : w.doScrollTo(0), a = !0;
                                    break;
                                case 35:
                                case 63275:
                                    w.railh && r ? w.doScrollPos(w.page.maxw, w.page.maxh) : w.doScrollTo(w.page.maxh), a = !0;
                                    break;
                                case 32:
                                    w.opt.spacebarenabled && (w.doScrollBy(s ? w.view.h : -w.view.h), a = !0);
                                    break;
                                case 27:
                                    w.zoomactive && (w.doZoom(), a = !0)
                            }
                            if (a) return w.cancelEvent(i)
                        }
                    }, w.opt.enablekeyboard && w.bind(document, x.isopera && !x.isopera12 ? "keypress" : "keydown", w.onkeypress), w.bind(document, "keydown", function(t) {
                        t.ctrlKey && (w.wheelprevented = !0)
                    }), w.bind(document, "keyup", function(t) {
                        t.ctrlKey || (w.wheelprevented = !1)
                    }), w.bind(window, "blur", function(t) {
                        w.wheelprevented = !1
                    }), w.bind(window, "resize", w.lazyResize), w.bind(window, "orientationchange", w.lazyResize), w.bind(window, "load", w.lazyResize), x.ischrome && !w.ispage && !w.haswrapper) {
                        var y = w.win.attr("style"),
                            a = parseFloat(w.win.css("width")) + 1;
                        w.win.css("width", a), w.synched("chromefix", function() {
                            w.win.attr("style", y)
                        })
                    }
                    w.onAttributeChange = function(t) {
                        w.lazyResize(w.isieold ? 250 : 30)
                    }, !1 !== d && (w.observerbody = new d(function(e) {
                        return e.forEach(function(e) {
                            return "attributes" == e.type ? t("body").hasClass("modal-open") ? w.hide() : w.show() : void 0
                        }), document.body.scrollHeight != w.page.maxh ? w.lazyResize(30) : void 0
                    }), w.observerbody.observe(document.body, {
                        childList: !0,
                        subtree: !0,
                        characterData: !1,
                        attributes: !0,
                        attributeFilter: ["class"]
                    })), w.ispage || w.haswrapper || (!1 !== d ? (w.observer = new d(function(t) {
                        t.forEach(w.onAttributeChange)
                    }), w.observer.observe(w.win[0], {
                        childList: !0,
                        characterData: !1,
                        attributes: !0,
                        subtree: !1
                    }), w.observerremover = new d(function(t) {
                        t.forEach(function(t) {
                            if (0 < t.removedNodes.length)
                                for (var e in t.removedNodes)
                                    if (w && t.removedNodes[e] == w.win[0]) return w.remove()
                        })
                    }), w.observerremover.observe(w.win[0].parentNode, {
                        childList: !0,
                        characterData: !1,
                        attributes: !1,
                        subtree: !1
                    })) : (w.bind(w.win, x.isie && !x.isie9 ? "propertychange" : "DOMAttrModified", w.onAttributeChange), x.isie9 && w.win[0].attachEvent("onpropertychange", w.onAttributeChange), w.bind(w.win, "DOMNodeRemoved", function(t) {
                        t.target == w.win[0] && w.remove()
                    }))), !w.ispage && w.opt.boxzoom && w.bind(window, "resize", w.resizeZoom), w.istextarea && w.bind(w.win, "mouseup", w.lazyResize), w.lazyResize(30)
                }
                if ("IFRAME" == this.doc[0].nodeName) {
                    var b = function() {
                        w.iframexd = !1;
                        var e;
                        try {
                            e = "contentDocument" in this ? this.contentDocument : this.contentWindow.document
                        } catch (n) {
                            w.iframexd = !0, e = !1
                        }
                        if (w.iframexd) return "console" in window && console.log("NiceScroll error: policy restriced iframe"), !0;
                        if (w.forcescreen = !0, w.isiframe && (w.iframe = {
                            doc: t(e),
                            html: w.doc.contents().find("html")[0],
                            body: w.doc.contents().find("body")[0]
                        }, w.getContentSize = function() {
                            return {
                                w: Math.max(w.iframe.html.scrollWidth, w.iframe.body.scrollWidth),
                                h: Math.max(w.iframe.html.scrollHeight, w.iframe.body.scrollHeight)
                            }
                        }, w.docscroll = t(w.iframe.body)), !x.isios && w.opt.iframeautoresize && !w.isiframe) {
                            w.win.scrollTop(0), w.doc.height("");
                            var i = Math.max(e.getElementsByTagName("html")[0].scrollHeight, e.body.scrollHeight);
                            w.doc.height(i)
                        }
                        w.lazyResize(30), x.isie7 && w.css(t(w.iframe.html), {
                            "overflow-y": "hidden"
                        }), w.css(t(w.iframe.body), {
                            "overflow-y": "hidden"
                        }), x.isios && w.haswrapper && w.css(t(e.body), {
                            "-webkit-transform": "translate3d(0,0,0)"
                        }), "contentWindow" in this ? w.bind(this.contentWindow, "scroll", w.onscroll) : w.bind(e, "scroll", w.onscroll), w.opt.enablemousewheel && w.bind(e, "mousewheel", w.onmousewheel), w.opt.enablekeyboard && w.bind(e, x.isopera ? "keypress" : "keydown", w.onkeypress), (x.cantouch || w.opt.touchbehavior) && (w.bind(e, "mousedown", w.ontouchstart), w.bind(e, "mousemove", function(t) {
                            return w.ontouchmove(t, !0)
                        }), w.opt.grabcursorenabled && x.cursorgrabvalue && w.css(t(e.body), {
                            cursor: x.cursorgrabvalue
                        })), w.bind(e, "mouseup", w.ontouchend), w.zoom && (w.opt.dblclickzoom && w.bind(e, "dblclick", w.doZoom), w.ongesturezoom && w.bind(e, "gestureend", w.ongesturezoom))
                    };
                    this.doc[0].readyState && "complete" == this.doc[0].readyState && setTimeout(function() {
                        b.call(w.doc[0], !1)
                    }, 500), w.bind(this.doc, "load", b)
                }
            }, this.showCursor = function(t, e) {
                if (w.cursortimeout && (clearTimeout(w.cursortimeout), w.cursortimeout = 0), w.rail) {
                    if (w.autohidedom && (w.autohidedom.stop().css({
                        opacity: w.opt.cursoropacitymax
                    }), w.cursoractive = !0), w.rail.drag && 1 == w.rail.drag.pt || ("undefined" != typeof t && !1 !== t && (w.scroll.y = Math.round(1 * t / w.scrollratio.y)), "undefined" != typeof e && (w.scroll.x = Math.round(1 * e / w.scrollratio.x))), w.cursor.css({
                        height: w.cursorheight,
                        top: w.scroll.y
                    }), w.cursorh) {
                        var n = w.hasreversehr ? w.scrollvaluemaxw - w.scroll.x : w.scroll.x;
                        w.cursorh.css(!w.rail.align && w.rail.visibility ? {
                            width: w.cursorwidth,
                            left: n + w.rail.width
                        } : {
                            width: w.cursorwidth,
                            left: n
                        }), w.cursoractive = !0
                    }
                    w.zoom && w.zoom.stop().css({
                        opacity: w.opt.cursoropacitymax
                    })
                }
            }, this.hideCursor = function(t) {
                w.cursortimeout || !w.rail || !w.autohidedom || w.hasmousefocus && "leave" == w.opt.autohidemode || (w.cursortimeout = setTimeout(function() {
                    w.rail.active && w.showonmouseevent || (w.autohidedom.stop().animate({
                        opacity: w.opt.cursoropacitymin
                    }), w.zoom && w.zoom.stop().animate({
                        opacity: w.opt.cursoropacitymin
                    }), w.cursoractive = !1), w.cursortimeout = 0
                }, t || w.opt.hidecursordelay))
            }, this.noticeCursor = function(t, e, n) {
                w.showCursor(e, n), w.rail.active || w.hideCursor(t)
            }, this.getContentSize = w.ispage ? function() {
                return {
                    w: Math.max(document.body.scrollWidth, document.documentElement.scrollWidth),
                    h: Math.max(document.body.scrollHeight, document.documentElement.scrollHeight)
                }
            } : w.haswrapper ? function() {
                return {
                    w: w.doc.outerWidth() + parseInt(w.win.css("paddingLeft")) + parseInt(w.win.css("paddingRight")),
                    h: w.doc.outerHeight() + parseInt(w.win.css("paddingTop")) + parseInt(w.win.css("paddingBottom"))
                }
            } : function() {
                return {
                    w: w.docscroll[0].scrollWidth,
                    h: w.docscroll[0].scrollHeight
                }
            }, this.onResize = function(t, e) {
                if (!w || !w.win) return !1;
                if (!w.haswrapper && !w.ispage) {
                    if ("none" == w.win.css("display")) return w.visibility && w.hideRail().hideRailHr(), !1;
                    w.hidden || w.visibility || w.showRail().showRailHr()
                }
                var n = w.page.maxh,
                    i = w.page.maxw,
                    o = w.view.h,
                    r = w.view.w;
                if (w.view = {
                    w: w.ispage ? w.win.width() : parseInt(w.win[0].clientWidth),
                    h: w.ispage ? w.win.height() : parseInt(w.win[0].clientHeight)
                }, w.page = e ? e : w.getContentSize(), w.page.maxh = Math.max(0, w.page.h - w.view.h), w.page.maxw = Math.max(0, w.page.w - w.view.w), w.page.maxh == n && w.page.maxw == i && w.view.w == r && w.view.h == o) {
                    if (w.ispage) return w;
                    if (n = w.win.offset(), w.lastposition && (i = w.lastposition, i.top == n.top && i.left == n.left)) return w;
                    w.lastposition = n
                }
                return 0 == w.page.maxh ? (w.hideRail(), w.scrollvaluemax = 0, w.scroll.y = 0, w.scrollratio.y = 0, w.cursorheight = 0, w.setScrollTop(0), w.rail.scrollable = !1) : (w.page.maxh -= w.opt.railpadding.top + w.opt.railpadding.bottom, w.rail.scrollable = !0), 0 == w.page.maxw ? (w.hideRailHr(), w.scrollvaluemaxw = 0, w.scroll.x = 0, w.scrollratio.x = 0, w.cursorwidth = 0, w.setScrollLeft(0), w.railh.scrollable = !1) : (w.page.maxw -= w.opt.railpadding.left + w.opt.railpadding.right, w.railh.scrollable = !0), w.railslocked = w.locked || 0 == w.page.maxh && 0 == w.page.maxw, w.railslocked ? (w.ispage || w.updateScrollBar(w.view), !1) : (w.hidden || w.visibility ? w.hidden || w.railh.visibility || w.showRailHr() : w.showRail().showRailHr(), w.istextarea && w.win.css("resize") && "none" != w.win.css("resize") && (w.view.h -= 20), w.cursorheight = Math.min(w.view.h, Math.round(w.view.h / w.page.h * w.view.h)), w.cursorheight = w.opt.cursorfixedheight ? w.opt.cursorfixedheight : Math.max(w.opt.cursorminheight, w.cursorheight), w.cursorwidth = Math.min(w.view.w, Math.round(w.view.w / w.page.w * w.view.w)), w.cursorwidth = w.opt.cursorfixedheight ? w.opt.cursorfixedheight : Math.max(w.opt.cursorminheight, w.cursorwidth), w.scrollvaluemax = w.view.h - w.cursorheight - w.cursor.hborder - (w.opt.railpadding.top + w.opt.railpadding.bottom), w.railh && (w.railh.width = 0 < w.page.maxh ? w.view.w - w.rail.width : w.view.w, w.scrollvaluemaxw = w.railh.width - w.cursorwidth - w.cursorh.wborder - (w.opt.railpadding.left + w.opt.railpadding.right)), w.ispage || w.updateScrollBar(w.view), w.scrollratio = {
                    x: w.page.maxw / w.scrollvaluemaxw,
                    y: w.page.maxh / w.scrollvaluemax
                }, w.getScrollTop() > w.page.maxh ? w.doScrollTop(w.page.maxh) : (w.scroll.y = Math.round(w.getScrollTop() * (1 / w.scrollratio.y)), w.scroll.x = Math.round(w.getScrollLeft() * (1 / w.scrollratio.x)), w.cursoractive && w.noticeCursor()), w.scroll.y && 0 == w.getScrollTop() && w.doScrollTo(Math.floor(w.scroll.y * w.scrollratio.y)), w)
            }, this.resize = w.onResize, this.lazyResize = function(t) {
                return t = isNaN(t) ? 30 : t, w.debounced("resize", w.resize, t), w
            }, this.jqbind = function(e, n, i) {
                w.events.push({
                    e: e,
                    n: n,
                    f: i,
                    q: !0
                }), t(e).bind(n, i)
            }, this.bind = function(t, e, n, i) {
                var o = "jquery" in t ? t[0] : t;
                "mousewheel" == e ? window.addEventListener || "onwheel" in document ? w._bind(o, "wheel", n, i || !1) : (t = "undefined" != typeof document.onmousewheel ? "mousewheel" : "DOMMouseScroll", v(o, t, n, i || !1), "DOMMouseScroll" == t && v(o, "MozMousePixelScroll", n, i || !1)) : o.addEventListener ? (x.cantouch && /mouseup|mousedown|mousemove/.test(e) && w._bind(o, "mousedown" == e ? "touchstart" : "mouseup" == e ? "touchend" : "touchmove", function(t) {
                    if (t.touches) {
                        if (2 > t.touches.length) {
                            var e = t.touches.length ? t.touches[0] : t;
                            e.original = t, n.call(this, e)
                        }
                    } else t.changedTouches && (e = t.changedTouches[0], e.original = t, n.call(this, e))
                }, i || !1), w._bind(o, e, n, i || !1), x.cantouch && "mouseup" == e && w._bind(o, "touchcancel", n, i || !1)) : w._bind(o, e, function(t) {
                    return (t = t || window.event || !1) && t.srcElement && (t.target = t.srcElement), "pageY" in t || (t.pageX = t.clientX + document.documentElement.scrollLeft, t.pageY = t.clientY + document.documentElement.scrollTop), !1 === n.call(o, t) || !1 === i ? w.cancelEvent(t) : !0
                })
            }, x.haseventlistener ? (this._bind = function(t, e, n, i) {
                w.events.push({
                    e: t,
                    n: e,
                    f: n,
                    b: i,
                    q: !1
                }), t.addEventListener(e, n, i || !1)
            }, this.cancelEvent = function(t) {
                return t ? (t = t.original ? t.original : t, t.preventDefault(), t.stopPropagation(), t.preventManipulation && t.preventManipulation(), !1) : !1
            }, this.stopPropagation = function(t) {
                return t ? (t = t.original ? t.original : t, t.stopPropagation(), !1) : !1
            }, this._unbind = function(t, e, n, i) {
                t.removeEventListener(e, n, i)
            }) : (this._bind = function(t, e, n, i) {
                w.events.push({
                    e: t,
                    n: e,
                    f: n,
                    b: i,
                    q: !1
                }), t.attachEvent ? t.attachEvent("on" + e, n) : t["on" + e] = n
            }, this.cancelEvent = function(t) {
                return (t = window.event || !1) ? (t.cancelBubble = !0, t.cancel = !0, t.returnValue = !1) : !1
            }, this.stopPropagation = function(t) {
                return (t = window.event || !1) ? (t.cancelBubble = !0, !1) : !1
            }, this._unbind = function(t, e, n, i) {
                t.detachEvent ? t.detachEvent("on" + e, n) : t["on" + e] = !1
            }), this.unbindAll = function() {
                for (var t = 0; t < w.events.length; t++) {
                    var e = w.events[t];
                    e.q ? e.e.unbind(e.n, e.f) : w._unbind(e.e, e.n, e.f, e.b)
                }
            }, this.showRail = function() {
                return 0 == w.page.maxh || !w.ispage && "none" == w.win.css("display") || (w.visibility = !0, w.rail.visibility = !0, w.rail.css("display", "block")), w
            }, this.showRailHr = function() {
                return w.railh ? (0 == w.page.maxw || !w.ispage && "none" == w.win.css("display") || (w.railh.visibility = !0, w.railh.css("display", "block")), w) : w
            }, this.hideRail = function() {
                return w.visibility = !1, w.rail.visibility = !1, w.rail.css("display", "none"), w
            }, this.hideRailHr = function() {
                return w.railh ? (w.railh.visibility = !1, w.railh.css("display", "none"), w) : w
            }, this.show = function() {
                return w.hidden = !1, w.railslocked = !1, w.showRail().showRailHr()
            }, this.hide = function() {
                return w.hidden = !0, w.railslocked = !0, w.hideRail().hideRailHr()
            }, this.toggle = function() {
                return w.hidden ? w.show() : w.hide()
            }, this.remove = function() {
                w.stop(), w.cursortimeout && clearTimeout(w.cursortimeout), w.doZoomOut(), w.unbindAll(), x.isie9 && w.win[0].detachEvent("onpropertychange", w.onAttributeChange), !1 !== w.observer && w.observer.disconnect(), !1 !== w.observerremover && w.observerremover.disconnect(), !1 !== w.observerbody && w.observerbody.disconnect(), w.events = null, w.cursor && w.cursor.remove(), w.cursorh && w.cursorh.remove(), w.rail && w.rail.remove(), w.railh && w.railh.remove(), w.zoom && w.zoom.remove();
                for (var e = 0; e < w.saved.css.length; e++) {
                    var n = w.saved.css[e];
                    n[0].css(n[1], "undefined" == typeof n[2] ? "" : n[2])
                }
                w.saved = !1, w.me.data("__nicescroll", "");
                var i = t.nicescroll;
                i.each(function(t) {
                    if (this && this.id === w.id) {
                        delete i[t];
                        for (var e = ++t; e < i.length; e++, t++) i[t] = i[e];
                        i.length--, i.length && delete i[i.length]
                    }
                });
                for (var o in w) w[o] = null, delete w[o];
                w = null
            }, this.scrollstart = function(t) {
                return this.onscrollstart = t, w
            }, this.scrollend = function(t) {
                return this.onscrollend = t, w
            }, this.scrollcancel = function(t) {
                return this.onscrollcancel = t, w
            }, this.zoomin = function(t) {
                return this.onzoomin = t, w
            }, this.zoomout = function(t) {
                return this.onzoomout = t, w
            }, this.isScrollable = function(e) {
                if (e = e.target ? e.target : e, "OPTION" == e.nodeName) return !0;
                for (; e && 1 == e.nodeType && !/^BODY|HTML/.test(e.nodeName);) {
                    var n = t(e),
                        n = n.css("overflowY") || n.css("overflowX") || n.css("overflow") || "";
                    if (/scroll|auto/.test(n)) return e.clientHeight != e.scrollHeight;
                    e = e.parentNode ? e.parentNode : !1
                }
                return !1
            }, this.getViewport = function(e) {
                for (e = e && e.parentNode ? e.parentNode : !1; e && 1 == e.nodeType && !/^BODY|HTML/.test(e.nodeName);) {
                    var n = t(e);
                    if (/fixed|absolute/.test(n.css("position"))) return n;
                    var i = n.css("overflowY") || n.css("overflowX") || n.css("overflow") || "";
                    if (/scroll|auto/.test(i) && e.clientHeight != e.scrollHeight || 0 < n.getNiceScroll().length) return n;
                    e = e.parentNode ? e.parentNode : !1
                }
                return !1
            }, this.triggerScrollEnd = function() {
                if (w.onscrollend) {
                    var t = w.getScrollLeft(),
                        e = w.getScrollTop();
                    w.onscrollend.call(w, {
                        type: "scrollend",
                        current: {
                            x: t,
                            y: e
                        },
                        end: {
                            x: t,
                            y: e
                        }
                    })
                }
            }, this.onmousewheel = function(t) {
                if (!w.wheelprevented) {
                    if (w.railslocked) return w.debounced("checkunlock", w.resize, 250), !0;
                    if (w.rail.drag) return w.cancelEvent(t);
                    if ("auto" == w.opt.oneaxismousemode && 0 != t.deltaX && (w.opt.oneaxismousemode = !1), w.opt.oneaxismousemode && 0 == t.deltaX && !w.rail.scrollable) return w.railh && w.railh.scrollable ? w.onmousewheelhr(t) : !0;
                    var e = +new Date,
                        n = !1;
                    return w.opt.preservenativescrolling && w.checkarea + 600 < e && (w.nativescrollingarea = w.isScrollable(t), n = !0), w.checkarea = e, w.nativescrollingarea ? !0 : ((t = y(t, !1, n)) && (w.checkarea = 0), t)
                }
            }, this.onmousewheelhr = function(t) {
                if (!w.wheelprevented) {
                    if (w.railslocked || !w.railh.scrollable) return !0;
                    if (w.rail.drag) return w.cancelEvent(t);
                    var e = +new Date,
                        n = !1;
                    return w.opt.preservenativescrolling && w.checkarea + 600 < e && (w.nativescrollingarea = w.isScrollable(t), n = !0), w.checkarea = e, w.nativescrollingarea ? !0 : w.railslocked ? w.cancelEvent(t) : y(t, !0, n)
                }
            }, this.stop = function() {
                return w.cancelScroll(), w.scrollmon && w.scrollmon.stop(), w.cursorfreezed = !1, w.scroll.y = Math.round(w.getScrollTop() * (1 / w.scrollratio.y)), w.noticeCursor(), w
            }, this.getTransitionSpeed = function(t) {
                var e = Math.round(10 * w.opt.scrollspeed);
                return t = Math.min(e, Math.round(t / 20 * w.opt.scrollspeed)), t > 20 ? t : 0
            }, w.opt.smoothscroll ? w.ishwscroll && x.hastransition && w.opt.usetransition && w.opt.smoothscroll ? (this.prepareTransition = function(t, e) {
                var n = e ? t > 20 ? t : 0 : w.getTransitionSpeed(t),
                    i = n ? x.prefixstyle + "transform " + n + "ms ease-out" : "";
                return w.lasttransitionstyle && w.lasttransitionstyle == i || (w.lasttransitionstyle = i, w.doc.css(x.transitionstyle, i)), n
            }, this.doScrollLeft = function(t, e) {
                var n = w.scrollrunning ? w.newscrolly : w.getScrollTop();
                w.doScrollPos(t, n, e)
            }, this.doScrollTop = function(t, e) {
                var n = w.scrollrunning ? w.newscrollx : w.getScrollLeft();
                w.doScrollPos(n, t, e)
            }, this.doScrollPos = function(t, e, n) {
                var i = w.getScrollTop(),
                    o = w.getScrollLeft();
                return (0 > (w.newscrolly - i) * (e - i) || 0 > (w.newscrollx - o) * (t - o)) && w.cancelScroll(), 0 == w.opt.bouncescroll && (0 > e ? e = 0 : e > w.page.maxh && (e = w.page.maxh), 0 > t ? t = 0 : t > w.page.maxw && (t = w.page.maxw)), w.scrollrunning && t == w.newscrollx && e == w.newscrolly ? !1 : (w.newscrolly = e, w.newscrollx = t, w.newscrollspeed = n || !1, w.timer ? !1 : void(w.timer = setTimeout(function() {
                    var n, i, o = w.getScrollTop(),
                        r = w.getScrollLeft();
                    n = t - r, i = e - o, n = Math.round(Math.sqrt(Math.pow(n, 2) + Math.pow(i, 2))), n = w.newscrollspeed && 1 < w.newscrollspeed ? w.newscrollspeed : w.getTransitionSpeed(n), w.newscrollspeed && 1 >= w.newscrollspeed && (n *= w.newscrollspeed), w.prepareTransition(n, !0), w.timerscroll && w.timerscroll.tm && clearInterval(w.timerscroll.tm), n > 0 && (!w.scrollrunning && w.onscrollstart && w.onscrollstart.call(w, {
                        type: "scrollstart",
                        current: {
                            x: r,
                            y: o
                        },
                        request: {
                            x: t,
                            y: e
                        },
                        end: {
                            x: w.newscrollx,
                            y: w.newscrolly
                        },
                        speed: n
                    }), x.transitionend ? w.scrollendtrapped || (w.scrollendtrapped = !0, w.bind(w.doc, x.transitionend, w.onScrollTransitionEnd, !1)) : (w.scrollendtrapped && clearTimeout(w.scrollendtrapped), w.scrollendtrapped = setTimeout(w.onScrollTransitionEnd, n)), w.timerscroll = {
                        bz: new T(o, w.newscrolly, n, 0, 0, .58, 1),
                        bh: new T(r, w.newscrollx, n, 0, 0, .58, 1)
                    }, w.cursorfreezed || (w.timerscroll.tm = setInterval(function() {
                        w.showCursor(w.getScrollTop(), w.getScrollLeft())
                    }, 60))), w.synched("doScroll-set", function() {
                        w.timer = 0, w.scrollendtrapped && (w.scrollrunning = !0), w.setScrollTop(w.newscrolly), w.setScrollLeft(w.newscrollx), w.scrollendtrapped || w.onScrollTransitionEnd()
                    })
                }, 50)))
            }, this.cancelScroll = function() {
                if (!w.scrollendtrapped) return !0;
                var t = w.getScrollTop(),
                    e = w.getScrollLeft();
                return w.scrollrunning = !1, x.transitionend || clearTimeout(x.transitionend), w.scrollendtrapped = !1, w._unbind(w.doc[0], x.transitionend, w.onScrollTransitionEnd), w.prepareTransition(0), w.setScrollTop(t), w.railh && w.setScrollLeft(e), w.timerscroll && w.timerscroll.tm && clearInterval(w.timerscroll.tm), w.timerscroll = !1, w.cursorfreezed = !1, w.showCursor(t, e), w
            }, this.onScrollTransitionEnd = function() {
                w.scrollendtrapped && w._unbind(w.doc[0], x.transitionend, w.onScrollTransitionEnd), w.scrollendtrapped = !1, w.prepareTransition(0), w.timerscroll && w.timerscroll.tm && clearInterval(w.timerscroll.tm), w.timerscroll = !1;
                var t = w.getScrollTop(),
                    e = w.getScrollLeft();
                return w.setScrollTop(t), w.railh && w.setScrollLeft(e), w.noticeCursor(!1, t, e), w.cursorfreezed = !1, 0 > t ? t = 0 : t > w.page.maxh && (t = w.page.maxh), 0 > e ? e = 0 : e > w.page.maxw && (e = w.page.maxw), t != w.newscrolly || e != w.newscrollx ? w.doScrollPos(e, t, w.opt.snapbackspeed) : (w.onscrollend && w.scrollrunning && w.triggerScrollEnd(), void(w.scrollrunning = !1))
            }) : (this.doScrollLeft = function(t, e) {
                var n = w.scrollrunning ? w.newscrolly : w.getScrollTop();
                w.doScrollPos(t, n, e)
            }, this.doScrollTop = function(t, e) {
                var n = w.scrollrunning ? w.newscrollx : w.getScrollLeft();
                w.doScrollPos(n, t, e)
            }, this.doScrollPos = function(t, e, n) {
                function i() {
                    if (w.cancelAnimationFrame) return !0;
                    if (w.scrollrunning = !0, d = 1 - d) return w.timer = a(i) || 1;
                    var t, e, n = 0,
                        o = e = w.getScrollTop();
                    w.dst.ay ? (o = w.bzscroll ? w.dst.py + w.bzscroll.getNow() * w.dst.ay : w.newscrolly, t = o - e, (0 > t && o < w.newscrolly || t > 0 && o > w.newscrolly) && (o = w.newscrolly), w.setScrollTop(o), o == w.newscrolly && (n = 1)) : n = 1, e = t = w.getScrollLeft(), w.dst.ax ? (e = w.bzscroll ? w.dst.px + w.bzscroll.getNow() * w.dst.ax : w.newscrollx, t = e - t, (0 > t && e < w.newscrollx || t > 0 && e > w.newscrollx) && (e = w.newscrollx), w.setScrollLeft(e), e == w.newscrollx && (n += 1)) : n += 1, 2 == n ? (w.timer = 0, w.cursorfreezed = !1, w.bzscroll = !1, w.scrollrunning = !1, 0 > o ? o = 0 : o > w.page.maxh && (o = w.page.maxh), 0 > e ? e = 0 : e > w.page.maxw && (e = w.page.maxw), e != w.newscrollx || o != w.newscrolly ? w.doScrollPos(e, o) : w.onscrollend && w.triggerScrollEnd()) : w.timer = a(i) || 1
                }
                if (e = "undefined" == typeof e || !1 === e ? w.getScrollTop(!0) : e, w.timer && w.newscrolly == e && w.newscrollx == t) return !0;
                w.timer && l(w.timer), w.timer = 0;
                var o = w.getScrollTop(),
                    r = w.getScrollLeft();
                (0 > (w.newscrolly - o) * (e - o) || 0 > (w.newscrollx - r) * (t - r)) && w.cancelScroll(), w.newscrolly = e, w.newscrollx = t, w.bouncescroll && w.rail.visibility || (0 > w.newscrolly ? w.newscrolly = 0 : w.newscrolly > w.page.maxh && (w.newscrolly = w.page.maxh)), w.bouncescroll && w.railh.visibility || (0 > w.newscrollx ? w.newscrollx = 0 : w.newscrollx > w.page.maxw && (w.newscrollx = w.page.maxw)), w.dst = {}, w.dst.x = t - r, w.dst.y = e - o, w.dst.px = r, w.dst.py = o;
                var s = Math.round(Math.sqrt(Math.pow(w.dst.x, 2) + Math.pow(w.dst.y, 2)));
                w.dst.ax = w.dst.x / s, w.dst.ay = w.dst.y / s;
                var c = 0,
                    u = s;
                if (0 == w.dst.x ? (c = o, u = e, w.dst.ay = 1, w.dst.py = 0) : 0 == w.dst.y && (c = r, u = t, w.dst.ax = 1, w.dst.px = 0), s = w.getTransitionSpeed(s), n && 1 >= n && (s *= n), w.bzscroll = s > 0 ? w.bzscroll ? w.bzscroll.update(u, s) : new T(c, u, s, 0, 1, 0, 1) : !1, !w.timer) {
                    (o == w.page.maxh && e >= w.page.maxh || r == w.page.maxw && t >= w.page.maxw) && w.checkContentSize();
                    var d = 1;
                    w.cancelAnimationFrame = !1, w.timer = 1, w.onscrollstart && !w.scrollrunning && w.onscrollstart.call(w, {
                        type: "scrollstart",
                        current: {
                            x: r,
                            y: o
                        },
                        request: {
                            x: t,
                            y: e
                        },
                        end: {
                            x: w.newscrollx,
                            y: w.newscrolly
                        },
                        speed: s
                    }), i(), (o == w.page.maxh && e >= o || r == w.page.maxw && t >= r) && w.checkContentSize(), w.noticeCursor()
                }
            }, this.cancelScroll = function() {
                return w.timer && l(w.timer), w.timer = 0, w.bzscroll = !1, w.scrollrunning = !1, w
            }) : (this.doScrollLeft = function(t, e) {
                var n = w.getScrollTop();
                w.doScrollPos(t, n, e)
            }, this.doScrollTop = function(t, e) {
                var n = w.getScrollLeft();
                w.doScrollPos(n, t, e)
            }, this.doScrollPos = function(t, e, n) {
                var i = t > w.page.maxw ? w.page.maxw : t;
                0 > i && (i = 0);
                var o = e > w.page.maxh ? w.page.maxh : e;
                0 > o && (o = 0), w.synched("scroll", function() {
                    w.setScrollTop(o), w.setScrollLeft(i)
                })
            }, this.cancelScroll = function() {}), this.doScrollBy = function(t, e) {
                var n = 0,
                    n = e ? Math.floor((w.scroll.y - t) * w.scrollratio.y) : (w.timer ? w.newscrolly : w.getScrollTop(!0)) - t;
                if (w.bouncescroll) {
                    var i = Math.round(w.view.h / 2); - i > n ? n = -i : n > w.page.maxh + i && (n = w.page.maxh + i)
                }
                return w.cursorfreezed = !1, i = w.getScrollTop(!0), 0 > n && 0 >= i ? w.noticeCursor() : n > w.page.maxh && i >= w.page.maxh ? (w.checkContentSize(), w.noticeCursor()) : void w.doScrollTop(n)
            }, this.doScrollLeftBy = function(t, e) {
                var n = 0,
                    n = e ? Math.floor((w.scroll.x - t) * w.scrollratio.x) : (w.timer ? w.newscrollx : w.getScrollLeft(!0)) - t;
                if (w.bouncescroll) {
                    var i = Math.round(w.view.w / 2); - i > n ? n = -i : n > w.page.maxw + i && (n = w.page.maxw + i)
                }
                return w.cursorfreezed = !1, i = w.getScrollLeft(!0), 0 > n && 0 >= i || n > w.page.maxw && i >= w.page.maxw ? w.noticeCursor() : void w.doScrollLeft(n)
            }, this.doScrollTo = function(t, e) {
                e && Math.round(t * w.scrollratio.y), w.cursorfreezed = !1, w.doScrollTop(t)
            }, this.checkContentSize = function() {
                var t = w.getContentSize();
                t.h == w.page.h && t.w == w.page.w || w.resize(!1, t)
            }, w.onscroll = function(t) {
                w.rail.drag || w.cursorfreezed || w.synched("scroll", function() {
                    w.scroll.y = Math.round(w.getScrollTop() * (1 / w.scrollratio.y)), w.railh && (w.scroll.x = Math.round(w.getScrollLeft() * (1 / w.scrollratio.x))), w.noticeCursor()
                })
            }, w.bind(w.docscroll, "scroll", w.onscroll), this.doZoomIn = function(e) {
                if (!w.zoomactive) {
                    w.zoomactive = !0, w.zoomrestore = {
                        style: {}
                    };
                    var n, i = "position top left zIndex backgroundColor marginTop marginBottom marginLeft marginRight".split(" "),
                        o = w.win[0].style;
                    for (n in i) {
                        var s = i[n];
                        w.zoomrestore.style[s] = "undefined" != typeof o[s] ? o[s] : ""
                    }
                    return w.zoomrestore.style.width = w.win.css("width"), w.zoomrestore.style.height = w.win.css("height"), w.zoomrestore.padding = {
                        w: w.win.outerWidth() - w.win.width(),
                        h: w.win.outerHeight() - w.win.height()
                    }, x.isios4 && (w.zoomrestore.scrollTop = t(window).scrollTop(), t(window).scrollTop(0)), w.win.css({
                        position: x.isios4 ? "absolute" : "fixed",
                        top: 0,
                        left: 0,
                        "z-index": r + 100,
                        margin: "0px"
                    }), i = w.win.css("backgroundColor"), ("" == i || /transparent|rgba\(0, 0, 0, 0\)|rgba\(0,0,0,0\)/.test(i)) && w.win.css("backgroundColor", "#fff"), w.rail.css({
                        "z-index": r + 101
                    }), w.zoom.css({
                        "z-index": r + 102
                    }), w.zoom.css("backgroundPosition", "0px -18px"), w.resizeZoom(), w.onzoomin && w.onzoomin.call(w), w.cancelEvent(e)
                }
            }, this.doZoomOut = function(e) {
                return w.zoomactive ? (w.zoomactive = !1, w.win.css("margin", ""), w.win.css(w.zoomrestore.style), x.isios4 && t(window).scrollTop(w.zoomrestore.scrollTop), w.rail.css({
                    "z-index": w.zindex
                }), w.zoom.css({
                    "z-index": w.zindex
                }), w.zoomrestore = !1, w.zoom.css("backgroundPosition", "0px 0px"), w.onResize(), w.onzoomout && w.onzoomout.call(w), w.cancelEvent(e)) : void 0
            }, this.doZoom = function(t) {
                return w.zoomactive ? w.doZoomOut(t) : w.doZoomIn(t)
            }, this.resizeZoom = function() {
                if (w.zoomactive) {
                    var e = w.getScrollTop();
                    w.win.css({
                        width: t(window).width() - w.zoomrestore.padding.w + "px",
                        height: t(window).height() - w.zoomrestore.padding.h + "px"
                    }), w.onResize(), w.setScrollTop(Math.min(w.page.maxh, e))
                }
            }, this.init(), t.nicescroll.push(this)
        },
        m = function(t) {
            var e = this;
            this.nc = t, this.steptime = this.lasttime = this.speedy = this.speedx = this.lasty = this.lastx = 0, this.snapy = this.snapx = !1, this.demuly = this.demulx = 0, this.lastscrolly = this.lastscrollx = -1, this.timer = this.chky = this.chkx = 0, this.time = function() {
                return +new Date
            }, this.reset = function(t, n) {
                e.stop();
                var i = e.time();
                e.steptime = 0, e.lasttime = i, e.speedx = 0, e.speedy = 0, e.lastx = t, e.lasty = n, e.lastscrollx = -1, e.lastscrolly = -1
            }, this.update = function(t, n) {
                var i = e.time();
                e.steptime = i - e.lasttime, e.lasttime = i;
                var i = n - e.lasty,
                    o = t - e.lastx,
                    r = e.nc.getScrollTop(),
                    s = e.nc.getScrollLeft(),
                    r = r + i,
                    s = s + o;
                e.snapx = 0 > s || s > e.nc.page.maxw, e.snapy = 0 > r || r > e.nc.page.maxh, e.speedx = o, e.speedy = i, e.lastx = t, e.lasty = n
            }, this.stop = function() {
                e.nc.unsynched("domomentum2d"), e.timer && clearTimeout(e.timer), e.timer = 0, e.lastscrollx = -1, e.lastscrolly = -1
            }, this.doSnapy = function(t, n) {
                var i = !1;
                0 > n ? (n = 0, i = !0) : n > e.nc.page.maxh && (n = e.nc.page.maxh, i = !0), 0 > t ? (t = 0, i = !0) : t > e.nc.page.maxw && (t = e.nc.page.maxw, i = !0), i ? e.nc.doScrollPos(t, n, e.nc.opt.snapbackspeed) : e.nc.triggerScrollEnd()
            }, this.doMomentum = function(t) {
                var n = e.time(),
                    i = t ? n + t : e.lasttime;
                t = e.nc.getScrollLeft();
                var o = e.nc.getScrollTop(),
                    r = e.nc.page.maxh,
                    s = e.nc.page.maxw;
                if (e.speedx = s > 0 ? Math.min(60, e.speedx) : 0, e.speedy = r > 0 ? Math.min(60, e.speedy) : 0, i = i && 60 >= n - i, (0 > o || o > r || 0 > t || t > s) && (i = !1), t = e.speedx && i ? e.speedx : !1, e.speedy && i && e.speedy || t) {
                    var a = Math.max(16, e.steptime);
                    a > 50 && (t = a / 50, e.speedx *= t, e.speedy *= t, a = 50), e.demulxy = 0, e.lastscrollx = e.nc.getScrollLeft(), e.chkx = e.lastscrollx, e.lastscrolly = e.nc.getScrollTop(), e.chky = e.lastscrolly;
                    var l = e.lastscrollx,
                        c = e.lastscrolly,
                        u = function() {
                            var t = 600 < e.time() - n ? .04 : .02;
                            e.speedx && (l = Math.floor(e.lastscrollx - e.speedx * (1 - e.demulxy)), e.lastscrollx = l, 0 > l || l > s) && (t = .1), e.speedy && (c = Math.floor(e.lastscrolly - e.speedy * (1 - e.demulxy)), e.lastscrolly = c, 0 > c || c > r) && (t = .1), e.demulxy = Math.min(1, e.demulxy + t), e.nc.synched("domomentum2d", function() {
                                e.speedx && (e.nc.getScrollLeft() != e.chkx && e.stop(), e.chkx = l, e.nc.setScrollLeft(l)), e.speedy && (e.nc.getScrollTop() != e.chky && e.stop(), e.chky = c, e.nc.setScrollTop(c)), e.timer || (e.nc.hideCursor(), e.doSnapy(l, c))
                            }), 1 > e.demulxy ? e.timer = setTimeout(u, a) : (e.stop(), e.nc.hideCursor(), e.doSnapy(l, c))
                        };
                    u()
                } else e.doSnapy(e.nc.getScrollLeft(), e.nc.getScrollTop())
            }
        },
        v = t.fn.scrollTop;
    t.cssHooks.pageYOffset = {
        get: function(e, n, i) {
            return (n = t.data(e, "__nicescroll") || !1) && n.ishwscroll ? n.getScrollTop() : v.call(e)
        },
        set: function(e, n) {
            var i = t.data(e, "__nicescroll") || !1;
            return i && i.ishwscroll ? i.setScrollTop(parseInt(n)) : v.call(e, n), this
        }
    }, t.fn.scrollTop = function(e) {
        if ("undefined" == typeof e) {
            var n = this[0] ? t.data(this[0], "__nicescroll") || !1 : !1;
            return n && n.ishwscroll ? n.getScrollTop() : v.call(this)
        }
        return this.each(function() {
            var n = t.data(this, "__nicescroll") || !1;
            n && n.ishwscroll ? n.setScrollTop(parseInt(e)) : v.call(t(this), e)
        })
    };
    var y = t.fn.scrollLeft;
    t.cssHooks.pageXOffset = {
        get: function(e, n, i) {
            return (n = t.data(e, "__nicescroll") || !1) && n.ishwscroll ? n.getScrollLeft() : y.call(e)
        },
        set: function(e, n) {
            var i = t.data(e, "__nicescroll") || !1;
            return i && i.ishwscroll ? i.setScrollLeft(parseInt(n)) : y.call(e, n), this
        }
    }, t.fn.scrollLeft = function(e) {
        if ("undefined" == typeof e) {
            var n = this[0] ? t.data(this[0], "__nicescroll") || !1 : !1;
            return n && n.ishwscroll ? n.getScrollLeft() : y.call(this)
        }
        return this.each(function() {
            var n = t.data(this, "__nicescroll") || !1;
            n && n.ishwscroll ? n.setScrollLeft(parseInt(e)) : y.call(t(this), e)
        })
    };
    var w = function(e) {
        var n = this;
        if (this.length = 0, this.name = "nicescrollarray", this.each = function(t) {
            for (var e = 0, i = 0; e < n.length; e++) t.call(n[e], i++);
            return n
        }, this.push = function(t) {
            n[n.length] = t, n.length++
        }, this.eq = function(t) {
            return n[t]
        }, e)
            for (var i = 0; i < e.length; i++) {
                var o = t.data(e[i], "__nicescroll") || !1;
                o && (this[this.length] = o, this.length++)
            }
        return this
    };
    ! function(t, e, n) {
        for (var i = 0; i < e.length; i++) n(t, e[i])
    }(w.prototype, "show hide toggle onResize resize remove stop doScrollPos".split(" "), function(t, e) {
        t[e] = function() {
            var t = arguments;
            return this.each(function() {
                this[e].apply(this, t)
            })
        }
    }), t.fn.getNiceScroll = function(e) {
        return "undefined" == typeof e ? new w(this) : this[e] && t.data(this[e], "__nicescroll") || !1
    }, t.extend(t.expr[":"], {
        nicescroll: function(e) {
            return t.data(e, "__nicescroll") ? !0 : !1
        }
    }), t.fn.niceScroll = function(e, n) {
        "undefined" != typeof n || "object" != typeof e || "jquery" in e || (n = e, e = !1), n = t.extend({}, n);
        var i = new w;
        "undefined" == typeof n && (n = {}), e && (n.doc = t(e), n.win = t(this));
        var o = !("doc" in n);
        return o || "win" in n || (n.win = t(this)), this.each(function() {
            var e = t(this).data("__nicescroll") || !1;
            e || (n.doc = o ? t(this) : n.doc, e = new g(n, t(this)), t(this).data("__nicescroll", e)), i.push(e)
        }), 1 == i.length ? i[0] : i
    }, window.NiceScroll = {
        getjQuery: function() {
            return t
        }
    }, t.nicescroll || (t.nicescroll = new w, t.nicescroll.options = h)
}), ! function(t, e) {
    "use strict";
    "function" == typeof define && define.amd ? define([], function() {
        return e.apply(t)
    }) : "object" == typeof exports ? module.exports = e.call(t) : t.Waves = e.call(t)
}("object" == typeof global ? global : this, function() {
    "use strict";

    function t(t) {
        return null !== t && t === t.window
    }

    function e(e) {
        return t(e) ? e : 9 === e.nodeType && e.defaultView
    }

    function n(t) {
        var e = typeof t;
        return "function" === e || "object" === e && !!t
    }

    function i(t) {
        return n(t) && t.nodeType > 0
    }

    function o(t) {
        var e = h.call(t);
        return "[object String]" === e ? d(t) : n(t) && /^\[object (HTMLCollection|NodeList|Object)\]$/.test(e) && t.hasOwnProperty("length") ? t : i(t) ? [t] : []
    }

    function r(t) {
        var n, i, o = {
                top: 0,
                left: 0
            },
            r = t && t.ownerDocument;
        return n = r.documentElement, "undefined" != typeof t.getBoundingClientRect && (o = t.getBoundingClientRect()), i = e(r), {
            top: o.top + i.pageYOffset - n.clientTop,
            left: o.left + i.pageXOffset - n.clientLeft
        }
    }

    function s(t) {
        var e = "";
        for (var n in t) t.hasOwnProperty(n) && (e += n + ":" + t[n] + ";");
        return e
    }

    function a(t, e, n) {
        if (n) {
            n.classList.remove("waves-rippling");
            var i = n.getAttribute("data-x"),
                o = n.getAttribute("data-y"),
                r = n.getAttribute("data-scale"),
                a = n.getAttribute("data-translate"),
                l = Date.now() - Number(n.getAttribute("data-hold")),
                c = 350 - l;
            0 > c && (c = 0), "mousemove" === t.type && (c = 150);
            var u = "mousemove" === t.type ? 2500 : p.duration;
            setTimeout(function() {
                var t = {
                    top: o + "px",
                    left: i + "px",
                    opacity: "0",
                    "-webkit-transition-duration": u + "ms",
                    "-moz-transition-duration": u + "ms",
                    "-o-transition-duration": u + "ms",
                    "transition-duration": u + "ms",
                    "-webkit-transform": r + " " + a,
                    "-moz-transform": r + " " + a,
                    "-ms-transform": r + " " + a,
                    "-o-transform": r + " " + a,
                    transform: r + " " + a
                };
                n.setAttribute("style", s(t)), setTimeout(function() {
                    try {
                        e.removeChild(n)
                    } catch (t) {
                        return !1
                    }
                }, u)
            }, c)
        }
    }

    function l(t) {
        if (m.allowEvent(t) === !1) return null;
        for (var e = null, n = t.target || t.srcElement; null !== n.parentElement;) {
            if (n.classList.contains("waves-effect") && !(n instanceof SVGElement)) {
                e = n;
                break
            }
            n = n.parentElement
        }
        return e
    }

    function c(t) {
        m.registerEvent(t);
        var e = l(t);
        if (null !== e)
            if ("touchstart" === t.type && p.delay) {
                var n = !1,
                    i = setTimeout(function() {
                        i = null, p.show(t, e)
                    }, p.delay),
                    o = function(o) {
                        i && (clearTimeout(i), i = null, p.show(t, e)), n || (n = !0, p.hide(o, e))
                    },
                    r = function(t) {
                        i && (clearTimeout(i), i = null), o(t)
                    };
                e.addEventListener("touchmove", r, !1), e.addEventListener("touchend", o, !1), e.addEventListener("touchcancel", o, !1)
            } else p.show(t, e), f && (e.addEventListener("touchend", p.hide, !1), e.addEventListener("touchcancel", p.hide, !1)), e.addEventListener("mouseup", p.hide, !1), e.addEventListener("mouseleave", p.hide, !1)
    }
    var u = u || {},
        d = document.querySelectorAll.bind(document),
        h = Object.prototype.toString,
        f = "ontouchstart" in window,
        p = {
            duration: 750,
            delay: 200,
            show: function(t, e, n) {
                if (2 === t.button) return !1;
                e = e || this;
                var i = document.createElement("div");
                i.className = "waves-ripple waves-rippling", e.appendChild(i);
                var o = r(e),
                    a = t.pageY - o.top,
                    l = t.pageX - o.left,
                    c = "scale(" + e.clientWidth / 100 * 3 + ")",
                    u = "translate(0,0)";
                n && (u = "translate(" + n.x + "px, " + n.y + "px)"), "touches" in t && t.touches.length && (a = t.touches[0].pageY - o.top, l = t.touches[0].pageX - o.left), i.setAttribute("data-hold", Date.now()), i.setAttribute("data-x", l), i.setAttribute("data-y", a), i.setAttribute("data-scale", c), i.setAttribute("data-translate", u);
                var d = {
                    top: a + "px",
                    left: l + "px"
                };
                i.classList.add("waves-notransition"), i.setAttribute("style", s(d)), i.classList.remove("waves-notransition"), d["-webkit-transform"] = c + " " + u, d["-moz-transform"] = c + " " + u, d["-ms-transform"] = c + " " + u, d["-o-transform"] = c + " " + u, d.transform = c + " " + u, d.opacity = "1";
                var h = "mousemove" === t.type ? 2500 : p.duration;
                d["-webkit-transition-duration"] = h + "ms", d["-moz-transition-duration"] = h + "ms", d["-o-transition-duration"] = h + "ms", d["transition-duration"] = h + "ms", i.setAttribute("style", s(d))
            },
            hide: function(t, e) {
                e = e || this;
                for (var n = e.getElementsByClassName("waves-rippling"), i = 0, o = n.length; o > i; i++) a(t, e, n[i])
            }
        },
        g = {
            input: function(t) {
                var e = t.parentNode;
                if ("i" !== e.tagName.toLowerCase() || !e.classList.contains("waves-effect")) {
                    var n = document.createElement("i");
                    n.className = t.className + " waves-input-wrapper", t.className = "waves-button-input", e.replaceChild(n, t), n.appendChild(t);
                    var i = window.getComputedStyle(t, null),
                        o = i.color,
                        r = i.backgroundColor;
                    n.setAttribute("style", "color:" + o + ";background:" + r), t.setAttribute("style", "background-color:rgba(0,0,0,0);")
                }
            },
            img: function(t) {
                var e = t.parentNode;
                if ("i" !== e.tagName.toLowerCase() || !e.classList.contains("waves-effect")) {
                    var n = document.createElement("i");
                    e.replaceChild(n, t), n.appendChild(t)
                }
            }
        },
        m = {
            touches: 0,
            allowEvent: function(t) {
                var e = !0;
                return /^(mousedown|mousemove)$/.test(t.type) && m.touches && (e = !1), e
            },
            registerEvent: function(t) {
                var e = t.type;
                "touchstart" === e ? m.touches += 1 : /^(touchend|touchcancel)$/.test(e) && setTimeout(function() {
                    m.touches && (m.touches -= 1)
                }, 500)
            }
        };
    return u.init = function(t) {
        var e = document.body;
        t = t || {}, "duration" in t && (p.duration = t.duration), "delay" in t && (p.delay = t.delay), f && (e.addEventListener("touchstart", c, !1), e.addEventListener("touchcancel", m.registerEvent, !1), e.addEventListener("touchend", m.registerEvent, !1)), e.addEventListener("mousedown", c, !1)
    }, u.attach = function(t, e) {
        t = o(t), "[object Array]" === h.call(e) && (e = e.join(" ")), e = e ? " " + e : "";
        for (var n, i, r = 0, s = t.length; s > r; r++) n = t[r], i = n.tagName.toLowerCase(), -1 !== ["input", "img"].indexOf(i) && (g[i](n), n = n.parentElement), n.className += " waves-effect" + e
    }, u.ripple = function(t, e) {
        t = o(t);
        var n = t.length;
        if (e = e || {}, e.wait = e.wait || 0, e.position = e.position || null, n)
            for (var i, s, a, l = {}, c = 0, u = {
                type: "mousedown",
                button: 1
            }, d = function(t, e) {
                return function() {
                    p.hide(t, e)
                }
            }; n > c; c++)
                if (i = t[c], s = e.position || {
                    x: i.clientWidth / 2,
                    y: i.clientHeight / 2
                }, a = r(i), l.x = a.left + s.x, l.y = a.top + s.y, u.pageX = l.x, u.pageY = l.y, p.show(u, i), e.wait >= 0 && null !== e.wait) {
                    var h = {
                        type: "mouseup",
                        button: 1
                    };
                    setTimeout(d(h, i), e.wait)
                }
    }, u.calm = function(t) {
        t = o(t);
        for (var e = {
            type: "mouseup",
            button: 1
        }, n = 0, i = t.length; i > n; n++) p.hide(e, t[n])
    }, u.displayEffect = function(t) {
        console.error("Waves.displayEffect() has been deprecated and will be removed in future version. Please use Waves.init() to initialize Waves effect"), u.init(t)
    }, u
}),
function(t, e, n, i) {
    var o = "growl",
        r = {
            element: "body",
            type: "info",
            allow_dismiss: !0,
            placement: {
                from: "top",
                align: "right"
            },
            offset: 20,
            spacing: 10,
            z_index: 1031,
            delay: 5e3,
            timer: 1e3,
            url_target: "_blank",
            mouse_over: !1,
            animate: {
                enter: "animated fadeInDown",
                exit: "animated fadeOutUp"
            },
            onShow: null,
            onShown: null,
            onHide: null,
            onHidden: null,
            icon_type: "class",
            template: '<div data-growl="container" class="alert" role="alert"><button type="button" aria-hidden="true" class="close" data-growl="dismiss">&times;</button><span data-growl="icon"></span><span data-growl="title"></span><span data-growl="message"></span><a href="#" data-growl="url"></a></div>'
        },
        s = function(e, n) {
            r = t.extend(!0, {}, r, n)
        },
        a = function(e) {
            e ? t('[data-growl="container"][data-growl-position="' + e + '"]').find('[data-growl="dismiss"]').trigger("click") : t('[data-growl="container"]').find('[data-growl="dismiss"]').trigger("click")
        },
        l = function(e, n, i) {
            var n = {
                content: {
                    message: "object" == typeof n ? n.message : n,
                    title: n.title ? n.title : null,
                    icon: n.icon ? n.icon : null,
                    url: n.url ? n.url : null
                }
            };
            i = t.extend(!0, {}, n, i), this.settings = t.extend(!0, {}, r, i), plugin = this, c(i, this.settings, plugin), this.$template = $template
        },
        c = function(t, e, n) {
            var i = {
                settings: e,
                element: e.element,
                template: e.template
            };
            "number" == typeof e.offset && (e.offset = {
                x: e.offset,
                y: e.offset
            }), $template = u(i), d($template, i.settings), h($template, i.settings), f($template, i.settings, n)
        },
        u = function(e) {
            var n = t(e.settings.template);
            return n.addClass("alert-" + e.settings.type), n.attr("data-growl-position", e.settings.placement.from + "-" + e.settings.placement.align), n.find('[data-growl="dismiss"]').css("display", "none"), n.removeClass("alert-dismissable"), e.settings.allow_dismiss && (n.addClass("alert-dismissable"), n.find('[data-growl="dismiss"]').css("display", "block")), n
        },
        d = function(t, e) {
            t.find('[data-growl="dismiss"]').css({
                "z-index": e.z_index - 1 >= 1 ? e.z_index - 1 : 1
            }), e.content.icon && ("class" == e.icon_type.toLowerCase() ? t.find('[data-growl="icon"]').addClass(e.content.icon) : t.find('[data-growl="icon"]').is("img") ? t.find('[data-growl="icon"]').attr("src", e.content.icon) : t.find('[data-growl="icon"]').append('<img src="' + e.content.icon + '" />')), e.content.title && t.find('[data-growl="title"]').html(e.content.title), e.content.message && t.find('[data-growl="message"]').html(e.content.message), e.content.url && (t.find('[data-growl="url"]').attr("href", e.content.url).attr("target", e.url_target), t.find('[data-growl="url"]').css({
                position: "absolute",
                top: 0,
                left: 0,
                width: "100%",
                height: "100%",
                "z-index": e.z_index - 2 >= 1 ? e.z_index - 2 : 1
            }))
        },
        h = function(e, n) {
            var i = n.offset.y,
                o = {
                    position: "body" === n.element ? "fixed" : "absolute",
                    margin: 0,
                    "z-index": n.z_index,
                    display: "inline-block"
                },
                r = !1;
            switch (t('[data-growl-position="' + n.placement.from + "-" + n.placement.align + '"]').each(function() {
                return i = Math.max(i, parseInt(t(this).css(n.placement.from)) + t(this).outerHeight() + n.spacing)
            }), o[n.placement.from] = i + "px", e.css(o), n.onShow && n.onShow(event), t(n.element).append(e), n.placement.align) {
                case "center":
                    e.css({
                        left: "50%",
                        marginLeft: -(e.outerWidth() / 2) + "px"
                    });
                    break;
                case "left":
                    e.css("left", n.offset.x + "px");
                    break;
                case "right":
                    e.css("right", n.offset.x + "px")
            }
            e.addClass("growl-animated"), e.one("webkitAnimationStart oanimationstart MSAnimationStart animationstart", function(t) {
                r = !0
            }), e.one("webkitAnimationEnd oanimationend MSAnimationEnd animationend", function(t) {
                n.onShown && n.onShown(t)
            }), setTimeout(function() {
                r || n.onShown && n.onShown(event)
            }, 600)
        },
        f = function(t, e, n) {
            if (t.addClass(e.animate.enter), t.find('[data-growl="dismiss"]').on("click", function() {
                n.close()
            }), t.on("mouseover", function(e) {
                t.addClass("hovering")
            }).on("mouseout", function() {
                t.removeClass("hovering")
            }), e.delay >= 1) {
                t.data("growl-delay", e.delay);
                var i = setInterval(function() {
                    var o = parseInt(t.data("growl-delay")) - e.timer;
                    (!t.hasClass("hovering") && "pause" == e.mouse_over || "pause" != e.mouse_over) && t.data("growl-delay", o), 0 >= o && (clearInterval(i), n.close())
                }, e.timer)
            }
        };
    l.prototype = {
        update: function(t, e) {
            switch (t) {
                case "icon":
                    "class" == this.settings.icon_type.toLowerCase() ? (this.$template.find('[data-growl="icon"]').removeClass(this.settings.content.icon), this.$template.find('[data-growl="icon"]').addClass(e)) : this.$template.find('[data-growl="icon"]').is("img") ? this.$template.find('[data-growl="icon"]') : this.$template.find('[data-growl="icon"]').find("img").attr().attr("src", e);
                    break;
                case "url":
                    this.$template.find('[data-growl="url"]').attr("href", e);
                    break;
                case "type":
                    this.$template.removeClass("alert-" + this.settings.type), this.$template.addClass("alert-" + e);
                    break;
                default:
                    this.$template.find('[data-growl="' + t + '"]').html(e)
            }
            return this
        },
        close: function() {
            var e = this.$template,
                n = this.settings,
                i = e.css(n.placement.from),
                o = !1;
            return n.onHide && n.onHide(event), e.addClass(this.settings.animate.exit), e.nextAll('[data-growl-position="' + this.settings.placement.from + "-" + this.settings.placement.align + '"]').each(function() {
                t(this).css(n.placement.from, i), i = parseInt(i) + n.spacing + t(this).outerHeight()
            }), e.one("webkitAnimationStart oanimationstart MSAnimationStart animationstart", function(t) {
                o = !0
            }), e.one("webkitAnimationEnd oanimationend MSAnimationEnd animationend", function(e) {
                t(this).remove(), n.onHidden && n.onHidden(e)
            }), setTimeout(function() {
                o || (e.remove(), n.onHidden && n.onHidden(event))
            }, 100), this
        }
    }, t.growl = function(t, e) {
        if (0 == t && "closeAll" == e.command) return a(e.position), !1;
        if (0 == t) return s(this, e), !1;
        var n = new l(this, t, e);
        return n
    }
}(jQuery, window, document), ! function(t, e, n) {
    "use strict";
    ! function i(t, e, n) {
        function o(s, a) {
            if (!e[s]) {
                if (!t[s]) {
                    var l = "function" == typeof require && require;
                    if (!a && l) return l(s, !0);
                    if (r) return r(s, !0);
                    var c = new Error("Cannot find module '" + s + "'");
                    throw c.code = "MODULE_NOT_FOUND", c
                }
                var u = e[s] = {
                    exports: {}
                };
                t[s][0].call(u.exports, function(e) {
                    var n = t[s][1][e];
                    return o(n ? n : e)
                }, u, u.exports, i, t, e, n)
            }
            return e[s].exports
        }
        for (var r = "function" == typeof require && require, s = 0; s < n.length; s++) o(n[s]);
        return o
    }({
        1: [
            function(i) {
                var o, r, s, a, l = function(t) {
                        return t && t.__esModule ? t : {
                            "default": t
                        }
                    },
                    c = i("./modules/handle-dom"),
                    u = i("./modules/utils"),
                    d = i("./modules/handle-swal-dom"),
                    h = i("./modules/handle-click"),
                    f = i("./modules/handle-key"),
                    p = l(f),
                    g = i("./modules/default-params"),
                    m = l(g),
                    v = i("./modules/set-params"),
                    y = l(v);
                s = a = function() {
                    function i(t) {
                        var e = s;
                        return e[t] === n ? m["default"][t] : e[t]
                    }
                    var s = arguments[0];
                    if (c.addClass(e.body, "stop-scrolling"), d.resetInput(), s === n) return u.logStr("SweetAlert expects at least 1 attribute!"), !1;
                    var a = u.extend({}, m["default"]);
                    switch (typeof s) {
                        case "string":
                            a.title = s, a.text = arguments[1] || "", a.type = arguments[2] || "";
                            break;
                        case "object":
                            if (s.title === n) return u.logStr('Missing "title" argument!'), !1;
                            a.title = s.title;
                            for (var l in m["default"]) a[l] = i(l);
                            a.confirmButtonText = a.showCancelButton ? "Confirm" : m["default"].confirmButtonText, a.confirmButtonText = i("confirmButtonText"), a.doneFunction = arguments[1] || null;
                            break;
                        default:
                            return u.logStr('Unexpected type of argument! Expected "string" or "object", got ' + typeof s), !1
                    }
                    y["default"](a), d.fixVerticalPosition(), d.openModal(arguments[1]);
                    for (var f = d.getModal(), g = f.querySelectorAll("button"), v = ["onclick", "onmouseover", "onmouseout", "onmousedown", "onmouseup", "onfocus"], w = function(t) {
                        return h.handleButton(t, a, f)
                    }, b = 0; b < g.length; b++)
                        for (var x = 0; x < v.length; x++) {
                            var S = v[x];
                            g[b][S] = w
                        }
                    d.getOverlay().onclick = w, o = t.onkeydown;
                    var T = function(t) {
                        return p["default"](t, a, f)
                    };
                    t.onkeydown = T, t.onfocus = function() {
                        setTimeout(function() {
                            r !== n && (r.focus(), r = n)
                        }, 0)
                    }
                }, s.setDefaults = a.setDefaults = function(t) {
                    if (!t) throw new Error("userParams is required");
                    if ("object" != typeof t) throw new Error("userParams has to be a object");
                    u.extend(m["default"], t)
                }, s.close = a.close = function() {
                    var i = d.getModal();
                    c.fadeOut(d.getOverlay(), 5), c.fadeOut(i, 5), c.removeClass(i, "showSweetAlert"), c.addClass(i, "hideSweetAlert"), c.removeClass(i, "visible");
                    var s = i.querySelector(".sa-icon.sa-success");
                    c.removeClass(s, "animate"), c.removeClass(s.querySelector(".sa-tip"), "animateSuccessTip"), c.removeClass(s.querySelector(".sa-long"), "animateSuccessLong");
                    var a = i.querySelector(".sa-icon.sa-error");
                    c.removeClass(a, "animateErrorIcon"), c.removeClass(a.querySelector(".sa-x-mark"), "animateXMark");
                    var l = i.querySelector(".sa-icon.sa-warning");
                    return c.removeClass(l, "pulseWarning"), c.removeClass(l.querySelector(".sa-body"), "pulseWarningIns"), c.removeClass(l.querySelector(".sa-dot"), "pulseWarningIns"), setTimeout(function() {
                        var t = i.getAttribute("data-custom-class");
                        c.removeClass(i, t)
                    }, 300), c.removeClass(e.body, "stop-scrolling"), t.onkeydown = o, t.previousActiveElement && t.previousActiveElement.focus(), r = n, clearTimeout(i.timeout), !0
                }, s.showInputError = a.showInputError = function(t) {
                    var e = d.getModal(),
                        n = e.querySelector(".sa-input-error");
                    c.addClass(n, "show");
                    var i = e.querySelector(".sa-error-container");
                    c.addClass(i, "show"), i.querySelector("p").innerHTML = t, e.querySelector("input").focus()
                }, s.resetInputError = a.resetInputError = function(t) {
                    if (t && 13 === t.keyCode) return !1;
                    var e = d.getModal(),
                        n = e.querySelector(".sa-input-error");
                    c.removeClass(n, "show");
                    var i = e.querySelector(".sa-error-container");
                    c.removeClass(i, "show")
                }, "undefined" != typeof t ? t.sweetAlert = t.swal = s : u.logStr("SweetAlert is a frontend module!")
            }, {
                "./modules/default-params": 2,
                "./modules/handle-click": 3,
                "./modules/handle-dom": 4,
                "./modules/handle-key": 5,
                "./modules/handle-swal-dom": 6,
                "./modules/set-params": 8,
                "./modules/utils": 9
            }
        ],
        2: [
            function(t, e, n) {
                Object.defineProperty(n, "__esModule", {
                    value: !0
                });
                var i = {
                    title: "",
                    text: "",
                    type: null,
                    allowOutsideClick: !1,
                    showConfirmButton: !0,
                    showCancelButton: !1,
                    closeOnConfirm: !0,
                    closeOnCancel: !0,
                    confirmButtonText: "OK",
                    confirmButtonColor: "#AEDEF4",
                    cancelButtonText: "Cancel",
                    imageUrl: null,
                    imageSize: null,
                    timer: null,
                    customClass: "",
                    html: !1,
                    animation: !0,
                    allowEscapeKey: !0,
                    inputType: "text",
                    inputPlaceholder: "",
                    inputValue: ""
                };
                n["default"] = i, e.exports = n["default"]
            }, {}
        ],
        3: [
            function(e, n, i) {
                Object.defineProperty(i, "__esModule", {
                    value: !0
                });
                var o = e("./utils"),
                    r = (e("./handle-swal-dom"), e("./handle-dom")),
                    s = function(e, n, i) {
                        function s(t) {
                            p && n.confirmButtonColor && (f.style.backgroundColor = t)
                        }
                        var c, u, d, h = e || t.event,
                            f = h.target || h.srcElement,
                            p = -1 !== f.className.indexOf("confirm"),
                            g = -1 !== f.className.indexOf("sweet-overlay"),
                            m = r.hasClass(i, "visible"),
                            v = n.doneFunction && "true" === i.getAttribute("data-has-done-function");
                        switch (p && n.confirmButtonColor && (c = n.confirmButtonColor, u = o.colorLuminance(c, -.04), d = o.colorLuminance(c, -.14)), h.type) {
                            case "mouseover":
                                s(u);
                                break;
                            case "mouseout":
                                s(c);
                                break;
                            case "mousedown":
                                s(d);
                                break;
                            case "mouseup":
                                s(u);
                                break;
                            case "focus":
                                var y = i.querySelector("button.confirm"),
                                    w = i.querySelector("button.cancel");
                                p ? w.style.boxShadow = "none" : y.style.boxShadow = "none";
                                break;
                            case "click":
                                var b = i === f,
                                    x = r.isDescendant(i, f);
                                if (!b && !x && m && !n.allowOutsideClick) break;
                                p && v && m ? a(i, n) : v && m || g ? l(i, n) : r.isDescendant(i, f) && "BUTTON" === f.tagName && sweetAlert.close()
                        }
                    },
                    a = function(t, e) {
                        var n = !0;
                        r.hasClass(t, "show-input") && (n = t.querySelector("input").value, n || (n = "")), e.doneFunction(n), e.closeOnConfirm && sweetAlert.close()
                    },
                    l = function(t, e) {
                        var n = String(e.doneFunction).replace(/\s/g, ""),
                            i = "function(" === n.substring(0, 9) && ")" !== n.substring(9, 10);
                        i && e.doneFunction(!1), e.closeOnCancel && sweetAlert.close()
                    };
                i["default"] = {
                    handleButton: s,
                    handleConfirm: a,
                    handleCancel: l
                }, n.exports = i["default"]
            }, {
                "./handle-dom": 4,
                "./handle-swal-dom": 6,
                "./utils": 9
            }
        ],
        4: [
            function(n, i, o) {
                Object.defineProperty(o, "__esModule", {
                    value: !0
                });
                var r = function(t, e) {
                        return new RegExp(" " + e + " ").test(" " + t.className + " ")
                    },
                    s = function(t, e) {
                        r(t, e) || (t.className += " " + e)
                    },
                    a = function(t, e) {
                        var n = " " + t.className.replace(/[\t\r\n]/g, " ") + " ";
                        if (r(t, e)) {
                            for (; n.indexOf(" " + e + " ") >= 0;) n = n.replace(" " + e + " ", " ");
                            t.className = n.replace(/^\s+|\s+$/g, "")
                        }
                    },
                    l = function(t) {
                        var n = e.createElement("div");
                        return n.appendChild(e.createTextNode(t)), n.innerHTML
                    },
                    c = function(t) {
                        t.style.opacity = "", t.style.display = "block"
                    },
                    u = function(t) {
                        if (t && !t.length) return c(t);
                        for (var e = 0; e < t.length; ++e) c(t[e])
                    },
                    d = function(t) {
                        t.style.opacity = "", t.style.display = "none"
                    },
                    h = function(t) {
                        if (t && !t.length) return d(t);
                        for (var e = 0; e < t.length; ++e) d(t[e])
                    },
                    f = function(t, e) {
                        for (var n = e.parentNode; null !== n;) {
                            if (n === t) return !0;
                            n = n.parentNode
                        }
                        return !1
                    },
                    p = function(t) {
                        t.style.left = "-9999px", t.style.display = "block";
                        var e, n = t.clientHeight;
                        return e = "undefined" != typeof getComputedStyle ? parseInt(getComputedStyle(t).getPropertyValue("padding-top"), 10) : parseInt(t.currentStyle.padding), t.style.left = "", t.style.display = "none", "-" + parseInt((n + e) / 2) + "px"
                    },
                    g = function(t, e) {
                        if (+t.style.opacity < 1) {
                            e = e || 16, t.style.opacity = 0, t.style.display = "block";
                            var n = +new Date,
                                i = function(t) {
                                    function e() {
                                        return t.apply(this, arguments)
                                    }
                                    return e.toString = function() {
                                        return t.toString()
                                    }, e
                                }(function() {
                                    t.style.opacity = +t.style.opacity + (new Date - n) / 100, n = +new Date, +t.style.opacity < 1 && setTimeout(i, e)
                                });
                            i()
                        }
                        t.style.display = "block"
                    },
                    m = function(t, e) {
                        e = e || 16, t.style.opacity = 1;
                        var n = +new Date,
                            i = function(t) {
                                function e() {
                                    return t.apply(this, arguments)
                                }
                                return e.toString = function() {
                                    return t.toString()
                                }, e
                            }(function() {
                                t.style.opacity = +t.style.opacity - (new Date - n) / 100, n = +new Date, +t.style.opacity > 0 ? setTimeout(i, e) : t.style.display = "none"
                            });
                        i()
                    },
                    v = function(n) {
                        if ("function" == typeof MouseEvent) {
                            var i = new MouseEvent("click", {
                                view: t,
                                bubbles: !1,
                                cancelable: !0
                            });
                            n.dispatchEvent(i)
                        } else if (e.createEvent) {
                            var o = e.createEvent("MouseEvents");
                            o.initEvent("click", !1, !1), n.dispatchEvent(o)
                        } else e.createEventObject ? n.fireEvent("onclick") : "function" == typeof n.onclick && n.onclick()
                    },
                    y = function(e) {
                        "function" == typeof e.stopPropagation ? (e.stopPropagation(), e.preventDefault()) : t.event && t.event.hasOwnProperty("cancelBubble") && (t.event.cancelBubble = !0)
                    };
                o.hasClass = r, o.addClass = s, o.removeClass = a, o.escapeHtml = l, o._show = c, o.show = u, o._hide = d, o.hide = h, o.isDescendant = f, o.getTopMargin = p, o.fadeIn = g, o.fadeOut = m, o.fireClick = v, o.stopEventPropagation = y
            }, {}
        ],
        5: [
            function(e, i, o) {
                Object.defineProperty(o, "__esModule", {
                    value: !0
                });
                var r = e("./handle-dom"),
                    s = e("./handle-swal-dom"),
                    a = function(e, i, o) {
                        var a = e || t.event,
                            l = a.keyCode || a.which,
                            c = o.querySelector("button.confirm"),
                            u = o.querySelector("button.cancel"),
                            d = o.querySelectorAll("button[tabindex]");
                        if (-1 !== [9, 13, 32, 27].indexOf(l)) {
                            for (var h = a.target || a.srcElement, f = -1, p = 0; p < d.length; p++)
                                if (h === d[p]) {
                                    f = p;
                                    break
                                }
                            9 === l ? (h = -1 === f ? c : f === d.length - 1 ? d[0] : d[f + 1], r.stopEventPropagation(a), h.focus(), i.confirmButtonColor && s.setFocusStyle(h, i.confirmButtonColor)) : 13 === l ? ("INPUT" === h.tagName && (h = c, c.focus()), h = -1 === f ? c : n) : 27 === l && i.allowEscapeKey === !0 ? (h = u, r.fireClick(h, a)) : h = n
                        }
                    };
                o["default"] = a, i.exports = o["default"]
            }, {
                "./handle-dom": 4,
                "./handle-swal-dom": 6
            }
        ],
        6: [
            function(n, i, o) {
                var r = function(t) {
                    return t && t.__esModule ? t : {
                        "default": t
                    }
                };
                Object.defineProperty(o, "__esModule", {
                    value: !0
                });
                var s = n("./utils"),
                    a = n("./handle-dom"),
                    l = n("./default-params"),
                    c = r(l),
                    u = n("./injected-html"),
                    d = r(u),
                    h = ".sweet-alert",
                    f = ".sweet-overlay",
                    p = function() {
                        var t = e.createElement("div");
                        for (t.innerHTML = d["default"]; t.firstChild;) e.body.appendChild(t.firstChild)
                    },
                    g = function(t) {
                        function e() {
                            return t.apply(this, arguments)
                        }
                        return e.toString = function() {
                            return t.toString()
                        }, e
                    }(function() {
                        var t = e.querySelector(h);
                        return t || (p(), t = g()), t
                    }),
                    m = function() {
                        var t = g();
                        return t ? t.querySelector("input") : void 0
                    },
                    v = function() {
                        return e.querySelector(f)
                    },
                    y = function(t, e) {
                        var n = s.hexToRgb(e);
                        t.style.boxShadow = "0 0 2px rgba(" + n + ", 0.8), inset 0 0 0 1px rgba(0, 0, 0, 0.05)"
                    },
                    w = function(n) {
                        var i = g();
                        a.fadeIn(v(), 10), a.show(i), a.addClass(i, "showSweetAlert"), a.removeClass(i, "hideSweetAlert"), t.previousActiveElement = e.activeElement;
                        var o = i.querySelector("button.confirm");
                        o.focus(), setTimeout(function() {
                            a.addClass(i, "visible")
                        }, 500);
                        var r = i.getAttribute("data-timer");
                        if ("null" !== r && "" !== r) {
                            var s = n;
                            i.timeout = setTimeout(function() {
                                var t = (s || null) && "true" === i.getAttribute("data-has-done-function");
                                t ? s(null) : sweetAlert.close()
                            }, r)
                        }
                    },
                    b = function() {
                        var t = g(),
                            e = m();
                        a.removeClass(t, "show-input"), e.value = c["default"].inputValue, e.setAttribute("type", c["default"].inputType), e.setAttribute("placeholder", c["default"].inputPlaceholder), x()
                    },
                    x = function(t) {
                        if (t && 13 === t.keyCode) return !1;
                        var e = g(),
                            n = e.querySelector(".sa-input-error");
                        a.removeClass(n, "show");
                        var i = e.querySelector(".sa-error-container");
                        a.removeClass(i, "show")
                    },
                    S = function() {
                        var t = g();
                        t.style.marginTop = a.getTopMargin(g())
                    };
                o.sweetAlertInitialize = p, o.getModal = g, o.getOverlay = v, o.getInput = m, o.setFocusStyle = y, o.openModal = w, o.resetInput = b, o.resetInputError = x, o.fixVerticalPosition = S
            }, {
                "./default-params": 2,
                "./handle-dom": 4,
                "./injected-html": 7,
                "./utils": 9
            }
        ],
        7: [
            function(t, e, n) {
                Object.defineProperty(n, "__esModule", {
                    value: !0
                });
                var i = '<div class="sweet-overlay" tabIndex="-1"></div><div class="sweet-alert"><div class="sa-icon sa-error">\n      <span class="sa-x-mark">\n        <span class="sa-line sa-left"></span>\n        <span class="sa-line sa-right"></span>\n      </span>\n    </div><div class="sa-icon sa-warning">\n      <span class="sa-body"></span>\n      <span class="sa-dot"></span>\n    </div><div class="sa-icon sa-info"></div><div class="sa-icon sa-success">\n      <span class="sa-line sa-tip"></span>\n      <span class="sa-line sa-long"></span>\n\n      <div class="sa-placeholder"></div>\n      <div class="sa-fix"></div>\n    </div><div class="sa-icon sa-custom"></div><h2>Title</h2>\n    <p>Text</p>\n    <fieldset>\n      <input type="text" tabIndex="3" />\n      <div class="sa-input-error"></div>\n    </fieldset><div class="sa-error-container">\n      <div class="icon">!</div>\n      <p>Not valid!</p>\n    </div><div class="sa-button-container">\n      <button class="cancel" tabIndex="2">Cancel</button>\n      <button class="confirm" tabIndex="1">OK</button>\n    </div></div>';
                n["default"] = i, e.exports = n["default"]
            }, {}
        ],
        8: [
            function(t, e, i) {
                Object.defineProperty(i, "__esModule", {
                    value: !0
                });
                var o = t("./utils"),
                    r = t("./handle-swal-dom"),
                    s = t("./handle-dom"),
                    a = ["error", "warning", "info", "success", "input", "prompt"],
                    l = function(t) {
                        var e = r.getModal(),
                            i = e.querySelector("h2"),
                            l = e.querySelector("p"),
                            c = e.querySelector("button.cancel"),
                            u = e.querySelector("button.confirm");
                        if (i.innerHTML = t.html ? t.title : s.escapeHtml(t.title).split("\n").join("<br>"), l.innerHTML = t.html ? t.text : s.escapeHtml(t.text || "").split("\n").join("<br>"), t.text && s.show(l), t.customClass) s.addClass(e, t.customClass), e.setAttribute("data-custom-class", t.customClass);
                        else {
                            var d = e.getAttribute("data-custom-class");
                            s.removeClass(e, d), e.setAttribute("data-custom-class", "")
                        }
                        if (s.hide(e.querySelectorAll(".sa-icon")), t.type && !o.isIE8()) {
                            var h = function() {
                                for (var i = !1, o = 0; o < a.length; o++)
                                    if (t.type === a[o]) {
                                        i = !0;
                                        break
                                    }
                                if (!i) return logStr("Unknown alert type: " + t.type), {
                                    v: !1
                                };
                                var l = ["success", "error", "warning", "info"],
                                    c = n; - 1 !== l.indexOf(t.type) && (c = e.querySelector(".sa-icon.sa-" + t.type), s.show(c));
                                var u = r.getInput();
                                switch (t.type) {
                                    case "success":
                                        s.addClass(c, "animate"), s.addClass(c.querySelector(".sa-tip"), "animateSuccessTip"), s.addClass(c.querySelector(".sa-long"), "animateSuccessLong");
                                        break;
                                    case "error":
                                        s.addClass(c, "animateErrorIcon"), s.addClass(c.querySelector(".sa-x-mark"), "animateXMark");
                                        break;
                                    case "warning":
                                        s.addClass(c, "pulseWarning"), s.addClass(c.querySelector(".sa-body"), "pulseWarningIns"), s.addClass(c.querySelector(".sa-dot"), "pulseWarningIns");
                                        break;
                                    case "input":
                                    case "prompt":
                                        u.setAttribute("type", t.inputType), u.value = t.inputValue, u.setAttribute("placeholder", t.inputPlaceholder), s.addClass(e, "show-input"), setTimeout(function() {
                                            u.focus(), u.addEventListener("keyup", swal.resetInputError)
                                        }, 400)
                                }
                            }();
                            if ("object" == typeof h) return h.v
                        }
                        if (t.imageUrl) {
                            var f = e.querySelector(".sa-icon.sa-custom");
                            f.style.backgroundImage = "url(" + t.imageUrl + ")", s.show(f);
                            var p = 80,
                                g = 80;
                            if (t.imageSize) {
                                var m = t.imageSize.toString().split("x"),
                                    v = m[0],
                                    y = m[1];
                                v && y ? (p = v, g = y) : logStr("Parameter imageSize expects value with format WIDTHxHEIGHT, got " + t.imageSize)
                            }
                            f.setAttribute("style", f.getAttribute("style") + "width:" + p + "px; height:" + g + "px")
                        }
                        e.setAttribute("data-has-cancel-button", t.showCancelButton), t.showCancelButton ? c.style.display = "inline-block" : s.hide(c), e.setAttribute("data-has-confirm-button", t.showConfirmButton), t.showConfirmButton ? u.style.display = "inline-block" : s.hide(u), t.cancelButtonText && (c.innerHTML = s.escapeHtml(t.cancelButtonText)), t.confirmButtonText && (u.innerHTML = s.escapeHtml(t.confirmButtonText)), t.confirmButtonColor && (u.style.backgroundColor = t.confirmButtonColor, r.setFocusStyle(u, t.confirmButtonColor)), e.setAttribute("data-allow-outside-click", t.allowOutsideClick);
                        var w = t.doneFunction ? !0 : !1;
                        e.setAttribute("data-has-done-function", w), t.animation ? "string" == typeof t.animation ? e.setAttribute("data-animation", t.animation) : e.setAttribute("data-animation", "pop") : e.setAttribute("data-animation", "none"), e.setAttribute("data-timer", t.timer)
                    };
                i["default"] = l, e.exports = i["default"]
            }, {
                "./handle-dom": 4,
                "./handle-swal-dom": 6,
                "./utils": 9
            }
        ],
        9: [
            function(e, n, i) {
                Object.defineProperty(i, "__esModule", {
                    value: !0
                });
                var o = function(t, e) {
                        for (var n in e) e.hasOwnProperty(n) && (t[n] = e[n]);
                        return t
                    },
                    r = function(t) {
                        var e = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(t);
                        return e ? parseInt(e[1], 16) + ", " + parseInt(e[2], 16) + ", " + parseInt(e[3], 16) : null
                    },
                    s = function() {
                        return t.attachEvent && !t.addEventListener
                    },
                    a = function(e) {
                        t.console && t.console.log("SweetAlert: " + e)
                    },
                    l = function(t, e) {
                        t = String(t).replace(/[^0-9a-f]/gi, ""), t.length < 6 && (t = t[0] + t[0] + t[1] + t[1] + t[2] + t[2]), e = e || 0;
                        var n, i, o = "#";
                        for (i = 0; 3 > i; i++) n = parseInt(t.substr(2 * i, 2), 16), n = Math.round(Math.min(Math.max(0, n + n * e), 255)).toString(16), o += ("00" + n).substr(n.length);
                        return o
                    };
                i.extend = o, i.hexToRgb = r, i.isIE8 = s, i.logStr = a, i.colorLuminance = l
            }, {}
        ]
    }, {}, [1]), "function" == typeof define && define.amd ? define(function() {
        return sweetAlert
    }) : "undefined" != typeof module && module.exports && (module.exports = sweetAlert)
}(window, document), ! function(t, e) {
    if ("function" == typeof define && define.amd) define(["exports", "module"], e);
    else if ("undefined" != typeof exports && "undefined" != typeof module) e(exports, module);
    else {
        var n = {
            exports: {}
        };
        e(n.exports, n), t.autosize = n.exports
    }
}(this, function(t, e) {
    "use strict";

    function n(t) {
        function e() {
            var e = window.getComputedStyle(t, null);
            "vertical" === e.resize ? t.style.resize = "none" : "both" === e.resize && (t.style.resize = "horizontal"), c = "content-box" === e.boxSizing ? -(parseFloat(e.paddingTop) + parseFloat(e.paddingBottom)) : parseFloat(e.borderTopWidth) + parseFloat(e.borderBottomWidth), i()
        }

        function n(e) {
            var n = t.style.width;
            t.style.width = "0px", t.offsetWidth, t.style.width = n, u = e, l && (t.style.overflowY = e), i()
        }

        function i() {
            var e = t.style.height,
                i = document.documentElement.scrollTop,
                o = document.body.scrollTop,
                r = t.style.height;
            t.style.height = "auto";
            var s = t.scrollHeight + c;
            if (0 === t.scrollHeight) return void(t.style.height = r);
            t.style.height = s + "px", document.documentElement.scrollTop = i, document.body.scrollTop = o;
            var a = window.getComputedStyle(t, null);
            if (a.height !== t.style.height) {
                if ("visible" !== u) return void n("visible")
            } else if ("hidden" !== u) return void n("hidden");
            if (e !== t.style.height) {
                var l = document.createEvent("Event");
                l.initEvent("autosize:resized", !0, !1), t.dispatchEvent(l)
            }
        }
        var o = void 0 === arguments[1] ? {} : arguments[1],
            r = o.setOverflowX,
            s = void 0 === r ? !0 : r,
            a = o.setOverflowY,
            l = void 0 === a ? !0 : a;
        if (t && t.nodeName && "TEXTAREA" === t.nodeName && !t.hasAttribute("data-autosize-on")) {
            var c = null,
                u = "hidden",
                d = function(e) {
                    window.removeEventListener("resize", i), t.removeEventListener("input", i), t.removeEventListener("keyup", i), t.removeAttribute("data-autosize-on"), t.removeEventListener("autosize:destroy", d), Object.keys(e).forEach(function(n) {
                        t.style[n] = e[n]
                    })
                }.bind(t, {
                    height: t.style.height,
                    resize: t.style.resize,
                    overflowY: t.style.overflowY,
                    overflowX: t.style.overflowX,
                    wordWrap: t.style.wordWrap
                });
            t.addEventListener("autosize:destroy", d), "onpropertychange" in t && "oninput" in t && t.addEventListener("keyup", i), window.addEventListener("resize", i), t.addEventListener("input", i), t.addEventListener("autosize:update", i), t.setAttribute("data-autosize-on", !0), l && (t.style.overflowY = "hidden"), s && (t.style.overflowX = "hidden", t.style.wordWrap = "break-word"), e()
        }
    }

    function i(t) {
        if (t && t.nodeName && "TEXTAREA" === t.nodeName) {
            var e = document.createEvent("Event");
            e.initEvent("autosize:destroy", !0, !1), t.dispatchEvent(e)
        }
    }

    function o(t) {
        if (t && t.nodeName && "TEXTAREA" === t.nodeName) {
            var e = document.createEvent("Event");
            e.initEvent("autosize:update", !0, !1), t.dispatchEvent(e)
        }
    }
    var r = null;
    "undefined" == typeof window || "function" != typeof window.getComputedStyle ? (r = function(t) {
        return t
    }, r.destroy = function(t) {
        return t
    }, r.update = function(t) {
        return t
    }) : (r = function(t, e) {
        return t && Array.prototype.forEach.call(t.length ? t : [t], function(t) {
            return n(t, e)
        }), t
    }, r.destroy = function(t) {
        return t && Array.prototype.forEach.call(t.length ? t : [t], i), t
    }, r.update = function(t) {
        return t && Array.prototype.forEach.call(t.length ? t : [t], o), t
    }), e.exports = r
});

if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
    $('html').addClass('ismobile');
}


$(function() {

    setTimeout(function() {

        //Nav Toggle
        $('body').addClass('toggled sw-toggled');
        $('body').on('change', '#toggle-width input:checkbox', function() {
            if ($(this).is(':checked')) {                
                setTimeout(function() {
                    $('body').addClass('toggled sw-toggled');
                }, 150);
            } else {
                setTimeout(function() {
                    $('body').removeClass('toggled sw-toggled');
                    $('.main-menu > li').removeClass('animated');
                }, 150);
            }
        });

        $('body').on('click', '.profile-menu > a', function(e) {
            e.preventDefault();
            $(this).parent().toggleClass('toggled');
            $(this).next().slideToggle(200);
        });

                
        $('.dropdown').on('shown.bs.dropdown', function(e) {
            if ($(this).attr('data-animation')) {
                $animArray = [];
                $animation = $(this).data('animation');
                $animArray = $animation.split(',');
                $animationIn = 'animated ' + $animArray[0];
                $animationOut = 'animated ' + $animArray[1];
                $animationDuration = ''
                if (!$animArray[2]) {
                    $animationDuration = 500; //if duration is not defined, default is set to 500ms
                } else {
                    $animationDuration = $animArray[2];
                }

                $(this).find('.dropdown-menu').removeClass($animationOut)
                $(this).find('.dropdown-menu').addClass($animationIn);                
            }
        });

        $('.dropdown').on('hide.bs.dropdown', function(e) {
            
            if ($(this).attr('data-animation')) {
                e.preventDefault();
                $this = $(this);
                $dropdownMenu = $this.find('.dropdown-menu');

                $dropdownMenu.addClass($animationOut);
                setTimeout(function() {
                    $this.removeClass('open')

                }, $animationDuration);
            }
        });


        $('body').on('click', '#menu-trigger, #chat-trigger', function(e) {
            e.preventDefault();
            var x = $(this).data('trigger');

            $(x).toggleClass('toggled');
            $(this).toggleClass('open');
            $('body').toggleClass('modal-open');

            //Close opened sub-menus
            $('.sub-menu.toggled').not('.active').each(function() {
                $(this).removeClass('toggled');
                $(this).find('ul').hide();
            });



            $('.profile-menu .main-menu').hide();

            if (x == '#sidebar') {

                $elem = '#sidebar';
                $elem2 = '#menu-trigger';

                $('#chat-trigger').removeClass('open');

                if (!$('#chat').hasClass('toggled')) {
                    $('#header').toggleClass('sidebar-toggled');
                } else {
                    $('#chat').removeClass('toggled');
                }
            }


            //When clicking outside
            if ($('#header').hasClass('sidebar-toggled')) {
                $(document).on('click', function(e) {
                    if (($(e.target).closest($elem).length === 0) && ($(e.target).closest($elem2).length === 0)) {
                        setTimeout(function() {
                            $('body').removeClass('modal-open');
                            $($elem).removeClass('toggled');
                            $('#header').removeClass('sidebar-toggled');
                            $($elem2).removeClass('open');
                        });
                    }
                });
            }
        })



    }, 300);
});