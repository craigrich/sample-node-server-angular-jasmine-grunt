/**
 * Admin Config
 * @namespace Configs
 */
(function() {

    'use strict';

    angular
        .module('gamestatsApp')
        .config(adminConfig);

    /**
     * @namespace adminConfig
     * @desc routing for admin pages
     * 
     * @memberOf Configs
     */
    function adminConfig($stateProvider) {
        $stateProvider
            .state('main.admin', {
                url: '/admin',
                templateUrl: 'app/admin/admin.html',
                controller: 'AdminCtrl',
                controllerAs: 'adminCtrl',
                authenticate: true
            });
    }

})();