/**
 * Admin Controller
 * @namespace Controllers
 */
(function() {

    'use strict';

    angular.module('gamestatsApp')
        .controller('AdminCtrl', AdminCtrl);

    /**
     * @namespace AdminCtrl
     * @desc controller for admin page
     * 
     * @memberOf Controllers
     */
    function AdminCtrl(UserService) {

        var vm = this;
        vm.users = UserService.query();
        vm.deleteUser = deleteUser;

        /**
         * @name deleteUser
         * @desc will call Authservice deleteUser and
         *       remove item from scope
         *
         * @param  {object} user data from user list
         * @memberOf Controllers.AdminCtrl
         */
        function deleteUser(user) {

            UserService.remove({
                id: user._id
            });

            vm.users.splice(vm.users.indexOf(user), 1);

        }

    }

})();