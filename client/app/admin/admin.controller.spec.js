'use strict';

describe('Controller: LoginCtrl', function() {

    var AdminCtrl, userServiceMock, $q, $scope;

    var users = [{
        _id: '5557a5a5bb91aa7360fdd69c',
        provider: 'local',
        name: 'Test User',
        email: 'test@test.com',
        role: 'user'
    }, {
        _id: '5557a5a5bb91aa7360fdd69d',
        provider: 'local',
        name: 'Admin',
        email: 'admin@admin.com',
        role: 'admin'
    }];

    beforeEach(module('gamestatsApp', 'stateMock'));

    beforeEach(inject(function($controller, $rootScope, _$q_, _$state_) {

        userServiceMock = jasmine.createSpyObj('UserService', ['query', 'remove']);
        userServiceMock.query.and.returnValue(users);

        AdminCtrl = $controller('AdminCtrl', {
            $scope: $scope,
            UserService: userServiceMock
        });

    }));

    it('should exist', function() {
        expect(!!AdminCtrl).toBe(true);
    });

    it('should show a list of all availble users', function() {
        expect(userServiceMock.query).toHaveBeenCalled();
        expect(AdminCtrl.users).toBe(users);
    });

    it('should delete a specific user', function() {
        AdminCtrl.deleteUser(users[0]);
        expect(userServiceMock.remove).toHaveBeenCalled();
        expect(AdminCtrl.users[0]).toBe(users[0]);
    });

});