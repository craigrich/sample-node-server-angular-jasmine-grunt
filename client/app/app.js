/**
 * Main App
 * @namespace Runblocks
 */
(function() {

    'use strict';

    angular
        .module('gamestatsApp', [
            'ngCookies',
            'ngResource',
            'ngMessages',
            'ngAnimate',            
            'ui.router'            
        ])
        .config(appConfig)
        .run(appRun);

    /**
     * @namespace appConfig
     * @desc sets html5 mode, add interceptors
     *       and default to metacritic route
     *
     * @memberOf Configs
     */
    function appConfig($urlRouterProvider, $locationProvider, $httpProvider) {
        $locationProvider.html5Mode(true);
        $httpProvider.interceptors.push('AuthInterceptor');
        $urlRouterProvider.otherwise('/server');
    }

    /**
     * @namespace appRun
     * @desc listens on state change, checks
     *       if state requries auth and
     *       if user is logged in 
     *
     * @memberOf Runblocks
     */
    function appRun($rootScope, $state, AuthService) {

        $rootScope.$on('$stateChangeStart', handleStateChange);

        function handleStateChange(event, next) {
            if (!AuthService.isLoggedIn() && next.authenticate) {
                $state.go('login');
            }
        }
    }

})();