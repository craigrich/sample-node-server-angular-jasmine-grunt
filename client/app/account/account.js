/**
 * Account Config
 * @namespace Configs
 */
(function() {

    'use strict';

    angular
        .module('gamestatsApp')
        .config(accountConfig);

    /**
     * @namespace accountConfig
     * @desc routing for account pages
     *
     * @memberOf Configs
     */
    function accountConfig($stateProvider) {
        $stateProvider
            .state('login', {
                url: '/login',
                templateUrl: 'app/account/login/login.html',
                controller: 'LoginCtrl',
                controllerAs: 'loginCtrl'
            })
            .state('signup', {
                url: '/signup',
                templateUrl: 'app/account/signup/signup.html',
                controller: 'SignupCtrl',
                controllerAs: 'signupCtrl'
            })
            .state('main.settings', {
                url: '/settings',
                templateUrl: 'app/account/settings/settings.html',
                controller: 'SettingsCtrl',
                controllerAs: 'settingsCtrl',
                authenticate: true
            });
    }

})();