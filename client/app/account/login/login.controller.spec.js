'use strict';

describe('Controller: LoginCtrl', function() {

    var LoginCtrl, $q, $state, $scope, $timeout, authServiceMock;

    var form = {};
    var user = {};

    beforeEach(module('gamestatsApp', 'stateMock'));
    beforeEach(inject(function($controller, $rootScope, _$q_, _$state_) {

        $q = _$q_;
        $state = _$state_;
        $scope = $rootScope.$new();
        authServiceMock = jasmine.createSpyObj('Auth', ['login']);

        LoginCtrl = $controller('LoginCtrl', {
            $scope: $scope,
            $state: $state,
            AuthService: authServiceMock
        });

    }));

    it('should exist', function() {
        expect(!!LoginCtrl).toBe(true);
    });

    describe('Client Side Validation', function() {

        beforeEach(function() {
            authServiceMock.login.and.returnValue($q.when('success'));
        });

        it('should call the login service with a valid email and password', function() {

            form.$valid = true;
            user.username = 'user@something.com';
            user.password = '1234';

            LoginCtrl
                .login(form, user);

            expect(authServiceMock.login).toHaveBeenCalled();

        });

        it('should not call the login service with an invalid username or password', function() {

            form.$valid = false;
            user.username = 'user@something.com';
            user.password = '';

            LoginCtrl
                .login(form, user);

            expect(authServiceMock.login).not.toHaveBeenCalled();

        });

    });


    describe('Server Side Validation', function() {

        beforeEach(function() {
            form.$valid = true;
            user.username = 'user@something.com';
            user.password = '1234';
        });

        it('should redirect to a given state on login success', function() {

            spyOn($state, 'go');
            authServiceMock.login.and.returnValue($q.when({}));

            LoginCtrl
                .login(form, user);

            $scope.$apply();
            expect($state.go).toHaveBeenCalledWith('main.server');
        });

        it('should show an error message on login failure', function() {

            authServiceMock.login.and.returnValue($q.reject({
                data: {
                    message: '401 - Forbidden'
                }
            }));

            LoginCtrl
                .login(form, user);

            $scope.$apply();
            expect(LoginCtrl.errors.other).toBe('401 - Forbidden');
        });

    });

});