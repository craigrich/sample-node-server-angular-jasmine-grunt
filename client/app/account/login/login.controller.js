/**
 * Login Controller
 * @namespace Controllers
 */
(function() {

    'use strict';

    angular
        .module('gamestatsApp')
        .controller('LoginCtrl', LoginCtrl);

    /**
     * @namespace LoginCtrl
     * @desc controller for login page
     *
     * @memberOf Controllers
     */
    function LoginCtrl($scope, AuthService, $state) {

        var vm = this;
        vm.errors = {};
        vm.login = login;

        //Add Theme Specific class to login page
        angular.element('.page__main').addClass('login-content');

        /**
         * @name login
         * @desc will call Authservice login and
         *       change state or set error message
         *
         * @param  {object} form current form on scope
         * @param  {object} user data captured from form
         * @memberOf Controllers.LoginCtrl
         */
        function login(form, user) {


            if (!form.$valid) {
                return false;
            }

            AuthService
                .login(user)
                .then(loginSuccess)
                .catch(loginFail);

            function loginSuccess() {
                $state.go('main.server');
                
            }

            function loginFail(err) {
                vm.errors.other = err.data.message;
            }
        }
    }

})();