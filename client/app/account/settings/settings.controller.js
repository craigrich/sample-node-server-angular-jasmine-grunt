/**
 * Settings Controller
 * @namespace Controllers
 */
(function() {

    'use strict';

    angular
        .module('gamestatsApp')
        .controller('SettingsCtrl', SettingsCtrl);

    /**
     * @namespace SettingsCtrl
     * @desc controller for settings page
     * 
     * @memberOf Controllers
     */
    function SettingsCtrl(AuthService) {

        var vm = this;
        vm.errors = {};
        vm.changePassword = changePassword;

        /**
         * @name changePassword
         * @desc will call the Authservice changepassword
         *       and set success/error message on scope
         *
         * @param  {object} form current form on scope
         * @param  {object} user data captured from form
         * @memberOf Controllers.SettingsCtrl
         */
        function changePassword(form, user) {

            if (!form.$valid) {
                return false;
            }

            AuthService
                .changePassword(user)
                .then(changePasswordSuccess)
                .catch(changePasswordFail);

            function changePasswordSuccess() {
                vm.message = 'Password successfully changed.';
            }

            function changePasswordFail() {
                vm.errors.other = 'Incorrect password';
                vm.message = '';
            }
        }

    }

})();