'use strict';

describe('Controller: SettingsCtrl', function() {

    var SettingsCtrl, $q, $state, $scope, $timeout, authServiceMock;

    var form = {};
    var user = {};

    beforeEach(module('gamestatsApp', 'stateMock'));
    beforeEach(inject(function($controller, $rootScope, _$q_, _$state_) {

        $q = _$q_;
        $scope = $rootScope.$new();
        authServiceMock = jasmine.createSpyObj('AuthService', ['changePassword']);

        SettingsCtrl = $controller('SettingsCtrl', {
            $scope: $scope,
            AuthService: authServiceMock
        });

    }));

    it('should exist', function() {
        expect(!!SettingsCtrl).toBe(true);
    });

    describe('Client Side Validation', function() {

        beforeEach(function() {
            authServiceMock.changePassword.and.returnValue($q.when('success'));
        });

        it('should call the change password service with a valid email and password', function() {

            form.$valid = true;
            user.oldPassword = 'pass123';
            user.newPassword = 'passABC';

            SettingsCtrl
                .changePassword(form, user);

            expect(authServiceMock.changePassword).toHaveBeenCalled();

        });

        it('should not call the change password service with an invalid username or password', function() {

            form.$valid = false;
            user.oldPassword = 'pass123';
            user.newPassword = '';

            SettingsCtrl
                .changePassword(form, user);

            expect(authServiceMock.changePassword).not.toHaveBeenCalled();

        });

    });


    describe('Server Side Validation', function() {

        beforeEach(function() {
            form.$valid = true;
            user.oldPassword = 'pass123';
            user.newPassword = 'passABC';
        });

        it('should show a success message on change password success', function() {

            authServiceMock.changePassword.and.returnValue($q.when({}));

            SettingsCtrl
                .changePassword(form, user);

            $scope.$apply();
            expect(SettingsCtrl.message).toBe('Password successfully changed.');

        });

        it('should show a error message on change password Failure', function() {

            authServiceMock.changePassword.and.returnValue($q.reject({}));

            SettingsCtrl
                .changePassword(form, user);

            $scope.$apply();
            expect(SettingsCtrl.errors.other).toBe('Incorrect password');
            expect(SettingsCtrl.message).toBe('');
        
        });

    });

});