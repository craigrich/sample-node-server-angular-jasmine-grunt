'use strict';

describe('Controller: SignupCtrl', function() {

    var SignupCtrl, $q, $state, $scope, $timeout, authServiceMock;

    var form = {};
    var user = {};

    beforeEach(module('gamestatsApp', 'stateMock'));
    beforeEach(inject(function($controller, $rootScope, _$q_, _$state_) {

        $q = _$q_;
        $state = _$state_;
        $scope = $rootScope.$new();
        authServiceMock = jasmine.createSpyObj('AuthService', ['createUser']);

        SignupCtrl = $controller('SignupCtrl', {
            $scope: $scope,
            $state: $state,
            AuthService: authServiceMock
        });

    }));

    it('should exist', function() {
        expect(!!SignupCtrl).toBe(true);
    });

    describe('Client Side Validation', function() {

        beforeEach(function() {
            authServiceMock.createUser.and.returnValue($q.when('success'));
        });

        it('should call the signup service with a valid email and password', function() {

            form.$valid = true;
            user.name = 'Bob Jones';
            user.email = 'user@something.com';
            user.password = '1234';

            SignupCtrl
                .register(form, user);

            expect(authServiceMock.createUser).toHaveBeenCalled();

        });

        it('should not call the signup service with an invalid username or password', function() {

            form.$valid = false;
            user.name = 'Bob Jones';
            user.email = 'user@something.com';
            user.password = '';

            SignupCtrl
                .register(form, user);

            expect(authServiceMock.createUser).not.toHaveBeenCalled();

        });

    });


    describe('Server Side Validation', function() {

        beforeEach(function() {
            form.$valid = true;
            form.email = {};
            form.email.$setValidity = jasmine.createSpy('$setValidity');

            user.name = 'Bob Jones';
            user.email = 'user@something.com';
            user.password = '1234';
        });

        it('should redirect to a given state on login success', function() {

            spyOn($state, 'go');
            authServiceMock.createUser.and.returnValue($q.when({}));

            SignupCtrl.register(form, user);
            $scope.$apply();
            
            expect($state.go).toHaveBeenCalledWith('main.server');

        });

        it('should show an error message on login failure', function() {

            authServiceMock.createUser.and.returnValue($q.reject({
                data: {
                    errors: {
                        email: {
                            message: 'The specified email address is already in use.',
                        }
                    }
                }
            }));

            SignupCtrl.register(form, user);
            $scope.$apply();

            expect(form.email.$setValidity).toHaveBeenCalledWith('mongoose', false);
            expect(SignupCtrl.errors.email).toBe('The specified email address is already in use.');

        });

    });

});