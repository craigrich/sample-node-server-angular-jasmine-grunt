/**
 * Signup Controller
 * @namespace Controllers
 */
(function() {

    'use strict';

    angular
        .module('gamestatsApp')
        .controller('SignupCtrl', SignupCtrl);

    /**
     * @namespace SignupCtrl
     * @desc controller for signup page
     * 
     * @memberOf Controllers
     */
    function SignupCtrl($scope, AuthService, $state) {

        var vm = this;
        vm.errors = {};
        vm.register = register;

        //Add Theme Specific class to login page
        angular.element('.page__main').addClass('login-content');

        /**
         * @name register
         * @desc will call Authservice createUser and
         *       change state or set error message
         *
         * @param  {object} form current form on scope
         * @param  {object} user data captured from form
         * @memberOf Controllers.SignupCtrl
         */
        function register(form, user) {

            vm.submitted = true;

            if (!form.$valid) {
                return false;
            }

            AuthService
                .createUser(user)
                .then(createUserSuccess)
                .catch(createUserFail);

            function createUserSuccess() {                
                $state.go('main.server');
                angular.element('body').removeClass('login-content');
            }

            function createUserFail(err) {

                vm.errors = {};
                angular.forEach(err.data.errors, setErrors);

                function setErrors(error, field) {
                    form[field].$setValidity('mongoose', false);
                    vm.errors[field] = error.message;

                }
            }

        }

    }

})();
