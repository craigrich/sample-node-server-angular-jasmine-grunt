'use strict';

xdescribe('Controller: GamesCtrl', function() {

    var MetacriticCtrl,
        barData = {
            data: 'BarData'
        },
        pieData = {
            data: 'PieData'
        },
        lineData = {
            data: 'LineData'
        };

    beforeEach(module('gamestatsApp'));

    beforeEach(inject(function($controller, $rootScope) {
        MetacriticCtrl = $controller('MetacriticCtrl', {
            $scope: $rootScope.$new(),
            graphBarData: barData,
            graphPieData: pieData,
            graphLineData: lineData
        });
    }));

    it('should bind chart configs to controller scope', function() {
        expect(MetacriticCtrl.bar).toEqual(barData);
        expect(MetacriticCtrl.pie).toEqual(pieData);
        expect(MetacriticCtrl.line).toEqual(lineData);        
    });

});