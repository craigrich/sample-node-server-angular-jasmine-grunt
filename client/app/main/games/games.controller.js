/**
 * GamesCtrl Controller
 * @namespace Controllers
 */
(function() {

    'use strict';

    angular
        .module('gamestatsApp')
        .controller('GamesCtrl', GamesCtrl);

    /**
     * @namespace GamesCtrl
     * @desc controller for games page and
     *       binds chart configs to scope
     *       
     * @memberOf Controllers
     */
    function GamesCtrl(graphBarData, graphPieData, graphLineData) {
        var vm = this;
        vm.bar = graphBarData;
        vm.pie = graphPieData;
        vm.line = graphLineData;
    }

})();