/**
 * Games Config
 * @namespace Configs
 */
(function() {

    'use strict';

    angular
        .module('gamestatsApp')
        .config(gamesConfig);

    /**
     * @namespace gamesConfig
     * @desc routing for games pages, resolves
     *       highchart configs with attached game data.
     * 
     * @memberOf Configs
     */
    function gamesConfig($stateProvider) {
        $stateProvider
            .state('main.games', {
                url: '/games',
                templateUrl: 'app/main/games/games.html',
                controller: 'gamesCtrl',
                controllerAs: 'gamesCtrl',
                resolve: {
                    graphBarData: graphBarData,
                    graphPieData: graphPieData,
                    graphLineData: graphLineData
                }
            });

        function graphBarData(HighChartService) {
            return HighChartService.getChart({
                config: '/data/bar.json',
                data: '/api/games/aggregate/top/platforms'
            });
        }

        function graphPieData(HighChartService) {
            return HighChartService.getPie({
                config: '/data/pie.json',
                data: '/api/games/aggregate/distribution'
            });
        }

        function graphLineData(HighChartService) {
            return HighChartService.getChart({
                config: '/data/line.json',
                data: '/api/games/aggregate/top/year'
            });
        }
    }

})();