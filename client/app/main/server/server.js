/**
 * Games Config
 * @namespace Configs
 */
(function() {

    'use strict';

    angular
        .module('gamestatsApp')
        .config(serverConfig);

    /**
     * @namespace serverConfig
     * @desc routing for server pages, resolves
     *       highchart configs with attached game data.
     * 
     * @memberOf Configs
     */
    function serverConfig($stateProvider) {
        $stateProvider
            .state('main.server', {
                url: '/server',
                templateUrl: 'app/main/server/server.html',
                controller: 'GamesCtrl',
                controllerAs: 'serverCtrl',
                resolve: {
                    graphBarData: graphBarData,
                    graphPieData: graphPieData,
                    graphLineData: graphLineData
                }
            });

        function graphBarData(HighChartService) {
            return HighChartService.getChart({
                config: '/data/bar.json',
                data: '/api/games/aggregate/top/platforms'
            });
        }

        function graphPieData(HighChartService) {
            return HighChartService.getPie({
                config: '/data/pie.json',
                data: '/api/games/aggregate/distribution'
            });
        }

        function graphLineData(HighChartService) {
            return HighChartService.getChart({
                config: '/data/line.json',
                data: '/api/games/aggregate/top/year'
            });
        }
    }

})();