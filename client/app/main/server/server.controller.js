/**
 * ServerCtrl Controller
 * @namespace Controllers
 */
(function() {

    'use strict';

    angular
        .module('gamestatsApp')
        .controller('ServerCtrl', ServerCtrl);

    /**
     * @namespace ServerCtrl
     * @desc controller for server page and
     *       binds chart configs to scope
     *       
     * @memberOf Controllers
     */
    function ServerCtrl(graphBarData, graphPieData, graphLineData) {
        var vm = this;
        vm.bar = graphBarData;
        vm.pie = graphPieData;
        vm.line = graphLineData;
    }

})();