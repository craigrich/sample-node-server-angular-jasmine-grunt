'use strict';

describe('Module: gamestatsApp', function() {

    var $state, highchartServiceMock;

    beforeEach(module('gamestatsApp'));

    beforeEach(module(function($provide) {
        highchartServiceMock = jasmine.createSpyObj('HighChartService', ['getChart', 'getPie']);
        $provide.value('HighChartService', highchartServiceMock);
    }));

    beforeEach(inject(function(_$location_, _$rootScope_, _$state_, _$httpBackend_, _$q_) {
        $state = _$state_;        
    }));

    beforeEach(function() {        
        $state.go('main.server');        
    });

    it('should resolve chart data', function() {        
        expect(highchartServiceMock.getChart).toHaveBeenCalled();
    });

});