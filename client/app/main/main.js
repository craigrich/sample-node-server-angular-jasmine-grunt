/**
 * Main Config
 * @namespace Configs
 */
(function() {

    'use strict';

    angular
        .module('gamestatsApp')
        .config(mainConfig);        

    /**
     * @namespace mainConfig
     * @desc Initial routing for app
     *
     * @memberOf Configs
     */
    function mainConfig($stateProvider) {
        $stateProvider
            .state('main', {
                url: '',
                templateUrl: 'app/main/main.html',
                controller: mainCtrl,
            });
    }

    function mainCtrl($scope) {
        $scope.materialPreloader = true;
        console.log('detect main ctrl');
    }

})();