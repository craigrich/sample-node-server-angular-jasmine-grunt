'use strict';

describe('Module: gamestatsApp', function() {

    var $rootScope,
        $state,
        AuthService;

    beforeEach(module('gamestatsApp', 'stateMock'));

    beforeEach(inject(function(_$rootScope_, _$state_, _AuthService_) {
        $rootScope = _$rootScope_;
        $state = _$state_;
        AuthService = _AuthService_;
    }));

    beforeEach(function() {
        spyOn($state, 'go');
    });

    it('should have a runblock', function() {
        var runBlock = angular.module('gamestatsApp')._runBlocks[0];
        expect(!!runBlock).toBe(true);
    });

    it('Should redirect to login on authorised route if the user is not logged in', function() {

        spyOn(AuthService, 'isLoggedIn').and.returnValue(false);

        $rootScope.$broadcast('$stateChangeStart', {
            name: 'main.settings',
            authenticate: true
        });

        expect(AuthService.isLoggedIn).toHaveBeenCalled();
        expect($state.go).toHaveBeenCalledWith('login');

    });


    it('Should continue to authorised route if the user is logged in', function() {

        spyOn(AuthService, 'isLoggedIn').and.returnValue(true);

        $rootScope.$broadcast('$stateChangeStart', {
            name: 'main.settings',
            authenticate: true
        });

        expect(AuthService.isLoggedIn).toHaveBeenCalled();
        expect($state.go).not.toHaveBeenCalled();

    });

});